iPlantAppsSplitter
==================

Small java program to split iPlants foundation all apps list into individual app json files. 
This list of all apps comes from the REST endpoint https://foundation.iplantc.org/apps-v1/apps/list


Run the program
---------------
    java -jar iPlantAppsSplitter.jar -i input.json -o outputs/

Run the program to get a help menu
----------------------------------
    java -jar iPlantAppsSplitter.jar -h

The help menu
-------------
      iPlant App JSON Converter - V1.0
    input file: [-i, --input-file]
    ouput directory: [-o, --output-directory]
    
    Defaults   input: iplant_tool_list_12-14.json
    output directory: outputs/
    
    This help: [-h, --help]

