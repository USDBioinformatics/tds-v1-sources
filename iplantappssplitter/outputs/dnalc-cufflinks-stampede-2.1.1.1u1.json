{
    "longDescription": "",
    "outputs": [],
    "inputs": [
        {
            "defaultValue": "/mkhalfan/cuffdiff_test/WT_rep1.bam",
            "details": {
                "visible": true,
                "description": "",
                "label": "RNA reads aligned with TopHat"
            },
            "id": "query1",
            "semantics": {
                "minCardinality": 1,
                "ontology": ["http://sswapmeet.sswap.info/util/Alignment"],
                "fileTypes": [
                    "BAM-0.1.2",
                    "SAM-0.1.2"
                ]
            },
            "value": {
                "default": "/mkhalfan/cuffdiff_test/WT_rep1.bam",
                "visible": true,
                "validator": "",
                "required": true
            }
        },
        {
            "defaultValue": "",
            "details": {
                "visible": true,
                "description": "Tells Cufflinks to use the supplied reference annotation (GFF) to guide RABT assembly. Reference transcripts will be tiled with faux-reads to provide additional information in assembly.",
                "label": "Guide annotation for RABT"
            },
            "id": "GUIDE_GTF",
            "semantics": {
                "minCardinality": 1,
                "ontology": ["http://sswapmeet.sswap.info/util/Annotation"],
                "fileTypes": [
                    "GTF-2.2",
                    "GFF-3.0"
                ]
            },
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "required": false
            }
        },
        {
            "defaultValue": "",
            "details": {
                "visible": true,
                "description": "Tells Cufflinks to ignore all reads that could have come from transcripts in this GTF file.",
                "label": "Mask annotation file"
            },
            "id": "MASK_GTF",
            "semantics": {
                "minCardinality": 1,
                "ontology": ["http://sswapmeet.sswap.info/util/Annotation"],
                "fileTypes": [
                    "GTF-2.2",
                    "GFF-3.0"
                ]
            },
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "required": false
            }
        },
        {
            "defaultValue": "/mkhalfan/cuffdiff_test/genome.fas",
            "details": {
                "visible": true,
                "description": "Tells Cufflinks to run its new bias detection and correction algorithm which can significantly improve accuracy of transcript abundance estimates.",
                "label": "Genome FASTA file"
            },
            "id": "BIAS_FASTA",
            "semantics": {
                "minCardinality": 1,
                "ontology": ["http://sswapmeet.sswap.info/util/Annotation"],
                "fileTypes": ["FASTA-0"]
            },
            "value": {
                "default": "/mkhalfan/cuffdiff_test/genome.fas",
                "visible": true,
                "validator": "",
                "required": false
            }
        },
        {
            "defaultValue": "",
            "details": {
                "visible": true,
                "description": "Tells Cufflinks to use the supplied reference annotation to estimate isoform expression. It will not assemble novel transcripts, and the program will ignore alignments not structurally compatible with any reference transcript.",
                "label": "Reference annotation"
            },
            "id": "ANNOTATION",
            "semantics": {
                "minCardinality": 1,
                "ontology": ["http://sswapmeet.sswap.info/util/Annotation"],
                "fileTypes": [
                    "GTF-2.2",
                    "GFF-3.0"
                ]
            },
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "required": false
            }
        }
    ],
    "parallelism": "SERIAL",
    "executionType": "HPC",
    "available": true,
    "defaultProcessors": null,
    "executionHost": "stampede.tacc.xsede.org",
    "public": true,
    "defaultMemory": null,
    "id": "dnalc-cufflinks-stampede-2.1.1.1u1",
    "checkpointable": false,
    "deploymentPath": "/ipcservices/applications/dnalc-cufflinks-stampede-2.1.1.1u1.zip",
    "label": "CuffLinks",
    "shortDescription": "Transcript assembly and basic quantitation for RNA-Seq",
    "helpURI": "http://cufflinks.cbcb.umd.edu/",
    "defaultRequestedTime": null,
    "version": "2.1.1.1",
    "templatePath": "cufflinks.sh",
    "modules": [
        "purge",
        "load TACC",
        "load irods",
        "load samtools"
    ],
    "revision": 1,
    "tags": [
        "next-gen",
        "assembly",
        "rnaseq"
    ],
    "testPath": "library/test.sh",
    "ontolog": ["http://sswap.info/iPlant/FoundationalAPI"],
    "name": "dnalc-cufflinks-stampede",
    "parameters": [
        {
            "defaultValue": "false",
            "details": {
                "description": "Tells Cufflinks to do an initial estimation procedure to more accurately weight reads mapping to multiple locations in the genome.",
                "label": "Perform multiply-matched read correction"
            },
            "id": "multiReadCorrect",
            "semantics": {"ontology": ["xs:boolean"]},
            "value": {
                "default": "false",
                "visible": true,
                "validator": "",
                "type": "bool",
                "required": false
            }
        },
        {
            "defaultValue": "0.15",
            "details": {
                "description": "",
                "label": "Pre-mRNA fraction"
            },
            "id": "preMrnaFraction",
            "semantics": {"ontology": ["xs:decimal"]},
            "value": {
                "default": "0.15",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": true
            }
        },
        {
            "defaultValue": "0.09",
            "details": {
                "description": "",
                "label": "Small anchor fraction"
            },
            "id": "smallAnchorFraction",
            "semantics": {"ontology": ["xs:decimal"]},
            "value": {
                "default": "0.09",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": true
            }
        },
        {
            "defaultValue": "0.1",
            "details": {
                "description": "",
                "label": "Coverage threshold for 3' trimming"
            },
            "id": "trim3dropoffFrac",
            "semantics": {"ontology": ["xs:decimal"]},
            "value": {
                "default": "0.1",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": true
            }
        },
        {
            "defaultValue": "10",
            "details": {
                "description": "",
                "label": "Minimum number of fragments needed for new transfrags"
            },
            "id": "minFragsPerTransfrag",
            "semantics": {"ontology": ["xs:integer"]},
            "value": {
                "default": "10",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": true
            }
        },
        {
            "defaultValue": "10",
            "details": {
                "description": "",
                "label": "Read overhang tolerance"
            },
            "id": "overhangTolerance",
            "semantics": {"ontology": ["xs:integer"]},
            "value": {
                "default": "10",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": true
            }
        },
        {
            "defaultValue": "0.1",
            "details": {
                "description": "",
                "label": "Minimum isoform fraction"
            },
            "id": "minIsoformFraction",
            "semantics": {"ontology": ["xs:decimal"]},
            "value": {
                "default": "0.1",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": true
            }
        },
        {
            "defaultValue": "false",
            "details": {
                "description": "Cufflinks normalizes by the upper quartile of the number of fragments mapping to individual loci instead of the total number of sequenced fragments.",
                "label": "Perform upper-quartile normalization"
            },
            "id": "upperQuartileNorm",
            "semantics": {"ontology": ["xs:boolean"]},
            "value": {
                "default": "false",
                "visible": true,
                "validator": "",
                "type": "bool",
                "required": false
            }
        },
        {
            "defaultValue": "10",
            "details": {
                "description": "",
                "label": "Minimum average coverage before beginning 3' trimming"
            },
            "id": "trim3avgcovThresh",
            "semantics": {"ontology": ["xs:integer"]},
            "value": {
                "default": "10",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": true
            }
        },
        {
            "defaultValue": "300000",
            "details": {
                "description": "",
                "label": "Maxiumum intron length"
            },
            "id": "maxIntronLength",
            "semantics": {"ontology": ["xs:integer"]},
            "value": {
                "default": "300000",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": true
            }
        },
        {
            "defaultValue": "true",
            "details": {
                "description": "Cufflinks counts all fragments, including those not compatible with any reference transcript, towards the number of mapped hits used in the FPKM denominator.",
                "label": "Use total hits to calculate FPKM"
            },
            "id": "totalHitsNorm",
            "semantics": {"ontology": ["xs:boolean"]},
            "value": {
                "default": "true",
                "visible": true,
                "validator": "",
                "type": "bool",
                "required": false
            }
        },
        {
            "defaultValue": "tophat",
            "details": {
                "description": "the name of the itinerant tophat job",
                "label": "job name"
            },
            "id": "jobName",
            "semantics": {"ontology": ["xs:string"]},
            "value": {
                "default": "tophat",
                "visible": true,
                "validator": "",
                "type": "string",
                "required": true
            }
        },
        {
            "defaultValue": "fr-unstranded",
            "details": {
                "description": "fr-unstranded|fr-firststrand|fr-unstranded",
                "label": "Library type"
            },
            "id": "libraryType",
            "semantics": {"ontology": ["xs:string"]},
            "value": {
                "default": "fr-unstranded",
                "visible": true,
                "validator": "",
                "type": "string",
                "required": true
            }
        },
        {
            "defaultValue": "50",
            "details": {
                "description": "",
                "label": "Minimum assembled intron length"
            },
            "id": "minIntronLength",
            "semantics": {"ontology": ["xs:integer"]},
            "value": {
                "default": "50",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": true
            }
        },
        {
            "defaultValue": "3500000",
            "details": {
                "description": "",
                "label": "Maximum genomic length for an assembled bundle"
            },
            "id": "maxBundleLength",
            "semantics": {"ontology": ["xs:integer"]},
            "value": {
                "default": "3500000",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": true
            }
        }
    ]
}