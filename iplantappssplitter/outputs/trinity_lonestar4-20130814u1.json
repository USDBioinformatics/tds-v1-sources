{
    "longDescription": "",
    "outputs": [
        {
            "defaultValue": "trinity_out.tgz",
            "details": {
                "description": "Contains assembly, tempfiles, checkpointing, etc.",
                "label": "Tarred Trinity output directory"
            },
            "id": "outputFolderTarball",
            "semantics": {
                "minCardinality": 1,
                "maxCardinality": 1,
                "ontology": ["http://sswapmeet.sswap.info/mime/application/X-tar"],
                "fileTypes": [""]
            },
            "value": {
                "default": "trinity_out.tgz",
                "validator": "trinity_out.tgz"
            }
        },
        {
            "defaultValue": "Trinity.fasta",
            "details": {
                "description": "",
                "label": "Trinity assembly file"
            },
            "id": "outputAssemblyFasta",
            "semantics": {
                "minCardinality": 1,
                "maxCardinality": 1,
                "ontology": ["http://sswapmeet.sswap.info/mime/application/X-multi-fasta"],
                "fileTypes": [""]
            },
            "value": {
                "default": "Trinity.fasta",
                "validator": "Trinity.fasta"
            }
        }
    ],
    "inputs": [
        {
            "defaultValue": "",
            "details": {
                "visible": true,
                "description": "--left",
                "label": "Left sequence library read file"
            },
            "id": "left",
            "semantics": {
                "minCardinality": 1,
                "ontology": [
                    "http://sswapmeet.sswap.info/mime/application/X-fasta",
                    "http://sswapmeet.sswap.info/mime/application/X-fastq"
                ],
                "fileTypes": [
                    "fastq-0",
                    "fasta-0"
                ]
            },
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "required": false
            }
        },
        {
            "defaultValue": "",
            "details": {
                "visible": true,
                "description": "--right",
                "label": "Right sequence library read file (pair with Left file)"
            },
            "id": "right",
            "semantics": {
                "minCardinality": 1,
                "ontology": [
                    "http://sswapmeet.sswap.info/mime/application/X-fasta",
                    "http://sswapmeet.sswap.info/mime/application/X-fastq"
                ],
                "fileTypes": [
                    "fastq-0",
                    "fasta-0"
                ]
            },
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "required": false
            }
        },
        {
            "defaultValue": "",
            "details": {
                "visible": true,
                "description": "--single",
                "label": "Single-end sequence file. Do not supply if you have specified paired-end data. It's like crossing the streams in Ghostbusters."
            },
            "id": "single",
            "semantics": {
                "minCardinality": 1,
                "ontology": [
                    "http://sswapmeet.sswap.info/mime/application/X-fasta",
                    "http://sswapmeet.sswap.info/mime/application/X-fastq"
                ],
                "fileTypes": [
                    "fastq-0",
                    "fasta-0"
                ]
            },
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "required": false
            }
        }
    ],
    "parallelism": "SERIAL",
    "executionType": "HPC",
    "available": true,
    "defaultProcessors": 1,
    "executionHost": "lonestar4.tacc.teragrid.org",
    "public": true,
    "defaultMemory": 999,
    "id": "trinity_lonestar4-20130814u1",
    "checkpointable": false,
    "deploymentPath": "/ipcservices/applications/trinity_lonestar4-20130814u1.zip",
    "label": "Trinity (r2013-08-14)",
    "shortDescription": "Trinity represents a novel method for the efficient and robust de novo reconstruction of transcriptomes from RNA-Seq data",
    "helpURI": "http://trinityrnaseq.sourceforge.net/",
    "defaultRequestedTime": "48:00:00",
    "version": "20130814",
    "templatePath": "assemble.bashx",
    "modules": [
        "purge",
        "load TACC",
        "load iRODS",
        "load bowtie/1.0.0",
        "load perl",
        "load samtools",
        "load java64/1.7.0"
    ],
    "revision": 1,
    "tags": [
        "next-gen",
        "assembler",
        "rnaseq"
    ],
    "testPath": "test.bashx",
    "ontolog": ["http://sswapmeet.sswap.info/agave/apps/Application"],
    "name": "trinity_lonestar4",
    "parameters": [
        {
            "defaultValue": "false",
            "details": {
                "description": "",
                "label": "Do not lock triplet-supported nodes"
            },
            "id": "no_triplet_lock",
            "semantics": {"ontology": ["xs:boolean"]},
            "value": {
                "default": "false",
                "visible": true,
                "validator": "\\d++",
                "type": "bool",
                "required": false
            }
        },
        {
            "defaultValue": "20000000",
            "details": {
                "description": "",
                "label": "Maximum number of reads to anchor within a single graph"
            },
            "id": "max_reads_per_graph",
            "semantics": {"ontology": ["xs:integer"]},
            "value": {
                "default": "20000000",
                "visible": true,
                "validator": "\\d++",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "1",
            "details": {
                "description": "",
                "label": "Min count for K-mers to be assembled"
            },
            "id": "min_kmer_cov",
            "semantics": {"ontology": ["xs:integer"]},
            "value": {
                "default": "1",
                "visible": true,
                "validator": "\\d++",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "22",
            "details": {
                "description": "",
                "label": "The number of tasks and OpenMP threads to use for Trinity execution"
            },
            "id": "CPU",
            "semantics": {"ontology": ["xs:integer"]},
            "value": {
                "default": "22",
                "visible": true,
                "validator": "\\d++",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "75",
            "details": {
                "description": "",
                "label": "Minimum overlap of reads with growing transcript path"
            },
            "id": "path_reinforcement_distance",
            "semantics": {"ontology": ["xs:integer"]},
            "value": {
                "default": "75",
                "visible": true,
                "validator": "\\d++",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "",
            "details": {
                "description": "",
                "label": "Optional strand-specific library type"
            },
            "id": "SS_lib_type",
            "semantics": {"ontology": ["xs:string"]},
            "value": {
                "default": "",
                "visible": true,
                "validator": "FR|RF|F|R",
                "type": "string",
                "required": false
            }
        },
        {
            "defaultValue": "200",
            "details": {
                "description": "",
                "label": "Minimum assembled contig length to report"
            },
            "id": "min_contig_length",
            "semantics": {"ontology": ["xs:integer"]},
            "value": {
                "default": "200",
                "visible": true,
                "validator": "\\d++",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "false",
            "details": {
                "description": "",
                "label": "Perform Jacard clipping (expensive)"
            },
            "id": "jacard_clip",
            "semantics": {"ontology": ["xs:boolean"]},
            "value": {
                "default": "false",
                "visible": true,
                "validator": "\\d++",
                "type": "bool",
                "required": false
            }
        },
        {
            "defaultValue": "",
            "details": {
                "description": "",
                "label": "Additional parameters to pass through to butterfly"
            },
            "id": "bfly_opts",
            "semantics": {"ontology": ["xs:string"]},
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "type": "string",
                "required": false
            }
        },
        {
            "defaultValue": "500",
            "details": {
                "description": "",
                "label": "Maximum length expected between fragment pairs"
            },
            "id": "group_pairs_distance",
            "semantics": {"ontology": ["xs:integer"]},
            "value": {
                "default": "500",
                "visible": true,
                "validator": "\\d++",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "fq",
            "details": {
                "description": "",
                "label": "File type of reads: (fq or fa)"
            },
            "id": "seqType",
            "semantics": {"ontology": ["xs:string"]},
            "value": {
                "default": "fq",
                "visible": true,
                "validator": "fa|fq",
                "type": "string",
                "required": true
            }
        },
        {
            "defaultValue": "10",
            "details": {
                "description": "",
                "label": "Only most supported (N) paths are extended from node A->B"
            },
            "id": "max_number_of_paths_per_node",
            "semantics": {"ontology": ["xs:integer"]},
            "value": {
                "default": "10",
                "visible": true,
                "validator": "\\d++",
                "type": "number",
                "required": false
            }
        }
    ]
}