{
    "longDescription": "",
    "outputs": [{
        "defaultValue": "bwa_output.bam",
        "details": {
            "description": "Sequence alignment file",
            "label": "SAM file"
        },
        "id": "outputSAM",
        "semantics": {
            "minCardinality": 1,
            "maxCardinality": 1,
            "ontology": ["http://sswapmeet.sswap.info/mime/text/X-sam"],
            "fileTypes": [""]
        },
        "value": {
            "default": "bwa_output.bam",
            "validator": ""
        }
    }],
    "inputs": [
        {
            "defaultValue": "",
            "details": {
                "visible": true,
                "description": "",
                "label": "Query FASTQ 1"
            },
            "id": "query1",
            "semantics": {
                "minCardinality": 1,
                "ontology": ["http://sswapmeet.sswap.info/mime/text/X-fastq"],
                "fileTypes": ["fastq-0"]
            },
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "required": true
            }
        },
        {
            "defaultValue": "",
            "details": {
                "visible": true,
                "description": "",
                "label": "Genome database FASTA file"
            },
            "id": "databaseFasta",
            "semantics": {
                "minCardinality": 1,
                "ontology": [
                    "http://sswapmeet.sswap.info/mime/text/X-multiFasta",
                    "http://sswapmeet.sswap.info/mime/text/X-fasta"
                ],
                "fileTypes": ["fasta-0"]
            },
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "required": true
            }
        },
        {
            "defaultValue": "",
            "details": {
                "visible": true,
                "description": "",
                "label": "Query FASTQ 2"
            },
            "id": "query2",
            "semantics": {
                "minCardinality": 1,
                "ontology": ["http://sswapmeet.sswap.info/mime/text/X-fastq"],
                "fileTypes": ["fastq-0"]
            },
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "required": false
            }
        }
    ],
    "parallelism": "PARALLEL",
    "executionType": "HPC",
    "available": true,
    "defaultProcessors": 48,
    "executionHost": "lonestar4.tacc.teragrid.org",
    "public": true,
    "defaultMemory": 4,
    "id": "bwa-lonestar-0.5.9u3",
    "checkpointable": false,
    "deploymentPath": "/ipcservices/applications/bwa-lonestar-0.5.9u3.zip",
    "label": "BWA@Lonestar",
    "shortDescription": "bwa 0.5.9 is a next gen sequence aligner",
    "helpURI": "http://bio-bwa.sourceforge.net/",
    "defaultRequestedTime": "24:00:00",
    "version": "0.5.9",
    "templatePath": "wrapper.sh",
    "modules": [
        "purge",
        "load TACC",
        "load launcher",
        "load samtools/0.1.18",
        "load irods"
    ],
    "revision": 3,
    "tags": [
        "next-gen",
        "aligner"
    ],
    "testPath": "library/test.sh",
    "ontolog": ["http://sswapmeet.sswap.info/agave/apps/Application"],
    "name": "bwa-lonestar",
    "parameters": [
        {
            "defaultValue": "32",
            "details": {
                "description": "",
                "label": "Seed length"
            },
            "id": "seedLength",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "32",
                "visible": true,
                "validator": "\\d++",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "0.04",
            "details": {
                "description": "",
                "label": "Max #diff (int) or missing prob under 0.02 err rate (float)"
            },
            "id": "mismatchTolerance",
            "semantics": {"ontology": ["xs:float"]},
            "value": {
                "default": "0.04",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "0",
            "details": {
                "description": "",
                "label": "Quality threshold for read trimming down to 35bp"
            },
            "id": "qualityForTrimming",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "0",
                "visible": true,
                "validator": "\\d++",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "11",
            "details": {
                "description": "",
                "label": "Gap open penalty"
            },
            "id": "gapOpenPenalty",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "11",
                "visible": true,
                "validator": "\\d++",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "4",
            "details": {
                "description": "",
                "label": "Gap extension penalty"
            },
            "id": "gapExtensionPenalty",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "4",
                "visible": true,
                "validator": "\\d++",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "0",
            "details": {
                "description": "",
                "label": "Length of 5' barcode"
            },
            "id": "barCodeLength",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "0",
                "visible": true,
                "validator": "\\d++",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "false",
            "details": {
                "description": "",
                "label": "Log-scaled gap penalty for long deletions"
            },
            "id": "logScaleGapPenalty",
            "semantics": {"ontology": ["xs:boolean"]},
            "value": {
                "default": "false",
                "visible": true,
                "validator": "",
                "type": "bool",
                "required": false
            }
        },
        {
            "defaultValue": "2000000",
            "details": {
                "description": "",
                "label": "Maximum entries in the queue"
            },
            "id": "maxEntriesQueue",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "2000000",
                "visible": true,
                "validator": "\\d++",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "3",
            "details": {
                "description": "",
                "label": "Mismatch penalty"
            },
            "id": "mismatchPenalty",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "3",
                "visible": true,
                "validator": "\\d++",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "30",
            "details": {
                "description": "",
                "label": "Stop searching when there are >INT equally best hits"
            },
            "id": "stopSearching",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "30",
                "visible": true,
                "validator": "\\d++",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "false",
            "details": {
                "description": "",
                "label": "Non-iterative mode: search for all n-difference hits (slooow)"
            },
            "id": "nonIterativeMode",
            "semantics": {"ontology": ["xs:boolean"]},
            "value": {
                "default": "false",
                "visible": true,
                "validator": "",
                "type": "bool",
                "required": false
            }
        },
        {
            "defaultValue": "2",
            "details": {
                "description": "",
                "label": "Maximum differences in the seed"
            },
            "id": "maxDifferenceSeed",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "2",
                "visible": true,
                "validator": "\\d++",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "1",
            "details": {
                "description": "",
                "label": "Maximum number or fraction of gap opens"
            },
            "id": "maxGapOpens",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "1",
                "visible": true,
                "validator": "\\d++",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "10",
            "details": {
                "description": "",
                "label": "Maximum occurrences for extending a long deletion"
            },
            "id": "maxOccLongDeletion",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "10",
                "visible": true,
                "validator": "\\d++",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "6",
            "details": {
                "description": "",
                "label": "Number of threads"
            },
            "id": "numThreads",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "6",
                "visible": true,
                "validator": "\\d++",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "5",
            "details": {
                "description": "",
                "label": "Do not put an indel within INT bp towards the ends"
            },
            "id": "noEndIndel",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "5",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "-1",
            "details": {
                "description": "",
                "label": "Maximum number of gap extensions, -1 for disabling long gaps"
            },
            "id": "maxGapExtensions",
            "semantics": {"ontology": ["xs:short"]},
            "value": {
                "default": "-1",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": false
            }
        }
    ]
}