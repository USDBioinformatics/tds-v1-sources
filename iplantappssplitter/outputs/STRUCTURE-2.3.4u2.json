{
    "longDescription": "population structure",
    "outputs": [],
    "inputs": [
        {
            "defaultValue": "",
            "details": {
                "visible": true,
                "description": "marker file",
                "label": "Input Data"
            },
            "id": "i",
            "semantics": {
                "minCardinality": 1,
                "ontology": ["xs:string"],
                "fileTypes": [""]
            },
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "required": true
            }
        },
        {
            "defaultValue": "",
            "details": {
                "visible": true,
                "description": "extra parameter file",
                "label": "Input Data"
            },
            "id": "e",
            "semantics": {
                "minCardinality": 1,
                "ontology": ["xs:string"],
                "fileTypes": [""]
            },
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "required": false
            }
        }
    ],
    "parallelism": "PARALLEL",
    "executionType": "HPC",
    "available": true,
    "defaultProcessors": 24,
    "executionHost": "lonestar4.tacc.teragrid.org",
    "public": true,
    "defaultMemory": 4,
    "id": "STRUCTURE-2.3.4u2",
    "checkpointable": false,
    "deploymentPath": "/ipcservices/applications/STRUCTURE-2.3.4u2.zip",
    "label": "structure",
    "shortDescription": "population structure",
    "helpURI": "http://pritch.bsd.uchicago.edu/structure.html",
    "defaultRequestedTime": "24:00:00",
    "version": "2.3.4",
    "templatePath": "structure_wrapper.sh",
    "modules": [
        "purge",
        "load TACC",
        "load irods",
        "load launcher"
    ],
    "revision": 2,
    "tags": [
        "GWAS",
        "population structure"
    ],
    "testPath": "structure_test_wrapper.sh",
    "ontolog": ["http://sswapmeet.sswap.info/agave/apps/Application"],
    "name": "STRUCTURE",
    "parameters": [
        {
            "defaultValue": "false",
            "details": {
                "description": "flag for using pop data",
                "label": "Use population data"
            },
            "id": "popflag",
            "semantics": {"ontology": ["xs:boolean"]},
            "value": {
                "default": "false",
                "visible": true,
                "validator": "",
                "type": "bool",
                "required": true
            }
        },
        {
            "defaultValue": "0",
            "details": {
                "description": "Number of extra columns",
                "label": "Number of additional columns before the marker data start"
            },
            "id": "extracols",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "0",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": true
            }
        },
        {
            "defaultValue": "true",
            "details": {
                "description": "Contain rows of marker name",
                "label": "Contain rows of marker name"
            },
            "id": "markernames",
            "semantics": {"ontology": ["xs:boolean"]},
            "value": {
                "default": "true",
                "visible": true,
                "validator": "",
                "type": "bool",
                "required": true
            }
        },
        {
            "defaultValue": "",
            "details": {
                "description": null,
                "label": "Number of individuals"
            },
            "id": "numinds",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "false",
            "details": {
                "description": "has pop info",
                "label": "Contains population identifier"
            },
            "id": "popdata",
            "semantics": {"ontology": ["xs:boolean"]},
            "value": {
                "default": "false",
                "visible": true,
                "validator": "",
                "type": "bool",
                "required": true
            }
        },
        {
            "defaultValue": "3",
            "details": {
                "description": "Number of runs",
                "label": "Number of runs for each assumed population number"
            },
            "id": "kruns",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "3",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": true
            }
        },
        {
            "defaultValue": "-9",
            "details": {
                "description": "Value given to missing marker data",
                "label": "Value given to missing marker data"
            },
            "id": "missing",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "-9",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": true
            }
        },
        {
            "defaultValue": "false",
            "details": {
                "description": "phase info",
                "label": "The data for each individual contains a line indicating phase"
            },
            "id": "phaseinfo",
            "semantics": {"ontology": ["xs:boolean"]},
            "value": {
                "default": "false",
                "visible": true,
                "validator": "",
                "type": "bool",
                "required": true
            }
        },
        {
            "defaultValue": "100",
            "details": {
                "description": "Number of repeats",
                "label": "Number of MCMC repeats after burnin"
            },
            "id": "numreps",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "100",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": true
            }
        },
        {
            "defaultValue": "1",
            "details": {
                "description": "Ploidy of data",
                "label": "Ploidy of data"
            },
            "id": "ploidy",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "1",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": true
            }
        },
        {
            "defaultValue": "false",
            "details": {
                "description": "Has phenotype",
                "label": "Contain phenotype information"
            },
            "id": "phenotype",
            "semantics": {"ontology": ["xs:boolean"]},
            "value": {
                "default": "false",
                "visible": true,
                "validator": "",
                "type": "bool",
                "required": true
            }
        },
        {
            "defaultValue": "100",
            "details": {
                "description": "Length of burnin period",
                "label": "Length of burnin period"
            },
            "id": "burnin",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "100",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": true
            }
        },
        {
            "defaultValue": "",
            "details": {
                "description": "Numerical file",
                "label": "Output file"
            },
            "id": "o",
            "semantics": {"ontology": ["xs:string"]},
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "type": "string",
                "required": true
            }
        },
        {
            "defaultValue": "",
            "details": {
                "description": null,
                "label": "Number of loci"
            },
            "id": "numloci",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "false",
            "details": {
                "description": "The phase info follows a Markov model",
                "label": "The phase info follows a Markov model"
            },
            "id": "markovphase",
            "semantics": {"ontology": ["xs:boolean"]},
            "value": {
                "default": "false",
                "visible": true,
                "validator": "",
                "type": "bool",
                "required": true
            }
        },
        {
            "defaultValue": "5",
            "details": {
                "description": null,
                "label": "Maximum populations to test"
            },
            "id": "maxpops",
            "semantics": {"ontology": ["xs:int"]},
            "value": {
                "default": "5",
                "visible": true,
                "validator": "",
                "type": "number",
                "required": false
            }
        },
        {
            "defaultValue": "true",
            "details": {
                "description": "has label",
                "label": "Has individual label"
            },
            "id": "label",
            "semantics": {"ontology": ["xs:boolean"]},
            "value": {
                "default": "true",
                "visible": true,
                "validator": "",
                "type": "bool",
                "required": true
            }
        },
        {
            "defaultValue": "false",
            "details": {
                "description": "Store data for individuals in a single line",
                "label": "Store data for individuals in a single line"
            },
            "id": "onerowperind",
            "semantics": {"ontology": ["xs:boolean"]},
            "value": {
                "default": "false",
                "visible": true,
                "validator": "",
                "type": "bool",
                "required": true
            }
        },
        {
            "defaultValue": "false",
            "details": {
                "description": "Contain rows of map distances between loci",
                "label": "Contain rows of map distances between loci"
            },
            "id": "mapdistances",
            "semantics": {"ontology": ["xs:boolean"]},
            "value": {
                "default": "false",
                "visible": true,
                "validator": "",
                "type": "bool",
                "required": true
            }
        }
    ]
}