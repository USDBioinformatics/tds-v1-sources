{
    "longDescription": "GeneSeqer, a parallel (MPI), gapped mapper for ESTs",
    "outputs": [],
    "inputs": [
        {
            "defaultValue": "",
            "details": {
                "visible": true,
                "description": "Sequence file in fastq, fasta, or sam format",
                "label": "ESTs:"
            },
            "id": "estSeq",
            "semantics": {
                "minCardinality": 1,
                "ontology": ["http://sswapmeet.sswap.info/sequence/FASTA"],
                "fileTypes": ["fasta-0"]
            },
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "required": false
            }
        },
        {
            "defaultValue": "",
            "details": {
                "visible": true,
                "description": "Sequence file in fasta format",
                "label": "genome target"
            },
            "id": "libfname",
            "semantics": {
                "minCardinality": 0,
                "ontology": ["http://sswapmeet.sswap.info/sequence/FASTA"],
                "fileTypes": ["fasta-0"]
            },
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "required": false
            }
        },
        {
            "defaultValue": "",
            "details": {
                "visible": true,
                "description": "Sequence file in genbank format",
                "label": "genbank target"
            },
            "id": "genbank",
            "semantics": {
                "minCardinality": 0,
                "ontology": ["http://sswapmeet.sswap.info/sequence/FASTA"],
                "fileTypes": ["fasta-0"]
            },
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "required": false
            }
        }
    ],
    "parallelism": "PARALLEL",
    "executionType": "HPC",
    "available": true,
    "defaultProcessors": 16,
    "executionHost": "stampede.tacc.xsede.org",
    "public": true,
    "defaultMemory": 2,
    "id": "GeneSeqer-5.0u1",
    "checkpointable": true,
    "deploymentPath": "/ipcservices/applications/GeneSeqer-5.0u1.zip",
    "label": "GeneSeqer5.0",
    "shortDescription": "Spliced alignment tool",
    "helpURI": "http://brendelgroup.org/bioinformatics2go/GeneSeqer.php",
    "defaultRequestedTime": "4:00:00",
    "version": "5.0",
    "templatePath": "geneseqer_wrapper.sh",
    "modules": [
        "purge",
        "load TACC",
        "load irods"
    ],
    "revision": 1,
    "tags": [
        "aligner",
        "NGS",
        "EST",
        "flatfile"
    ],
    "testPath": "test/geneseqer_wrapper_test.sh",
    "ontolog": ["http://sswapmeet.sswap.info/sequenceServices/SequenceServices"],
    "name": "GeneSeqer",
    "parameters": [
        {
            "defaultValue": "",
            "details": {
                "description": null,
                "label": "word size"
            },
            "id": "wsize",
            "semantics": {"ontology": ["xs:string"]},
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "type": "string",
                "required": false
            }
        },
        {
            "defaultValue": "",
            "details": {
                "description": null,
                "label": "Species name"
            },
            "id": "Species",
            "semantics": {"ontology": ["xs:string"]},
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "type": "string",
                "required": true
            }
        },
        {
            "defaultValue": "",
            "details": {
                "description": null,
                "label": "optional XML and GFF outputs"
            },
            "id": "gffOption",
            "semantics": {"ontology": ["xs:string"]},
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "type": "string",
                "required": false
            }
        },
        {
            "defaultValue": "",
            "details": {
                "description": null,
                "label": "Est Seq formatting"
            },
            "id": "EstFormat",
            "semantics": {"ontology": ["xs:string"]},
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "type": "string",
                "required": false
            }
        },
        {
            "defaultValue": "",
            "details": {
                "description": null,
                "label": "minimum EST coverage"
            },
            "id": "minESTc",
            "semantics": {"ontology": ["xs:string"]},
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "type": "string",
                "required": false
            }
        },
        {
            "defaultValue": "",
            "details": {
                "description": null,
                "label": "max num ests"
            },
            "id": "maxnest",
            "semantics": {"ontology": ["xs:string"]},
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "type": "string",
                "required": false
            }
        },
        {
            "defaultValue": "16",
            "details": {
                "description": null,
                "label": "number of threads"
            },
            "id": "nthreads",
            "semantics": {"ontology": ["xs:string"]},
            "value": {
                "default": "16",
                "visible": true,
                "validator": "",
                "type": "string",
                "required": true
            }
        },
        {
            "defaultValue": "GSOutput",
            "details": {
                "description": null,
                "label": "Name for output directory"
            },
            "id": "outPutFile",
            "semantics": {"ontology": ["xs:string"]},
            "value": {
                "default": "GSOutput",
                "visible": true,
                "validator": "",
                "type": "string",
                "required": true
            }
        },
        {
            "defaultValue": "",
            "details": {
                "description": null,
                "label": "html output"
            },
            "id": "htmlOut",
            "semantics": {"ontology": ["xs:string"]},
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "type": "string",
                "required": false
            }
        },
        {
            "defaultValue": "",
            "details": {
                "description": null,
                "label": "q HSP chain"
            },
            "id": "minqHSPc",
            "semantics": {"ontology": ["xs:string"]},
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "type": "string",
                "required": false
            }
        },
        {
            "defaultValue": "",
            "details": {
                "description": null,
                "label": "segment size"
            },
            "id": "sgmntsze",
            "semantics": {"ontology": ["xs:string"]},
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "type": "string",
                "required": false
            }
        },
        {
            "defaultValue": "",
            "details": {
                "description": null,
                "label": "minimum quality HSP"
            },
            "id": "minqHSP",
            "semantics": {"ontology": ["xs:string"]},
            "value": {
                "default": "",
                "visible": true,
                "validator": "",
                "type": "string",
                "required": false
            }
        },
        {
            "defaultValue": "L",
            "details": {
                "description": null,
                "label": "Genome Seq formatting"
            },
            "id": "genomeFormat",
            "semantics": {"ontology": ["xs:string"]},
            "value": {
                "default": "L",
                "visible": true,
                "validator": "",
                "type": "string",
                "required": false
            }
        }
    ]
}