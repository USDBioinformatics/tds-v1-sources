/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.bqt;

/**
 *
 * @author work
 */
public final class Constants {

    public static final String MAPDB_DIR = "mapdb";
    public static final String MAPDB_NAME = "db";
    public static final String LUCENE_DIR = "lucene";
    public static final String DICTIONARY_DB = "dictionary";
    public static final String ONTOLOGY_NAMES_DB = "ontology-names";
    public static final String TOOL_NAMES_DB = "tool-names";
    public static final String TOOLS_DB = "tools";

    private Constants() {}
}
