
package edu.usd.bme.bqt;


import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.vocabulary.RDFS;
import static edu.usd.bme.bqt.Constants.*;
import edu.usd.bme.btl.tq.lucene.analyzer.BQTAnalyzer;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParserBase;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.MMapDirectory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.mapdb.DB;
import org.mapdb.DBMaker;

public class RayKinsella {

    private static final Logger LOG = Logger.getLogger(RayKinsella.class.getName());

    private static final File BASE_DATA_PATH = new File("/tmp/tdsindexes");
    private static final File MAPDB_PATH = new File(BASE_DATA_PATH, "mapdb");
    private static final File LUCENE_PATH = new File(BASE_DATA_PATH, "lucene");

    private static final String HOST = "http://jacksons.usd.edu";
    private static final String TOOLS_URL =
            HOST + "/BTL-REST/resources/tools";
    private static final String RESOURCE_URL =
            HOST + "/BTL-REST/resources/repos/getbytoolids";
    private static final String COUNT_URL =
            HOST + "/BTL-REST/resources/tools/count";

    public static void main(String[] args) {
        RayKinsella ray = new RayKinsella();
        validatePaths(BASE_DATA_PATH, MAPDB_PATH, LUCENE_PATH);
        ray.buildIt();
    }

    private static void validatePaths(File ...  paths) {
        for (File path : paths) {
            if (path.exists() == false
                    && path.mkdirs() == false) {
                String msg = "Unable to create directory: "
                        + path.getAbsolutePath();
                throw new RuntimeException(msg);
            } else {
				//MapDB throws a fit if we try to add a new db with the same
				//name as one that exists.  So I'm going to just purge all the
				//folders we want to make.
				try {
					if (!path.equals(BASE_DATA_PATH)) {
						//purge directories that aren't /tmp, or our base
						purgeDirectory(path);
					}
				} catch (Exception e) {
					System.out.println("Can't delete path: "+path);
				}
			}
        }
    }

	private static void purgeDirectory(File dir) {
		for (File file: dir.listFiles()) {
			if (file.isDirectory()) purgeDirectory(file);
			file.delete();
		}
	}

    private void buildIt() {
        this.loadTools();
		System.out.println("Hi");
        this.loadOntologies("EDAM", "NGSONTO", "SWO");
        this.createIndex();
        this.searchOntTerms();
        
    }

    private int getToolCount() {
        try (Reader rdr = this.get(COUNT_URL)) {
            JSONArray ja = new JSONArray(new JSONTokener(rdr));
            return ja.getInt(0);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void loadTools() {
        DB db = this.openDb();
        Set<String> tools = db
                .createTreeSet(TOOLS_DB)
                .makeStringSet();
        Set<String> toolNms = db
                .createTreeSet(TOOL_NAMES_DB)
                .makeStringSet();
        int toolCount = this.getToolCount();
        String repoId = null;

        for (int i = 0, inc = 100; i < toolCount; i += inc) {
            // the range requests against the btl-rest endpoint do not return
            // records based upon their row ordinal but instead uses the
            // record's id field. Currently this works as the ids are
            // consecutive but until this is updated or replaced, decrement the
            // high mark so records are not duplicated
            int highMk = (i + inc) < toolCount ? (i + inc - 1) : toolCount;
            JSONArray list = new JSONArray();
            JSONObject[] repos = new JSONObject[inc];
            JSONArray ja;

            try (Reader rdr = this.get(TOOLS_URL + "/" + i + "/" + highMk)) {
                ja = new JSONArray(new JSONTokener(rdr));

                LOG.log(Level.INFO, "Load tools pass: {0}", i / inc);
//                for (int y = i; y < highMk; y += 1) {
//                    JSONObject obj = new JSONObject();
//                    obj.put("toolId", y);
//                    //repoId and repoName can be null
//                    obj.put("repoId", repoId);
//                    obj.put("repoName", repoId);
//                    list.put(obj);
//                }

                // build the toolId list using their actual ids and not an
                // assumed 1 based ordinal
                for (int j = 0, l = ja.length(); j < l; j += 1) {
                    JSONObject obj = new JSONObject();
                    obj.put("toolId", ja.getJSONObject(j).getInt("id"));
                    list.put(obj);
                }
            } catch (IOException e) {
                throw new RuntimeException();
            }

            String jsonOriginArray =
                    getOriginationRepositoryForTools(list.toString());
            list = new JSONArray(jsonOriginArray);

            for (int j = 0; j < list.length(); j++) {
                JSONObject obj = list.getJSONObject(j);
                int idx = obj.getInt("toolId") - i;
                if(repos[idx] == null) repos[idx] = obj;
            }

            for (int x = 0; x < ja.length(); x++) {
                JSONObject j = ja.getJSONObject(x);
                //set origination repository for tool
                try {
                    if(j.getJSONObject("bets").getJSONArray("tool_type").getString(0).equals("Link to literature")) {
                        continue;
                    }
                } catch(JSONException e) {
                    
                }
                try {
                    JSONObject repo = repos[x];
                    j.getJSONObject("bets").put("repoName", repo.getString("repoName"));
                } catch (Exception e) {
                    j.getJSONObject("bets").put("repoName", "Not Specified");
                }
//                  toolMp.put(j.getInt("id"), j.toString());
                        
                    tools.add(j.toString());
                    toolNms.add(j.getString("name"));
            }
        }

        LOG.log(
                Level.INFO,
                "Tool count: {0}\nTool list size: {1}",
                new Object[] { toolCount, tools.size()});
        db.commit();
        db.close();
    }

    private void loadOntologies(String ... ontNms) {
        DB db = this.openDb();
        Set<String> ontNames = db
                .createTreeSet(ONTOLOGY_NAMES_DB)
                .makeStringSet();
        Map<String, Set<String>> dictionary = db
                .createTreeMap(DICTIONARY_DB)
                .make();

        for (String ontNm : ontNms) {
            this.loadOntology(ontNm, ontNames, dictionary);
        }
        dictionary.remove("");

        db.commit();
        db.close();
    }

    private void loadOntology(String ontology, Set<String> ontNames, Map<String, Set<String>> dictionary ) {
        File ontFl = new File("src/main/resources/xrdf/" + ontology + ".xrdf");
        try (InputStream is = new BufferedInputStream(new FileInputStream(ontFl))) {// getClass().getResourceAsStream("/xrdf/" + ontology + ".xrdf");
        OntModel m = ModelFactory.createOntologyModel();
        m.read(is, "RDF/XML");
        String NS = "http://www.geneontology.org/formats/oboInOwl#";
        String ExSyn = "hasExactSynonym"; // future: add hasNarrowSynonym?
        Property synRDF = m.getProperty(NS + ExSyn);
        ExtendedIterator<OntClass> i = m.listClasses();
        while (i.hasNext()) {
            OntClass cls = i.next();
            List<String> names = new ArrayList<>();
            NodeIterator labels = cls.listPropertyValues(RDFS.label);
            ExtendedIterator synonyms = cls.listPropertyValues(synRDF);
            if (!labels.hasNext() && !synonyms.hasNext()) continue;
            while (labels.hasNext()) {
                RDFNode label = labels.next();
                ontNames.add(label.toString());
                names.add(label.toString().toLowerCase());
                while (synonyms.hasNext()) {
                    names.add(synonyms.next().toString().toLowerCase());
                }
                for (String s : names) {
                    Set<String> temp = new TreeSet<>(names);
                    temp.remove(s);
                    if (temp.isEmpty()) break;
                    if (dictionary.containsKey(s)) {
                        dictionary.get(s).addAll(temp);
                    } else {
                        dictionary.put(s, temp);
                    }
                }
            }
        }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public Map<String, Set<String>> getDictionary() {
        DB db = this.openDb();
        return db.getTreeMap(DICTIONARY_DB);
    }
	
	public boolean dbExists() {
			File openDbFl = new File(MAPDB_PATH, "db");
			return openDbFl.exists();
	}

    public void createIndex() {
        try {
            DB db = this.openDb();
            Set<String> toolDefStrings = db.getTreeSet(TOOLS_DB);
            Path path = Paths.get(LUCENE_PATH.getAbsolutePath());
            Directory dir =  new SimpleFSDirectory(path);
            Analyzer analyzer = new BQTAnalyzer();
            IndexWriterConfig cfg;
            IndexWriter writer;

            analyzer.setVersion(Version.LATEST);
            cfg = new IndexWriterConfig(analyzer);
            cfg.setRAMBufferSizeMB(256);
            writer = new IndexWriter(dir, cfg);

            for (String toolDef : toolDefStrings) {
                Document d = this.createDoc(toolDef);
                writer.addDocument(d);
                if (writer.maxDoc() % 100 == 0) {
                    LOG.log(Level.INFO, "Indexed {0} documents.", writer.maxDoc());
                }
            }

            writer.close();
            db.delete(TOOLS_DB);
            db.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void searchOntTerms() {
        DB db = this.openDb();
        try {
            Set<String> ontNames = db
                    .getTreeSet(ONTOLOGY_NAMES_DB);
            Map<String, Set<String>> dictionary = db
                .getTreeMap(DICTIONARY_DB);
            List<String> notFound = new ArrayList<>();
            Path path = Paths.get(LUCENE_PATH.getAbsolutePath());
            Directory dir = new MMapDirectory(path);
            IndexReader indxRdr = DirectoryReader.open(dir);
            IndexSearcher indxSrchr = new IndexSearcher(indxRdr);
            Analyzer analyzer = new BQTAnalyzer();

            for (String nm : ontNames) {
                String qryS = QueryParserBase.escape(nm);
                Query qry = new QueryParser("FULL_TEXT", analyzer).parse("\"" + qryS + "\"");
                if (indxSrchr.count(qry) == 0) {
                    notFound.add(nm);
                }
            }

            ontNames.removeAll(notFound);
            LOG.log(
                    Level.INFO,
                    "Removed {0} unmatched ontological terms.",
                    ontNames.size());
            db.commit();

        } catch (IOException | ParseException e) {
            throw new RuntimeException(e);
        } finally {
            db.close();
        }
    }
	
    private DB openDb() {
        File openDbFl = new File(MAPDB_PATH, "db");
        return DBMaker
            .newFileDB(openDbFl)
            .transactionDisable()
            .mmapFileEnable()
            .asyncWriteEnable()
            .make();
    }

    private String getOriginationRepositoryForTools(String jsonToolIds) {
        try (Reader rdr = post(RESOURCE_URL, jsonToolIds)) {
            StringWriter wtr = new StringWriter();
            while (true) {
                int ch = rdr.read();
                if (ch == -1) {
                    break;
                }
                wtr.append((char) ch);
            }
            return wtr.toString();
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    private Document createDoc(String toolDef) {
        JSONObject j = new JSONObject(toolDef);
        Document doc = new org.apache.lucene.document.Document();

        doc.add(new StringField(
                "id",
                String.valueOf(j.getInt("id")),
                Field.Store.YES));
        doc.add(new StringField(
                "name",
                j.getString("name"),
                Field.Store.YES));
//        doc.add(new TextField(
//                "BETS",
//                j.getJSONObject("bets").toString(),
//                Field.Store.YES));
        doc.add(new TextField("FULL_TEXT", toolDef, Field.Store.YES));
        return doc;
    }

//    private Document addCategoryField(Document doc, JSONObject ctgJs, String key, String fldNm) {
//        JSONArray fldAr = ctgJs.optJSONArray(key);
//
//        if (fldAr != null) {
//            for (int i = 0, l = fldAr.length(); i < l; i += 1) {
//                JSONObject fldJs = fldAr.getJSONObject(i);
//                String fldVl = fldJs.getString("name");
//                doc.add(new TextField(fldNm, fldVl, Field.Store.YES));
//            }
//        }
//
//        return doc;
//    }

    private Reader get(String urlToGet) {
        try {
            URL u = new URL(urlToGet);
            HttpURLConnection conn = (HttpURLConnection) u.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            return new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));

        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    private Reader post(String urlToPost, String JSONData) {
        try {
            URL u = new URL(urlToPost);
            HttpURLConnection conn = (HttpURLConnection) u.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json");

            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(JSONData);
            wr.flush();
            return new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    
}
