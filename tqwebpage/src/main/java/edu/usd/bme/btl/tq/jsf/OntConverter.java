
package edu.usd.bme.btl.tq.jsf;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import edu.usd.bme.btl.tq.Ontology;

/**
 *
 * @author Menno.VanDiermen
 */
@FacesConverter("ontConverter")
public class OntConverter implements Converter {
    
    /**
     * 
     * @param fc FacesContext
     * @param uic UIComponent
     * @param value
     * 
     * @return 
     */
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && value.trim().length() > 0) {
            try {
                QueryService service = (QueryService) fc.getExternalContext().getApplicationMap().get("queryService");
                return null; // return service.getOntologies().get(Integer.parseInt(value));
            } catch(NumberFormatException e) {
                System.out.println("Invalid ontology: " + value);
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid ontology."));
            }
        }
        else {
            return null;
        }
    }
 
    /**
     * 
     * @param fc FacesContext
     * @param uic UIComponent
     * @param object
     * @return  ontology id
     */
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
            return "" + ((Ontology) object).getId();
        }
        else {
            return null;
        }
    }   
    
}
