
package edu.usd.bme.btl.tq.jsf;

import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;


class FacesContextProducer {

    @Produces
    FacesContext getFacesContext() {
        return FacesContext.getCurrentInstance();
    }    

}
