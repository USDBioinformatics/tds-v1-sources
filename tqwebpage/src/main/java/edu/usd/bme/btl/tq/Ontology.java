
package edu.usd.bme.btl.tq;

import java.io.Serializable;


/**
 * This class represent an Ontology term.
 * 
 * @author Menno.VanDiermen
 */
public class Ontology implements Serializable {
    private final String term, link;
    private int id;
    
    /**
     * Constructor that sets the term and link attributes of the class
     * @param t (term)
     * @param l (link)
     */
    public Ontology(String t, String l) {
        term = t; link = l;
    }
    
    /**
     * toString returns the term + the link of this class
     * 
     * @return term + link
     */
    @Override
    public String toString() {
        return getTerm() + " " + getLink();
    }

    /**
     * Two ontology objects are equal if their terms are equal
     * 
     * @param obj (ontology object)
     * 
     * @return 
     */
    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Ontology)){
            return false;
        }
        Ontology o = (Ontology) obj;
        return this.term.equals(o.getTerm());
    }

    @Override
    public int hashCode() {
        return term.hashCode();
    }

    /**
     * @return the term
     */
    public String getTerm() {
        return term;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    
    
}
