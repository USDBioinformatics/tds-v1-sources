/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.tq.lucene.analyzer;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseTokenizer;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.KStemFilter;
import org.apache.lucene.analysis.standard.StandardAnalyzer;

/**
 *
 * @author Menno.VanDiermen
 */
public class BQTAnalyzer extends Analyzer {
    
    @Override
    protected TokenStreamComponents createComponents(String string) {
        final Tokenizer source = new LowerCaseTokenizer();
        TokenStream result = new StopFilter(source, StandardAnalyzer.STOP_WORDS_SET);
        result = new KStemFilter(result);
        return new TokenStreamComponents(source, result);
    }
}