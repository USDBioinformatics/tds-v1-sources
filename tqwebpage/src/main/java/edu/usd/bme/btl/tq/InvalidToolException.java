

package edu.usd.bme.btl.tq;

/**
 * Exception class used to indicate a problem with an analytic tool 
 * @author clushbou
 */
public class InvalidToolException extends Exception {
    
    /**
     * Default constructor InvalidToolException
     */
    public InvalidToolException(){
        
    }
    
    /** InvalidToolException constructor with an associated message
     * 
     * @param message 
     */
    public InvalidToolException(String message){
        super(message);
    }
    
}
