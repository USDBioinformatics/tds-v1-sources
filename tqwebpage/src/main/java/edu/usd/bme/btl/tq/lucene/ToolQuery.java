
package edu.usd.bme.btl.tq.lucene;

import edu.usd.bme.btl.tq.lucene.analyzer.SynonymEngine;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.queryparser.classic.QueryParserBase;


public class ToolQuery {

    private static final Logger LOG = Logger.getLogger(ToolQuery.class.getName());

    private static final String BRDR =
            "\n****************************************" +
            "****************************************\n";
    private static final String AND = "AND";
    private static final String OR = "OR";
    private static final String LP = Character.toString('(');
    private static final String QT = Character.toString('"');
    private static final String RP = Character.toString(')');
    private static final String SP = " ";
    private static final String TAB = "\t";
    private final String query;
    
    private final SynonymEngine engine;
    private final Shingler shingler = new Shingler(2,5);

    public ToolQuery(String freeQuery, List<String> selectedTerms, SynonymEngine engine) {
        StringBuilder qryBldr = new StringBuilder();
        this.engine = engine;
        this.appendTermValues(qryBldr, selectedTerms);
        this.appendFreeQuery(qryBldr, freeQuery);
        this.query = qryBldr.toString();
        //this.logQuery(freeQuery, selectedTerms);
    }

    private void appendTermValues(StringBuilder qryBldr, List<String> selectedTerms) {
        StringBuilder trmBldr = new StringBuilder();
 
        if (selectedTerms == null || selectedTerms.isEmpty()) {
            return;
        }

        trmBldr.append(LP).append(SP); boolean multiClause = false;
        if(selectedTerms.size() > 1) multiClause = true;
        for (String trm : selectedTerms){
            String escTrm = QueryParserBase.escape(trm);
            trm = trm.toLowerCase();
            
            if(multiClause && engine.hasSynoyms(trm))
                trmBldr
                        .append(LP)
                        .append(SP)
                        ;
            
            trmBldr
                    .append(QT)
                    .append(escTrm)
                    .append(QT)
                    .append(SP)
                    ;
            
            if(engine.hasSynoyms(trm)) {
                
                appendSynonyms(trmBldr, engine.getSynonyms(trm), 0.7);
            }
            if(multiClause && engine.hasSynoyms(trm)) trmBldr.append(RP).append(SP);
            trmBldr
                    .append(AND)
                    .append(SP)
                    ;
        }

        if (trmBldr.toString().equals(LP + SP + QT + QT + SP + AND + SP)) {
            return;
        }

        qryBldr.append(trmBldr.toString().replaceFirst("AND[ ]$", RP));
    }

    private void appendFreeQuery(StringBuilder qryBldr, String freeQuery) {
        List<String> fqL;

        if (freeQuery == null) {
            return;
        }

        freeQuery = freeQuery.trim();
        if (qryBldr.length() > 0) {
                qryBldr.append(SP).append(AND).append(SP);
        }

        fqL = this.extractFreeQueryTerms(freeQuery);
        this.appendFreeQuery(qryBldr, fqL);
    }

    private List<String> extractFreeQueryTerms(String freeQuery) {
        StringBuilder fqBldr = new StringBuilder();
        boolean phrase = false; String phr = "";
        StringBuilder clsBldr = new StringBuilder();

        String[] arr = freeQuery.split("\\s+");
        for (int i = 0; i<arr.length; i++) {
            String s = arr[i].trim();
            String low = s.toLowerCase();
            
            if (s.startsWith(QT)) {
                phrase = true;
                shingler.purge();
                phr = low.substring(1, low.length());
                if(s.equals(QT) && clsBldr.length() > 0) {
                    clsBldr.append(SP);
                }
                clsBldr.append(QT).append(QueryParserBase.escape(s.substring(1, s.length())));
            }
            else if (phrase) {
                
                phr += SP+low;
                if (s.endsWith(QT)) {
                    phrase = false;
                    
                    clsBldr
                            .append(SP)
                            .append(QueryParserBase.escape(s.substring(0, s.length()-1)))
                            .append(QT)
                            ;
                    
                    phr = phr.toLowerCase().substring(1, phr.length()-1);
                    if(engine.hasSynoyms(phr)) {
                        clsBldr.append(SP);
                        appendSynonyms(clsBldr, engine.getSynonyms(phr), 0.5);
                        fqBldr
                                .append(LP)
                                .append(SP)
                                .append(clsBldr.toString())
                                .append(RP)
                                ;
                        continue;
                    } else {
                        clsBldr.append(TAB);
                        fqBldr.append(clsBldr.toString());
                    }
                    clsBldr = new StringBuilder();
                    phr = "";
                } else {
                    clsBldr
                        .append(SP)
                        .append(s)
                        ;
                }
            } else {
                if(engine.hasSynoyms(low)) {
                    fqBldr
                            .append(LP)
                            .append(SP)
                            .append(QueryParserBase.escape(s))
                            .append(SP)
                            ;
                    appendSynonyms(fqBldr, engine.getSynonyms(low), 0.5);
                    fqBldr
                            .append(RP)
                            .append(TAB);
                } else {
                    fqBldr
                            .append(QueryParserBase.escape(s))
                            .append(TAB)
                            ;
                }
                shingler.add(low);
                List<String> shingles = shingler.getShingles();
                for(int j = shingles.size()-1; j>0; j--) {
                    String sh = shingles.get(j).trim();
                    if(!sh.isEmpty()) {
                        if(engine.hasSynoyms(sh)) {
                            fqBldr
                                    .append(LP)
                                    .append(SP)
                                    .append(QT)
                                    .append(QueryParserBase.escape(sh))
                                    .append(QT)
                                    .append("^0.5")
                                    ;
                            appendSynonyms(fqBldr, engine.getSynonyms(sh), 0.25);
                            fqBldr
                                    .append(RP)
                                    .append(TAB);
                            break;
                        }
                    }
                }
            }
            if(i == arr.length - 1 && clsBldr.length() > 0) fqBldr.append(QueryParserBase.escape(clsBldr.toString()));
        }
        return Arrays.asList(fqBldr.toString().split(TAB));
    }
    
    private void appendFreeQuery(StringBuilder qryBldr, List<String> fqL) {
        if (fqL.isEmpty() == false) {
            qryBldr.append(LP).append(SP);
            for (String fqS : fqL) {
                fqS = fqS.trim();
                if (fqS.startsWith(QT) && fqS.endsWith(QT)) {
                    fqS = fqS.substring(1, fqS.length() - 1);
                    qryBldr
                            .append(QT)
                            .append(fqS) //QueryParser.escape(fqS))
                            .append(QT)
                            .append(SP)
                            ;
                } else {
                    qryBldr
                            .append(fqS) //QueryParser.escape(fqS))
                            .append(SP)
                            ;
                }
            }
            qryBldr.append(RP);
        }
    }
    
    private void appendSynonyms(StringBuilder qryBldr, Set<String> syns, double boost) {
        for(String syn: syns) {
            if(syn.contains("^")) syn = syn.replace('^', '-');
            if(syn.contains(":")) syn = syn.replace(":", "\\:");
            qryBldr
                    .append(OR)
                    .append(SP)
                    .append(QT)
                    .append(QueryParserBase.escape(syn))
                    .append(QT)
                    .append("^"+boost)
                    .append(SP)
                    ;
        }
    }

    private void logQuery(String freeQuery, List<String> selectedTerms) {
        StringBuilder sb = new StringBuilder();

        sb
                .append(BRDR)
                .append("The query constructed from the selected terms: ")
                ;
        if (selectedTerms != null) {
            sb.append("[ ");
            for (String trm : selectedTerms) {
                sb
                        .append("\n\t")
                        .append(trm)
                        .append(",")
                        ;
            }
            sb.deleteCharAt(sb.length() - 1).append(" ]");
        } else {
            sb.append((String) null);
        }

        sb.append("\nand the free query: ");
        if (freeQuery != null) {
            sb
                    .append("\n\t\"")
                    .append(freeQuery)
                    .append("\"")
                    ;
        } else {
            sb.append(freeQuery);
        }

        sb
                .append("\nis:\n\t")
                .append(this.getQueryString())
                .append(BRDR)
                ;
        LOG.log(Level.INFO, "{0}", sb);
    }
    
    public String getQueryString() {
        return this.query;
    }

    public boolean isEmpty() {
        return this.query.trim().isEmpty();
    }

    @Override
    public String toString() {
        return this.getQueryString();
    }
    
    private class Shingler {
        
        private String[] buffer;
        private int min, max;
        private int head = 0, tail = 0, n = 0;
        Set<String> constructed = new TreeSet<String>();
        
        protected Shingler(int min, int max) {
            buffer = new String[max];
            this.min = min; this.max = max;
        }
        
        public void add(String s) {
            if(n==0) {
                buffer[tail] = s;
                n++;
            } else if(n < max) {
                buffer[++tail] = s;
                n++;
            } else {
                tail = head;
                if(head < max-1) head++;
                else head = 0;
                buffer[head] = s;
            }
        }
        
        public void purge() {
            buffer = new String[max];
            head = 0; tail = 0;
            n = 0;
        }
        
        private String[] getSortedBuffer() {
            String[] ret = new String[max];
            int tempHead = head;
            for(int i = 0; i<n; i++) {
                if(tempHead == max-1) tempHead = 0;
                ret[i] = buffer[tempHead++];
            }
            return ret;
        }
        
        public List<String> getShingles() {
            if(n < min) return Collections.EMPTY_LIST;
            List<String> ret = new ArrayList<>();
            for(int i = min; i <= max; i++) {
                if(i>n) break;
                String shin = ""; int idx = tail;
                for(int j = 0; j < i; j++) {
                    if(j>n) break;
                    shin = buffer[idx--] + SP + shin;
                    if(idx < 0) idx = n < max ? n-1 : max-1;
                }
                if(!constructed.contains(shin)) {
                    ret.add(shin); constructed.add(shin);
                }
            }
            return ret;
        }
    }
}