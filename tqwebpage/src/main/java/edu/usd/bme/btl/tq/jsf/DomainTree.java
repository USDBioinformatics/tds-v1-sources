
package edu.usd.bme.btl.tq.jsf;

import javax.annotation.PostConstruct;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 * This class represents the Domain tree in the GUI. The nodes are hard
 * coded with predefined values. Each node in the tree is represented by 
 * TreeNodeData (i.e. label and icon)
 * 
 * @author Carol Lushbough
 */
public class DomainTree {

    private TreeNode root;

    @PostConstruct
    public void init() {
        root = new DefaultTreeNode("Root", null);
        TreeNode node0 = new DefaultTreeNode(new TreeNodeData("Biology", "ui-icon-biology"), root);
        TreeNode node1 = new DefaultTreeNode(new TreeNodeData("Chemistry", "ui-icon-chemistry"), root);
        TreeNode node2 = new DefaultTreeNode(new TreeNodeData("Computer Science", "ui-icon-computer"), root);
        TreeNode node3 = new DefaultTreeNode(new TreeNodeData("Informatics", "ui-icon-informatics"), root);
        TreeNode node5 = new DefaultTreeNode(new TreeNodeData("Mathematics", "ui-icon-math"), root);
        TreeNode node6 = new DefaultTreeNode(new TreeNodeData("Medicine", "ui-icon-medicine"), root);
        TreeNode node8 = new DefaultTreeNode(new TreeNodeData("Physics", "ui-icon-physics"), root);

        //Biology Nodes
        TreeNode node00 = new DefaultTreeNode(new TreeNodeData("Chemical Biology", "ui-icon-data-format1"), node0);
        TreeNode node03 = new DefaultTreeNode(new TreeNodeData("Computational Biology", "ui-icon-data-format1"), node0);
        TreeNode node05 = new DefaultTreeNode(new TreeNodeData("Developmental Biology", "ui-icon-data-format1"), node0);
        TreeNode node07 = new DefaultTreeNode(new TreeNodeData("Evolutionary Biology", "ui-icon-data-format1"), node0);
        TreeNode node09 = new DefaultTreeNode(new TreeNodeData("Genome Biology", "ui-icon-data-format1"), node0);
        TreeNode node011 = new DefaultTreeNode(new TreeNodeData("Marine Biology ", "ui-icon-data-format1"), node0);
        TreeNode node013 = new DefaultTreeNode(new TreeNodeData("Microbiology", "ui-icon-data-format1"), node0);
        TreeNode node015 = new DefaultTreeNode(new TreeNodeData("Molecular Biology ", "ui-icon-data-format1"), node0);
        TreeNode node017 = new DefaultTreeNode(new TreeNodeData("Neurobiology", "ui-icon-data-format1"), node0);
        TreeNode node019 = new DefaultTreeNode(new TreeNodeData("Structural Biology", "ui-icon-data-format1"), node0);
        TreeNode node021 = new DefaultTreeNode(new TreeNodeData("System Biology", "ui-icon-data-format1"), node0);
        TreeNode node023 = new DefaultTreeNode(new TreeNodeData("Theoretical Biology", "ui-icon-data-format1"), node0);

        //Chemistry Nodes
        TreeNode node10 = new DefaultTreeNode(new TreeNodeData("Analytical Chemistry", "ui-icon-data-format1"), node1);
        TreeNode node13 = new DefaultTreeNode(new TreeNodeData("Biochemistry", "ui-icon-data-format1"), node1);
        TreeNode node15 = new DefaultTreeNode(new TreeNodeData("Computational Chemistry", "ui-icon-data-format1"), node1);
        TreeNode node17 = new DefaultTreeNode(new TreeNodeData("Medicinal Chemistry", "ui-icon-data-format1"), node1);
        TreeNode node19 = new DefaultTreeNode(new TreeNodeData("Synthetic Chemistry", "ui-icon-data-format1"), node1);

        //Computer Science
        TreeNode node21 = new DefaultTreeNode(new TreeNodeData("Application Programming Interface", "ui-icon-data-format1"), node2);
        TreeNode node23 = new DefaultTreeNode(new TreeNodeData("Computer Programming", "ui-icon-my-folder"), node2);
        TreeNode node231 = new DefaultTreeNode(new TreeNodeData("C Programming Tutorial", "ui-icon-data-format1"), node23);
        TreeNode node233 = new DefaultTreeNode(new TreeNodeData("Programming Language", "ui-icon-data-format1"), node23);
        TreeNode node27 = new DefaultTreeNode(new TreeNodeData("Data Mining", "ui-icon-data-format1"), node2);
        TreeNode node28 = new DefaultTreeNode(new TreeNodeData("Dynamic Programming Algorithm", "ui-icon-data-format1"), node2);
        TreeNode node29 = new DefaultTreeNode(new TreeNodeData("Software Engineering", "ui-icon-data-format1"), node2);

        //Informatics
        TreeNode node30 = new DefaultTreeNode(new TreeNodeData("Bioinformatics", "ui-icon-data-format1"), node3);
        TreeNode node33 = new DefaultTreeNode(new TreeNodeData("Cheminformatics", "ui-icon-data-format1"), node3);
        TreeNode node35 = new DefaultTreeNode(new TreeNodeData("eBioinformatics", "ui-icon-data-format1"), node3);
        TreeNode node37 = new DefaultTreeNode(new TreeNodeData("Medical Informatics", "ui-icon-data-format1"), node3);
        TreeNode node39 = new DefaultTreeNode(new TreeNodeData("Viral Bioinformatics", "ui-icon-data-format1"), node3);

        //Mathematics
        TreeNode node50 = new DefaultTreeNode(new TreeNodeData("Applied Mathematics", "ui-icon-data-format1"), node5);
        TreeNode node53 = new DefaultTreeNode(new TreeNodeData("Mathematica", "ui-icon-data-format1"), node5);
        TreeNode node55 = new DefaultTreeNode(new TreeNodeData("Mathematical Modeling", "ui-icon-data-format1"), node5);
        TreeNode node57 = new DefaultTreeNode(new TreeNodeData("Pure Mathematics", "ui-icon-data-format1"), node5);

        //Medicine
        TreeNode node63 = new DefaultTreeNode(new TreeNodeData("Critical Care Medicine", "ui-icon-data-format1"), node6);
        TreeNode node69 = new DefaultTreeNode(new TreeNodeData("Molecular Medicine", "ui-icon-data-format1"), node6);
        TreeNode node695 = new DefaultTreeNode(new TreeNodeData("Personalized Medicine", "ui-icon-data-format1"), node6);
        TreeNode node697 = new DefaultTreeNode(new TreeNodeData("Respiratory Medicine", "ui-icon-data-format1"), node6);
        TreeNode node698 = new DefaultTreeNode(new TreeNodeData("Systems Medicine", "ui-icon-data-format1"), node6);
        TreeNode node694 = new DefaultTreeNode(new TreeNodeData("Veterinary Medicine", "ui-icon-data-format1"), node6);

        //Physics
        TreeNode node80 = new DefaultTreeNode(new TreeNodeData("Biophysics", "ui-icon-data-format1"), node8);

    }

    public TreeNode getRoot() {
        return root;
    }


}
