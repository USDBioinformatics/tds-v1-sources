package edu.usd.bme.btl.tq;

import java.io.Serializable;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class represents a parameter of an analytic tool as described in the
 * BETS analytic tool description
 *
 * @author Carol Lushbough
 */
public class Parameter implements Serializable {

    boolean visible;
    String description;
    String name;
    String validator;
    HashMap<String, String> constraint = new HashMap<String, String>();
    String label;
    boolean required;
    String type;
    String displayParameter;
    String format;

    /**
     * Default constructor
     */
    public Parameter() {

    }

    /**
     * Create a parameter using a JSON string representing a tool parameter
     *
     * @param jsonBETS
     * @throws JSONException
     */
    public Parameter(String jsonBETS) throws JSONException {

        JSONObject obj = new JSONObject(jsonBETS);
        setParameters(obj);
    }

    /**
     * Create a parameter using a JSON Object representing a tool parameter
     *
     * @param jsonBETS
     * @throws JSONException
     */
    public Parameter(JSONObject jsonBETS) throws JSONException {
        setParameters(jsonBETS);
    }

    /**
     * Set this parameter's attributes using a JSON object
     *
     * @param obj - JSON Object representing a parameter
     *
     * @throws JSONException
     */
    public void setParameters(JSONObject obj) throws JSONException {
        this.displayParameter = "";

        try {
            String locName = obj.getString("name");
            setName(locName);
            this.displayParameter += "Name: " + locName;
        } catch (JSONException e) {

        }
        try {
            boolean locVisible = obj.getBoolean("visible");
            setVisible(locVisible);
        } catch (JSONException e) {
            setVisible(false);
        }
        try {
            String locDescription = obj.getString("description");
            setDescription(locDescription);
            this.displayParameter += " Description: " + locDescription;

        } catch (JSONException e) {
            setDescription("");
        }

        try {
            String locValidator = obj.getString("validator");
            setValidator(locValidator);
        } catch (JSONException e) {
            setValidator("");
        }
        try {
            JSONArray jsonConstraints = obj.getJSONArray("constraint");
            setConstraints(jsonConstraints);
        } catch (JSONException e) {
            e.printStackTrace();
            this.constraint = new HashMap();
        }
        try {
            String locLabel = obj.getString("label");
            setLabel(locLabel);
        } catch (JSONException e) {
            setLabel("");
        }
        try {
            boolean locRequired = obj.getBoolean("required");
            setRequired(locRequired);
        } catch (JSONException e) {
            setRequired(false);
        }
        try {
            String locType = obj.getString("type");
            setType(locType);
            this.displayParameter += " Type: " + locType;

        } catch (JSONException e) {

        }
        try {
            String locFormat = obj.getString("format");
            setFormat(locFormat);
            this.displayParameter += " Format: " + locFormat;

        } catch (JSONException e) {

        }
    }

    /**
     *
     * @return visible
     */
    public boolean getVisible() {
        return visible;
    }

    /**
     *
     * @param visible
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    /**
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return validator
     */
    public String getValidator() {
        return validator;
    }

    /**
     *
     * @param validator
     */
    public void setValidator(String validator) {
        this.validator = validator;
    }

    /**
     *
     * @return constraint
     */
    public HashMap getConstraint() {
        return constraint;
    }

    /**
     *
     * @param constraint
     */
    public void setConstraint(HashMap constraint) {
        this.constraint = constraint;
    }

    /**
     * This method takes a JSONArray will a list of JSONObjects representing
     * parameter constraints and puts them in a HashMap as value label pairs.
     *
     * @param jsonList
     * @throws JSONException
     */
    public void setConstraints(JSONArray jsonList) throws JSONException {

        for (int i = 0; i < jsonList.length(); i++) {
            try {
                JSONObject constraint = jsonList.getJSONObject(i);

                String value = constraint.getString("value");
                String label = constraint.getString("label");
                this.constraint.put(label, value);
            } catch (Exception e) {
            }
        }

    }

    /**
     *
     * @return label
     */
    public String getLabel() {
        return label;
    }

    /**
     *
     * @param label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     *
     * @return
     */
    public boolean getRequired() {
        return required;
    }

    /**
     *
     * @param required
     */
    public void setRequired(boolean required) {
        this.required = required;
    }

    /**
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return displayParameter
     */
    public String getDisplayParameter() {
        return displayParameter;
    }

    /**
     * 
     * @param displayParameter 
     */
    public void setDisplayParameter(String displayParameter) {
        this.displayParameter = displayParameter;
    }

    /**
     * 
     * @return format
     */
    public String getFormat() {
        return format;
    }

    /**
     * 
     * @param format 
     */
    public void setFormat(String format) {
        this.format = format;
    }

}
