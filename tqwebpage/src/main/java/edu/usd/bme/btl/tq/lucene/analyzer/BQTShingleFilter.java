/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.tq.lucene.analyzer;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.KeywordAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionLengthAttribute;
import org.apache.lucene.util.AttributeSource;

/**
 * This filter construct shingles (word-based n-grams) of lengths min through max
 * If not specified, default shingle min and max length is 2.
 * @author Menno.VanDiermen
 */
public class BQTShingleFilter extends TokenFilter {
    
    private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
    private final PositionIncrementAttribute posIncrAtt = addAttribute(PositionIncrementAttribute.class);
    private final PositionLengthAttribute posLenAtt = addAttribute(PositionLengthAttribute.class);
    private final KeywordAttribute keywordAtt = addAttribute(KeywordAttribute.class);
    
    private final int min, max;
    private final Deque<String> shingleQueue;
    private final GramBuffer buff;
    private AttributeSource.State current;
    
    /**
     * 
     * @param in
     * @param min
     * @param max
     */
    public BQTShingleFilter(TokenStream in, int min, int max) {
        super(in); this.min = min; this.max = max; 
        buff = new GramBuffer(); shingleQueue = new ArrayDeque<String>();
        termAtt.resizeBuffer(100);
        
        throw new UnsupportedOperationException("NOT SUPPORTED");
    }
    
    public BQTShingleFilter(TokenStream in, int max) {
        this(in, 2, max);
    }
    
    public BQTShingleFilter(TokenStream in) {
        this(in, 2, 2);
    }

    /**
     * NOT CURRENTLY SUPPORTED
     * @return
     * @throws IOException 
     */
    @Override
    public final boolean incrementToken() throws IOException {
        printBuffer();
        if(haveShingle()) return true;
        if(input.incrementToken()) {
            buff.add(new String(termAtt.buffer()).substring(0, termAtt.length()));
            current = captureState();
            buildShingles();
            return true;
        }
        return false;
    }
    
    private boolean haveShingle() {
        if(shingleQueue.size() > 0) {
            String s = shingleQueue.poll();
            restoreState(current);
            this.clearAttributes();
            termAtt.setEmpty();
            termAtt.append(s);
            keywordAtt.setKeyword(false);
            posIncrAtt.setPositionIncrement(0);
            return true;
        }
        return false;
    }
    
    private void buildShingles() {
        List<String> shingles = buff.getShingles();
        for(String s: shingles) shingleQueue.add(s);
    }
    
    public void printBuffer() {
        int h = buff.head;
         String s = "[";
        while(h != buff.tail+1) {
            String tok = buff.buffer[h++];
            s = s + tok +",";
            if(h == max) h = 0;
        }
        s = s+"]"; s = s.replace(",]", "]");
        System.out.println(s);
    }
    
    private class GramToken {
        
        private final AttributeSource.State state;
        private String token;
        
        private GramToken(GramToken gt) {
            this.state = gt.getState();
            this.token = gt.getToken();
        }
        
        private GramToken(AttributeSource.State state, String token) {
            this.state = state;
            this.token = token;
        }

        /**
         * @return the state
         */
        public AttributeSource.State getState() {
            return state;
        }

        /**
         * @return the token
         */
        public String getToken() {
            return token;
        }

        /**
         * @param token the token to set
         */
        public void setToken(String token) {
            this.token = token;
        }
        
        public GramToken copy() {
            return new GramToken(this);
        }
    }
    
    /**
     * This is a ring buffer to contain the tokens to compose n-grams.
     * Head is the oldest token, tail is the newest token.
     */
    private  class GramBuffer {
        
        private String[] buffer;
        private int head = 0, tail = 0, n = 0;
        Set<String> constructed = new TreeSet<String>();
        
        protected GramBuffer() {
            buffer = new String[max];
        }
        
        public void add(String s) {
            if(n==0) {
                buffer[tail] = s;
                n++;
            } else if(n < max) {
                buffer[++tail] = s;
                n++;
            } else {
                tail = head;
                if(head < max-1) head++;
                else head = 0;
                buffer[head] = s;
            }
        }
        
        public void purge() {
            buffer = new String[max];
            head = 0; tail = 0;
            n = 0;
        }
        
        private String[] getSortedBuffer() {
            String[] ret = new String[max];
            int tempHead = head;
            for(int i = 0; i<n; i++) {
                if(tempHead == max-1) tempHead = 0;
                ret[i] = buffer[tempHead++];
            }
            return ret;
        }
        
        public List<String> getShingles() {
            if(n < min) return Collections.EMPTY_LIST;
            List<String> ret = new ArrayList<>();
            for(int i = min; i <= max; i++) {
                if(i>n) break;
                String shin = ""; int idx = tail;
                for(int j = 0; j < i; j++) {
                    if(j>n) break;
                    shin = buffer[idx--] + " " + shin;
                    if(idx < 0) idx = n < max ? n-1 : max-1;
                }
                if(!constructed.contains(shin)) {
                    ret.add(shin); constructed.add(shin);
                }
            }
            return ret;
        }
    }
}
