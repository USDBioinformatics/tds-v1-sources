
package edu.usd.bme.btl.tq.lucene;


import edu.usd.bme.btl.tq.lucene.analyzer.BQTAnalyzer;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.MMapDirectory;
import org.json.JSONObject;
import java.io.File;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.queryparser.complexPhrase.ComplexPhraseQueryParser;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.similarities.ClassicSimilarity;


/**
 *
 * @author Menno.VanDiermen
 */
public class LuceneService {
//    private final static String INDEX_PATH = ""; // change to appropriate index file path
    private static final Logger LOG = Logger.getLogger(LuceneService.class.getName());
    
    private final Directory dir;
    private final IndexReader reader; 
    private final IndexSearcher searcher;
    private final Analyzer queryAnalyzer;//, indexAnalyzer;

    /**
     * Constructor for Lucene indexer.
     * @throws IOException 
     */
    public LuceneService(File path) {
        Path indxP = Paths.get(path.getAbsolutePath());
        try {
            this.dir = new MMapDirectory(indxP);
            this.reader = DirectoryReader.open(dir);
        } catch (IOException e) {
            throw new RuntimeException();
        }
        this.searcher = new IndexSearcher(this.reader);
        this.searcher.setSimilarity(new ClassicSimilarity());
        this.queryAnalyzer = new BQTAnalyzer();
        //this.indexAnalyzer = new SimpleAnalyzer();
    }
    
    /**
     * get the number of documents (tools) that have been indexed
     * 
     * @return number of tools
     */
    public int numberOfDocuments(){
        return reader.numDocs();
    }
    
    private Document createDoc(String bets) {
        JSONObject j = new JSONObject(bets);
        Document doc = new Document();
        doc.add(new StringField("name", j.getString("name"), Field.Store.YES));
        doc.add(new TextField("BETS", bets, Field.Store.YES));
        return doc;
    }

    public String getBets(int id) {
        try {
            IndexReader indxRdr = DirectoryReader.open(this.dir);
            IndexSearcher indxSrchr = new IndexSearcher(indxRdr);
            Query qry = new QueryParser("id", new StandardAnalyzer()).parse(String.valueOf(id));
            TopDocs topDocs = indxSrchr.search(qry, 1);
            JSONObject toolDefJson;
            Document doc;

            if (topDocs.totalHits < 1) {
                return null;
            }

            doc = indxSrchr.doc(topDocs.scoreDocs[0].doc);
            toolDefJson = new JSONObject(doc.get("FULL_TEXT"));
            return toolDefJson.getJSONObject("bets").toString();
        } catch (IOException | ParseException e) {
            return null;
        }
    }

//    public String[] search(String query) throws IOException, ParseException {
//
//        Query q = buildQuery(query);
//        IndexReader reader = DirectoryReader.open(dir);
//
//        IndexSearcher searcher = new IndexSearcher(reader);
//        int hitsPerPage = 10000;
//        TopScoreDocCollector tsdC = TopScoreDocCollector.create(hitsPerPage);
//        searcher.search(q, tsdC);
//        TopDocs docs = tsdC.topDocs(); //searcher.search(q, hitsPerPage);
//        String[] tools = new String[docs.scoreDocs.length]; int i = 0;
//        for(ScoreDoc d: docs.scoreDocs) {
//            Document doc = reader.document(d.doc);
//            tools[i++] = doc.get("BETS");
//        }
//        return tools;
//    }

    public SearchResult search(ToolQuery tlQry, int start, int count) 
            throws IOException, ParseException {
        Query q = buildQuery(tlQry);
        TopScoreDocCollector tsdC = TopScoreDocCollector.create(start + count);
        searcher.search(q, tsdC);

        return new SearchResult(reader, tsdC.topDocs(start, count));
    }

    private Query buildQuery(ToolQuery tlQry) throws ParseException {
        ComplexPhraseQueryParser qp = new ComplexPhraseQueryParser("FULL_TEXT", queryAnalyzer);
        qp.setInOrder(true);
        Query q = qp.parse(tlQry.getQueryString());
//        System.out.println("Query: " + q.toString());
        return q;
    }


//    public List<String> getToolNames() {
//        IndexReader read;
//        try {
//            read = DirectoryReader.open(dir);
//        } catch (IOException ex) {
//            ex.printStackTrace();
//            return new ArrayList<String>();
//        }
//        List<String> names = new ArrayList<String>();
//        int numDocs = read.numDocs();
//        for(int i = 0; i<numDocs; i++) {
//            try {
//                Document doc = read.document(i);
//                names.add(doc.getField("name").stringValue());
//            } catch (IOException ex) {
//                ex.printStackTrace();
//            }
//        }
//        return names;
//    }
}