package edu.usd.bme.btl.tq.jsf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.json.JSONObject;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.TreeNode;
import edu.usd.bme.btl.tq.Tool;
import edu.usd.bme.btl.tq.lucene.ToolQuery;
import edu.usd.bme.btl.tq.validation.QueryValidator;
import java.io.File;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.inject.Inject;
import org.primefaces.component.datalist.DataList;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Menno.VanDiermen
 */
@ManagedBean
@ViewScoped
public class TermQueryBean implements Serializable {

    private final List<String> chosenTerms = new ArrayList<>();
    private final List<String> selections = new ArrayList<>();

    private final List<Tool> searchedTools = new ArrayList<>();
    private TreeNode selectedNode;
    private DomainTree domainTree;
    private MethodTree methodTree;
    private DataFormatTree dataFormatTree;
    private LazyToolDataModel lazyTlMdl;

    boolean hasSelection; //is autocomplet value being used?

    private Tool selectedTool;
    private String currentBETS;
    private String query;
    private String displayQuery;

    private String curTxt;

    boolean renderSearchList; //to view search as a list when true
    boolean renderSearchTable; //to view search as a table when true
    boolean viewList = true; // boolean value for select button: view as table or list

    boolean searchView; // in search view rather than browse view
    boolean browseView = true;

    boolean browsePanelView = true; //render the browse panel when true
    boolean domainView; //render the domain panel when true
    boolean methodView; //render the method panel when true
    boolean dataFormatView; //render the data format panel when true

    @ManagedProperty("#{queryService}")
    private QueryService service;

    @Inject
    private FacesContext fcsCtx;

    @Inject
    private ExternalContext exCtx;

    /**
     * The default constructor initializes the domain, method, and data format
     * Primefaces trees to be displayed in the GUI
     */
    public TermQueryBean() {
        this.domainTree = new DomainTree();
        domainTree.init();
        this.methodTree = new MethodTree();
        methodTree.init();
        this.dataFormatTree = new DataFormatTree();
        dataFormatTree.init();
    }

    @PostConstruct
    private void init() {
        this.lazyTlMdl = new LazyToolDataModel(this.service);
        
        
        /*
        // QUERY VALIDATION CODE. UNCOMMENT TO VALIDATE LOCALLY.
        // REMOVE OR COMMENT FOR PUBLIC-FACING DEPLOYMENT.
        String fNm = "C:\\BIDS Data\\TDS_Test\\queries.txt"; //location of queries file
        File qSet = new File(fNm);                           //see QueryValidator.java for formatting instructions
        QueryValidator qv = new QueryValidator(this);
        try {
            qv.validateQueries(qSet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        /**/
    }

    /**
     * adds selected item to selections list
     *
     * @param e
     */
    public void handleSelect(SelectEvent e) {
        hasSelection = true;

        String term = (String) e.getObject();
        selections.add(term);
        this.curTxt = null;
    }

    /**
     * removes unselected item from selections list
     *
     * @param e
     */
    public void handleUnselect(UnselectEvent e) {
        String term = (String) e.getObject();
        selections.remove(term);
        if (selections.isEmpty()) {
            hasSelection = false;
        }
    }

    /**
     * return list of strings matching current user query for autocomplete
     *
     * @param query
     * @return
     */
    public List<String> completeTerm(String query) {

        this.curTxt = query;
        return this.service.filterSearchTerms(query);
    }

    /**
     * retrieve BETS for the current "selectTool" and stores it the currentBETS
     * attribute
     */
    public void prepareBETS() {
        try {
            this.currentBETS = service.getBETSForTool(selectedTool.getId());
            JSONObject jsonObj = new JSONObject(currentBETS);
            this.currentBETS = jsonObj.toString(2);
        } catch (Exception e) {
            this.fcsCtx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Info", "Unable to retrieve BETS."));
        }
    }

    /**
     * switchView - this method is used to switch the GUI tool list to list view
     * or table view. When this.renderSearchList is true, the GUI displays the
     * tools in a list format. When this.renderSearchTable is true, the tools
     * are displayed in a Primefaces data table
     */
    public void switchView() {

        if (this.viewList) {
            this.renderSearchList = true;
            this.renderSearchTable = false;
            this.viewList = false;
        } else {
            this.renderSearchList = false;
            this.renderSearchTable = true;
            this.viewList = true;
        }

    }

    /**
     * This search method was initiated by the user entering a query in the
     * autocomplete input text box in the GUI. The selections string array list
     * holds the autocomplete selected terms. the query attribute holds any free
     * text entered by the user. These attributes are cleared after the search
     * is complete.
     *
     * @param requestSearchView - used to indicate the search was initiated by
     * values entered in the autocomplete input text box.
     */
    public void search(boolean requestSearchView) {
// TODO rename this method
        Map<String, String[]> mp = this.exCtx.getRequestParameterValuesMap();
        if (query != null) {
            selections.add(query.trim());
        }
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("results-table-form:data");
        dataTable.setFirst(0);
        DataList datalist = (DataList) FacesContext.getCurrentInstance().getViewRoot().findComponent("results-form:dataresultlist");
        datalist.setFirst(0);
        // tools displayed as a list is the default so we set renderSearchList
        // to true
        setRenderSearchList(true);
        this.renderSearchTable = false;
        viewList = true; //button on GUI to indicate Table or list view of resutls
        searchView = true;

        // all other views set to false
        browsePanelView = false; //disable browse panel for search results
        browseView = false;
        domainView = false;
        methodView = false;
        dataFormatView = false;
        this.displayQuery = buildDisplayQuery(this.curTxt, this.selections);
//        search();

    }

    /**
     * set the view flags so that the Browse panel will be displayed
     */
    public void browse() {
        browsePanelView = true; //false for search
        setSearchView(false);
        setBrowseView(true);
        setDomainView(false);
        setMethodView(false);
        setDataFormatView(false);
    }

    /**
     * set the view flags so that the Domain panel will be displayed
     */
    public void domain() {
        browsePanelView = true; //false for search
        setSearchView(false);
        setBrowseView(false);
        setDomainView(true);
        setMethodView(false);
        setDataFormatView(false);
        this.searchedTools.clear();
    }

    /**
     * set the view flags so that the Method panel will be displayed
     */
    public void method() {
        browsePanelView = true; //false for search
        setSearchView(false);
        setBrowseView(false);
        setDomainView(false);
        setMethodView(true);
        setDataFormatView(false);
//        searchedTools = new ArrayList<Tool>();
        this.searchedTools.clear();
    }

    /**
     * set the view flags so that the Data Format panel will be displayed
     */
    public void dataFormat() {
        browsePanelView = true; //false for search
        setSearchView(false);
        setBrowseView(false);
        setDomainView(false);
        setMethodView(false);
        setDataFormatView(true);
    }

    /**
     * A node was selected from a Primefaces tree in the GUI. The
     * NodeSelectEvent contains the TreeNode that was selected. This tree node
     * has the string query that is to be executed (e.g. Computational Biology).
     * The tree node value is the query. Each term in the query is "anded"
     * before being sent to the search method.
     *
     * @param event contains the tree node that was selected.
     */
    public void onNodeSelect(NodeSelectEvent event) {

//        String query = event.getTreeNode().toString();
        // and terms for the search
//        query = query.trim().replaceAll(" ", " && ");
//        search("\"" + query + "\"");

		// We need to reset the page of results we are on each time we click
		// on a new node - bill.
        DataList datalist = (DataList) FacesContext.getCurrentInstance().getViewRoot().findComponent("browse-data-form:data");
		if (datalist != null) {
			// If datalist is null, this is the first time we've browsed so
			// we don't need to set it to the first page.
			datalist.setFirst(0);
		}
		
        String ndQry = event.getTreeNode().toString();
        this.searchedTools.clear();
        this.selections.clear();
        this.chosenTerms.clear();
        this.viewList = true;
        this.selections.add(ndQry);
    }

    /**
     * The chosenTerms contains the list of terms entered or selected by the
     * user in the auto complete input text box.
     *
     * @return the chosenTerms
     */
    public List<String> getChosenTerms() {
        return chosenTerms;
    }

    /**
     * The chosenTerms contains the list of terms entered or selected by the
     * user in the auto complete input text box.
     *
     * @param chosenTerms the chosenTerms to set
     */
    public void setChosenTerms(List<String> chosenTerms) {
        this.chosenTerms.clear();
        if (chosenTerms != null) {
            this.chosenTerms.addAll(chosenTerms);
        }
//        this.chosenTerms = chosenTerms;
    }

    /**
     * When users select a tool from the list or table of tools in the GUI, the
     * selectedTool attribute is set to the tool that the user selected
     *
     * @return the selectedTool
     */
    public Tool getSelectedTool() {
        return selectedTool;
    }

    /**
     * When users select a tool from the list or table of tools in the GUI, the
     * selectedTool attribute is set to the tool that the user selected
     *
     * @param selectedTool the selectedTool to set
     */
    public void setSelectedTool(Tool selectedTool) {
        this.selectedTool = selectedTool;
    }

    /**
     * @return the service
     */
    public QueryService getService() {
        return service;
    }

    /**
     * @param service the service to set
     */
    public void setService(QueryService service) {
        this.service = service;
    }

    /**
     * The searchedTools attribute holds the list of tools that met the search
     * criteria entered by the user. Results are set in the searchTool attribute
     * in the search() method.
     *
     * @return the searchedTools
     */
    public List<Tool> getSearchedTools() {
        return searchedTools;
    }

    /**
     * The searchedTools attribute holds the list of tools that met the search
     * criteria entered by the user. Results are set in the searchTool attribute
     * in the search() method.
     *
     * @param searchedTools the searchedTools to set
     */
    public void setSearchedTools(List<Tool> searchedTools) {
        this.searchedTools.clear();
        if (searchedTools != null) {
            this.searchedTools.addAll(searchedTools);
        }
//        this.searchedTools = searchedTools;
    }

    /**
     * The selections attribute holds the list of terms selected from the auto
     * complete list in the GUI.
     *
     * @return selections
     */
    public List<String> getSelections() {
        return selections;
    }

    /**
     * The selections attribute holds the list of terms selected from the auto
     * complete list in the GUI.
     *
     * @param selections
     */
    public void setSelections(List<String> selections) {
        this.selections.clear();
        if (selections != null) {
            this.selections.addAll(selections);
        }
//        this.selections = selections;
    }

    /**
     * This method returns the JSON string representing the currently selected
     * tool. toString(2) formats the JSON by indicating each new substring
     * should be indented 2 spaces.
     *
     * @return
     */
    public String getCurrentBETS() {
        try {
            return new JSONObject(currentBETS).toString(2);
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * This method returns the JSON string representing the currently selected
     * tool.
     *
     * @param currentBETS
     */
    public void setCurrentBETS(String currentBETS) {
        this.currentBETS = currentBETS;
    }

    /**
     * Should the list of tools be rendered as a list?
     *
     * @return
     */
    public boolean getRenderSearchList() {
        return renderSearchList;
    }

    /**
     * Should the list of tools be rendered as a list?
     *
     * @param renderSearchList
     */
    public void setRenderSearchList(boolean renderSearchList) {

        this.renderSearchList = renderSearchList;
    }

    /**
     * Should the list of tools be rendered as a table?
     *
     * @return
     */
    public boolean getRenderSearchTable() {

        return renderSearchTable;
    }

    /**
     * Should the list of tools be rendered as a table?
     *
     * @param renderSearchTable
     */
    public void setRenderSearchTable(boolean renderSearchTable) {
        this.renderSearchTable = renderSearchTable;
    }

    /**
     * Boolean used to control the selectBooleanButton in the GUI so user can
     * toggle back and forth between list and table view of tool results
     *
     * @return
     */
    public boolean getViewList() {
        return viewList;
    }

    /**
     * Boolean used to control the selectBooleanButton in the GUI so user can
     * toggle back and forth between list and table view of tool results
     *
     * @param viewList
     */
    public void setViewList(boolean viewList) {
        this.viewList = viewList;
    }

    /**
     * Boolean used to indicate is the user is in search or browse mode. Search
     * mode is when user entered values in the search input box. Browse view is
     * when the user is selected values from the browse tree.
     *
     * @return
     */
    public boolean getSearchView() {

        return searchView;
    }

    /**
     * Boolean used to indicate is the user is in search or browse mode. Search
     * mode is when user entered values in the search input box. Browse view is
     * when the user is selected values from the browse tree.
     *
     * @param searchView - user in search mode when true
     */
    public void setSearchView(boolean searchView) {
        this.searchView = searchView;
    }

    /**
     * Boolean used to indicate is the user is in search or browse mode. Search
     * mode is when user entered values in the search input box. Browse view is
     * when the user is selected values from the browse tree.
     *
     * @return browseView - - user in browse mode when true
     */
    public boolean getBrowseView() {
        return browseView;
    }

    /**
     * Boolean used to indicate is the user is in search or browse mode. Search
     * mode is when user entered values in the search input box. Browse view is
     * when the user is selected values from the browse tree.
     *
     * @param browseView - user in browse mode when true
     */
    public void setBrowseView(boolean browseView) {
        this.browseView = browseView;
    }

    /**
     * Domain panel is rendered and displayed in the GUI when domainView is true
     *
     * @return
     */
    public boolean getDomainView() {
        return domainView;
    }

    /**
     * Domain panel is rendered and displayed in the GUI when domainView is true
     *
     * @param domainView
     */
    public void setDomainView(boolean domainView) {
        this.domainView = domainView;
    }

    /**
     * Method panel is rendered and displayed in the GUI when methodView is true
     *
     * @return
     */
    public boolean getMethodView() {
        return methodView;
    }

    /**
     * Method panel is rendered and displayed in the GUI when methodView is true
     *
     * @param methodView
     */
    public void setMethodView(boolean methodView) {
        this.methodView = methodView;
    }

    /**
     * Data format panel is rendered and displayed in the GUI when
     * dataFormatView is true
     *
     * @return
     */
    public boolean getDataFormatView() {
        return dataFormatView;
    }

    /**
     * Data format panel is rendered and displayed in the GUI when
     * dataFormatView is true
     *
     * @param dataFormatView
     */
    public void setDataFormatView(boolean dataFormatView) {
        this.dataFormatView = dataFormatView;
    }

    /**
     * Selected node in a GUI Primefaces tree
     *
     * @return
     */
    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    /**
     * Selected node in a GUI Primefaces tree
     *
     * @param selectedNode
     */
    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    /**
     * Primefaces domain tree object
     *
     * @return
     */
    public DomainTree getDomainTree() {
        return domainTree;
    }

    /**
     * Primefaces domain tree object
     *
     * @param domainTree
     */
    public void setDomainTree(DomainTree domainTree) {
        this.domainTree = domainTree;
    }

    /**
     * Primefaces method tree object
     *
     * @return
     */
    public MethodTree getMethodTree() {
        return methodTree;
    }

    /**
     * Primefaces method tree object
     *
     * @param methodTree
     */
    public void setMethodTree(MethodTree methodTree) {
        this.methodTree = methodTree;
    }

    /**
     * Primefaces data format tree object
     *
     * @return
     */
    public DataFormatTree getDataFormatTree() {
        return dataFormatTree;
    }

    /**
     * Primefaces data format tree object
     *
     * @param dataFormatTree
     */
    public void setDataFormatTree(DataFormatTree dataFormatTree) {
        this.dataFormatTree = dataFormatTree;
    }

    /**
     * Should the browse panel be rendered?
     *
     * @return
     */
    public boolean isBrowsePanelView() {
        return browsePanelView;
    }

    /**
     * Should the browse panel be rendered?
     *
     * @param browsePanelView
     */
    public void setBrowsePanelView(boolean browsePanelView) {
        this.browsePanelView = browsePanelView;
    }

    public LazyToolDataModel getLazyToolResult() {

        ToolQuery tlQ = service.buildToolQuery(this.curTxt, this.selections);
        this.lazyTlMdl.setQuery(tlQ);
        return this.lazyTlMdl;
    }

    /**
     * This methods build a query that can be displayed to the user
     *
     * @param freeQuery
     * @param selectedTerms
     * @return
     */
    private String buildDisplayQuery(String freeQuery, List<String> selectedTerms) {
        StringBuilder sb = new StringBuilder();
        sb.append("Query: ");
        if (selectedTerms != null) {
            for (String trm : selectedTerms) {
                sb
                        .append("\n\t")
                        .append(trm)
                        .append(" ");
            }
        }

        if (freeQuery != null) {
            sb
                    .append("\t")
                    .append(freeQuery);
        }

        String display = sb.toString();
        if (display.length() > 150) {
            display = display.substring(0, 150) + "...";
        }

        return display;
    }

    /**
     * query to be display to users
     *
     * @return
     */
    public String getDisplayQuery() {
        return this.displayQuery;
    }

    public void setDisplayQuery(String displayQuery) {
        this.displayQuery = displayQuery;
    }

    public String getNumberOfTools() {
        int numberOfTools = service.getNumberOfTools();
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
        return numberFormat.format(numberOfTools);

    }
    
    public String[] validateQueries(String[] names, String[] queries) {
        if (names.length != queries.length) {
            System.out.println("Validate failed: Number of names does not match number of queries");
            return null;
        }
        String[] ret = new String[queries.length];
        for (int i = 0; i < queries.length; i++) {
            String name = names[i];
            System.out.println("SEARCHING: " + name);
            this.curTxt = queries[i];
            this.selections.clear();
            getLazyToolResult();
            this.search(true);
            boolean found = false, stop = false;
            int start = 0, pgSz = 100;
            ToolQuery tlQ = this.service.buildToolQuery(curTxt, selections);
            this.lazyTlMdl.setQuery(tlQ);
            //System.out.println("++++++++++++++++++++++++++");
            //System.out.println("Query: " + tlQ.toString());
            int numDocs = this.lazyTlMdl.getRowCount();
            String res = ret[i] = names[i] + "\t " + queries[i] + "\t " + "NOT FOUND" + "\t" + numDocs;
            while (!found) {
                
                if(start > numDocs) break;
                //System.out.println("Results " + start + "-" + (start + pgSz - 1));
                List<Tool> out = lazyTlMdl.load(start, pgSz, null, SortOrder.UNSORTED, null);
                numDocs = lazyTlMdl.getRowCount();
                if (out.isEmpty()) {
                    res = names[i] + "\t " + queries[i] + "\t " + "NOT FOUND" + "\t" + numDocs;
                    break;
                }
                int j = start;
                for (Tool tl : out) {
                    String nm = tl.getName();
                    if (nm.toLowerCase().contains(name.toLowerCase())) {
                        res = names[i] + "\t " + queries[i] + "\t " + (j+1) + "\t" + numDocs;
                        //System.out.println("Entry found: Position " + j);
                        found = true;
                    }
                    j++;
                }
                if(stop) break;
                start += pgSz;
            }
            //System.out.println("++++++++++++++++++++++++++");
            ret[i] = res;
        }
        return ret;
    }
}
