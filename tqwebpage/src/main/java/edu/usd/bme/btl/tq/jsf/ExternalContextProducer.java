
package edu.usd.bme.btl.tq.jsf;

import javax.enterprise.inject.Produces;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;


public class ExternalContextProducer {

    @Produces
    ExternalContext getExternalContext() {
        return FacesContext.getCurrentInstance().getExternalContext();
    }
}
