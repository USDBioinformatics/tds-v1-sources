/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.tq.jsf;

import javax.annotation.PostConstruct;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;


/**
 * This class represents the Domain tree in the GUI. The nodes are hard
 * coded with predefined values. Each node in the tree is represented by 
 * TreeNodeData (i.e. label and icon)
 * 
 * @author Carol Lushbough
 */
public class MethodTree {

    private TreeNode root;

    @PostConstruct
    public void init() {
        root = new DefaultTreeNode("Root", null);
        TreeNode node0 = new DefaultTreeNode(new TreeNodeData("Alignment", "ui-icon-alignment"), root);
        TreeNode node1 = new DefaultTreeNode(new TreeNodeData("Annotation", "ui-icon-annotation"), root);
        TreeNode node2 = new DefaultTreeNode(new TreeNodeData("Assembly", "ui-icon-assembly"), root);
        TreeNode node3 = new DefaultTreeNode(new TreeNodeData("Imaging", "ui-icon-imaging"), root);
        TreeNode node5 = new DefaultTreeNode(new TreeNodeData("Mapping", "ui-icon-mapping"), root);
        TreeNode node7 = new DefaultTreeNode(new TreeNodeData("Pathway Analysis", "ui-icon-pathway"), root);
        TreeNode node6 = new DefaultTreeNode(new TreeNodeData("Sequencing", "ui-icon-sequencing"), root);
        TreeNode node8 = new DefaultTreeNode(new TreeNodeData("Visualization", "ui-icon-visualization"), root);

        //Alignment 
        TreeNode node00 = new DefaultTreeNode(new TreeNodeData("Alignment Analysis", "ui-icon-data-format1"), node0);
        TreeNode node03 = new DefaultTreeNode(new TreeNodeData("Genome Alignment", "ui-icon-data-format1"), node0);
        TreeNode node05 = new DefaultTreeNode(new TreeNodeData("Local Sequence Alignment", "ui-icon-data-format1"), node0);
        TreeNode node07 = new DefaultTreeNode(new TreeNodeData("Multiple Sequence Alignment","ui-icon-my-folder"), node0);

        TreeNode node09 = new DefaultTreeNode(new TreeNodeData("Pairwise Sequence Alignment", "ui-icon-data-format1"), node0);
        TreeNode node011 = new DefaultTreeNode(new TreeNodeData("Pairwise Structure Alignment", "ui-icon-data-format1"), node0);
        TreeNode node013 = new DefaultTreeNode(new TreeNodeData("Protein Alignment", "ui-icon-my-folder"), node0);
        TreeNode node015 = new DefaultTreeNode(new TreeNodeData("RNA-Seq Alignment", "ui-icon-data-format1"), node0);
        TreeNode node017 = new DefaultTreeNode(new TreeNodeData("Secondary Structure Alignment", "ui-icon-data-format1"), node0);
        TreeNode node019 = new DefaultTreeNode(new TreeNodeData("Sequence Alignment", "ui-icon-my-folder"), node0);
        TreeNode node021 = new DefaultTreeNode(new TreeNodeData("Structure Alignment", "ui-icon-data-format1"), node0);

        //Chemistry Nodes
        TreeNode node10 = new DefaultTreeNode(new TreeNodeData("Multiple Protein Sequence Alignment", "ui-icon-data-format1"), node07);

        TreeNode node13 = new DefaultTreeNode(new TreeNodeData("Protein Sequence Alignment", "ui-icon-data-format1"), node013);
        TreeNode node15 = new DefaultTreeNode(new TreeNodeData("Protein Structure Alignment", "ui-icon-data-format1"), node013);

        TreeNode node17 = new DefaultTreeNode(new TreeNodeData("Sequence Alignment Analysis", "ui-icon-data-format1"), node019);
        TreeNode node19 = new DefaultTreeNode(new TreeNodeData("Sequence Alignment Comparison", "ui-icon-data-format1"), node019);
        TreeNode node191 = new DefaultTreeNode(new TreeNodeData("Sequence Alignment Editing", "ui-icon-data-format1"), node019);
        TreeNode node192 = new DefaultTreeNode(new TreeNodeData("Sequence Alignment Visualization", "ui-icon-data-format1"), node019);
        TreeNode node193 = new DefaultTreeNode(new TreeNodeData("Sequence Profile Alignment", "ui-icon-data-format1"), node019);

        //Computer Science
        TreeNode node21 = new DefaultTreeNode(new TreeNodeData("Annotation Editing", "ui-icon-data-format1"), node1);
        TreeNode node23 = new DefaultTreeNode(new TreeNodeData("Annotation Retrieval", "ui-icon-data-format1"), node1);
        TreeNode node231 = new DefaultTreeNode(new TreeNodeData("Annotation Track", "ui-icon-data-format1"), node1);
        TreeNode node233 = new DefaultTreeNode(new TreeNodeData("Annotation Visualization", "ui-icon-data-format1"), node1);
        TreeNode node27 = new DefaultTreeNode(new TreeNodeData("Data Annotation", "ui-icon-data-format1"), node1);
        TreeNode node28 = new DefaultTreeNode(new TreeNodeData("Genome Annotation", "ui-icon-data-format1"), node1);
        TreeNode node29 = new DefaultTreeNode(new TreeNodeData("SNP Annotation", "ui-icon-data-format1"), node1);
        TreeNode node291 = new DefaultTreeNode(new TreeNodeData("Sequence Annotation", "ui-icon-data-format1"), node1);

        //Informatics
        TreeNode node30 = new DefaultTreeNode(new TreeNodeData("De-novo Assembly", "ui-icon-data-format1"), node2);
        TreeNode node37 = new DefaultTreeNode(new TreeNodeData("Genome Assembly", "ui-icon-data-format1"), node2);
        TreeNode node39 = new DefaultTreeNode(new TreeNodeData("Localized Reassembly", "ui-icon-data-format1"), node2);
        TreeNode node40 = new DefaultTreeNode(new TreeNodeData("Mapping Assembly", "ui-icon-data-format1"), node2);
        TreeNode node41 = new DefaultTreeNode(new TreeNodeData("Sequence Assembly", "ui-icon-data-format1"), node2);
        TreeNode node42 = new DefaultTreeNode(new TreeNodeData("Transcriptome Assembly", "ui-icon-data-format1"), node2);

        //Mathematics
        TreeNode node50 = new DefaultTreeNode(new TreeNodeData("Biological Imaging", "ui-icon-data-format1"), node3);
        TreeNode node55 = new DefaultTreeNode(new TreeNodeData("Image", "ui-icon-data-format1"), node3);
        TreeNode node57 = new DefaultTreeNode(new TreeNodeData("Image Analysis", "ui-icon-data-format1"), node3);
        TreeNode node58 = new DefaultTreeNode(new TreeNodeData("Medical Imaging", "ui-icon-data-format1"), node3);

        //Medicine
        TreeNode node63 = new DefaultTreeNode(new TreeNodeData("Epitope Mapping", "ui-icon-data-format1"), node5);
        TreeNode node69 = new DefaultTreeNode(new TreeNodeData("Genetic Mapping", "ui-icon-data-format1"), node5);
        TreeNode node695 = new DefaultTreeNode(new TreeNodeData("Haplotype Mapping", "ui-icon-data-format1"), node5);
        TreeNode node697 = new DefaultTreeNode(new TreeNodeData("Mapping Assembly", "ui-icon-data-format1"), node5);
        TreeNode node698 = new DefaultTreeNode(new TreeNodeData("Ontology Mapping", "ui-icon-data-format1"), node5);
        TreeNode node694 = new DefaultTreeNode(new TreeNodeData("Physical Mapping", "ui-icon-data-format1"), node5);
        TreeNode node6944 = new DefaultTreeNode(new TreeNodeData("Read Mapping", "ui-icon-data-format1"), node5);
        TreeNode node6945 = new DefaultTreeNode(new TreeNodeData("Split Read Mapping", "ui-icon-data-format1"), node5);
        TreeNode node6946 = new DefaultTreeNode(new TreeNodeData("Tools for Mapping High-throughput Sequencing Data", "ui-icon-data-format1"), node5);

        //Physics
        TreeNode node80 = new DefaultTreeNode(new TreeNodeData("Disease Pathways", "ui-icon-data-format1"), node7);
        TreeNode node81 = new DefaultTreeNode(new TreeNodeData("Metabolic Pathways", "ui-icon-data-format1"), node7);
        TreeNode node82 = new DefaultTreeNode(new TreeNodeData("Pathway Tools", "ui-icon-data-format1"), node7);
        TreeNode node83 = new DefaultTreeNode(new TreeNodeData("Signaling Pathways", "ui-icon-data-format1"), node7);

        TreeNode node90 = new DefaultTreeNode(new TreeNodeData("DNA Sequencing", "ui-icon-data-format1"), node6);
        TreeNode node91 = new DefaultTreeNode(new TreeNodeData("De Novo Sequencing", "ui-icon-data-format1"), node6);
        TreeNode node92 = new DefaultTreeNode(new TreeNodeData("Exome Sequencing", "ui-icon-data-format1"), node6);
        TreeNode node93 = new DefaultTreeNode(new TreeNodeData("High-throughput Sequencing", "ui-icon-data-format1"), node6);
        TreeNode node97 = new DefaultTreeNode(new TreeNodeData("Sequencing Quality Control", "ui-icon-data-format1"), node6);
        TreeNode node98 = new DefaultTreeNode(new TreeNodeData("Whole Genome Sequencing", "ui-icon-data-format1"), node6);

        TreeNode node70 = new DefaultTreeNode(new TreeNodeData("Annotation Visualization", "ui-icon-data-format1"), node8);
        TreeNode node71 = new DefaultTreeNode(new TreeNodeData("Data Visualization", "ui-icon-data-format1"), node8);
        TreeNode node72 = new DefaultTreeNode(new TreeNodeData("Genome Visualisation", "ui-icon-data-format1"), node8);
        TreeNode node78 = new DefaultTreeNode(new TreeNodeData("Sequence Alignment Visualisation", "ui-icon-data-format1"), node8);
        TreeNode node75 = new DefaultTreeNode(new TreeNodeData("Sequence Assembly Visualisation", "ui-icon-data-format1"), node8);

    }

    public TreeNode getRoot() {
        return root;
    }

}
