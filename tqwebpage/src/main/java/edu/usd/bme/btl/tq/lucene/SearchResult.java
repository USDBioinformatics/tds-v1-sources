
package edu.usd.bme.btl.tq.lucene;

import java.io.IOException;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.TopDocs;


public class SearchResult {

    private final int ttl;
    private final String[] bets;

    public SearchResult(IndexReader reader, TopDocs td) throws IOException {
        this.ttl = (td.totalHits < 5000) ? td.totalHits : 5000;
        this.bets = new String[td.scoreDocs.length];
        for (int i = 0, l = (td.scoreDocs.length < 5000) ? td.scoreDocs.length : 5000; i < l; i += 1) {
            Document doc = reader.document(td.scoreDocs[i].doc);
            this.bets[i] = doc.get("FULL_TEXT");
        }
    }

    public int getTotal() {
        return ttl;
    }

    public String[] getBets() {
        return bets;
    }
}
