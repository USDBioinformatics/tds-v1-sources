/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.tq.lucene.analyzer;

import java.io.IOException;
import java.util.Arrays;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.KeywordAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionLengthAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;

/**
 *
 * @author Menno.VanDiermen
 */
public class BQTKeywordFilter extends TokenFilter {
    
    private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
    private final PositionIncrementAttribute posIncrAtt = addAttribute(PositionIncrementAttribute.class);
    private final PositionLengthAttribute posLenAtt = addAttribute(PositionLengthAttribute.class);
    private final TypeAttribute typeAtt = addAttribute(TypeAttribute.class);
    private final OffsetAttribute offsetAtt = addAttribute(OffsetAttribute.class);
    private final KeywordAttribute keywordAtt = addAttribute(KeywordAttribute.class);
    
    int tok = 1;
    /**
     * This class marks all original search terms as keywords, so they will not be
     * discarded by filters farther up the analysis chain that preserve keywords.
     * @param in 
     */
    public BQTKeywordFilter(TokenStream in) {
        super(in);
        throw new UnsupportedOperationException("NOT SUPPORTED");
    }

    @Override
    public final boolean incrementToken() throws IOException {
        if(!input.incrementToken()) {
            return false;
        }
        System.out.println("Buffer: " + Arrays.toString(termAtt.buffer()));
        System.out.println("KeyToken #"+tok+++": "+new String(termAtt.buffer()).substring(0, termAtt.length()));
        keywordAtt.setKeyword(true);
        return true;
    }
    
}
