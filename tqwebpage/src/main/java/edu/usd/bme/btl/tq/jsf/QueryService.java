package edu.usd.bme.btl.tq.jsf;

import static edu.usd.bme.bqt.Constants.*;
import edu.usd.bme.btl.tq.lucene.ToolQuery;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import edu.usd.bme.btl.tq.lucene.LuceneService;
import edu.usd.bme.btl.tq.lucene.SearchResult;
import edu.usd.bme.btl.tq.lucene.analyzer.SynonymEngine;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.queryparser.classic.ParseException;

/**
 *
 * @author Menno.VanDiermen
 */
@ManagedBean(name = "queryService", eager = true)
@ApplicationScoped
public class QueryService implements Serializable {

    private static final Logger LOG =
            Logger.getLogger(QueryService.class.getName());

    private static final String BASE_DATA_PATH_PROPERTY = 
            "BIOQUERYTOOL_DATA_DIR";

    private final Map<String, String> searchTerms = new TreeMap<>();
    private LuceneService lucene;
    private QueryPreprocessor qb;

    public QueryService() {}

    @PostConstruct
    private void initialize() {
        String bsDir = System.getProperty(BASE_DATA_PATH_PROPERTY);
        File mapDbPath;
        File lucenePath;

        if (bsDir == null) {
            String msg = "The system property: <"
                    + BASE_DATA_PATH_PROPERTY +"> is not set. Please set the "
                    + "property to the directory containing the Lucene index "
                    + "and the MapDB database for the application";
            LOG.log(Level.SEVERE, msg);
            throw new RuntimeException(msg);
        }

        mapDbPath = new File(new File(bsDir, MAPDB_DIR), MAPDB_NAME);
        lucenePath = new File(bsDir, LUCENE_DIR);
        DB db = DBMaker
                .newFileDB(mapDbPath)
                .readOnly()
                .make();
        Map<String, Set<String>> dictionary = db
                .getTreeMap(DICTIONARY_DB);
        Set<String> ontNames = db
                .getTreeSet(ONTOLOGY_NAMES_DB);
        Set<String> toolNames = db
                .getTreeSet(TOOL_NAMES_DB);

        for (String s : ontNames) {
            this.searchTerms.put(s.toLowerCase(), s);
        }

        for (String s : toolNames) {
            this.searchTerms.put(s.toLowerCase(), s);
        }

        this.lucene = new LuceneService(lucenePath);
        this.qb = new QueryPreprocessor(new TreeMap<>(dictionary));

        db.close();
    }

//    public String[] search(String query) throws IOException, ParseException {
//        return lucene.search(query);
//    }

    public SearchResult search(ToolQuery tlQry, int start, int count)
            throws IOException, ParseException {
        return this.lucene.search(tlQry, start, count);
    }

    /**
     * get the number of tools in the Lucene index
     * 
     * @return number of tools
     */
    public int getNumberOfTools(){
        return lucene.numberOfDocuments();
    }
//    /**
//     * Combine ontologies and toolnames, sorted alphabetically with PQ heapsort
//     */
//    private void loadSearchTerms() {
//        searchTerms = new TreeSet<>();
//        for (String o : ontNames) {
//            searchTerms.add(o);
//        }
//        for (String n : toolNames) {
//            searchTerms.add(n);
//        }
//    }

//    /**
//     * @return the tools
//     */
//    public Set<String> getTools() {
//        return tools;
//    }

//    /**
//     * @return the searchTerms
//     */
//    public Set<String> getSearchTerms() {
//        return Collections.unmodifiableSet(this.searchTerms);
//    }

    public List<String> filterSearchTerms(String prefix) {
        List<String> fltrS = new ArrayList<>();
        prefix = prefix.toLowerCase();

        for (Map.Entry<String, String> mpE : this.searchTerms.entrySet()) {
            if (mpE.getKey().startsWith(prefix)) {
                fltrS.add(mpE.getValue());
            }
        }

        return fltrS;
    }

    /**
     * return bets for tool with id toolId
     * @param toolId
     * @return 
     */
    public String getBETSForTool(int toolId) {
        return this.lucene.getBets(toolId);
    }
    
    public ToolQuery buildToolQuery(String freeText, List<String> selections) {
        return qb.build(freeText, selections);
    }
    
    public int indexSize() {
        return lucene.numberOfDocuments();
    }
    
    public static class QueryPreprocessor {
        
        private SynonymEngine engine;
        
        public QueryPreprocessor(Map<String, Set<String>> dict) {
            engine = new SynonymEngine(dict);
        }
        
        public ToolQuery build(String freeText, List<String> selections) {
            return new ToolQuery(freeText, selections, engine);
        }
    }

}
