package edu.usd.bme.btl.tq.jsf;

/**
 * This class is used to represent a tree node object. The label attribute is
 * also the query value. The icon is the name of the icon that should be
 * displayed in the tree for that node value
 *
 * @author Carol Lushbough
 */
public class TreeNodeData {

    String label;
    String icon;

    /**
     * Create a tree node using the labe and icon passed in as parameters
     *
     * @param label
     * @param icon
     */
    public TreeNodeData(String label, String icon) {
        this.label = label;
        this.icon = icon;
    }

    /**
     *
     * @return label
     */
    public String getLabel() {
        return label;
    }

    /**
     *
     * @param label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * 
     * @return icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * 
     * @param icon 
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * toString is to return the label of the node
     * @return 
     */
    public String toString() {
        return label;
    }

}
