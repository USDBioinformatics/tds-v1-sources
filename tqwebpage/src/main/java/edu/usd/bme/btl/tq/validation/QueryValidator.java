/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.tq.validation;

import edu.usd.bme.btl.tq.jsf.TermQueryBean;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

/**
 *
 * @author Menno.VanDiermen
 */
public class QueryValidator {
    
    TermQueryBean bean;
    
    public QueryValidator(TermQueryBean tqb) {
        bean = tqb;
    }
    
    /**
     * Validates queries in a single text file
     * Name, query, keywords must be tab-separated
     * Queries must take the form -> NAME\tQUERY\tKEYWORDS
     * NAME must be exact match for names of items found in DB
     * @param qSet 
     */
    public void validateQueries(File qSet) throws FileNotFoundException, IOException {
        String dir = qSet.getParent();
        
        File fullAResults = new File(dir+"FullOut.tsv");
        File partialAResults = new File(dir+"PartialOut.tsv");
        
        List<String> names = new ArrayList<>();
        List<String> queries = new ArrayList<>();
        List<String> keywords = new ArrayList<>();
        
        BufferedReader br = new BufferedReader(new FileReader(qSet));
        String line;
        
        while((line = br.readLine()) != null) {
            String[] div = line.split("\\t");
            names.add(div[0]);
            queries.add(div[1]);
            keywords.add(div[2]);
        }
        
        String[] nms = names.toArray(new String[0]);
        String[] qOut = bean.validateQueries(nms, queries.toArray(new String[0]));
        BufferedWriter bFR = new BufferedWriter(new FileWriter(fullAResults));
        bFR.append("NAME\tQUERY\tPOSITION\tRESULTS"); bFR.append(System.lineSeparator());
        for (int i = 0; i < qOut.length; i++) {
            bFR.append(qOut[i]);
            bFR.append(System.lineSeparator());
        }
        bFR.close();
        String[] partOut = bean.validateQueries(nms, keywords.toArray(new String[0]));
        BufferedWriter bPR = new BufferedWriter(new FileWriter(partialAResults));
        bPR.append("NAME\tQUERY\tPOSITION\tRESULTS"); bPR.append(System.lineSeparator());
        for (int i = 0; i < partOut.length; i++) {
            bPR.append(partOut[i]);
            bPR.append(System.lineSeparator());
        }
        bPR.close();
    }
    
    
    /**
     * Function for validating BIDS results based on Openi abstracts
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public void validateOpeniAbstracts() throws FileNotFoundException, IOException {
        File dir = new File("C:\\BIDS Data\\Openi_DB\\samples");
        File[] samples = dir.listFiles();
        
        File fullAResults = new File("C:\\BIDS Data\\Openi_DB\\FullOut.tsv");
        File partialAResults = new File("C:\\BIDS Data\\Openi_DB\\PartialOut.tsv");
        
        int numTests = 200;
        int[] idxs = new int[numTests];
        
        for (int i = 0; i < numTests; i++) {
            idxs[i] = (int) (Math.random() * samples.length);
        }
        
        String[] fullQs = new String[numTests];
        String[] partQs = new String[numTests];
        String[] names = new String[numTests];
        for (int i = 0; i < numTests; i++) {
            File f = samples[idxs[i]];
            String name = f.getName();
            name = name.substring(0, name.lastIndexOf('.'));
            names[i] = name;
            System.out.println("NAME: " + name);
            BufferedReader br = new BufferedReader(new FileReader(f));
            String line; StringBuilder sb = new StringBuilder();
            while((line = br.readLine()) !=  null) {
                sb.append(line);
            }
            br.close();
            JSONObject j = new JSONObject(sb.toString());
            String abstr = j.getString("abstract");
            abstr = abstr.replaceAll("\\<.\\>[<>\\(\\)]", "").replaceAll("\\\\*", "");
            fullQs[i] = abstr;
            String[] split = abstr.split("[.]");
            partQs[i] = split[0];
            if (abstr.isEmpty()) {
                idxs[i] = idxs[i] + 1;
                i = i-1;
            }
        }
        String[] fullOut = bean.validateQueries(names, fullQs);
        BufferedWriter bFR = new BufferedWriter(new FileWriter(fullAResults));
        bFR.append("NAME\tQUERY\tPOSITION\tRESULTS"); bFR.append(System.lineSeparator());
        for (int i = 0; i < fullOut.length; i++) {
            bFR.append(fullOut[i]);
            bFR.append(System.lineSeparator());
        }
        bFR.close();
        String[] partOut = bean.validateQueries(names, partQs);
        BufferedWriter bPR = new BufferedWriter(new FileWriter(partialAResults));
        bPR.append("NAME\tQUERY\tPOSITION\tRESULTS"); bPR.append(System.lineSeparator());
        for (int i = 0; i < partOut.length; i++) {
            bPR.append(partOut[i]);
            bPR.append(System.lineSeparator());
        }
        bPR.close();
    }
}