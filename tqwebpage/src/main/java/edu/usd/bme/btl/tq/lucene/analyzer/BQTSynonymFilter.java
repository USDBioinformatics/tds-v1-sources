/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.tq.lucene.analyzer;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.KeywordAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionLengthAttribute;
import org.apache.lucene.util.AttributeSource;

/**
 *
 * @author Menno.VanDiermen
 */
public class BQTSynonymFilter extends TokenFilter {
    
    private final SynonymEngine engine;
    private Deque<String> synonymQueue;
    private AttributeSource.State current;
    private double synonymBoost;
    private final Set<String> unique;
    
    private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
    private final PositionIncrementAttribute posIncrAtt = addAttribute(PositionIncrementAttribute.class);
    private final PositionLengthAttribute posLenAtt = addAttribute(PositionLengthAttribute.class);
    private final KeywordAttribute keywordAtt = addAttribute(KeywordAttribute.class);
    
    /**
     * Constructor for custom synonym tokenfilter:
     * 
     * @param in input tokenstream
     * @param dictionary synonym dictionary
     * @param boost boost factor (i.e. weight) for synonyms
     */
    public BQTSynonymFilter(TokenStream in, Map<String, Set<String>> dictionary, double boost) {
        super(in); engine = new SynonymEngine(dictionary);
        synonymQueue = new ArrayDeque<String>(); synonymBoost = boost;
        termAtt.resizeBuffer(100); unique = new TreeSet<String>();
        throw new UnsupportedOperationException("NOT SUPPORTED");
    }
    
    @Override
    public final boolean incrementToken() throws IOException {
        if (synonymQueue.size() > 0) {
            restoreState(current);
            this.clearAttributes();
            String app = '"'+synonymQueue.poll().trim()+'"'+'^'+synonymBoost;
            termAtt.setEmpty();
            termAtt.append(app);
            keywordAtt.setKeyword(false);
            posIncrAtt.setPositionIncrement(0);
            return true;
        }
        while(input.incrementToken()) {
            String tok = new String(termAtt.buffer()).substring(0, termAtt.length());
            System.out.println("Token received: "+tok+", Position to last: "+posIncrAtt.getPositionIncrement());
            if ((addAliasesToStack() || keywordAtt.isKeyword()) && !unique.contains(tok)) {
                current = captureState();
                unique.add(tok);
                posIncrAtt.setPositionIncrement(1);
                return true;
            }
        }
        return false;
    }

    private boolean addAliasesToStack() throws IOException {
        Set<String> synonyms = engine.getSynonyms(new String(termAtt.buffer()).substring(0, termAtt.length()));
        if (synonyms == null) {
            return false;
        }
        for (String synonym : synonyms) {
            synonymQueue.add(synonym);
        }
        //System.out.println("Term: " + new String(termAtt.buffer()).substring(0, termAtt.length()) + " Synonyms: " + synonyms.toString());
        return true;
    }
    
    public void setSynonymBoost(double synonymBoost) {
        this.synonymBoost = synonymBoost;
    }       
}