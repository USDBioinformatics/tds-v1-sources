package edu.usd.bme.btl.tq.jsf;

import edu.usd.bme.btl.tq.Tool;
import edu.usd.bme.btl.tq.lucene.SearchResult;
import edu.usd.bme.btl.tq.lucene.ToolQuery;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.lucene.queryparser.classic.ParseException;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

public class LazyToolDataModel extends LazyDataModel<Tool> {

    private final QueryService qrySvc;
    private ToolQuery tlQry;
    private Map<Integer, String> toolRepositories;

    public LazyToolDataModel(QueryService qrySvc) {
        this.qrySvc = qrySvc;
        
        try {
            BTLRESTRepositoryClient client = new BTLRESTRepositoryClient();
            toolRepositories = client.getListOfToolRepositories();
        } catch (Exception e) {
            if (toolRepositories == null) {
                toolRepositories = new HashMap();
            }
        }

    }

    public void setQuery(ToolQuery tlQry) {
        this.tlQry = tlQry;
    }

    @Override
    public List<Tool> load(
            int first,
            int pageSize,
            String sortField,
            SortOrder sortOrder,
            Map<String, Object> filters) {

        if (this.tlQry.isEmpty()) {
            return Collections.EMPTY_LIST;
        }

        try {
            List<Tool> tlL = new ArrayList<>();
            SearchResult rslt = this.qrySvc.search(this.tlQry, first, pageSize);
            super.setRowCount(rslt.getTotal());

            for (String tl : rslt.getBets()) {
                Tool aTool = new Tool(tl);
                aTool.setOriginationRepository(toolRepositories.get(aTool.getId()));
                tlL.add(aTool);
            }

            return tlL;
        } catch (IOException | ParseException e) {
            throw new RuntimeException(e);
        }
    }

}
