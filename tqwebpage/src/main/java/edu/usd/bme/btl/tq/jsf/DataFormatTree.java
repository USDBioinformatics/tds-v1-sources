
package edu.usd.bme.btl.tq.jsf;

import javax.annotation.PostConstruct;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 * This class represents the Data format tree in the GUI. The nodes are hard
 * coded with predefined values. Each node in the tree is represented by 
 * TreeNodeData (i.e. label and icon)
 * 
 * @author Carol Lushbough
 */
public class DataFormatTree {

    private TreeNode root;

    @PostConstruct
    public void init() {
        root = new DefaultTreeNode("Root", null);
        TreeNode node000 = new DefaultTreeNode(new TreeNodeData("Data Format", "ui-icon-data-format"), root);

        TreeNode node1 = new DefaultTreeNode(new TreeNodeData("Alignment Format", "ui-icon-data-format1"), node000);
        TreeNode node0 = new DefaultTreeNode(new TreeNodeData("ASN.1 Sequence Format", "ui-icon-data-format1"), node000);
        TreeNode node2 = new DefaultTreeNode(new TreeNodeData("BAM Format", "ui-icon-data-format1"), node000);
        TreeNode node3 = new DefaultTreeNode(new TreeNodeData("BED Format", "ui-icon-data-format1"), node000);
        TreeNode node5 = new DefaultTreeNode(new TreeNodeData("Binary Format", "ui-icon-data-format1"), node000);
        TreeNode node6 = new DefaultTreeNode(new TreeNodeData("EMBL Format", "ui-icon-data-format1"), node000);
        TreeNode node8 = new DefaultTreeNode(new TreeNodeData("FASTA Format", "ui-icon-data-format1"), node000);

        //Biology Nodes
        TreeNode node00 = new DefaultTreeNode(new TreeNodeData("FASTQ-like Format", "ui-icon-data-format1"), node000);
        TreeNode node03 = new DefaultTreeNode(new TreeNodeData("GenBank Format", "ui-icon-data-format1"), node000);
        TreeNode node05 = new DefaultTreeNode(new TreeNodeData("Graph Format", "ui-icon-data-format1"), node000);
        TreeNode node07 = new DefaultTreeNode(new TreeNodeData("HMMER Format", "ui-icon-data-format1"), node000);
        TreeNode node09 = new DefaultTreeNode(new TreeNodeData("Image Format", "ui-icon-data-format1"), node000);
        TreeNode node011 = new DefaultTreeNode(new TreeNodeData("Map Format", "ui-icon-data-format1"), node000);
        TreeNode node013 = new DefaultTreeNode(new TreeNodeData("Matrix Format", "ui-icon-data-format1"), node000);
        TreeNode node015 = new DefaultTreeNode(new TreeNodeData("Nexus Format", "ui-icon-data-format1"), node000);
        TreeNode node017 = new DefaultTreeNode(new TreeNodeData("OWL Format", "ui-icon-data-format1"), node000);
        TreeNode node019 = new DefaultTreeNode(new TreeNodeData("PHYLIP Format", "ui-icon-data-format1"), node000);
        TreeNode node021 = new DefaultTreeNode(new TreeNodeData("Phylogenetic Tree Format", "ui-icon-data-format1"), node000);
        TreeNode node024 = new DefaultTreeNode(new TreeNodeData("RDF Format", "ui-icon-data-format1"), node000);
        TreeNode node026 = new DefaultTreeNode(new TreeNodeData("SAM Format", "ui-icon-data-format1"), node000);
        node000.setExpanded(true);
    }

    public TreeNode getRoot() {
        return root;
    }

}
