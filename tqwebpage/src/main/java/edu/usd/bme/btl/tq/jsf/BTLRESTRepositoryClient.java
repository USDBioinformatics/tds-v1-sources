package edu.usd.bme.btl.tq.jsf;

import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class represents a client of the BTL-REST API. Specifically the
 * repository endpoints.
 *
 * @author Carol Lushbough
 */
public class BTLRESTRepositoryClient {

    public static final String BASE_URL = "http://jacksons.usd.edu/BTL-REST/resources";

    private WebTarget webTarget;
    private Client client;
    Map<Integer, String> repositories;
    Map<Integer, String> toolRepositories;


    /**
     * Constructor sets up the initial client and repository endpoint
     */
    public BTLRESTRepositoryClient() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URL).path("repos");
    }

    /**
     * Get list of repositories uses the BTL-REST repository endpoint to get the
     * names and ids of the original tool repositories
     * [{"id":1,"name":"SEQanswers","url":null}, ...
     * 
     * @return a HashMap of repositories where the repository id is the key
     * 
     * @throws ClientErrorException
     * @throws JSONException 
     */
    public Map<Integer, String> getListOfRepositories() throws ClientErrorException, JSONException {
        WebTarget resource = webTarget;
        String jsonList = resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(String.class);
        repositories = new HashMap();
        JSONArray jList = new JSONArray(jsonList);
        for (int i = 0; i < jList.length(); i++) {
            JSONObject obj = jList.getJSONObject(i);
            Integer repositoryId = obj.getInt("id");
            String repositoryName = obj.getString("name");
            repositories.put(repositoryId, repositoryName);
        }
        return repositories;
    }

    /**
     * Get list of Tool Repositories uses the BTL-REST repository endpoint to get 
     * the tool ids and their associated repository name.
     * [{"id":1,"toolId":1,"repoId":1},{"id":2,"toolId":2,"repoId":1}, ...
     * 
     * Use repoId and the repositories HashMap to get the name of the repository
     * for the tool
     * 
     * @return HashMap where the key is the tool id and associated data is the
     *         name of the original repository
     * 
     * @throws ClientErrorException
     * @throws JSONException 
     */
    public Map<Integer, String> getListOfToolRepositories() throws ClientErrorException, JSONException {
        if (repositories == null) {
            getListOfRepositories();
        }
        webTarget = client.target(BASE_URL).path("repos/tools");
        WebTarget resource = webTarget;
        String jsonList = resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(String.class);

        toolRepositories = new HashMap();
        JSONArray jList = new JSONArray(jsonList);

        for (int i = 0; i < jList.length(); i++) {
            JSONObject obj = jList.getJSONObject(i);
            Integer toolId = obj.getInt("toolId");
            Integer repositoryId = obj.getInt("repoId");
            String repositoryName = repositories.get(repositoryId);
            toolRepositories.put(toolId, repositoryName);
        }

        return toolRepositories;
    }
}
