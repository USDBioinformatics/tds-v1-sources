/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.tq;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class represents an analytic tool
 *
 * @author Menno.VanDiermen
 */
public class Tool implements Comparable, Serializable {


    // EDIT BELOW FIELDS TO ADD MORE ATTRIBUTES SHOWN IN SEARCH ENGINE
    private String name, summary, jstring;
    private int id, citations;
    private List<Parameter> inputs = new ArrayList<Parameter>();
    private List<Parameter> parameters = new ArrayList<Parameter>();
    private List<Ontology> terms;
    private int matchedTerms;
    private String description;
    private String author;
    List<String> links = new ArrayList<String>();
    private String version;
    String originationRepository;
    List<String> methods = new ArrayList<String>();
    List<String> domains = new ArrayList<String>();
    HashMap<String, String> applications = new HashMap<String, String>();
    List<String> applicationList = new ArrayList<String>();

    /**
     * Constructor that sets the name, summary and json string of a tool
     *
     * @param nm (name)
     * @param sum (summary)
     * @param jstr (JSON string)
     */
    public Tool(String nm, String sum, String jstr) {
        name = nm;
        summary = sum;
        jstring = jstr;
    }

    /**
     * Constructor that sets the tool attributes using the JSON BETS string
     *
     * @param toolDefJson representing the BETS specification for the tool
     *
     * @throws JSONException
     */
    public Tool(String toolDefJson) throws JSONException {
        JSONObject jsonTool = null;
        //       this.jstring = toolDefJson;
        this.jstring = "";
        JSONObject mainObj = new JSONObject(toolDefJson);
        jsonTool = mainObj.getJSONObject("bets");

        this.name = jsonTool.getString("name");
        try {
            int idstr = mainObj.getInt("id");
            this.id = idstr;
        } catch (JSONException e) {
            this.id = 0;
        }
        try {
            this.description = jsonTool.getString("description");
        } catch (JSONException e) {
            this.description = "";
        }

        try {
            this.summary = jsonTool.getString("summary");
        } catch (JSONException e) {
            this.summary = "No Summary";
        }
        try {
            this.version = jsonTool.getString("version");
        } catch (JSONException e) {
            this.version = "Not provided";
        }
        try {
            JSONArray jCit = jsonTool.getJSONArray("cited");
            this.setCitations((int) jCit.get(0));
        } catch (JSONException e) {
            this.setCitations(0);
        }

        try {
            JSONArray jsonInputs = jsonTool.getJSONArray("inputs");
            for (int i = 0; i < jsonInputs.length(); i++) {
                JSONObject obj = jsonInputs.getJSONObject(i);
                Parameter param = new Parameter(obj);
                inputs.add(param);
            }
        } catch (JSONException e) {
            this.setInputs(new ArrayList<Parameter>());
        }
        try {
            JSONArray params = jsonTool.getJSONArray("parameters");
            for (int i = 0; i < params.length(); i++) {
                JSONObject obj = params.getJSONObject(i);
                Parameter param = new Parameter(obj);
                parameters.add(param);
            }
        } catch (JSONException e) {
            this.setParameters(new ArrayList<Parameter>());
        }
        try {
            this.links = new ArrayList<String>();
            JSONArray links = jsonTool.getJSONArray("links");
            this.setLinks(links);
        } catch (JSONException e) {

        }
        try {
            this.author = jsonTool.getString("author");
        } catch (JSONException e) {
            this.author = "Author not specified";
        }

        try {
            this.domains = getDomains(jsonTool);
        } catch (JSONException e) {
            this.domains = new ArrayList<String>();
        }
        try {
            this.methods = getMethods(jsonTool);
        } catch (JSONException e) {
            this.methods = new ArrayList<String>();
        }
        try {
            this.applications = getApplications(jsonTool);
            this.applicationList = new ArrayList<String>(this.applications.keySet());
        } catch (JSONException e) {
            this.applications = new HashMap<String, String>();
        }
    }

    /**
     * longSummary: returns true if the summary is longer that 350 characters,
     * false otherwise.
     *
     * @return
     */
    public boolean longSummary() {
        return summary.length() > 350;
    }

    /**
     * getSubSummary: if the summary is longer that 350 characters, return the
     * first 350 characters.
     *
     * @return
     */
    public String getSubSummary() {
        if (longSummary()) {
            return summary.substring(0, 350);
        }
        return summary;
    }

    /**
     * @return the name
     */
    public String getName() {
        if (this.name == null) {
            return null;
        }
        return name.toLowerCase();
    }

    /**
     * @return the summary
     */
    public String getSummary() {
        return summary;
    }

    /**
     * @return the jstring
     */
    public String getJstring() {
        return jstring;
    }

    public void setJstring(String json) {
        this.jstring = json;
    }

    /**
     * @return the matchedTerms
     */
    public int getMatchedTerms() {
        return matchedTerms;
    }

    /**
     * @param matchedTerms the matchedTerms to set
     */
    public void setMatchedTerms(int matchedTerms) {
        this.matchedTerms = matchedTerms;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the citations
     */
    public int getCitations() {
        return citations;
    }

    /**
     * @param citations the citations to set
     */
    public void setCitations(int citations) {
        this.citations = citations;
    }

    /**
     * @return the terms
     */
    public List<Ontology> getTerms() {
        return terms;
    }

    /**
     * @return the inputs
     */
    public List<Parameter> getInputs() {
        return inputs;
    }

    /**
     * @param inputs the inputs to set
     */
    public void setInputs(List<Parameter> inputs) {
        this.inputs = inputs;
    }

    /**
     * @return the parameters
     */
    public List<Parameter> getParameters() {
        return parameters;
    }

    /**
     * @param parameters the parameters to set
     */
    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    /**
     * @param terms the terms to set
     */
    public void setTerms(List<Ontology> terms) {
        this.terms = terms;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     */
    public String getAuthor() {
        return author;
    }

    /**
     *
     * @param author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     *
     * @return
     */
    public List<String> getLinks() {
        return links;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getOriginationRepository() {
        return originationRepository;
    }

    public void setOriginationRepository(String originationRepository) {
        this.originationRepository = originationRepository;
    }

    public List<String> getMethods() {
        return methods;
    }

    public void setMethods(List<String> methods) {
        this.methods = methods;
    }

    public List<String> getDomains() {
        return domains;
    }

    public void setDomains(List<String> domains) {
        this.domains = domains;
    }

    public List<String> getApplicationList() {
        return applicationList;
    }

    public void setApplicationList(List<String> applicationList) {
        this.applicationList = applicationList;
    }

    public HashMap<String, String> getApplications() {
        return applications;
    }

    public void setApplications(HashMap<String, String> applications) {
        this.applications = applications;
    }

    /**
     * setLinks: receives a JSON array representis a list of link JSON Objects.
     * For each link object, get the url and place it in the links list.
     *
     * @param jsonLinks
     * @throws JSONException
     */
    public void setLinks(JSONArray jsonLinks) throws JSONException {
        this.links = new ArrayList<String>();
        for (int i = 0; i < jsonLinks.length(); i++) {
            JSONObject obj = jsonLinks.getJSONObject(i);
            String aLink = obj.getString("url");
            try {
                new java.net.URL(aLink);
                this.links.add(aLink);
            } catch (Exception e) {
                // don't add to list if not a valid link
            }
        }
    }

    private List<String> getDomains(JSONObject jsonTool) throws JSONException {

        List<String> locDomains = new ArrayList<String>();
        JSONObject category = jsonTool.getJSONObject("category");

        JSONArray jsonDomains = category.getJSONArray("domain");
        for (int i = 0; i < jsonDomains.length(); i++) {
            try {
                JSONObject obj = jsonDomains.getJSONObject(i);
                locDomains.add(obj.getString("name"));
            } catch (Exception e) {
            }
        }
        return locDomains;
    }

    private List<String> getMethods(JSONObject jsonTool) throws JSONException {

        List<String> locDomains = new ArrayList<String>();
        JSONObject category = jsonTool.getJSONObject("category");

        JSONArray jsonDomains = category.getJSONArray("method");
        for (int i = 0; i < jsonDomains.length(); i++) {
            JSONObject obj = jsonDomains.getJSONObject(i);
            locDomains.add(obj.getString("name"));
        }
        return locDomains;
    }

    private HashMap<String, String> getApplications(JSONObject jsonTool) throws JSONException {

        HashMap<String, String> locApplication = new HashMap<String, String>();
        JSONObject category = jsonTool.getJSONObject("category");

        JSONArray jsonAppliations = category.getJSONArray("application");
        for (int i = 0; i < jsonAppliations.length(); i++) {

            JSONObject obj = jsonAppliations.getJSONObject(i);
            String name = obj.getString("Name");
            String description;
            try {
                description = obj.getString("definitions");
            } catch (JSONException e) {
                description = "";
            }
            locApplication.put(name, description);
        }
        return locApplication;
    }

    @Override
    public int compareTo(Object o
    ) {
        if (o == null) {
            throw new NullPointerException("Null Parameter");
        } else if (!(o instanceof Tool)) {
            throw new ClassCastException("Possible ClassLoader Issue");
        }
        Tool t = (Tool) o;
        if (this.getMatchedTerms() == t.getMatchedTerms()) {
            return this.getName().compareTo(t.getName());
        }
        return t.getMatchedTerms() - this.getMatchedTerms();
    }
}
