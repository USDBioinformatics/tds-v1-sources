/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.tq.lucene.analyzer;

import java.util.Map;
import java.util.Set;

/**
 *
 * @author Menno.VanDiermen
 */
public class SynonymEngine  {
    
    private final Map<String, Set<String>> synonymDictionary;
    
    public SynonymEngine(Map<String, Set<String>> dictionary) {
        synonymDictionary = dictionary;
    }
    
    public Set<String> getSynonyms(String term) {
        return synonymDictionary.get(term);
    }
    
    public boolean hasSynoyms(String term) {
        if(synonymDictionary.get(term) == null) {
            return false;
        } else return !synonymDictionary.get(term).isEmpty();
    }
}