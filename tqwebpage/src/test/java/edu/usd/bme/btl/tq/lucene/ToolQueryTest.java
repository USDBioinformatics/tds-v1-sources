
package edu.usd.bme.btl.tq.lucene;

import edu.usd.bme.bqt.RayKinsella;
import edu.usd.bme.btl.tq.jsf.QueryService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;


public class ToolQueryTest {

    private QueryBuilder qb;
    private QueryService.QueryPreprocessor qp;
    
    public ToolQueryTest() {
    }

    @Before
    public void setUp() {
        RayKinsella ray = new RayKinsella();
        this.qb = new QueryBuilder();
        this.qp = new QueryService.QueryPreprocessor(ray.getDictionary());
    }
    
    @Test
    public void testQP() {
        String free = "statistics \"gene resource\" genbank questionable dog food";
        List<String> list = new ArrayList<>();
        list.add("functional mapping");
        list.add("pathway");
        list.add("plant");
        ToolQuery tlQry = qp.build(free, list);
        System.out.println("");
        assertTrue(tlQry.getQueryString().length() > free.length());
    }
    
    @Test
    public void testEmptyQuery() {
        ToolQuery cut = this.qp.build(null, null);
        assertThat(cut, isEmptyQuery());

        cut = this.qp.build(null, Collections.EMPTY_LIST);
        assertThat(cut, isEmptyQuery());

        cut = this.qp.build("", Collections.EMPTY_LIST);
        assertThat(cut, isEmptyQuery());

        cut = this.qp.build("", asList(""));
        assertThat(cut, isEmptyQuery());
    }

    @Test
    public void testSingleFreeTerm() {
        String trm = "Ego";
        Query exp = this.qb
                .startGroup()
                .term(trm)
                .endGroup()
                .build();
        this.testQueryMatch(trm, exp);
    }

    @Test
    public void testMultipleFreeTerms() {
        String trm = "Gin...and smoke...and lies";
        Query exp = this.qb
                .startGroup()
                .term(trm)
                .endGroup()
                .build()
                ;
        this.testQueryMatch(trm, exp);
    }
    
    @Test
    public void testQuotedFreeTerm() {
        String trm = "dcba \"ab cd\" dcba";
        Query exp = this.qb
                .startGroup()
                .term("dcba")
                .space()
                .quotedTerm("ab cd")
                .space()
                .term("dcba")
                .endGroup()
                .build()
                ;
        this.testQueryMatch(trm, exp);
    }

    @Test
    public void testUnmatchedInlineQuoteFreeTerm() {
        String trm = "ab cd   \"efg hi jk lmnop";
        Query exp = this.qb
                .startGroup()
                .term("ab")
                .space()
                .term("cd")
                .space()
                .term("\\\"efg")
                .space()
                .term("hi")
                .space()
                .term("jk")
                .space()
                .term("lmnop")
                .endGroup()
                .build()
                ;
        this.testQueryMatch(trm, exp);
    }

    @Test
    public void testUnmatchedPrefixQuoteFreeTerm() {
        String trm = "\"dcba ABCD";
        Query exp = this.qb
                .startGroup()
                .term("\\\"dcba")
                .space()
                .term("ABCD")
                .endGroup()
                .build()
                ;
        this.testQueryMatch(trm, exp);
    }

    @Test
    public void testUnmatchedSuffixQuoteFreeTerm() {
        String trm = "dcba ABCD\"";
        Query exp = this.qb
                .startGroup()
                .term("dcba")
                .space()
                .term("ABCD\\\"")
                .endGroup()
                .build()
                ;
        this.testQueryMatch(trm, exp);
    }
    
    @Test
    public void testQuoteInlineFreeTerm() {
        String trm = "ab\"cd";
        Query exp = this.qb
                .startGroup()
                .term("ab\\\"cd")
                .endGroup()
                .build()
                ;
        this.testQueryMatch(trm, exp);
    }

    @Test
    public void testQuotePrefixFreeTerm() {
        String trm = "\"dcba";
        Query exp = this.qb
                .startGroup()
                .term("\\\"dcba")
                .endGroup()
                .build()
                ;
        this.testQueryMatch(trm, exp);
    }

    @Test
    public void testQuoteSuffixFreeTerm() {
        String trm = "dcba\"";
        Query exp = this.qb
                .startGroup()
                .term("dcba\\\"")
                .endGroup()
                .build()
                ;
        this.testQueryMatch(trm, exp);
    }

    @Test
    public void testForEscapedFreeTerm() {
        String trm = "\" + - && || ! ( ) { } [ ] ^ \" ~ * ? : \\";
        Query exp = this.qb
                .startGroup()
                .term("\\\" \\+ \\- \\&\\& \\|\\| \\! \\( \\) \\{ \\} \\[ \\] \\^ \\\" \\~ \\* \\? \\: \\\\")
                .endGroup()
                .build()
                ;
        this.testQueryMatch(trm, exp);
    }
    
    @Test
    public void testWhiteSpaceInFreeQuery() {
        String trm = "   \ta  b\n\r\nc  \t  d    \n";
        Query exp = this.qb
                .startGroup()
                .term("a")
                .space()
                .term("b")
                .space()
                .term("c")
                .space()
                .term("d")
                .endGroup()
                .build()
                ;
        this.testQueryMatch(trm, exp);
    }

    @Test
    public void testSingleSelectedTerm() {
        String trm = "dog food";
        Query exp = this.qb
                .startGroup()
                .quotedTerm(trm)
                .endGroup()
                .build();
        List<String> trmL = asList(trm);
        this.testQueryMatch(trmL, exp);
    }

    @Test
    public void testMultipleSelectedTerms() {
        Query exp = this.qb
                .startGroup()
                .quotedTerm("dog food")
                .and()
                .quotedTerm("cats")
                .and()
                .quotedTerm("global thermonuclear war")
                .endGroup()
                .build()
                ;

        List<String> trmL = asList(
                "dog food",
                "cats",
                "global thermonuclear war");
        this.testQueryMatch(trmL, exp);
    }

    @Test
    public void testForEscapedSelectedTerm() {
        Query exp = this.qb
                .startGroup()
                .quotedTerm("\\\" \\+ \\- \\&\\& \\|\\| \\! \\( \\) \\{ \\} \\[ \\] \\^ \\\" \\~ \\* \\? \\: \\\\")
                .endGroup()
                .build()
                ;
        String trm = "\" + - && || ! ( ) { } [ ] ^ \" ~ * ? : \\";
        List<String> trmL = asList(trm);
        this.testQueryMatch(trmL, exp);
    }

    @Test
    public void testMultiTermQuery() {
        String slctdTrm = "dog food";
        List<String> slctL = asList(slctdTrm);
        String freeTrm = "libertine";
        Query exp = this.qb
                .startGroup()
                .quotedTerm(slctdTrm)
                .endGroup()
                .and()
                .startGroup()
                .term(freeTrm)
                .endGroup()
                .build()
                ;
        this.testQueryMatch(freeTrm, slctL, exp);
    }
    
    private void testQueryMatch(List<String> selectedTerms, Query exp) {
        this.testQueryMatch(null, selectedTerms, exp);
    }
    private void testQueryMatch(String freeText, Query exp) {
        this.testQueryMatch(freeText, null, exp);
    }
    
    private void testQueryMatch(String freeText, List<String> selectedTerms, Query exp) {
        ToolQuery cut = this.qp.build(freeText, selectedTerms);
        assertThat(cut, queryIs(exp));
    }
    private static TypeSafeMatcher<ToolQuery> isEmptyQuery() {
        return new TypeSafeMatcher<ToolQuery>() {
            ToolQuery cut;
            @Override
            public boolean matchesSafely(ToolQuery act) {
                cut = act;
                return act.getQueryString().isEmpty();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(cut.getQueryString());
            }
            
        };
    }

    private static List<String> asList(String ... s) {
        return Arrays.asList(s);
    }

    private static TypeSafeMatcher<ToolQuery> queryIs(final Query exp) {
        return new TypeSafeMatcher<ToolQuery>(){
            @Override
            protected boolean matchesSafely(ToolQuery cut) {
                return cut.getQueryString().equals(exp.toString());
            }

            @Override
            protected void describeMismatchSafely(
                    ToolQuery cut,
                    Description mismatchDescription) {
                mismatchDescription
                        .appendText("was -> \"")
                        .appendValue(cut)
                        .appendText("\"");
                        ;
            }

            @Override
            public void describeTo(Description description) {
                description
                        .appendText("query -> \"")
                        .appendValue(exp)
                        .appendText("\"");
                        ;
            }
            
        };
    }

    private class Query {
        private final String qry;

        private Query(String qry) {
            this.qry = qry;
        }

        @Override
        public String toString() {
            return this.qry;
        }
    }

    private class QueryBuilder {
        private static final String AND = "AND";
        private static final char LP = '(';
        private static final char RP = ')';
        private static final char SP = ' ';
        private static final char QT = '\"';
 
        private final StringBuilder sb = new StringBuilder();

        private QueryBuilder() {}

        private QueryBuilder startGroup() {
            this.sb.append(LP).append(SP);
            return this;
        }
   
        private QueryBuilder endGroup() {
            this.sb.append(SP).append(RP);
            return this;
        }

        private QueryBuilder and() {
            this.sb
                    .append(SP)
                    .append(AND)
                    .append(SP)
                    ;
            return this;
        }
 
        private QueryBuilder term(String trm) {
            this.sb.append(trm);
            return this;
        }

        private QueryBuilder space() {
            this.sb.append(SP);
            return this;
        }

        private QueryBuilder quotedTerm(String trm) {
            this.sb
                    .append(QT)
                    .append(trm)
                    .append(QT);
            return this;
        }

        private void clear() {
            this.sb.delete(0, this.sb.length());
        }

        private Query build() {
            return new Query(this.sb.toString());
        }

        @Override
        public String toString() {
            return this.sb.toString();
        }
    }
}
