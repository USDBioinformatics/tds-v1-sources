/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.tq.lucene;

import edu.usd.bme.bqt.RayKinsella;
import edu.usd.bme.btl.tq.jsf.QueryService;
import edu.usd.bme.btl.tq.lucene.analyzer.BQTAnalyzer;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.complexPhrase.ComplexPhraseQueryParser;
import org.apache.lucene.search.Query;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Menno.VanDiermen
 */
public class QueryParseTest {
    
    ComplexPhraseQueryParser qp;
    QueryService.QueryPreprocessor qpp;
    
    public QueryParseTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        RayKinsella ray = new RayKinsella();
		if(!ray.dbExists()) {
			RayKinsella.main(new String[] {});
		}
        qp = new ComplexPhraseQueryParser("FULL_TEXT", new BQTAnalyzer());
        qp.setInOrder(true);
        qpp = new QueryService.QueryPreprocessor(ray.getDictionary());
		}
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testBasicBoost() throws ParseException {
        String test = "statistics\\^1.5 calculation\\^1.2", scope = "FULL_TEXT"; 
        System.out.println("TEST QUERY: " + test);
        System.out.println("---------------------------------------------------------------------------");
        Query q = new ComplexPhraseQueryParser(scope, new SimpleAnalyzer()).parse(test);
        System.out.println("Full Basic Query: " + q.toString());
        System.out.println("---------------------------------------------------------------------------");
        assertTrue(q.toString().length() >= test.length());
    }
    
    
    @Test
    public void testSingleTerm() throws ParseException {
        String test = "statistics", scope = "FULL_TEXT"; 
        System.out.println("TEST QUERY: " + test);
        System.out.println("---------------------------------------------------------------------------");
        Query q = qp.parse(qpp.build(test, null).getQueryString());
        System.out.println("Full Single Query: " + q.toString(scope));
        System.out.println("---------------------------------------------------------------------------");
        assertTrue(q.toString(scope).length() >= test.length());
    }
    
    @Test
    public void testMultiTerm() throws ParseException {
        String test = "statistical calculation", scope = "FULL_TEXT"; 
        System.out.println("TEST QUERY: " + test);
        System.out.println("---------------------------------------------------------------------------");
        Query q = qp.parse(qpp.build(test, null).getQueryString());
        System.out.println("Full MultiTerm Query: " + q.toString(scope));
        System.out.println("---------------------------------------------------------------------------");
        assertTrue(q.toString(scope).length() >= test.length());
    }
    
    @Test
    public void testMultiPhrase() throws ParseException {
        String test = "\"nucleic acid folding\" statistics", scope = "FULL_TEXT"; 
        System.out.println("TEST QUERY: " + test);
        System.out.println("---------------------------------------------------------------------------");
        Query q = qp.parse(qpp.build(test, null).getQueryString());
        System.out.println("Full MultiPhrase Query: " + q.toString());
        System.out.println("---------------------------------------------------------------------------");
        assertTrue(q.toString(scope).length() >= test.length());
    }
    
    @Test
    public void test5Gram() throws ParseException {
        String test = "nucleic acid sequence alignment analysis statistics", scope = "FULL_TEXT"; 
        System.out.println("TEST QUERY: " + test);
        System.out.println("---------------------------------------------------------------------------");
        Query q = qp.parse(qpp.build(test, null).getQueryString());
        System.out.println("Full 5-Gram Query: " + q.toString());
        System.out.println("---------------------------------------------------------------------------");
        assertTrue(q.toString(scope).length() >= test.length());
    }
    
    @Test
    public void testNoValueTerms() throws ParseException {
        String test = "questionable dog food", scope = "FULL_TEXT"; 
        System.out.println("TEST QUERY: " + test);
        System.out.println("---------------------------------------------------------------------------");
        Query q = qp.parse(qpp.build(test, null).getQueryString());
        System.out.println("Full No-Value Query: " + q.toString(scope));
        System.out.println("---------------------------------------------------------------------------");
        assertTrue(q.toString(scope).length() >= test.length()); // should not discard original search terms
    }
    
    @Test
    public void testBoostedTerm() throws ParseException {
        String test = "statistics^1.5 calculation^1.2", scope = "FULL_TEXT"; 
        System.out.println("TEST QUERY: " + test);
        System.out.println("---------------------------------------------------------------------------");
        Query q = qp.parse(qpp.build(test, null).getQueryString());
        System.out.println("Full Boosted Query: " + q.toString(scope));
        System.out.println("---------------------------------------------------------------------------");
        assertTrue(q.toString().length() >= test.length());
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
