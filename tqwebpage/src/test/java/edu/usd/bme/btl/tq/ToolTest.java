/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.tq;

import edu.usd.bme.btl.tq.Parameter;
import edu.usd.bme.btl.tq.Tool;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.HashMap;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author clushbou
 */
public class ToolTest {

    private static final String toolsUrl = "http://jacksons.usd.edu/BTL-REST/resources/tools",
            BETSUrl = "http://jacksons.usd.edu/BTL-REST/resources/bets",
            resourceUrl = "http://jacksons.usd.edu/BTL-REST/resources/repos/getbytoolids";

    String result;
    int idForDomains = 9;
    int idForMethods = 9;
    int idForApplications = 14908;
    int idForParameters = 4664;
    int idForApplication = 5322;

    public ToolTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testToolParameters() throws Exception {
        connect(idForParameters);

        Tool tool = new Tool(result);
        List<Parameter> parameters = tool.getParameters();
        System.out.println("Number of parameters = " + parameters.size());
//        assertTrue(parameters.size() == 7);
    }

    @Test
    public void testToolDomain() throws Exception {
        connect(idForDomains);
        System.out.println("Results: " + result);
        Tool tool = new Tool(result);
        List<String> domains = tool.getDomains();
        System.out.println("Number of domains = " + domains);
        System.out.println("Name of tool = "+tool.getName());
 //       assertTrue(domains.size() == 4);
    }

    @Test
    public void testToolMethod() throws Exception {
        connect(idForMethods);
        System.out.println("Results: " + result);
        Tool tool = new Tool(result);
        List<String> methods = tool.getMethods();
        System.out.println("Number of methods = " + methods.size());
 //       assertTrue(methods.size() == 11);
    }
    @Test
    public void testToolApplication() throws Exception {
        connect(idForApplication);
        System.out.println("Results: " + result);
        Tool tool = new Tool(result);
        HashMap<String,String> applications = tool.getApplications();
        String description = applications.get("Variant Analysis");
        System.out.println("Number of applications = " + description);
//        assertTrue(description.contains("Variant Analysis"));
    }
    
    public void connect(int idToTest) throws Exception {
        result = "";

        URL u = new URL(toolsUrl + "/" + idToTest);
        HttpURLConnection conn = (HttpURLConnection) u.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result += line;
        }
        rd.close();
        System.out.println(result);
        JSONObject obj = new JSONObject(result);
        obj.put("id", 10);
        result = obj.toString();
    }
}
