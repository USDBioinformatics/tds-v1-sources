package seqdatafromjson;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * The SeqDataFromJson class reads three json files (one with a list of all
 * tools from SEQanswers, one list of all references from SEQanswers, and one
 * with a list of all links from SEQanswers). It combines the three lists into
 * one array of JsonObjects and writes each JsonObject to its own file in the
 * specified output folder.
 *
 * @author Shayla Gustafson
 */
public class SeqDataFromJson {

    /**
     * Main method to run all operations
     *
     * @param args
     */
    public static void main(String[] args) {
        //Use the following link to download this file -> change path to wherever you save it
        // http://seqanswers.com/wiki/Special:Ask/-5B-5BCategory:Bioinformatics-20application-5D-5D/-3F%3D-20Name-23/-3FSoftware-20version/-3FBiological-20technology/-3FNumber-20of-20citations/-3FPM-20page-20counter/-3FInstitute/-3FEmail-20address/-3FCreated-20by/-3FInput-20format/-3FOutput-20format/-3FMaintained/-3FLibrary/-3FSoftware-20summary%3DSummary/-3FBiological-20domain%3DBio-20Tags/-3FBioinformatics-20method%3DMeth-20Tags/-3FSoftware-20feature%3DFeatures/-3FLanguage/-3FLicence/-3FOperating-20system%3DOS/mainlabel%3DName/limit%3D2000/searchlabel%3D/format%3Djson
        String toolsFile = "Inputs/SEQTools.json";
        //Use the following link to download this file -> change path to wherever you save it
        // http://seqanswers.com/wiki/Special:Ask/-5B-5BCategory:Reference-5D-5D/-3F%3D-20Name-23/-3FAuthor/-3FYear/-3FJournal/-3FTitle/-3FReference-20describes/mainlabel%3DName/limit%3D2000/searchlabel%3D/format%3Djson
        String referencesFile = "Inputs/SEQReferences.json";
        //Use the following link to download this file -> change path to wherever you save it
        // http://seqanswers.com/wiki/Special:Ask/-5B-5BCategory:URL-5D-5D/-3F%3D-20Name-23/-3FURL/-3FURL-20type/-3FURL-20describes/mainlabel%3DName/limit%3D2000/searchlabel%3D/format%3Djson
        String linksFile = "Inputs/SEQLinks.json";
        //Use the following link to download a list of all tools and their long description -> long description is not provided in toolsFile
        // http://seqanswers.com/w/index.php?title=Special:Export  -> 'Bioinformatics application' in the Category box -> Add -> Export (make sure 'Include only the current revision, not the full history' and 'Save file' are checked)
        String descriptionFile = "Inputs/SEQDescriptions.xml";
        //Directory to output the individual files to
        String outputFolderDir = "Outputs";
        SeqDataFromJson instance = new SeqDataFromJson();
        //Get an array of JSONObject from each of the input files 
        ArrayList<JSONObject> allTools = instance.createToolsArray(toolsFile);
        ArrayList<JSONObject> allReferences = instance.createReferenceArray(referencesFile);
        ArrayList<JSONObject> allLinks = instance.createLinkArray(linksFile);
        ArrayList<Description> allDesc = instance.createDescriptionsArray(descriptionFile);
        //Assign the references/links to the tools they belong to
        allTools = instance.combineLists(allTools, allReferences, allLinks, allDesc);
        //Output files to directory
        instance.createDirectory(outputFolderDir);
        for (int i = 0; i < allTools.size(); i++) {
            instance.outputJsonFile(allTools.get(i).toString(), outputFolderDir + "\\" + i + ".json");
        }
        System.out.println("Seq Data Read Complete: " + allTools.size() + " tools, "
                + allReferences.size() + " references, and " + allLinks.size() + " links.");
    }

    /**
     * Default constructor for SeqDataFromJson object
     */
    public SeqDataFromJson() {
    }

    /**
     * Create an ArrayList of JSONObjects from the Tools json file
     *
     * @param toolsFile
     * @return ArrayList<JSONObject> of all tools in JSON file
     */
    private ArrayList<JSONObject> createToolsArray(String toolsFile) {
        JSONArray tools = null;
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader(toolsFile));
            JSONObject jsonObject = (JSONObject) obj;

            tools = (JSONArray) jsonObject.get("items");

        } catch (FileNotFoundException e) {
            System.out.println("Tools json file not found! " + e.getMessage());
        } catch (IOException e) {
            System.out.println("IOException: " + e.getMessage());
        } catch (ParseException ex) {
            System.out.println("ParseException: " + ex.getMessage());
        }
        return tools;
    }

    /**
     * Create an ArryList of JSONObjects from the References json file
     *
     * @param referencesFile
     * @return ArrayList of JSONObjects of all references in JSON file
     */
    private ArrayList<JSONObject> createReferenceArray(String referencesFile) {
        JSONArray references = null;
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader(referencesFile));
            JSONObject jsonObject = (JSONObject) obj;

            references = (JSONArray) jsonObject.get("items");
        } catch (FileNotFoundException e) {
            System.out.println("Reference json file not found! " + e.getMessage());
        } catch (IOException e) {
            System.out.println("IOException: " + e.getMessage());
        } catch (ParseException ex) {
            System.out.println("ParseException: " + ex.getMessage());
        }
        return references;
    }

    /**
     * Create an ArryList of JSONObjects from the Links json file
     *
     * @param linksFile
     * @return ArrayList of JSONObjects of all links in JSON file
     */
    private ArrayList<JSONObject> createLinkArray(String linksFile) {
        JSONArray links = null;
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader(linksFile));
            JSONObject jsonObject = (JSONObject) obj;

            links = (JSONArray) jsonObject.get("items");

        } catch (FileNotFoundException e) {
            System.out.println("Links json file not found! " + e.getMessage());
        } catch (IOException e) {
            System.out.println("IOException: " + e.getMessage());
        } catch (ParseException ex) {
            System.out.println("ParseException: " + ex.getMessage());
        }
        return links;
    }

    /**
     * Create an ArryList of JSONObjects from the Links json file
     *
     * @param linksFile
     * @return ArrayList of JSONObjects of all links in JSON file
     */
    private ArrayList<Description> createDescriptionsArray(String descriptionsFile) {
        File descFile = new File(descriptionsFile);
        ArrayList<Description> allDescriptions = new ArrayList<>();
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(descFile);

            //optional, but recommended
            //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("page");
            System.out.println("NodeList = " + nList.getLength());
            Element eElement;
            Description desc;
            for (int i = 0; i < nList.getLength(); i++) {
                desc = new Description();
                eElement = (Element) nList.item(i);
                String title = eElement.getElementsByTagName("title").item(0).getTextContent();
                desc.setTitle(title);
                String text = eElement.getElementsByTagName("text").item(0).getTextContent();
                int begin = text.indexOf("}}") + 2;
                if (begin > 0) {
                    text = text.substring(begin);
                }
                text = text.replace("{{Links}}", "").replace("{{References}}", "").replace("{{Link box}}", "");
                desc.setText(text);

                allDescriptions.add(desc);
            }
        } catch (FileNotFoundException e) {
            System.out.println("Description json file not found! " + e.getMessage());
        } catch (IOException e) {
            System.out.println("IOException: " + e.getMessage());
        } catch (SAXException ex) {
            System.out.println("SAXException: " + ex.getMessage());
        } catch (ParserConfigurationException ex) {
            System.out.println("ParserConfigurationException: " + ex.getMessage());
        }
        return allDescriptions;
    }

    /**
     * Loops through all JSONObjects in the references and links arrays and
     * assigns them to the appropriate tool in the tools JSONObject array
     *
     * @param tools
     * @param references
     * @param links
     * @return ArrayList of JSONObjects of all the tools with the
     * links/references assigned
     */
    public ArrayList<JSONObject> combineLists(ArrayList<JSONObject> tools, ArrayList<JSONObject> references, ArrayList<JSONObject> links, ArrayList<Description> descs) {
        String toolName;
        //Loop through all links
        for (int i = 0; i < links.size(); i++) {
            try {
                toolName = links.get(i).get("url_describes").toString();
                for (int j = 0; j < tools.size(); j++) {
                    if (("[\"" + tools.get(j).get("label").toString().toLowerCase() + "\"]").equals(toolName.toLowerCase())) {
                        //This link describes this tool 
                        ArrayList<JSONObject> list = null;
                        list = (ArrayList<JSONObject>) (tools.get(j).get("links"));
                        if (list == null) {
                            list = new ArrayList<>();
                        }
                        list.add(links.get(i));
                        tools.get(j).put("links", list);
                    }
                }
            } catch (Exception e) {
                System.out.println("Link Problem: " + i);
            }
        }
        //Loop through all references
        for (int i = 0; i < references.size(); i++) {
            try {
                toolName = references.get(i).get("reference_describes").toString();
                for (int j = 0; j < tools.size(); j++) {
                    if (("[\"" + tools.get(j).get("label").toString().toLowerCase() + "\"]").equals(toolName.toLowerCase())) {
                        //This reference describes this tool 
                        ArrayList<JSONObject> list = null;
                        list = (ArrayList<JSONObject>) (tools.get(j).get("references"));
                        if (list == null) {
                            list = new ArrayList<>();
                        }
                        list.add(references.get(i));
                        tools.get(j).put("references", list);
                    }
                }
            } catch (Exception e) {
                System.out.println(i + "Reference Problem: " + e);
            }
        }
        //Loop through all descriptions
        for (int i = 0; i < descs.size(); i++) {
            try {
                toolName = descs.get(i).getTitle();
                for (int j = 0; j < tools.size(); j++) {
                    if ((tools.get(j).get("label").toString().toLowerCase()).equals(toolName.toLowerCase())) {
                        //This description describes this tool 
                  //      JSONObject textObject = (JSONObject) tools.get(j).get("references");
                        tools.get(j).put("description", descs.get(i).getText());
                    }
                }
            } catch (Exception e) {
                System.out.println(i + "Description Problem: " + e);
            }
        }
        return tools;
    }

    /**
     * Create the output directory if it doesn't already exist
     *
     * @param directoryName
     */
    public void createDirectory(String directoryName) {
        File file = new File(directoryName);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
    }

    /**
     * Write the information to a file with the path specified
     *
     * @param outputContext
     * @param outputFile
     */
    public void outputJsonFile(String outputContext, String outputFile) {
        FileWriter writer;
        try {
            writer = new FileWriter(outputFile);
            writer.write(outputContext);
            writer.close();
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }

    public class Description {

        private String title;
        private String text;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

    }
}
