# SeqDataFromJson
Purpose
----------------------------------
SeqDataFromJson takes a series of JSON files from SEQanswers and converts them into individual JSON files for each of the Bioinformatics tools in the SEQanswers repository. 

How it works
--------------------------------
SeqDataFromJson takes in four files as input. These files can be found at the URL's specified in the code. The application breaks down the JSON array of tools into individual tools and appends the properties from the other 3 files to each of the tools as necessary. Once the JSON object is complete with all information about each tool, it saves the data into a JSON file in the output folder. 

