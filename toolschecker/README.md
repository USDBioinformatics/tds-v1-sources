# ToolsChecker
Purpose
--------
ToolsChecker checks the validity of tools 4 repositories (iPlant, BioLinksDirectory, Galaxy, and SEQanswers) and saves the valid tools (in XML or JSON format) into specified output folders. 

How it works
----------
The ToolsChecker takes in a folder of JSON/XML files from each of the four repositories (iPlant, BioLinksDirectory, Galaxy, and SEQanswers). It checks to make sure that each file will be able to successfully convert to a BETSV1 object using the BetsConverter. It outputs 4 'success' folders and up to 4 'error' folders, one for each repository. Error files should be hand checked to find possible fixes. The JSON/XML files in the 'success' folders are ready to be converted and added to the Bioinformatics Tools Library database. 

Inputs
--------
Use BioLinksDir, iPlantAppsSplitter, GalaxyToolCreator, and SeqDataFromJson to create the input folders.
