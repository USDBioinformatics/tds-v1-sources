package toolschecker;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.FileUtils;
import org.usd.edu.btl.betsconverter.BETSV1.BETSV1;
import org.usd.edu.btl.betsconverter.BLDV1.BLDV1;
import org.usd.edu.btl.betsconverter.GalaxyV1.Tool;
import org.usd.edu.btl.betsconverter.OMICToolsV1.OMICToolV1;
import org.usd.edu.btl.betsconverter.SeqV1.SeqV1;
import org.usd.edu.btl.betsconverter.iPlantV1.IplantV1;
import org.usd.edu.btl.converters.BLDConverter;
import org.usd.edu.btl.converters.GalaxyConverter;
import org.usd.edu.btl.converters.IplantConverter;
import org.usd.edu.btl.converters.OMICToolConverter;
import org.usd.edu.btl.converters.SeqConverter;
import org.w3c.dom.Document;

/**
 * The ToolsChecker is to be used after retrieving json/xml files for the tools
 * from each of the sources (iPlant, Bioinformatics Link Directory, Seq, and
 * Galaxy) and before trying to add the files as tools in the Database for the
 * BioToolsLibrary. DOES NOT ADD THE TOOLS TO THE DATABASE! ONLY ENSURES THAT
 * FILES ARE VALID AND CAN BE CONVERTED!
 *
 * The ToolsChecker reads in all the files in the directories from each tool
 * source (iPlant, Bioinformatics Link Directory, Seq, and Galaxy), determines
 * whether the file will be a valid input for the BETS Converter, and creates 8
 * new directories (a 'success' and an 'error' for each of the sources)
 * containing the files according.
 *
 * @author Shayla.Gustafson
 */
public class ToolsChecker {

    static final String IPLANT_INPUT = "C:\\Users\\Shayla.Gustafson\\Documents\\Bioinformatics\\ToolsChecker_Inputs\\IPlant_Outputs\\outputs/";
    static final String BLD_INPUT = "C:\\Users\\Shayla.Gustafson\\Documents\\Bioinformatics\\ToolsChecker_Inputs\\BioLinksDir_Outputs/";
    static final String SEQ_INPUT = "C:\\Users\\Shayla.Gustafson\\Documents\\Bioinformatics\\ToolsChecker_Inputs\\SEQData_Outputs2/";
//    static final String SEQ_INPUT = "C:\\Users\\Shayla.Gustafson\\Documents\\Bioinformatics\\ToolsChecker_Inputs\\SEQData_Outputs/";
//    static final String GALAXY_INPUT = "C:\\Users\\Shayla.Gustafson\\Documents\\Bioinformatics\\ToolsChecker_Inputs\\Galaxy_Outputs\\galaxy-tools-xml/";
    static final String GALAXY_INPUT = "C:\\Users\\Shayla.Gustafson\\Documents\\Bioinformatics\\ToolsChecker_Inputs\\Galaxy_Outputs/";
    static final String OMIC_INPUT = "C:\\Users\\Shayla.Gustafson\\Documents\\Bioinformatics\\ToolsChecker_Inputs\\Omic_Outputs/";
    static final String FILE_OUTPUT_ROOT = "C:\\Users\\Shayla.Gustafson\\Desktop\\";
    static ArrayList<File> allGalaxyXMLFiles;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        checkIplantFiles();
        checkBldFiles();
        checkSeqFiles();
        checkGalaxyFiles();
        checkOmicFiles();
    }

    //----------------------------------------------------------------------------------
    //--------------TESTING IPLANT TOOLS------------------------------------------------
    //----------------------------------------------------------------------------------
    /**
     * Reads in iPlant files from folder specified in getIplantFiles() and
     * checks to see if the files will map properly to BETS using the BETS
     * converter. If file is valid, copies file to good file directory. If file
     * will produce errors, copies to bad file directory.
     */
    public static void checkIplantFiles() {
        File[] inputFiles = getIplantFiles(); //get JSON files from iplant folder 
        System.out.println("Number of iPlant files = " + inputFiles.length);
        int ct = 0; //track number of good files
        int errors = 0; //track number of error files 
        for (File x : inputFiles) {
            if (iplantToBETS(x)) {
                //copy to good ouput directory - include 'ct' in the name to avoid overwriting files in directory 
                File dest = new File(FILE_OUTPUT_ROOT + "iPlant_Success\\" + ct + "_" + x.getName());
                try {
                    FileUtils.copyFile(x, dest);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ct++;
            } else {
                //copy to bad output directory - include 'ct' in the name to avoid overwriting files in directory 
                //iPlant_Errors directory is not created in cases of no error files 
                File dest = new File(FILE_OUTPUT_ROOT + "iPlant_Errors\\" + errors + "_" + x.getName());
                try {
                    FileUtils.copyFile(x, dest);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                errors++;
            }
        }
    }

    /**
     * iplantToBets takes an iplant JSON file converts it to bets. Returns true
     * if no exceptions were thrown in the process (will be valid file for BETS
     * converter).
     *
     * @param iplantJSON File
     * @return boolean
     */
    public static boolean iplantToBETS(File iplantJSON) {
        String betsJSON;
        BETSV1 bets = null;
        ObjectMapper mapper = new ObjectMapper(); //create new Jackson Mapper
        IplantV1 ipTool = null;
        try {
            //map input json files to iplant class
            ipTool = mapper.readValue(iplantJSON, IplantV1.class);
            bets = IplantConverter.toBETS(ipTool); //pass the iplant tool spec, convert to bets
            ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
            betsJSON = ow.writeValueAsString(bets); //write Json as String

            //Make sure none of the following is null because that could throw a null pointer exception when writing to the DB
            if (ipTool == null | bets == null) {
                throw new IOException("Either ipTool or bets is null.");
            }
            //These attributes are used in writing tool to database
            if (bets.getName() == null | bets.getName().trim().length() == 0) {
                throw new IOException("Name is null. ");
            }
            if (ow.writeValueAsString(bets).getBytes() == null) {
                throw new IOException("ow.writeValueAsString(bets).getBytes() is null.");
            }
            //No exceptions were thrown - return true;
            return true;
        } catch (IOException e) {
            System.out.println("====================Error with file: " + iplantJSON.getName() + "============");
            System.out.println(e.getMessage());
            //An exception was thrown - return false
            return false;
        }
    }

    /**
     * getIplantFiles creates an array of files from the folder specified as
     * rootPath. RootPath folder comes from the iPlantAppsSplitter.
     *
     * @return File[]
     */
    public static File[] getIplantFiles() {
        String rootPath = IPLANT_INPUT;
        File f = new File(rootPath);
        File[] files = f.listFiles();
        System.out.println("---iPlant files have been created!---");
        return files;
    }

    //----------------------------------------------------------------------------------
    //--------------TESTING BIOINFORMATICS LINK DIRECTORY TOOLS-------------------------
    //----------------------------------------------------------------------------------
    /**
     * Reads in Bioinformatics Links Directory files from folder specified in
     * getBLDFiles() and checks to see if the files will map properly to BETS
     * using the BETS converter. If file is valid, copies file to good file
     * directory. If file will produce errors, copies to bad file directory.
     */
    public static void checkBldFiles() {
        File[] inputFiles = getBldFiles();
        System.out.println("Number of Bioinformatics Link Directory files = " + inputFiles.length);
        int ct = 0; //track number of good files
        int errors = 0; //track number of error files 
        for (File x : inputFiles) {
            if (bldToBETS(x)) {
                //copy to good ouput directory - include 'ct' in the name to avoid overwriting files in directory 
                File dest = new File(FILE_OUTPUT_ROOT + "BLD_Success\\" + ct + "_" + x.getName());
                try {
                    FileUtils.copyFile(x, dest);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ct++;
            } else {
                //copy to bad output directory - include 'errors' in the name to avoid overwriting files in directory 
                //BLD_Errors directory is not created in cases of no error files 
                File dest = new File(FILE_OUTPUT_ROOT + "BLD_Errors\\" + errors + "_" + x.getName());
                try {
                    FileUtils.copyFile(x, dest);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                errors++;
            }
        }
    }

    /**
     * bldToBETS takes a BLD JSON File and creates a BETS object. Returns true
     * if no exceptions were thrown in the process (will be valid file for BETS
     * converter).
     *
     * @param bldJSON File
     * @return boolean
     */
    public static boolean bldToBETS(File bldJSON) {
        String betsJSON;
        BETSV1 bets = null;
        ObjectMapper mapper = new ObjectMapper(); //create new Jackson Mapper
        BLDV1 bldTool;
        try {
            //map input json files to bld class
            bldTool = mapper.readValue(bldJSON, BLDV1.class);
            bets = BLDConverter.toBETS(bldTool); //convert the bld tool
            bets.setName(bets.getDisplay_name());
            bets.setSummary(bets.getDescription());
            ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
            betsJSON = ow.writeValueAsString(bets); //write Json as String

            //Check to make sure that all necessary fields are not null
            if (bldTool == null | bets == null) {
                throw new IOException("Either bldTool or bets is null.");
            }
            //These attributes are used in writing tool to the database
            if (bets.getName() == null | bets.getName().trim().length() == 0) {
                throw new IOException("Name is null. ");
            }
            if (ow.writeValueAsString(bets).getBytes() == null) {
                throw new IOException("ow.writeValueAsString(bets).getBytes() is null.");
            }
            //No exceptions were thrown
            return true;
        } catch (IOException e) {
            System.out.println("====================Error with file: " + bldJSON.getName() + "============");
            System.out.println(e.getMessage());
            //An exception was thrown, so not a valid file 
            return false;
        }
    }

    /**
     * getBldFiles creates an array of files from the directory specified as
     * rootPath. RootPath directory comes from the BioLinksDir project
     *
     * @return File[]
     */
    public static File[] getBldFiles() {
        String rootPath = BLD_INPUT;
        File f = new File(rootPath);
        File[] files = f.listFiles();
        System.out.println("---bld files have been created!---");
        return files;
    }

    //----------------------------------------------------------------------------------
    //-----------------TESTING SEQ TOOLS------------------------------------------------
    //----------------------------------------------------------------------------------
    /**
     * Reads in Seq files from folder specified in getSeqFiles() and checks to
     * see if the files will map properly to BETS using the BETS converter. If
     * file is valid, copies file to good file directory. If file will produce
     * errors, copies to bad file directory.
     */
    public static void checkSeqFiles() {
        File[] inputFiles = getSeqFiles();
        System.out.println("Number of Seq files = " + inputFiles.length);
        int ct = 0; //track number of good files
        int errors = 0; //track number of error files 
        for (File x : inputFiles) {
            if (seqToBETS(x)) {
                //copy to good ouput directory - include 'ct' in the name to avoid overwriting files in directory 
                File dest = new File(FILE_OUTPUT_ROOT + "Seq_Success\\" + ct + "_" + x.getName());
                try {
                    FileUtils.copyFile(x, dest);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ct++;
            } else {
                //copy to bad output directory - include 'errors' in the name to avoid overwriting files in directory 
                //Seq_Errors directory is not created in cases of no error files 
                File dest = new File(FILE_OUTPUT_ROOT + "Seq_Errors\\" + errors + "_" + x.getName());
                try {
                    FileUtils.copyFile(x, dest);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                errors++;
            }
        }
    }

    /**
     * seqToBETS takes a seq JSON File and creates a BETS object. Returns true
     * if no exceptions were thrown in the process (will be valid file for BETS
     * converter).
     *
     * @param seqJSON File
     */
    public static boolean seqToBETS(File seqJSON) {
        String betsJSON;
        BETSV1 bets = null;
        ObjectMapper mapper = new ObjectMapper(); //create new Jackson Mapper
        SeqV1 seqTool;
        try {
            //map seq json files to seq class
            seqTool = mapper.readValue(seqJSON, SeqV1.class);
            bets = SeqConverter.toBETS(seqTool); //convert the seq tool
            ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
            betsJSON = ow.writeValueAsString(bets); //write Json as String

            //Check to make sure that all necessary fields are not null
            if (seqTool == null | bets == null) {
                throw new IOException("Either seqTool or bets is null.");
            }
            if (bets.getName() == null | bets.getName().trim().length() == 0) {
                throw new IOException("Name is null. ");
            }
            //No exceptions were thrown
            return true;
        } catch (IOException e) {
            System.out.println("====================Error with file: " + seqJSON.getName() + "============");
            System.out.println(e.getMessage());
            //An exception was thrown
            return false;
        }
    }

    /**
     * getSeqFiles creates an array of files from directory specified as
     * rootPath. RootPath directory comes from the SEQData project
     *
     * @return File[]
     */
    public static File[] getSeqFiles() {
        String rootPath = SEQ_INPUT;
        File f = new File(rootPath);
        File[] files = f.listFiles();
        System.out.println("---Seq files have been created!---");
        return files;
    }

    //----------------------------------------------------------------------------------
    //--------------TESTING GALAXY TOOLS------------------------------------------------
    //----------------------------------------------------------------------------------
    /**
     * Reads in Galaxy xml files from folder specified in getGalaxyFiles() and
     * checks to see if the files will map properly to BETS using the BETS
     * converter. If file is valid, copies file to good file directory. If file
     * will produce errors, copies to bad file directory.
     */
    public static void checkGalaxyFiles() {
        allGalaxyXMLFiles = new ArrayList<>();
        getGalaxyFiles();
        System.out.println("GalaxyFile = " + allGalaxyXMLFiles.size());
        int ct = 0; //track number of good files
        int errors = 0; //track number of error files 
        for (File x : allGalaxyXMLFiles) {
            if (galaxyToBETS(x)) {
                //copy to bad output directory - include 'ct' in the name to avoid overwriting files in directory 
                File dest = new File(FILE_OUTPUT_ROOT + "Galaxy_Success\\" + ct + "_" + x.getName());
                try {
                    FileUtils.copyFile(x, dest);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ct++;
            } else {
                //copy to bad output directory - include 'errors' in the name to avoid overwriting files in directory 
                File dest = new File(FILE_OUTPUT_ROOT + "Galaxy_Errors\\" + errors + "_" + x.getName());
                try {
                    FileUtils.copyFile(x, dest);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                errors++;
            }

        }
    }

    /**
     * galaxyToBETS takes a galaxy XML file and creates a BETS object. Returns
     * true if no Exceptions were thrown in the process (will be valid file for
     * BETS converter).
     *
     * @param galaxyXML File
     * @return boolean
     */
    public static boolean galaxyToBETS(File galaxyXML) {
        String betsJSON;
        BETSV1 bets = null;
        ObjectMapper mapper = new ObjectMapper(); //create new Jackson Mapper
        Tool galaxyTool; //Galaxy main class is stupidly named 'Tool'
        InputStream infile = null;
        try {
            //map input json files to galaxy class
            JAXBContext jaxbContext = JAXBContext.newInstance(Tool.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller(); //Unmarshalling – Conversion XML content into a Java Object.
            infile = new FileInputStream(galaxyXML);
            galaxyTool = (Tool) unmarshaller.unmarshal(infile);
            bets = GalaxyConverter.toBETS(galaxyTool);
            ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
            betsJSON = ow.writeValueAsString(bets); //write Json as String

            //Check to make sure that all necessary fields are not null
            if (galaxyTool == null | bets == null) {
                throw new IOException("Either galaxyTool or bets is null.");
            }
            if (bets.getName() == null | bets.getName().trim().length() == 0) {
                throw new IOException("Name is null. ");
            }
            String version = bets.getVersion();
            String summary = bets.getSummary();
        } catch (Exception e) {
            System.out.println("====================Error with file: " + galaxyXML.getName() + "============");
            System.out.println(e.getMessage());
            //An exception was thrown
            return false;
        } finally {
            try {
                infile.close();
            } catch (Exception e) {
                System.out.println("Terrible Things Afoot");
                //An exception was thrown 
                return false;
            }
        }
        //No exceptions were thrown
        return true;
    }

    /**
     * getGalaxyFiles creates an array of files from the directory specified as
     * rootPath. RootPath comes from the GalaxyLinkCreator project (directory
     * called 'clone'). Directory created by GalaxyLinkCreator will contain many
     * subfolders and files that are not Tools. getGalaxyFile will remove all
     * sub-directories and return only the xml files that need to be passes to
     * the BETS converter.
     *
     * @return File[]
     */
    public static File[] getGalaxyFiles() {
        String rootPath = GALAXY_INPUT;
        File f = new File(rootPath);
        File[] files = f.listFiles();
        for (File x : files) {
            getFilesFromFolder(x);
        }
        System.out.println("---galaxy files have been created!---");
        return files;
    }

    /**
     * Recursive method to get only the valid XML Files (no Directories or other
     * entities), adding them to ArrayList called allGalaxyXMLFiles.
     *
     * @param file
     */
    public static void getFilesFromFolder(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File x : files) {
                getFilesFromFolder(x);
            }
        } else {
            if (checkValidGalaxyFormat(file)) {
                allGalaxyXMLFiles.add(file);
            }
        }
    }

    /**
     * Checks that inputFile is a Galaxy 'tool' with a 'name' and 'id'.
     *
     * @param inputFile
     * @return boolean
     */
    public static boolean checkValidGalaxyFormat(File inputFile) {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
        }
        Document doc;
        try {
            doc = dBuilder.parse(inputFile);
            if (!doc.getDocumentElement().getNodeName().equals("tool")) {
                return false;
            } else {

                if (doc.getElementsByTagName("name") == null) {
                    return false;
                }
                if (doc.getElementsByTagName("id") == null) {
                    return false;
                }
            }
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    //----------------------------------------------------------------------------------
    //-----------------TESTING OMIC TOOLS------------------------------------------------
    //----------------------------------------------------------------------------------
    /**
     * Reads in OMIC files from folder specified in getOmicFiles() and checks to
     * see if the files will map properly to BETS using the BETS converter. If
     * file is valid, copies file to good file directory. If file will produce
     * errors, copies to bad file directory.
     */
    public static void checkOmicFiles() {
        File[] inputFiles = getOmicFiles();
        System.out.println("Number of Omic files = " + inputFiles.length);
        int ct = 0; //track number of good files
        int errors = 0; //track number of error files 
        for (File x : inputFiles) {
            if (omicToBETS(x)) {
                //copy to good ouput directory - include 'ct' in the name to avoid overwriting files in directory 
                File dest = new File(FILE_OUTPUT_ROOT + "Omic_Success\\" + ct + "_" + x.getName());
                try {
                    FileUtils.copyFile(x, dest);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ct++;
            } else {
                //copy to bad output directory - include 'errors' in the name to avoid overwriting files in directory 
                //Omic_Errors directory is not created in cases of no error files 
                File dest = new File(FILE_OUTPUT_ROOT + "Omic_Errors\\" + errors + "_" + x.getName());
                try {
                    FileUtils.copyFile(x, dest);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                errors++;
            }
        }
    }

    /**
     * omicToBETS takes a omic JSON File and creates a BETS object. Returns true
     * if no exceptions were thrown in the process (will be valid file for BETS
     * converter).
     *
     * @param omicJSON File
     */
    public static boolean omicToBETS(File omicJSON) {
        String betsJSON;
        BETSV1 bets = null;
        ObjectMapper mapper = new ObjectMapper(); //create new Jackson Mapper
        OMICToolV1 omicTool;
        try {
            //map omic json files to seq class
            omicTool = mapper.readValue(omicJSON, OMICToolV1.class);
            bets = OMICToolConverter.toBETS(omicTool); //convert the omic tool
            ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
            betsJSON = ow.writeValueAsString(bets); //write Json as String

            //Check to make sure that all necessary fields are not null
            if (omicTool == null | bets == null) {
                throw new IOException("Either seqTool or bets is null.");
            }
            if (bets.getName() == null | bets.getName().trim().length() == 0) {
                throw new IOException("Name is null. ");
            }
            //No exceptions were thrown
            return true;
        } catch (IOException e) {
            System.out.println("====================Error with file: " + omicJSON.getName() + "============");
            System.out.println(e.getMessage());
            //An exception was thrown
            return false;
        } catch (Exception ex) {
            System.out.println("====================Error with file: " + omicJSON.getName() + "=============");
            System.out.println(ex.getMessage());
            return false;
        }
    }

    /**
     * getSeqFiles creates an array of files from directory specified as
     * rootPath. RootPath directory comes from the SEQData project
     *
     * @return File[]
     */
    public static File[] getOmicFiles() {
        String rootPath = OMIC_INPUT;
        File f = new File(rootPath);
        File[] files = f.listFiles();
        System.out.println("---Omic files have been created!---");
        return files;
    }
}
