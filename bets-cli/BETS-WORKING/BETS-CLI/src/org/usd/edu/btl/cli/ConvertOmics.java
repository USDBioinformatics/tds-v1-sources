/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usd.edu.btl.cli;

import org.usd.edu.btl.betsconverter.OMICToolsV1.OMICToolV1;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.io.File;
import java.io.IOException;
import org.usd.edu.btl.betsconverter.BETSV1.BETSV1;
import org.usd.edu.btl.converters.OMICToolConverter;

/**
 *
 * @author Bill Conn <Bill.Conn@usd.edu>
 */
public class ConvertOmics {
    /**
     * Default Constructor
     */
    public ConvertOmics() {
        
    }
    
    public void toBets(String inputF, String outFile) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        OMICToolV1 omicTool;
        File input = new File(inputF);
        
        try {
            omicTool = mapper.readValue(input, OMICToolV1.class);
            System.out.println("Reading from " + input);
            
            BETSV1 betsOutput = OMICToolConverter.toBETS(omicTool);
//            String betsJSON = writer.writeValueAsString(omicTool);
            
            if (outFile == null) {
                /*===============PRINT JSON TO CONSOLE AND FILES =================== */
                System.out.println("************************************************\n"
                        + "*********PRINTING OUT FIRST CONVERSION************\n"
                        + "   --------------OMICS--> BETS--------------\n"
                        + "************************************************\n");
                //print objects as Json using jackson

                System.out.println("=== OMICS TO BETS JSON - OUTPUT === \n");
                System.out.println(writer.writeValueAsString(betsOutput));
            } else {
                System.out.println("Writing to file...");
                //write to files
                writer.writeValue(new File(outFile), betsOutput);
                System.out.println(outFile + " has been created successfully");
                System.exit(1);
            }
            
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
