package org.usd.edu.btl.betsconverter.BETSV1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Objects;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 *
 * @author Tyler.Jones
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "name",
    "display_name",
    "version",
    "summary",
    "description",
    "limitations",
    "example",
    "links",
    "references",
    "author",
    "created_at",
    "owner",
    "maintained",
    "availability",
    "contact",
    "category",
    "technology",
    "programming_language",
    "license",
    "operating_system",
    "software_features",
    "software_libraries",
    "inputs",
    "parameters",
    "outputs",
    "requirements",
    "quality",
    "learn_flow",
    "release_date",
    "algorithm",
    "iplant_specific",
    "seq_specific",
    "galaxy_specific",
    "bld_specific",
    "parallelism"
})
public class BETSV1 {

    @JsonProperty("name")
    private String name;
    @JsonProperty("display_name")
    private String display_name;
    @JsonProperty("version")
    private String version;
    @JsonProperty("summary")
    private String summary;
    @JsonProperty("description")
    private String description;
    @JsonProperty("limitations")
    private String limitations;
    @JsonProperty("example")
    private List<Example> example = new ArrayList<Example>();
    @JsonProperty("links")
    private List<Link> links = new ArrayList<Link>();
    @JsonProperty("references")
    private List<Reference> references = new ArrayList<Reference>();
    @JsonProperty("author")
    private String author;
    @JsonProperty("created_at")
    private String created_at;
    @JsonProperty("owner")
    private String owner;
    @JsonProperty("maintained")
    private String maintained;
    @JsonProperty("availability")
    private List<Availability> availability = new ArrayList<Availability>();
    @JsonProperty("contact")
    private Contact contact;
    @JsonProperty("category")
    private Category category;
    @JsonProperty("technology")
    private List<Technology> technology = new ArrayList<Technology>();
    @JsonProperty("programming_language")
    private List<Programming_language> programming_language = new ArrayList<Programming_language>();
    @JsonProperty("license")
    private List<License> license = new ArrayList<License>();
    @JsonProperty("operating_system")
    private List<Operating_system> operating_system = new ArrayList<Operating_system>();
    @JsonProperty("software_features")
    private String software_features;
    @JsonProperty("software_libraries")
    private String software_libraries;
    @JsonProperty("inputs")
    private List<Input> inputs = new ArrayList<Input>();
    @JsonProperty("parameters")
    private List<Parameter> parameters = new ArrayList<Parameter>();
    @JsonProperty("outputs")
    private List<Output> outputs = new ArrayList<Output>();
    @JsonProperty("requirements")
    private String requirements;
    @JsonProperty("quality")
    private List<Quality_> quality = new ArrayList<Quality_>();
    @JsonProperty("learn_flow")
    private List<Learn_flow> learn_flow = new ArrayList<Learn_flow>();
    @JsonProperty("release_date")
    private String release_date;
    @JsonProperty("algorithm")
    private List<Algorithm> algorithm = new ArrayList<Algorithm>();
    @JsonProperty("validation_date")
    private String validation_date;
    @JsonProperty("nature_of_tool")
    private List<String> nature_of_tool = new ArrayList<String>();
    @JsonProperty("tool_type")
    private List<String> tool_type = new ArrayList<String>();
    @JsonProperty("interfaces")
    private List<String> interfaces = new ArrayList<String>();
    @JsonProperty("iplant_specific")
    private Iplant_specific iplant_specific;
    @JsonProperty("seq_specific")
    private Seq_specific seq_specific;
    @JsonProperty("galaxy_specific")
    private Galaxy_specific galaxy_specific;
    @JsonProperty("bld_specific")
    private Bld_specific bld_specific;
    @JsonProperty("omic_specific")
    private OMIC_specific omic_specific;
    @JsonProperty("parallelism")
    private String parallelism;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    @JsonProperty("display_name")
    public String getDisplay_name() {
        return display_name;
    }

    /**
     *
     * @param display_name
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    /**
     *
     * @return
     */
    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     *
     * @return
     */
    @JsonProperty("summary")
    public String getSummary() {
        return summary;
    }

    /**
     *
     * @param summary
     */
    @JsonProperty("summary")
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     *
     * @return
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     */
    @JsonProperty("limitations")
    public String getLimitations() {
        return limitations;
    }

    /**
     *
     * @param limitations
     */
    @JsonProperty("limitations")
    public void setLimitations(String limitations) {
        this.limitations = limitations;
    }

    /**
     *
     * @return
     */
    @JsonProperty("example")
    public List<Example> getExample() {
        return example;
    }

    /**
     *
     * @param example
     */
    @JsonProperty("example")
    public void setExample(List<Example> example) {
        this.example = example;
    }

    /**
     *
     * @return
     */
    @JsonProperty("links")
    public List<Link> getLinks() {
        return links;
    }

    /**
     *
     * @param links
     */
    @JsonProperty("links")
    public void setLinks(List<Link> links) {
        this.links = links;
    }

    /**
     *
     * @return
     */
    @JsonProperty("references")
    public List<Reference> getReferences() {
        return references;
    }

    /**
     *
     * @param references
     */
    @JsonProperty("references")
    public void setReferences(List<Reference> references) {
        this.references = references;
    }

    /**
     *
     * @return
     */
    @JsonProperty("author")
    public String getAuthor() {
        return author;
    }

    /**
     *
     * @param author
     */
    @JsonProperty("author")
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     *
     * @return
     */
    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    /**
     *
     * @param created_at
     */
    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     *
     * @return
     */
    @JsonProperty("owner")
    public String getOwner() {
        return owner;
    }

    /**
     *
     * @param owner
     */
    @JsonProperty("owner")
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     *
     * @return
     */
    @JsonProperty("maintained")
    public String getMaintained() {
        return maintained;
    }

    /**
     *
     * @param maintained
     */
    @JsonProperty("maintained")
    public void setMaintained(String maintained) {
        this.maintained = maintained;
    }

    /**
     *
     * @return
     */
    @JsonProperty("availability")
    public List<Availability> getAvailability() {
        return availability;
    }

    /**
     *
     * @param availability
     */
    @JsonProperty("availability")
    public void setAvailability(List<Availability> availability) {
        this.availability = availability;
    }

    /**
     *
     * @return
     */
    @JsonProperty("contact")
    public Contact getContact() {
        return contact;
    }

    /**
     *
     * @param contact
     */
    @JsonProperty("contact")
    public void setContact(Contact contact) {
        this.contact = contact;
    }

    /**
     *
     * @return
     */
    @JsonProperty("category")
    public Category getCategory() {
        return category;
    }

    /**
     *
     * @param category
     */
    @JsonProperty("category")
    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     *
     * @return
     */
    @JsonProperty("technology")
    public List<Technology> getTechnology() {
        return technology;
    }

    /**
     *
     * @param technology
     */
    @JsonProperty("technology")
    public void setTechnology(List<Technology> technology) {
        this.technology = technology;
    }

    /**
     *
     * @return
     */
    @JsonProperty("programming_language")
    public List<Programming_language> getProgramming_language() {
        return programming_language;
    }

    /**
     *
     * @param programming_language
     */
    @JsonProperty("programming_language")
    public void setProgramming_language(List<Programming_language> programming_language) {
        this.programming_language = programming_language;
    }

    /**
     *
     * @return
     */
    @JsonProperty("license")
    public List<License> getLicense() {
        return license;
    }

    /**
     *
     * @param license
     */
    @JsonProperty("license")
    public void setLicense(List<License> license) {
        this.license = license;
    }

    /**
     *
     * @return
     */
    @JsonProperty("operating_system")
    public List<Operating_system> getOperating_system() {
        return operating_system;
    }

    /**
     *
     * @param operating_system
     */
    @JsonProperty("operating_system")
    public void setOperating_system(List<Operating_system> operating_system) {
        this.operating_system = operating_system;
    }

    /**
     *
     * @return
     */
    @JsonProperty("software_features")
    public String getSoftware_features() {
        return software_features;
    }

    /**
     *
     * @param software_features
     */
    @JsonProperty("software_features")
    public void setSoftware_features(String software_features) {
        this.software_features = software_features;
    }

    /**
     *
     * @return
     */
    @JsonProperty("software_libraries")
    public String getSoftware_libraries() {
        return software_libraries;
    }

    /**
     *
     * @param software_libraries
     */
    @JsonProperty("software_libraries")
    public void setSoftware_libraries(String software_libraries) {
        this.software_libraries = software_libraries;
    }

    /**
     *
     * @return
     */
    @JsonProperty("inputs")
    public List<Input> getInputs() {
        return inputs;
    }

    /**
     *
     * @param inputs
     */
    @JsonProperty("inputs")
    public void setInputs(List<Input> inputs) {
        this.inputs = inputs;
    }

    /**
     *
     * @return
     */
    @JsonProperty("parameters")
    public List<Parameter> getParameters() {
        return parameters;
    }

    /**
     *
     * @param parameters
     */
    @JsonProperty("parameters")
    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    /**
     *
     * @return
     */
    @JsonProperty("outputs")
    public List<Output> getOutputs() {
        return outputs;
    }

    /**
     *
     * @param outputs
     */
    @JsonProperty("outputs")
    public void setOutputs(List<Output> outputs) {
        this.outputs = outputs;
    }

    /**
     *
     * @return
     */
    @JsonProperty("requirements")
    public String getRequirements() {
        return requirements;
    }

    /**
     *
     * @param requirements
     */
    @JsonProperty("requirements")
    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    /**
     *
     * @return
     */
    @JsonProperty("quality")
    public List<Quality_> getQuality() {
        return quality;
    }

    /**
     *
     * @param quality
     */
    @JsonProperty("quality")
    public void setQuality(List<Quality_> quality) {
        this.quality = quality;
    }

    /**
     *
     * @return
     */
    @JsonProperty("learn_flow")
    public List<Learn_flow> getLearn_flow() {
        return learn_flow;
    }

    /**
     *
     * @param learn_flow
     */
    @JsonProperty("learn_flow")
    public void setLearn_flow(List<Learn_flow> learn_flow) {
        this.learn_flow = learn_flow;
    }

    /**
     *
     * @return
     */
    @JsonProperty("release_date")
    public String getRelease_date() {
        return release_date;
    }

    /**
     *
     * @param release_date
     */
    @JsonProperty("release_date")
    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    /**
     *
     * @return
     */
    @JsonProperty("algorithm")
    public List<Algorithm> getAlgorithm() {
        return algorithm;
    }

    /**
     *
     * @param algorithm
     */
    @JsonProperty("algorithm")
    public void setAlgorithm(List<Algorithm> algorithm) {
        this.algorithm = algorithm;
    }

    /**
     *
     * @return
     */
    @JsonProperty("validation_date")
    public String getValidation_date() {
        return validation_date;
    }

    /**
     *
     * @param validation_date
     */
    @JsonProperty("validation_date")
    public void setValidation_date(String validation_date) {
        this.validation_date = validation_date;
    }

    /**
     *
     * @return
     */
    @JsonProperty("nature_of_tool")
    public List<String> getNature_of_tool() {
        return nature_of_tool;
    }

    /**
     *
     * @param nature_of_tool
     */
    @JsonProperty("nature_of_tool")
    public void setNature_of_tool(List<String> nature_of_tool) {
        this.nature_of_tool = nature_of_tool;
    }

    /**
     *
     * @return
     */
    @JsonProperty("tool_type")
    public List<String> getTool_type() {
        return tool_type;
    }

    /**
     *
     * @param tool_type
     */
    @JsonProperty("tool_type")
    public void setTool_type(List<String> tool_type) {
        this.tool_type = tool_type;
    }

    /**
     *
     * @return
     */
    @JsonProperty("interfaces")
    public List<String> getInterfaces() {
        return interfaces;
    }

    /**
     *
     * @param interfaces
     */
    @JsonProperty("interfaces")
    public void setInterfaces(List<String> interfaces) {
        this.interfaces = interfaces;
    }

    /**
     *
     * @return
     */
    @JsonProperty("iplant_specific")
    public Iplant_specific getIplant_specific() {
        return iplant_specific;
    }

    /**
     *
     * @param iplant_specific
     */
    @JsonProperty("iplant_specific")
    public void setIplant_specific(Iplant_specific iplant_specific) {
        this.iplant_specific = iplant_specific;
    }

    /**
     *
     * @return
     */
    @JsonProperty("seq_specific")
    public Seq_specific getSeq_specific() {
        return seq_specific;
    }

    /**
     *
     * @param seq_specific
     */
    @JsonProperty("seq_specific")
    public void setSeq_specific(Seq_specific seq_specific) {
        this.seq_specific = seq_specific;
    }

    /**
     *
     * @return
     */
    @JsonProperty("omic_specific")
    public OMIC_specific getOmic_specific() {
        return omic_specific;
    }

    /**
     *
     * @param omic_specific
     */
    @JsonProperty("omic_specific")
    public void setOmic_specific(OMIC_specific omic_specific) {
        this.omic_specific = omic_specific;
    }

    /**
     *
     * @return
     */
    @JsonProperty("galaxy_specific")
    public Galaxy_specific getGalaxy_specific() {
        return galaxy_specific;
    }

    /**
     *
     * @param galaxy_specific
     */
    @JsonProperty("galaxy_specific")
    public void setGalaxy_specific(Galaxy_specific galaxy_specific) {
        this.galaxy_specific = galaxy_specific;
    }

    /**
     *
     * @return
     */
    @JsonProperty("bld_specific")
    public Bld_specific getBld_specific() {
        return bld_specific;
    }

    /**
     *
     * @param bld_specific
     */
    @JsonProperty("bld_specific")
    public void setBld_specific(Bld_specific bld_specific) {
        this.bld_specific = bld_specific;
    }

    /**
     *
     * @return
     */
    @JsonProperty("parallelism")
    public String getParallelism() {
        return parallelism;
    }

    /**
     *
     * @param parallelism
     */
    @JsonProperty("parallelism")
    public void setParallelism(String parallelism) {
        this.parallelism = parallelism;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
//        StringBuilder sb = new StringBuilder();
//        sb.append("***BETS JSON FILE***\n");
//        sb.append("Name= " + getName() + "\n");
//        sb.append("Display Name= " + getDisplay_name() + "\n");
//
//        return sb.toString();
        String results;
        ObjectMapper mapper = new ObjectMapper();

        try {
            results = mapper.writeValueAsString(this);
        } catch (Exception JsonProcessingException) {
            results = "";
        }
        return results;
    }

    /**
     * Combines this BETSV1 object with the param BETSV1 object. Only fills
     * empty fields and concatenates the lists. If both objects have a
     * particular field, keeps the original. If the field is a list and both
     * objects have that field, concatenates the lists.
     *
     * @param other
     * @return
     */
    public BETSV1 combine(BETSV1 other) {
        if (other == null) {
            return this;
        }
        if (this.name == null || this.name.isEmpty()) {
            this.setName(other.getName());
        }
        if (this.display_name == null || this.display_name.isEmpty()) {
            this.setDisplay_name(other.getDisplay_name());
        }
        if (this.version == null || this.version.isEmpty()) {
            this.setVersion(other.getVersion());
        }
        if (this.summary == null || this.summary.isEmpty()) {
            this.setSummary(other.getSummary());
        }
        if (this.description == null || this.description.isEmpty()) {
            this.setDescription(other.getDescription());
        }
        if (this.limitations == null || this.limitations.isEmpty()) {
            this.setLimitations(other.getLimitations());
        }
        //Example Section
        if (other.getExample() != null && !other.getExample().isEmpty()) {
            if (this.example == null || this.example.isEmpty()) {
                //this.example = new ArrayList<>();
                //If the current one is null, use the other Example list
                this.setExample(other.getExample());
            } else {
                //If they both have examples, we should merge examples.
                for (Example ex : other.getExample()) {
                    Boolean unique = true;
                    for (Example ex2 : this.getExample()) {
                        if (ex.equals(ex2)) {
                            unique = false;
                        }
                    }
                    if (unique){
                        this.example.add(ex);
                    }
                }
            }
        }
        //Links Section
        if (other.getLinks() != null && !other.getLinks().isEmpty()) {
            if (this.links == null || this.links.isEmpty()) {
                //this.links = new ArrayList<>();
                //If the current one is null, use other Link list
                this.setLinks(other.getLinks());
            } else {
                //If they both have Links, we should merge them
                for (Link link : other.getLinks()) {
                    Boolean unique = true;
                    for (Link link2 : this.getLinks()) {
                        if (link.equals(link2)) {
                            unique = false;
                        }
                    }
                    if(unique) {
                        this.links.add(link);
                    }
                }
            }
        }
        //References Section
        if (other.getReferences() != null && !other.getReferences().isEmpty()) {
            if (this.references == null || this.references.isEmpty()) {
                //this.references = new ArrayList<>();
                this.setReferences(other.getReferences());
            } else {
                //If they both have References, we need to merge them
                for (Reference ref : other.getReferences()) {
                    if(!this.references.contains(ref)) {
                        this.references.add(ref);
                    }
                }
            }
        }
        if (this.author == null || this.author.isEmpty()) {
            this.setAuthor(other.getAuthor());
        }
        if (this.created_at == null || this.created_at.isEmpty()) {
            this.setCreated_at(other.getCreated_at());
        }
        if (this.owner == null || this.owner.isEmpty()) {
            this.setOwner(other.getOwner());
        }
        if (this.maintained == null || this.maintained.isEmpty()) {
            this.setMaintained(other.getMaintained());
        }
        if (other.getAvailability() != null && !other.getAvailability().isEmpty()) {
            if (this.availability == null || this.availability.isEmpty()) {
                //this.availability = new ArrayList<>();
                this.setAvailability(other.getAvailability());
            } else {
                //They both have Availibilities and we should merge them
                for (Availability av : other.getAvailability()) {
                    if (!this.availability.contains(av)) {
                        this.availability.add(av);
                    }
                }
            }
        }
        if (this.contact == null && other.getContact() != null) {
            this.contact = other.getContact();
        } else if (this.contact != null && other.getContact() != null) {
            if (this.contact.getName() == null || this.contact.getName().isEmpty()) {
                this.contact.setName(other.getContact().getName());
            }
            if (this.contact.getPhone() == null || this.contact.getPhone().isEmpty()) {
                this.contact.setPhone(other.getContact().getPhone());
            }
            if (this.contact.getEmail() == null || this.contact.getEmail().isEmpty()) {
                this.contact.setEmail(other.getContact().getPhone());
            }
            if (this.contact.getAddress() == null || this.contact.getAddress().isEmpty()) {
                this.contact.setAddress(other.getContact().getAddress());
            }
            if (this.contact.getInstitution() == null || this.contact.getInstitution().isEmpty()) {
                this.contact.setInstitution(other.getContact().getInstitution());
            }
        }
        if (this.category == null && other.getCategory() != null) {
            this.category = other.getCategory();
        } else if (this.category != null && other.getCategory() != null) {
            if (this.category.getId() == null || this.category.getId().isEmpty()) {
                this.category.setId(other.getCategory().getId());
            }
            if (this.category.getName() == null || this.category.getName().isEmpty()) {
                this.category.setName(other.getCategory().getName());
            }
            if (this.category.getPath() == null || this.category.getPath().isEmpty()) {
                this.category.setPath(other.getCategory().getPath());
            }
            //Category -> Application Section
            if (other.category.getApplication() != null && !other.category.getApplication().isEmpty()) {
                if (this.category.getApplication() == null || this.category.getApplication().isEmpty()) {
                    //If this categories applications are null or empty, use other's
                    this.category.setApplication(other.category.getApplication());
                } else {
                    //Both have Applications, so we need to merge them
                    for (Application app : other.category.getApplication()) {
                        if (!this.category.getApplication().contains(app)) {
                            this.category.getApplication().add(app);
                        }
                    }
                }
            }
            if (other.category.getDomain() != null && !other.category.getDomain().isEmpty()) {
                if (this.category.getDomain() == null || this.category.getDomain().isEmpty()) {
                    //if this categories domains are null or empty, use other's
                    this.category.setDomain(other.category.getDomain());
                } else {
                    //Both have domains, so we need to merge them
                    for (Domain dom: other.category.getDomain()) {
                        if (!this.category.getDomain().contains(dom)) {
                            this.category.getDomain().add(dom);
                        }
                    }
                }
            }
            if (other.category.getMethod() != null && !other.category.getMethod().isEmpty()) {
                if (this.category.getMethod() == null || this.category.getMethod().isEmpty()) {
                    //If this categories methods are null or empty, use other's
                    this.category.setMethod(other.category.getMethod());
                } else {
                    //Both have methods, so we need to merge them
                    for (Method meth : other.category.getMethod()) {
                        if (!this.category.getMethod().contains(meth)) {
                            this.category.getMethod().add(meth);
                        }
                    }
                }
            }
            if (other.category.getTags() != null && !other.category.getTags().isEmpty()) {
                if (this.category.getTags() == null || this.category.getTags().isEmpty()) {
                    //if this object has no tags, use other's
                    this.category.setTags(other.category.getTags());
                } else {
                    //Both have tags, need to merge
                    for (String tag : other.category.getTags()) {
                        if (!this.category.getTags().contains(tag)) {
                            this.category.getTags().add(tag);
                        }
                    }
                }
            }
            if (other.category.getFormat() != null && this.category.getFormat() == null) {
                this.category.setFormat(other.category.getFormat());
            }
            if (other.category.getNgs_provider() != null && this.category.getNgs_provider() == null) {
                this.category.setNgs_provider(other.category.getNgs_provider());
            }
            if (other.category.getSequence_platform() != null && this.category.getSequence_platform() == null) {
                this.category.setSequence_platform(other.category.getSequence_platform());
            }
            if (other.category.getReference() != null && this.category.getReference() == null) {
                this.category.setReference(other.category.getReference());
            }
        }
        if (other.getTechnology() != null && !other.getTechnology().isEmpty()) {
            if (this.technology == null || this.technology.isEmpty()) {
                this.setTechnology(other.getTechnology());
            } else {
                //need to merge them
                for (Technology tech : other.getTechnology()) {
                    if (!this.getTechnology().contains(tech)) {
                        this.getTechnology().add(tech);
                    }
                }
            }
        }
        if (other.getProgramming_language() != null && !other.getTechnology().isEmpty()) {
            if (this.programming_language == null || this.programming_language.isEmpty()) {
                this.setProgramming_language(other.getProgramming_language());
            } else {
                //need to merge them
                for (Programming_language prog : other.getProgramming_language()) {
                    if (!this.getProgramming_language().contains(prog)) {
                        this.getProgramming_language().add(prog);
                    }
                }
            }
        }
        if (other.getLicense() != null && !other.getLicense().isEmpty()) {
            if (this.license == null || this.license.isEmpty()) {
                this.setLicense(other.getLicense());
            } else {
                //need to merge them
                for (License lic : other.getLicense()) {
                    if (!this.getLicense().contains(lic)) {
                        this.getLicense().add(lic);
                    }
                }
            }
        }
        if (other.getOperating_system() != null && !other.getOperating_system().isEmpty()) {
            if (this.operating_system == null || this.operating_system.isEmpty()) {
                this.setOperating_system(other.getOperating_system());
            } else {
                //need to merge them
                for (Operating_system os : other.getOperating_system()) {
                    if (!this.getOperating_system().contains(os)) {
                        this.getOperating_system().add(os);
                    }
                }
            }
        }
        if (this.software_features == null || this.software_features.isEmpty()) {
            this.setSoftware_features(other.getSoftware_features());
        }
        if (this.software_libraries == null || this.software_libraries.isEmpty()) {
            this.setSoftware_libraries(other.getSoftware_libraries());
        }
        //Inputs Section
        if (other.getInputs() != null && !other.getInputs().isEmpty()) {
            if (this.inputs == null || this.inputs.isEmpty()) {
                this.inputs = other.getInputs();
            } else {
                //Need to merge the two sets of inputs
                for (Input in : other.getInputs()) {
                    if (!this.getInputs().contains(in)) {
                        this.getInputs().add(in);
                    }
                }
            }
        }
        if (other.getParameters() != null && !other.getParameters().isEmpty()) {
            if (this.parameters == null || this.parameters.isEmpty()) {
                this.parameters = other.getParameters();
            } else {
                //Need to merge them
                for (Parameter param : other.getParameters()) {
                    if (!this.parameters.contains(param)) {
                        this.parameters.add(param);
                    }
                }
            }
        }
        if (other.getOutputs() != null && !other.getOutputs().isEmpty()) {
            if (this.outputs == null || this.outputs.isEmpty()) {
                this.outputs = other.getOutputs();
            } else {
                //need to merge them
                for (Output out : other.getOutputs()) {
                    if (!this.getOutputs().contains(out)) {
                        this.getOutputs().add(out);
                    }
                }
            }
        }
        if (this.requirements == null || this.requirements.isEmpty()) {
            this.setRequirements(other.getRequirements());
        }
        if (other.getQuality() != null && !other.getQuality().isEmpty()) {
            if (this.quality == null || this.quality.isEmpty()) {
                this.quality = other.getQuality();
            } else {
                for (Quality_ qual : other.getQuality()) {
                    if (!this.getQuality().contains(qual)) {
                        this.getQuality().add(qual);
                    }
                }
            }
        }
        if (other.getLearn_flow() != null && !other.getLearn_flow().isEmpty()) {
            if (this.learn_flow == null || this.getLearn_flow().isEmpty()) {
                this.learn_flow = other.getLearn_flow();
            } else {
                for (Learn_flow lf : other.getLearn_flow()) {
                    if (!this.getLearn_flow().contains(lf)) {
                        this.getLearn_flow().add(lf);
                    }
                }
            }
        }
        if (this.release_date == null || this.release_date.isEmpty()) {
            this.setRelease_date(other.getRelease_date());
        }
        if (other.getAlgorithm() != null && !other.getAlgorithm().isEmpty()) {
            if (this.algorithm == null || this.getAlgorithm().isEmpty()) {
                this.algorithm = other.getAlgorithm();
            } else {
                for (Algorithm alg : other.getAlgorithm()) {
                    if (!this.getAlgorithm().contains(alg)) {
                        this.getAlgorithm().add(alg);
                    }
                }
            }
        }
        if (this.validation_date == null || this.validation_date.isEmpty()) {
            this.setValidation_date(other.getValidation_date());
        }
        if (other.getNature_of_tool() != null && !other.getNature_of_tool().isEmpty()) {
            if (this.nature_of_tool == null || this.getNature_of_tool().isEmpty()) {
                this.nature_of_tool = other.getNature_of_tool();
            } else {
                for (String tool : other.getNature_of_tool()) {
                    if (!this.getNature_of_tool().contains(tool)) {
                        this.getNature_of_tool().add(tool);
                    }
                }
            }
        }
        if (other.getTool_type() != null && !other.getTool_type().isEmpty()) {
            if (this.tool_type == null || this.getTool_type().isEmpty()) {
                this.tool_type = other.getTool_type();
            } else {
                for (String type : other.getTool_type()) {
                    if (!this.getTool_type().contains(type)) {
                        this.getTool_type().add(type);
                    }
                }
            }
        }
        if (other.getInterfaces() != null && !other.getInterfaces().isEmpty()) {
            if (this.interfaces == null || this.getInterfaces().isEmpty()) {
                this.interfaces = other.getInterfaces();
            } else {
                for (String iface : other.getInterfaces()) {
                    if (!this.getInterfaces().contains(iface)) {
                        this.getInterfaces().add(iface);
                    }
                }
            }
        }
        if (this.parallelism == null || this.parallelism.isEmpty()) {
            this.setParallelism(other.getParallelism());
        }
        if (other.getIplant_specific() != null) {
            this.setIplant_specific(other.getIplant_specific());
        }
        if (other.getSeq_specific() != null) {
            this.setSeq_specific(other.getSeq_specific());
        }
        if (other.getGalaxy_specific() != null) {
            this.setGalaxy_specific(other.getGalaxy_specific());
        }
        if (other.getBld_specific() != null) {
            this.setBld_specific(other.getBld_specific());
        }
        if (other.getOmic_specific() != null) {
            this.setOmic_specific(other.getOmic_specific());
        }
        return this;
    }

    /**
     *
     * @return
     */
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    /**
     *
     * @param name
     * @param value
     */
    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.name);
        hash = 47 * hash + Objects.hashCode(this.display_name);
        hash = 47 * hash + Objects.hashCode(this.version);
        hash = 47 * hash + Objects.hashCode(this.summary);
        hash = 47 * hash + Objects.hashCode(this.description);
        hash = 47 * hash + Objects.hashCode(this.limitations);
        hash = 47 * hash + Objects.hashCode(this.example);
        hash = 47 * hash + Objects.hashCode(this.links);
        hash = 47 * hash + Objects.hashCode(this.references);
        hash = 47 * hash + Objects.hashCode(this.author);
        hash = 47 * hash + Objects.hashCode(this.created_at);
        hash = 47 * hash + Objects.hashCode(this.owner);
        hash = 47 * hash + Objects.hashCode(this.maintained);
        hash = 47 * hash + Objects.hashCode(this.availability);
        hash = 47 * hash + Objects.hashCode(this.contact);
        hash = 47 * hash + Objects.hashCode(this.category);
        hash = 47 * hash + Objects.hashCode(this.technology);
        hash = 47 * hash + Objects.hashCode(this.programming_language);
        hash = 47 * hash + Objects.hashCode(this.license);
        hash = 47 * hash + Objects.hashCode(this.operating_system);
        hash = 47 * hash + Objects.hashCode(this.software_features);
        hash = 47 * hash + Objects.hashCode(this.software_libraries);
        hash = 47 * hash + Objects.hashCode(this.inputs);
        hash = 47 * hash + Objects.hashCode(this.parameters);
        hash = 47 * hash + Objects.hashCode(this.outputs);
        hash = 47 * hash + Objects.hashCode(this.requirements);
        hash = 47 * hash + Objects.hashCode(this.quality);
        hash = 47 * hash + Objects.hashCode(this.learn_flow);
        hash = 47 * hash + Objects.hashCode(this.release_date);
        hash = 47 * hash + Objects.hashCode(this.algorithm);
        hash = 47 * hash + Objects.hashCode(this.validation_date);
        hash = 47 * hash + Objects.hashCode(this.nature_of_tool);
        hash = 47 * hash + Objects.hashCode(this.tool_type);
        hash = 47 * hash + Objects.hashCode(this.interfaces);
        hash = 47 * hash + Objects.hashCode(this.iplant_specific);
        hash = 47 * hash + Objects.hashCode(this.seq_specific);
        hash = 47 * hash + Objects.hashCode(this.galaxy_specific);
        hash = 47 * hash + Objects.hashCode(this.bld_specific);
        hash = 47 * hash + Objects.hashCode(this.omic_specific);
        hash = 47 * hash + Objects.hashCode(this.parallelism);
        hash = 47 * hash + Objects.hashCode(this.additionalProperties);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BETSV1 other = (BETSV1) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.display_name, other.display_name)) {
            return false;
        }
        if (!Objects.equals(this.version, other.version)) {
            return false;
        }
        if (!Objects.equals(this.summary, other.summary)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.limitations, other.limitations)) {
            return false;
        }
        if (!Objects.equals(this.example, other.example)) {
            return false;
        }
        if (!Objects.equals(this.links, other.links)) {
            return false;
        }
        if (!Objects.equals(this.references, other.references)) {
            return false;
        }
        if (!Objects.equals(this.author, other.author)) {
            return false;
        }
        if (!Objects.equals(this.created_at, other.created_at)) {
            return false;
        }
        if (!Objects.equals(this.owner, other.owner)) {
            return false;
        }
        if (!Objects.equals(this.maintained, other.maintained)) {
            return false;
        }
        if (!Objects.equals(this.availability, other.availability)) {
            return false;
        }
        if (!Objects.equals(this.contact, other.contact)) {
            return false;
        }
        if (!Objects.equals(this.category, other.category)) {
            return false;
        }
        if (!Objects.equals(this.technology, other.technology)) {
            return false;
        }
        if (!Objects.equals(this.programming_language, other.programming_language)) {
            return false;
        }
        if (!Objects.equals(this.license, other.license)) {
            return false;
        }
        if (!Objects.equals(this.operating_system, other.operating_system)) {
            return false;
        }
        if (!Objects.equals(this.software_features, other.software_features)) {
            return false;
        }
        if (!Objects.equals(this.software_libraries, other.software_libraries)) {
            return false;
        }
        if (!Objects.equals(this.inputs, other.inputs)) {
            return false;
        }
        if (!Objects.equals(this.parameters, other.parameters)) {
            return false;
        }
        if (!Objects.equals(this.outputs, other.outputs)) {
            return false;
        }
        if (!Objects.equals(this.requirements, other.requirements)) {
            return false;
        }
        if (!Objects.equals(this.quality, other.quality)) {
            return false;
        }
        if (!Objects.equals(this.learn_flow, other.learn_flow)) {
            return false;
        }
        if (!Objects.equals(this.release_date, other.release_date)) {
            return false;
        }
        if (!Objects.equals(this.algorithm, other.algorithm)) {
            return false;
        }
        if (!Objects.equals(this.validation_date, other.validation_date)) {
            return false;
        }
        if (!Objects.equals(this.nature_of_tool, other.nature_of_tool)) {
            return false;
        }
        if (!Objects.equals(this.tool_type, other.tool_type)) {
            return false;
        }
        if (!Objects.equals(this.interfaces, other.interfaces)) {
            return false;
        }
        if (!Objects.equals(this.iplant_specific, other.iplant_specific)) {
            return false;
        }
        if (!Objects.equals(this.seq_specific, other.seq_specific)) {
            return false;
        }
        if (!Objects.equals(this.galaxy_specific, other.galaxy_specific)) {
            return false;
        }
        if (!Objects.equals(this.bld_specific, other.bld_specific)) {
            return false;
        }
        if (!Objects.equals(this.omic_specific, other.omic_specific)) {
            return false;
        }
        if (!Objects.equals(this.parallelism, other.parallelism)) {
            return false;
        }
        if (!Objects.equals(this.additionalProperties, other.additionalProperties)) {
            return false;
        }
        return true;
    }

}
