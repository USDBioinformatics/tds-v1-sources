package org.usd.edu.btl.betsconverter.GalaxyV1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Category", propOrder = {
    "value",
    "description"
})
public class Category {

    /**
     *
     */
    @XmlAttribute(name = "value")
    protected String value;

    /**
     *
     */
    @XmlAttribute(name = "description")
    protected String description;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }
    
    /**
     * Gets the  of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description of the description property.
     * 
     * @param description
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
