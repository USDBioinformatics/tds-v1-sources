package org.usd.edu.btl.betsconverter.BETSV1;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Generated;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 *
 * @author Shayla.Gustafson
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "RRID",
    "dependencies",
    "repo_name",
    "repo_revision"
})
public class OMIC_specific {

    @JsonProperty("RRID")
    private String RRID;
    @JsonProperty("dependencies")
    private String dependencies;
    @JsonProperty("code_maturity")
    private List<String> code_maturity;
    @JsonProperty("other_information")
    private String other_information;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     */
    @JsonProperty("RRID")
    public String getRRID() {
        return RRID;
    }

    /**
     *
     * @param RRID
     */
    @JsonProperty("RRID")
    public void setRRID(String RRID) {
        this.RRID = RRID;
    }

    /**
     *
     * @return
     */
    @JsonProperty("dependencies")
    public String getDependencies() {
        return dependencies;
    }

    /**
     *
     * @param dependencies
     */
    @JsonProperty("dependencies")
    public void setDependencies(String dependencies) {
        this.dependencies = dependencies;
    }

    /**
     *
     * @return
     */
    @JsonProperty("code_maturity")
    public List<String> getCode_maturity() {
        return code_maturity;
    }

    /**
     *
     * @param code_maturity
     */
    @JsonProperty("code_maturity")
    public void setCode_maturity(List<String> code_maturity) {
        this.code_maturity = code_maturity;
    }

    /**
     *
     * @return
     */
    @JsonProperty("other_information")
    public String getOther_information() {
        return other_information;
    }

    /**
     *
     * @param other_information
     */
    @JsonProperty("other_information")
    public void setOther_information(String other_information) {
        this.other_information = other_information;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     *
     * @return
     */
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    /**
     *
     * @param name
     * @param value
     */
    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.RRID);
        hash = 47 * hash + Objects.hashCode(this.dependencies);
        hash = 47 * hash + Objects.hashCode(this.code_maturity);
        hash = 47 * hash + Objects.hashCode(this.other_information);
        hash = 47 * hash + Objects.hashCode(this.additionalProperties);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OMIC_specific other = (OMIC_specific) obj;
        if (!Objects.equals(this.RRID, other.RRID)) {
            return false;
        }
        if (!Objects.equals(this.dependencies, other.dependencies)) {
            return false;
        }
        if (!Objects.equals(this.code_maturity, other.code_maturity)) {
            return false;
        }
        if (!Objects.equals(this.other_information, other.other_information)) {
            return false;
        }
        if (!Objects.equals(this.additionalProperties, other.additionalProperties)) {
            return false;
        }
        return true;
    }

}
