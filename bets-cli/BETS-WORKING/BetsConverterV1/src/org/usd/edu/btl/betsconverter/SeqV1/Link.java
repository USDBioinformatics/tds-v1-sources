/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.usd.edu.btl.betsconverter.SeqV1;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
"url",
"url_type",
"url_describes"
})
public class Link {

@JsonProperty("url")
private Object url;
@JsonProperty("url_type")
private Object urlType;
@JsonProperty("url_describes")
private Object urlDescribes;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The url
*/
@JsonProperty("url")
public Object getUrl() {
return url;
}

/**
* 
* @param url
* The url
*/
@JsonProperty("url")
public void setUrl(Object url) {
this.url = url;
}

/**
* 
* @return
* The urlType
*/
@JsonProperty("url_type")
public Object getUrlType() {
return urlType;
}

/**
* 
* @param urlType
* The url_type
*/
@JsonProperty("url_type")
public void setUrlType(Object urlType) {
this.urlType = urlType;
}

/**
* 
* @return
* The urlDescribes
*/
@JsonProperty("url_describes")
public Object getUrlDescribes() {
return urlDescribes;
}

/**
* 
* @param urlDescribes
* The url_describes
*/
@JsonProperty("url_describes")
public void setUrlDescribes(Object urlDescribes) {
this.urlDescribes = urlDescribes;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
