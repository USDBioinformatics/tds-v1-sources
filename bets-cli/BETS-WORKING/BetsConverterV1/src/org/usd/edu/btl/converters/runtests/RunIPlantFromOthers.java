package org.usd.edu.btl.converters.runtests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import org.usd.edu.btl.betsconverter.BETSV1.BETSV1;
import org.usd.edu.btl.betsconverter.BLDV1.BLDV1;
import org.usd.edu.btl.betsconverter.GalaxyV1.Tool;
import org.usd.edu.btl.betsconverter.OMICToolsV1.OMICToolV1;
import org.usd.edu.btl.betsconverter.SeqV1.SeqV1;
import org.usd.edu.btl.betsconverter.iPlantV1.IplantV1;
import org.usd.edu.btl.converters.BETSConverter;
import org.usd.edu.btl.converters.BLDConverter;
import org.usd.edu.btl.converters.GalaxyConverter;
import org.usd.edu.btl.converters.IplantConverter;
import org.usd.edu.btl.converters.OMICToolConverter;
import org.usd.edu.btl.converters.SeqConverter;

/**
 *
 * @author Shayla
 */
public class RunIPlantFromOthers {

    File bldFile = new File("test_inputs/BLDTool.json");
    File galaxyFile = new File("test_inputs/GalaxyTool.xml");
    File omicFile = new File("test_inputs/OMICTool.json");
    File seqFile = new File("test_inputs/SeqTool.json");

    /**
     *
     * @param args
     */
    public static void main(String[] args) throws Exception {
        RunIPlantFromOthers run = new RunIPlantFromOthers();

        BETSV1 bets;
        //System.out.println("Read first line: "+input.);
        ObjectMapper mapper = new ObjectMapper(); //create new Jackson Mapper

        IplantV1 ipTool;
        //BETSV1 betsTool;

        try {
//          bets = run.getBetsFromGalaxy();
//            bets = run.getBetsFromBLD();
//            bets = run.getBetsFromSeq();
            bets = run.getBetsFromOmic();
            

            //call betsToIplant()
            //Pre: Bets is filled with the values from the iPlant Input tool
            //Post: Bets has been converted back to iplant, should match the original ipInput file.
            IplantV1 iplant = BETSConverter.toIplant(bets);

            Map<String, Object> map = iplant.getAdditionalProperties();
            System.out.println("Addtl Props = " + map.isEmpty());

            /*===============PRINT JSON TO CONSOLE AND FILES =================== */
            System.out.println("************************************************\n"
                    + "*********PRINTING OUT FIRST CONVERSION************\n"
                    + "----------iPlant --> Bets --> iPlant--------------\n"
                    + "************************************************\n");
            //print objects as Json using jackson
            ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
            String betsJson = ow.writeValueAsString(bets); //write Json as String
            String iPlantJson = ow.writeValueAsString(iplant); //write Json as String

            System.out.println("=== IPLANT TO BETS JSON === \n"
                    + betsJson);
            System.out.println("=== BETS TO IPLANT JSON === \n"
                    + iPlantJson);

            //write to files
            //ow.writeValue(new File("bets_Converted_toIplant.json"), betsJson);
            //ow.writeValue(new File("iPlant_OUTPUT.json"), iPlantJson);
        } catch (Exception e) {
            System.out.println("Uh-Oh");
            throw(e);
        }
    }

    public BETSV1 getBetsFromGalaxy() throws IOException {
        String betsJSON;
        BETSV1 bets = null;
        ObjectMapper mapper = new ObjectMapper(); //create new Jackson Mapper
        Tool galaxyTool; //Galaxy main class is stupidly named 'Tool'

        InputStream infile = null;

        try {
            //map input json files to galaxy class
            JAXBContext jaxbContext = JAXBContext.newInstance(Tool.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller(); //Unmarshalling – Conversion XML content into a Java Object.
            infile = new FileInputStream(galaxyFile);
            galaxyTool = (Tool) unmarshaller.unmarshal(infile);
            bets = GalaxyConverter.toBETS(galaxyTool);
            ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
            betsJSON = ow.writeValueAsString(bets); //write Json as String
//            System.out.println("=== GALAXY TO BETS JSON === \n" + betsJSON);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            //throw new Exception(e.fillInStackTrace());
        } finally {
            try {
                infile.close();
            } catch (Exception e) {
                System.out.println("Terrible things afoot");
            }
        }
        return bets;
    }
    
    public BETSV1 getBetsFromBLD() throws IOException {
        BETSV1 bets = new BETSV1();
        ObjectMapper mapper = new ObjectMapper(); //create new Jackson Mapper
        //map input json files to iplant class
        BLDV1 bld = mapper.readValue(bldFile, BLDV1.class);

        //map input json file to Bets class
        //betsTool = mapper.readValue(betsInput, BETSV1.class);
        bets = BLDConverter.toBETS(bld); //pass the iplant tool spec, convert to bets

        return bets;
    }
    
    public BETSV1 getBetsFromSeq() throws IOException {
        BETSV1 bets = new BETSV1();
        ObjectMapper mapper = new ObjectMapper(); //create new Jackson Mapper
        //map input json files to iplant class
        SeqV1 seq = mapper.readValue(seqFile, SeqV1.class);

        //map input json file to Bets class
        //betsTool = mapper.readValue(betsInput, BETSV1.class);
        //call iplantToBets()
        bets = SeqConverter.toBETS(seq); //pass the iplant tool spec, convert to bets

        return bets;
    }
    
    public BETSV1 getBetsFromOmic() throws IOException {
        BETSV1 bets = new BETSV1();
        ObjectMapper mapper = new ObjectMapper(); //create new Jackson Mapper
        //map input json files to iplant class
        OMICToolV1 omic = mapper.readValue(omicFile, OMICToolV1.class);

        //map input json file to Bets class
        //betsTool = mapper.readValue(betsInput, BETSV1.class);
        //call iplantToBets()
        bets = OMICToolConverter.toBETS(omic); //pass the iplant tool spec, convert to bets

        return bets;
    }

}
