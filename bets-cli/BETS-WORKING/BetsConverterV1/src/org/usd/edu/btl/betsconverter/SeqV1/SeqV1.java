package org.usd.edu.btl.betsconverter.SeqV1;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.usd.edu.btl.betsconverter.BETSV1.Reference__;

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "label",
    "software_version",
    "biological_technology",
    "number_of_citations",
    "pm_page_counter",
    "institute",
    "email_address",
    "created_by",
    "input_format",
    "output_format",
    "maintained",
    "library",
    "summary",
    "bio_tags",
    "meth_tags",
    "features",
    "language",
    "licence",
    "os",
    "description"
})
public class SeqV1 {

    @JsonProperty("label")
    private String label;
    @JsonProperty("software_version")
    private Object softwareVersion;
    @JsonProperty("biological_technology")
    private List<String> biologicalTechnology;
    @JsonProperty("number_of_citations")
    private Object numberOfCitations;
    @JsonProperty("pm_page_counter")
    private Object pmPageCounter;
    @JsonProperty("institute")
    private List<String> institute;
    @JsonProperty("email_address")
    private Object emailAddress;
    @JsonProperty("created_by")
    private List<String> createdBy;
    @JsonProperty("input_format")
    private List<String> inputFormat;
    @JsonProperty("output_format")
    private List<String> outputFormat;
    @JsonProperty("maintained")
    private List<String> maintained;
    @JsonProperty("library")
    private List<String> library;
    @JsonProperty("summary")
    private List<String> summary = new ArrayList<String>();
    @JsonProperty("bio_tags")
    private List<String> bioTags = new ArrayList<String>();
    @JsonProperty("meth_tags")
    private List<String> methTags = new ArrayList<String>();
    @JsonProperty("features")
    private List<String> features;
    @JsonProperty("language")
    private List<String> language = new ArrayList<String>();
    @JsonProperty("licence")
    private List<String> licence;
    @JsonProperty("os")
    private List<String> os = new ArrayList<String>();
    @JsonProperty("references")
    private List<Reference> references;
    @JsonProperty("links")
    private List<Link> links;
    @JsonProperty("description")
    private String description; 
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return The label
     */
    @JsonProperty("label")
    public String getLabel() {
        return label;
    }

    /**
     *
     * @param label The label
     */
    @JsonProperty("label")
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     *
     * @return The softwareVersion
     */
    @JsonProperty("software_version")
    public Object getSoftwareVersion() {
        return softwareVersion;
    }

    /**
     *
     * @param softwareVersion The software_version
     */
    @JsonProperty("software_version")
    public void setSoftwareVersion(Object softwareVersion) {
        this.softwareVersion = softwareVersion;
    }

    /**
     *
     * @return The biologicalTechnology
     */
    @JsonProperty("biological_technology")
    public List<String> getBiologicalTechnology() {
        return biologicalTechnology;
    }

    /**
     *
     * @param biologicalTechnology The biological_technology
     */
    @JsonProperty("biological_technology")
    public void setBiologicalTechnology(List<String> biologicalTechnology) {
        this.biologicalTechnology = biologicalTechnology;
    }

    /**
     *
     * @return The numberOfCitations
     */
    @JsonProperty("number_of_citations")
    public Object getNumberOfCitations() {
        return numberOfCitations;
    }

    /**
     *
     * @param numberOfCitations The number_of_citations
     */
    @JsonProperty("number_of_citations")
    public void setNumberOfCitations(Object numberOfCitations) {
        this.numberOfCitations = numberOfCitations;
    }

    /**
     *
     * @return The pmPageCounter
     */
    @JsonProperty("pm_page_counter")
    public Object getPmPageCounter() {
        return pmPageCounter;
    }

    /**
     *
     * @param pmPageCounter The pm_page_counter
     */
    @JsonProperty("pm_page_counter")
    public void setPmPageCounter(Object pmPageCounter) {
        this.pmPageCounter = pmPageCounter;
    }

    /**
     *
     * @return The institute
     */
    @JsonProperty("institute")
    public List<String> getInstitute() {
        return institute;
    }

    /**
     *
     * @param institute The institute
     */
    @JsonProperty("institute")
    public void setInstitute(List<String> institute) {
        this.institute = institute;
    }

    /**
     *
     * @return The emailAddress
     */
    @JsonProperty("email_address")
    public Object getEmailAddress() {
        return emailAddress;
    }

    /**
     *
     * @param emailAddress The email_address
     */
    @JsonProperty("email_address")
    public void setEmailAddress(Object emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     *
     * @return The createdBy
     */
    @JsonProperty("created_by")
    public List<String> getCreatedBy() {
        return createdBy;
    }

    /**
     *
     * @param createdBy The created_by
     */
    @JsonProperty("created_by")
    public void setCreatedBy(List<String> createdBy) {
        this.createdBy = createdBy;
    }

    /**
     *
     * @return The inputFormat
     */
    @JsonProperty("input_format")
    public List<String> getInputFormat() {
        return inputFormat;
    }

    /**
     *
     * @param inputFormat The input_format
     */
    @JsonProperty("input_format")
    public void setInputFormat(List<String> inputFormat) {
        this.inputFormat = inputFormat;
    }

    /**
     *
     * @return The outputFormat
     */
    @JsonProperty("output_format")
    public List<String> getOutputFormat() {
        return outputFormat;
    }

    /**
     *
     * @param outputFormat The output_format
     */
    @JsonProperty("output_format")
    public void setOutputFormat(List<String> outputFormat) {
        this.outputFormat = outputFormat;
    }

    /**
     *
     * @return The maintained
     */
    @JsonProperty("maintained")
    public List<String> getMaintained() {
        return maintained;
    }

    /**
     *
     * @param maintained The maintained
     */
    @JsonProperty("maintained")
    public void setMaintained(List<String> maintained) {
        this.maintained = maintained;
    }

    /**
     *
     * @return The library
     */
    @JsonProperty("library")
    public List<String> getLibrary() {
        return library;
    }

    /**
     *
     * @param library The library
     */
    @JsonProperty("library")
    public void setLibrary(List<String> library) {
        this.library = library;
    }

    /**
     *
     * @return The summary
     */
    @JsonProperty("summary")
    public List<String> getSummary() {
        return summary;
    }

    /**
     *
     * @param summary The summary
     */
    @JsonProperty("summary")
    public void setSummary(List<String> summary) {
        this.summary = summary;
    }

    /**
     *
     * @return The bioTags
     */
    @JsonProperty("bio_tags")
    public List<String> getBioTags() {
        return bioTags;
    }

    /**
     *
     * @param bioTags The bio_tags
     */
    @JsonProperty("bio_tags")
    public void setBioTags(List<String> bioTags) {
        this.bioTags = bioTags;
    }

    /**
     *
     * @return The methTags
     */
    @JsonProperty("meth_tags")
    public List<String> getMethTags() {
        return methTags;
    }

    /**
     *
     * @param methTags The meth_tags
     */
    @JsonProperty("meth_tags")
    public void setMethTags(List<String> methTags) {
        this.methTags = methTags;
    }

    /**
     *
     * @return The features
     */
    @JsonProperty("features")
    public List<String> getFeatures() {
        return features;
    }

    /**
     *
     * @param features The features
     */
    @JsonProperty("features")
    public void setFeatures(List<String> features) {
        this.features = features;
    }

    /**
     *
     * @return The language
     */
    @JsonProperty("language")
    public List<String> getLanguage() {
        return language;
    }

    /**
     *
     * @param language The language
     */
    @JsonProperty("language")
    public void setLanguage(List<String> language) {
        this.language = language;
    }

    /**
     *
     * @return The licence
     */
    @JsonProperty("licence")
    public List<String> getLicence() {
        return licence;
    }

    /**
     *
     * @param licence The licence
     */
    @JsonProperty("licence")
    public void setLicence(List<String> licence) {
        this.licence = licence;
    }

    /**
     *
     * @return The os
     */
    @JsonProperty("os")
    public List<String> getOs() {
        return os;
    }

    /**
     *
     * @param os The os
     */
    @JsonProperty("os")
    public void setOs(List<String> os) {
        this.os = os;
    }

    @JsonProperty("references")
    public List<Reference> getReferences() {
        return references;
    }

    @JsonProperty("references")
    public void setReferences(List<Reference> references) {
        this.references = references;
    }
    
    
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

//    /**
//     * Sets references from List<BETSV1.Reference__>
//     * @param references 
//     */
//    public void setReferences2(List<Reference__> references) {
//        this.references = new ArrayList<Reference>();
//        Reference newRef;
//        for(Reference__ betsRef : references) {
//            newRef = new Reference();
//            newRef.setTitle(betsRef.getTitle());
//            newRef.setAuthor(betsRef.getAuthor());
//            newRef.setYear(betsRef.getYear());
//            newRef.setJournal(betsRef.getAuthor());
//            this.references.add(newRef);
//        }
//    }
//    
//    /**
//     * Sets references from List<BETSV1.Reference__>
//     * @param references 
//     */
//    public List<Reference__> getReferences2() {
//        List<Reference__> betsRefsList = new ArrayList<Reference__>();
//        Reference__ betsRef;
//        for(Reference seqRef : references) {
//            betsRef = new Reference__();
//            betsRef.setTitle(seqRef.getTitle().toString());
//            betsRef.setAuthor(seqRef.getAuthor().toString());
//            betsRef.setYear(seqRef.getYear().toString());
//            betsRef.setJournal(seqRef.getAuthor().toString());
//            betsRefsList.add(betsRef);
//        }
//        return betsRefsList;
//    }
    @JsonProperty("links")
    public List<Link> getLinks() {
        return links;
    }

    @JsonProperty("links")
    public void setLinks(List<Link> links) {
        this.links = links;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
