package org.usd.edu.btl.betsconverter.BETSV1;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.Objects;
import javax.annotation.Generated;

/**
 *
 * @author Tyler.Jones
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "name",
    "id",
    "path"
})
public class BLD_categories {

    @JsonProperty("category_name")
    private String categoryName;
    @JsonProperty("category_id")
    private int categoryId;
    @JsonProperty("category_path")
    private String categoryPath;

    /**
     *
     * @return
     */
    @JsonProperty("category_name")
    public String getCategoryName() {
        return categoryName;
    }

    /**
     *
     * @param categoryName
     */
    @JsonProperty("category_name")
    public void setCategoryName(String category_name) {
        this.categoryName = category_name;
    }

    /**
     *
     * @return
     */
    @JsonProperty("category_id")
    public Integer getCategoryId() {
        return categoryId;
    }

    /**
     *
     * @param categoryId
     */
    @JsonProperty("category_id")
    public void setCategoryId(Integer category_id) {
        this.categoryId = category_id;
    }

    /**
     *
     * @return
     */
    @JsonProperty("category_path")
    public String getCategoryPath() {
        return categoryPath;
    }

    /**
     *
     * @param categoryPath
     */
    @JsonProperty("category_path")
    public void setCategoryPath(String category_path) {
        this.categoryPath = category_path;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + Objects.hashCode(this.categoryName);
        hash = 11 * hash + this.categoryId;
        hash = 11 * hash + Objects.hashCode(this.categoryPath);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BLD_categories other = (BLD_categories) obj;
        if (!Objects.equals(this.categoryName, other.categoryName)) {
            return false;
        }
        if (this.categoryId != other.categoryId) {
            return false;
        }
        if (!Objects.equals(this.categoryPath, other.categoryPath)) {
            return false;
        }
        return true;
    }
    
}
