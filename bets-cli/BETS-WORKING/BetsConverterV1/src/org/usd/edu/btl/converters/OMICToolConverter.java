package org.usd.edu.btl.converters;

import java.util.ArrayList;
import java.util.List;
import org.usd.edu.btl.betsconverter.BETSV1.Application;
import org.usd.edu.btl.betsconverter.BETSV1.BETSV1;
import org.usd.edu.btl.betsconverter.BETSV1.Category;
import org.usd.edu.btl.betsconverter.BETSV1.Contact;
import org.usd.edu.btl.betsconverter.BETSV1.Input;
import org.usd.edu.btl.betsconverter.BETSV1.Link;
import org.usd.edu.btl.betsconverter.BETSV1.OMIC_specific;
import org.usd.edu.btl.betsconverter.BETSV1.Operating_system;
import org.usd.edu.btl.betsconverter.BETSV1.Output;
import org.usd.edu.btl.betsconverter.BETSV1.Programming_language;
import org.usd.edu.btl.betsconverter.BETSV1.Quality_;
import org.usd.edu.btl.betsconverter.BETSV1.Reference;
import org.usd.edu.btl.betsconverter.BETSV1.Technology;
import org.usd.edu.btl.betsconverter.OMICToolsV1.OMICToolV1;

/**
 * Converts a OMICToolsV1 object to a BETSV1 object
 *
 *
 * @author Shayla.Gustafson
 */
public class OMICToolConverter {

    /**
     *
     * @param seqTool
     * @return
     */
    @SuppressWarnings("empty-statement")
    public static BETSV1 toBETS(OMICToolV1 omicTool) {
        BETSV1 bets = new BETSV1();

        //Name
        if (omicTool.getName() != null) {
            bets.setName(omicTool.getName().trim());
        }
        
        //Display Name 
        if (omicTool.getProjectLongName() != null ) {
            bets.setDisplay_name(omicTool.getProjectLongName().trim());
        }

        //Summary
        if (omicTool.getDescription() != null) {
            bets.setSummary(omicTool.getDescription().trim());
        }

        //Version
        if (omicTool.getCurrentRelease() != null) {
            if(omicTool.getCurrentRelease().toLowerCase().contains(" version")) {
                //OMIC tool calls the version "Tool Name version x.xx" - only want "x.xx"
                int place = omicTool.getCurrentRelease().toLowerCase().indexOf(" version") + 8; //this should be the end of the word version
                String correctVersion = omicTool.getCurrentRelease().substring(place);
                omicTool.setCurrentRelease(correctVersion);
            }
            bets.setVersion(omicTool.getCurrentRelease().trim());
        }

        //Author
        if (omicTool.getMaintainer() != null) {
            bets.setAuthor(omicTool.getMaintainer().trim());
        }

        //Inputs
        if (omicTool.getInput() != null && omicTool.getInput().length() > 0) {
            List<Input> inputs = new ArrayList<>();
            Input input = new Input();
            input.setName("Input 1");
            input.setFormat(omicTool.getInput().trim());
            inputs.add(input);
            bets.setInputs(inputs);
        }

        //Outputs
        if (omicTool.getOutput() != null && omicTool.getOutput().length() > 0) {
            List<Output> outputs = new ArrayList<>();
            Output output = new Output();
            output.setName("Output 1");
            output.setFormat(omicTool.getOutput().trim());
            bets.setOutputs(outputs);
        }

        //Created At
        if (omicTool.getCreatedAt() != null) {
            bets.setCreated_at(omicTool.getCreatedAt().trim());
        }

        //Parallelism
        if (omicTool.getParallelComputation() != null) {
            bets.setParallelism(omicTool.getParallelComputation().trim());
        }

        //Quality
        if (omicTool.getOverallRating() != null || omicTool.getProblemReported() != null) {
            List<Quality_> qual = new ArrayList<>();
            if (omicTool.getOverallRating() != null) {
                Quality_ qual1 = new Quality_();
                qual1.setType("User Rating");
                qual1.setValue(omicTool.getOverallRating().trim());
                qual.add(qual1);
            }
            if (omicTool.getProblemReported() != null) {
                Quality_ qual1 = new Quality_();
                qual1.setType("Number of Problems Reported");
                qual1.setValue(omicTool.getProblemReported().trim());
                qual.add(qual1);
            }
            bets.setQuality(qual);
        }

        //Contact
        if (omicTool.getEmail() != null) {
            Contact con = new Contact();
            con.setEmail(omicTool.getEmail().trim());
            bets.setContact(con);
        }

        //License
        if (omicTool.getLicense() != null) {
            String[] licenses = {omicTool.getLicense()};
            if (omicTool.getLicense().contains(",")) {
                licenses = omicTool.getLicense().split(",");
            }
            ArrayList lic = new ArrayList<>();
            for (int i = 0; i < licenses.length; i++) {
                lic.add(licenses[i].trim());
            }
            bets.setLicense(lic);
        }

        //Reference
        if (omicTool.getArticle() != null || omicTool.getPubMed() != null) {
            List<Reference> refs = new ArrayList<>();
            Reference newRef = new Reference();
            if (omicTool.getArticle() != null) {
                newRef.setName(omicTool.getArticle().trim());
                if (omicTool.getArticle().contains(")")) {
                    int location = omicTool.getArticle().indexOf(")");
                    String realArticle = omicTool.getArticle().substring(location + 1).trim();
                    newRef.setName(realArticle);
                }
            }
            if (omicTool.getPubMed() != null) {
                newRef.setUrl(omicTool.getPubMed().trim());
            }
            refs.add(newRef);
            bets.setReferences(refs);
        }

        //Links
        List<Link> links = new ArrayList<Link>();

        if (omicTool.getURL() != null && omicTool.getURL().length() > 0) {
            Link newLink = new Link();
            newLink.setType("Homepage");
            newLink.setDescription("URL of Tool");
            newLink.setUrl(omicTool.getURL().trim());
            links.add(newLink);
        }

        if (omicTool.getMailingList() != null && omicTool.getMailingList().length() > 0) {
            Link newLink = new Link();
            newLink.setType("Mailing List");
            newLink.setUrl(omicTool.getMailingList().trim());
            links.add(newLink);
        }

        if (omicTool.getDocumentation() != null && omicTool.getDocumentation().length() > 0) {
            Link newLink = new Link();
            newLink.setType("Documentation");
            newLink.setUrl(omicTool.getDocumentation().trim());
            links.add(newLink);
        }

        if (omicTool.getForum() != null && omicTool.getForum().length() > 0) {
            Link newLink = new Link();
            newLink.setType("Forum");
            newLink.setUrl(omicTool.getForum().trim());
            links.add(newLink);
        }

        if (omicTool.getFunding() != null && omicTool.getFunding().length() > 0) {
            Link newLink = new Link();
            newLink.setType("Funding");
            newLink.setUrl(omicTool.getFunding().trim());
            links.add(newLink);
        }

        if (links.size() > 0) {
            bets.setLinks(links);
        }

        //Programming Language
        if (omicTool.getLanguage() != null && omicTool.getLanguage().length() > 0) {
            String[] languages = {omicTool.getLanguage()};
            if (omicTool.getLanguage().contains(",")) {
                languages = omicTool.getLanguage().split(",");
            }
            ArrayList<Programming_language> lang = new ArrayList<>();
            for (int i = 0; i < languages.length; i++) {
                Programming_language l = new Programming_language();
                l.setName(languages[i].trim());
                lang.add(l);
            }
            bets.setProgramming_language(lang);
        }

        //Operating System
        if (omicTool.getOperatingSystem() != null && omicTool.getOperatingSystem().length() > 0) {
            String[] systems = {omicTool.getOperatingSystem()};
            if (omicTool.getOperatingSystem().contains(",")) {
                systems = omicTool.getOperatingSystem().split(",");
            }
            ArrayList<Operating_system> sys = new ArrayList<>();
            for (int i = 0; i < systems.length; i++) {
                Operating_system s = new Operating_system();
                s.setName(systems[i].trim());
                sys.add(s);
            }
            bets.setOperating_system(sys);
        }

        //Technology
        if (omicTool.getBiologicalTechnology() != null && omicTool.getBiologicalTechnology().length() > 0) {
            String[] techs = {omicTool.getBiologicalTechnology()};
            if (omicTool.getBiologicalTechnology().contains(",")) {
                techs = omicTool.getBiologicalTechnology().split(",");
            }
            ArrayList<Technology> tech = new ArrayList<>();
            for (int i = 0; i < techs.length; i++) {
                Technology t = new Technology();
                t.setName(techs[i].trim());
                tech.add(t);
            }
            bets.setTechnology(tech);
        }

        //Limitations
        if (omicTool.getAnyRestrictionsToUse() != null && omicTool.getAnyRestrictionsToUse().length() > 0) {
            bets.setLimitations(omicTool.getAnyRestrictionsToUse().trim());
        }

        //Category
        if (omicTool.getCategories() != null && omicTool.getCategories().size() > 0) {
            Category cat = new Category();
            List<Application> apps = new ArrayList<>();
            for(int i = 0; i < omicTool.getCategories().size() - 1; i++) { // the last 'Category' is the name of the tool, so skip that one
                Application app = new Application();
                app.setName(omicTool.getCategories().get(i));
                app.setDefinitions("OMIC Tool Category Position: #" + i);
                apps.add(app);
            }
            cat.setApplication(apps);
            bets.setCategory(cat);
        }

        //Validation Date
        if (omicTool.getValidationDate() != null && omicTool.getValidationDate().length() > 0) {
            bets.setValidation_date(omicTool.getValidationDate().trim());
        }

        //Nature of Tool
        if (omicTool.getNatureOfTool() != null && omicTool.getNatureOfTool().length() > 0) {
            String[] natures = {omicTool.getNatureOfTool()};
            if (omicTool.getNatureOfTool().contains(",")) {
                natures = omicTool.getNatureOfTool().split(",");
            }
            ArrayList<String> n = new ArrayList<>();
            for (int i = 0; i < natures.length; i++) {
                n.add(natures[i].trim());
            }
            bets.setNature_of_tool(n);
        }

        //Interface
        if (omicTool.getInterface() != null && omicTool.getInterface().length() > 0) {
            String[] interfaces = {omicTool.getInterface()};
            if (omicTool.getInterface().contains(",")) {
                interfaces = omicTool.getInterface().split(",");
            }
            ArrayList<String> n = new ArrayList<>();
            for (int i = 0; i < interfaces.length; i++) {
                n.add(interfaces[i].trim());
            }
            bets.setInterfaces(n);
        }

        //Type of Tool
        if (omicTool.getTypeOfTool()!= null && omicTool.getTypeOfTool().length() > 0) {
            String[] types = {omicTool.getTypeOfTool()};
            if (omicTool.getTypeOfTool().contains(",")) {
                types = omicTool.getTypeOfTool().split(",");
            }
            ArrayList<String> n = new ArrayList<>();
            for (int i = 0; i < types.length; i++) {
                n.add(types[i].trim());
            }
            bets.setTool_type(n);
        }

        //TODO: Icon
        
        
        //----------------OMIC Specific-----------------------------
        OMIC_specific omic = new OMIC_specific();

        //RRID
        if (omicTool.getRRID() != null && omicTool.getRRID().length() > 0) {
            omic.setRRID(omicTool.getRRID().trim());
        }

        //Depends
        if (omicTool.getDepends() != null && omicTool.getDepends().length() > 0) {
            omic.setDependencies(omicTool.getDepends().trim());
        }

        //Code maturity
        if (omicTool.getCodeMaturity() != null && omicTool.getCodeMaturity().length() > 0) {
            String[] mats = {omicTool.getCodeMaturity()};
            if (omicTool.getCodeMaturity().contains(",")) {
                mats = omicTool.getCodeMaturity().split(",");
            }
            ArrayList<String> m = new ArrayList<>();
            for (int i = 0; i < mats.length; i++) {
                m.add(mats[i].trim());
            }
            omic.setCode_maturity(m);
        }

        //Other Information
        if (omicTool.getOtherInformation() != null && omicTool.getOtherInformation().length() > 0) {
            omic.setOther_information(omicTool.getOtherInformation().trim());
        }

        bets.setOmic_specific(omic);

        return bets;
    }
}
