/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usd.edu.btl.betsconverter.SeqV1;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "id",
    "author",
    "year",
    "journal",
    "name",
    "reference_describes"
})
public class Reference {

    @JsonProperty("label")
    private String label;
    @JsonProperty("author")
    private List<String> author;
    @JsonProperty("year")
    private List<String> year;
    @JsonProperty("journal")
    private List<String> journal;
    @JsonProperty("title")
    private List<String> title;
    @JsonProperty("reference_describes")
    private Object referenceDescribes;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return The label
     */
    @JsonProperty("label")
    public String getLabel() {
        return label;
    }

    /**
     *
     * @param author The label
     */
    @JsonProperty("label")
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     *
     * @return The author
     */
    @JsonProperty("author")
    public List<String> getAuthor() {
        return author;
    }

    /**
     *
     * @param author The author
     */
    @JsonProperty("author")
    public void setAuthor(List<String> author) {
        this.author = author;
    }

    /**
     *
     * @return The year
     */
    @JsonProperty("year")
    public List<String> getYear() {
        return year;
    }

    /**
     *
     * @param year The year
     */
    @JsonProperty("year")
    public void setYear(List<String> year) {
        this.year = year;
    }

    /**
     *
     * @return The journal
     */
    @JsonProperty("journal")
    public List<String> getJournal() {
        return journal;
    }

    /**
     *
     * @param journal The journal
     */
    @JsonProperty("journal")
    public void setJournal(List<String> journal) {
        this.journal = journal;
    }

    /**
     *
     * @return The title
     */
    @JsonProperty("title")
    public List<String> getTitle() {
        return title;
    }

    /**
     *
     * @param title The title
     */
    @JsonProperty("title")
    public void setTitle(List<String> title) {
        this.title = title;
    }

    /**
     *
     * @return The referenceDescribes
     */
    @JsonProperty("reference_describes")
    public Object getReferenceDescribes() {
        return referenceDescribes;
    }

    /**
     *
     * @param referenceDescribes The reference_describes
     */
    @JsonProperty("reference_describes")
    public void setReferenceDescribes(Object referenceDescribes) {
        this.referenceDescribes = referenceDescribes;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
