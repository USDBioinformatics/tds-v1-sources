package org.usd.edu.btl.converters;

import java.util.ArrayList;
import java.util.List;
import org.usd.edu.btl.betsconverter.BETSV1.Application;
import org.usd.edu.btl.betsconverter.BETSV1.Availability;
import org.usd.edu.btl.betsconverter.BETSV1.BETSV1;
import org.usd.edu.btl.betsconverter.BETSV1.Category;
import org.usd.edu.btl.betsconverter.BETSV1.Galaxy_specific;
import org.usd.edu.btl.betsconverter.BETSV1.Input;
import org.usd.edu.btl.betsconverter.BETSV1.Link;
import org.usd.edu.btl.betsconverter.BETSV1.Output;
import org.usd.edu.btl.betsconverter.BETSV1.Quality;
import org.usd.edu.btl.betsconverter.BETSV1.Quality_;
import org.usd.edu.btl.betsconverter.GalaxyV1.Conditional;
import org.usd.edu.btl.betsconverter.GalaxyV1.Data;
import org.usd.edu.btl.betsconverter.GalaxyV1.Inputs;
import org.usd.edu.btl.betsconverter.GalaxyV1.Param;
import org.usd.edu.btl.betsconverter.GalaxyV1.Requirement;
import org.usd.edu.btl.betsconverter.GalaxyV1.Repeat;
import org.usd.edu.btl.betsconverter.GalaxyV1.Requirements;
import org.usd.edu.btl.betsconverter.GalaxyV1.Tool;

/**
 *
 * @author Tyler.Jones
 */
public class GalaxyConverter {

    /**
     *
     * @param galaxy
     * @return
     */
    public static BETSV1 toBETS(Tool galaxy) {
        BETSV1 bets = new BETSV1();

        bets.setName(galaxy.getId());
        bets.setDisplay_name(galaxy.getName());
        bets.setVersion(galaxy.getVersion());
        bets.setSummary(galaxy.getHelp());
        bets.setDescription(galaxy.getDescription());

        //Set Inputs 
        if (galaxy.getInputs() != null) {
            Inputs ins = galaxy.getInputs();
            if (ins.getInputElement() != null && ins.getInputElement().size() > 0) {
                List<Input> inputList = new ArrayList<>();
                Input newInput;
                List<Object> elements = ins.getInputElement();
                for (Object o : elements) {
                    List<Param> pars = new ArrayList<>();
                    Param par = null;
                    if (o.getClass() == Param.class) {
                        par = (Param) o;
                        pars.add(par);
                    } else if (o.getClass() == Conditional.class) {
                        Conditional con = (Conditional) o;
                        if (con.getParam() != null) {
                            par = con.getParam();
                            pars.add(par);
                        }
                    } else if (o.getClass() == Repeat.class) {
                        pars.addAll(((Repeat) o).getInputElement());
                    }
                    //Use param to set values of newInput
                    for (int i = 0; i < pars.size(); i++) {
                        par = pars.get(i);
                        newInput = new Input();
                        if (par.getType() != null && par.getType().value() != null) {
                            newInput.setType(par.getType().value());
                        }
                        if (par.getLabel1() != null) {
                            newInput.setName(par.getLabel1());
                        }
                        if (par.getFormat() != null) {
                            newInput.setFormat(par.getFormat());
                        }
                        if (par.getHelp() != null) {
                            newInput.setDescription(par.getHelp());
                        }
                        if (par.getValue() != null) {
                            newInput.setDefault(par.getValue());
                        }
                        if (par.getOptional() != null && par.getOptional().toLowerCase().equals("true")) {
                            newInput.setRequired(Boolean.TRUE);
                        } else if (par.getOptional() != null && par.getOptional().toLowerCase().equals("false")) {
                            newInput.setRequired(Boolean.FALSE);
                        }
                        inputList.add(newInput);
                    }

                }
                bets.setInputs(inputList);
            }
        }

        //Set Outputs
        if (galaxy.getOutputs() != null) {
            if (galaxy.getOutputs().getData() != null && galaxy.getOutputs().getData().size() > 0) {
                List<Output> outputList = new ArrayList<>();
                for (Data d : galaxy.getOutputs().getData()) {
                    Output newOutput = new Output();
                    if (d.getName() != null) {
                        newOutput.setName(d.getName());
                    }
                    if (d.getFormat() != null) {
                        newOutput.setFormat(d.getFormat());
                    }
                    outputList.add(newOutput);
                }
                bets.setOutputs(outputList);
            }
        }

        //Set ToolType
        if (galaxy.getToolType() != null && galaxy.getToolType().value() != null) {
            List<String> type = new ArrayList<>();
            type.add(galaxy.getToolType().value());
            bets.setTool_type(type);
        }

        //Set Parallelism
        if (galaxy.getParallelism() != null) {
            if (galaxy.getParallelism().getMethod() != null) {
                if (galaxy.getParallelism().getMethod().value() != null) {
                    bets.setParallelism(galaxy.getParallelism().getMethod().value());
                }
            }
        }

        //Set Requirements
        if (galaxy.getRequirements() != null) {
            Requirements r = galaxy.getRequirements();
            String req = "";
            if (r.getRequirement() != null && r.getRequirement().size() > 0) {
                try {
                    req += r.getRequirement().get(0).getValue() + " (" + r.getRequirement().get(0).getVersion() + ")";
                } catch (Exception e) {
                }
            }
            if (r.getRequirement() != null && r.getRequirement().size() > 1) {
                for (int i = 1; i < r.getRequirement().size(); i++) {
                    try {
                        req += ", " + r.getRequirement().get(i).getValue() + " (" + r.getRequirement().get(i).getVersion() + ")";
                    } catch (Exception e) {
                    }
                }
            }
            bets.setRequirements(req);
        }

        //Set Links
        if (galaxy.getRepoLink() != null) {
            List<Link> links = new ArrayList<>();
            Link link = new Link();
            link.setType("Homepage");
            link.setDescription("URL of Tool");
            link.setUrl(galaxy.getRepoLink());
        }

        //Set Availability
        if (galaxy.getAvailable() != null) {
            List<Availability> avail = new ArrayList<>();
            Availability newAvail = new Availability();
            newAvail.setStatus(galaxy.getAvailable());
            avail.add(newAvail);
            bets.setAvailability(avail);
        }

        //Set Owner
        if (galaxy.getOwner() != null) {
            bets.setOwner(galaxy.getOwner());
        }

        //Set Quality
        if (galaxy.getQuality() != null || galaxy.getVerified() != null) {
            Quality_ qual = new Quality_();
            if (galaxy.getQuality() != null) {
                qual.setValue(galaxy.getQuality());
            }
            if (galaxy.getVerified() != null) {
                qual.setType(galaxy.getVerified());
            }
            List<Quality_> qualList = new ArrayList<>();
            qualList.add(qual);
            bets.setQuality(qualList);
        }

        //Set Category
        if (galaxy.getCategories() != null && galaxy.getCategories().getCategory().size() > 0) {
            Category cat = new Category();
            List<Application> applications = new ArrayList<>();
            for (org.usd.edu.btl.betsconverter.GalaxyV1.Category c : galaxy.getCategories().getCategory()) {
                if (c.getValue() != null) {
                    Application a = new Application();
                    a.setName(c.getValue());
                    a.setDefinitions(c.getDescription());
                    applications.add(a);
                }
            }
            cat.setApplication(applications);
            bets.setCategory(cat);
        }

        //Set Galaxy Specific
        Galaxy_specific galSpec = new Galaxy_specific();
        if (galaxy.getRepoLink() != null) {
            galSpec.setRepo_link(galaxy.getRepoLink());
        }
        if (galaxy.getRevision() != null) {
            galSpec.setRepo_revision(galaxy.getRevision());
        }
        bets.setGalaxy_specific(galSpec);
        return bets;
    }

}
