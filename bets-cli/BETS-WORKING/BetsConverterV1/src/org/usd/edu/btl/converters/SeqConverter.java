package org.usd.edu.btl.converters;

import java.util.ArrayList;
import java.util.List;
import org.usd.edu.btl.betsconverter.BETSV1.Application;
import org.usd.edu.btl.betsconverter.BETSV1.BETSV1;
import org.usd.edu.btl.betsconverter.BETSV1.Category;
import org.usd.edu.btl.betsconverter.BETSV1.Contact;
import org.usd.edu.btl.betsconverter.BETSV1.Domain;
import org.usd.edu.btl.betsconverter.BETSV1.Input;
import org.usd.edu.btl.betsconverter.BETSV1.License;
import org.usd.edu.btl.betsconverter.BETSV1.Link;
import org.usd.edu.btl.betsconverter.BETSV1.Method;
import org.usd.edu.btl.betsconverter.BETSV1.Operating_system;
import org.usd.edu.btl.betsconverter.BETSV1.Output;
import org.usd.edu.btl.betsconverter.BETSV1.Programming_language;
import org.usd.edu.btl.betsconverter.BETSV1.Reference;
import org.usd.edu.btl.betsconverter.BETSV1.Reference__;
import org.usd.edu.btl.betsconverter.BETSV1.Seq_specific;
import org.usd.edu.btl.betsconverter.BETSV1.Technology;
import org.usd.edu.btl.betsconverter.SeqV1.SeqV1;

/**
 *
 * @author Tyler.Jones
 */
public class SeqConverter {

    /**
     *
     * @param seqTool
     * @return
     */
    public static BETSV1 toBETS(SeqV1 seqTool) {
        BETSV1 bets = new BETSV1();
        bets.setName(seqTool.getLabel());

        //Seq summary ARRAY OF SUMMARY STRINGS to BETS STRING
        //NEED TO LOOK AT !!!!!!!!!!!!!!!!!!! 
        if (seqTool.getSummary().isEmpty()) {
            bets.setSummary("NO SUMMARY AVAILABLE");
        } else {
            List<String> summaryList = seqTool.getSummary();
            bets.setSummary(summaryList.get(0));
        }

        //set bets Version
        if (seqTool.getSoftwareVersion() != null) {
            bets.setVersion(seqTool.getSoftwareVersion().toString().replace("[", "").replace("]", ""));
        }

        //set bets Description
        if (seqTool.getDescription() == null || seqTool.getDescription().isEmpty()) {
            bets.setDescription("NO DESCRIPTION AVAILABLE");
        } else {
            bets.setDescription(seqTool.getDescription());
        }

        //set bets Category
        //List<Category> betsCatList = new ArrayList<>();
        Category betsCategory = new Category();

        //Seq bio_tags to BETS category.domain
        List<Domain> betsDoms = new ArrayList<>();
        List<String> bioTagStrings = seqTool.getBioTags();

        for (int i = 0; i < bioTagStrings.size(); i++) {
            Domain betsDom = new Domain();
            String bioTagStr = bioTagStrings.get(i);
            betsDom.setName(bioTagStr);
            betsDoms.add(betsDom);
        }

        betsCategory.setDomain(betsDoms);

        //set method
        //Seq meth_tags to BETS Catgory.method[]
        List<Method> betsMeths = new ArrayList<>();
        List<String> methStrings = seqTool.getMethTags();

        //for each method, add it to the method object
        for (int i = 0; i < methStrings.size(); i++) {
            Method betsMeth = new Method();
            String methStr = methStrings.get(i);
            betsMeth.setName(methStr);
            betsMeths.add(betsMeth);
        }
        //add the meths list to category.method
        betsCategory.setMethod(betsMeths);

        // betsCatList.add(betsCategory);
        bets.setCategory(betsCategory);

        //seq language to BETS Programming_language
        List<Programming_language> pgmLangs = new ArrayList<>();
        //get array of pgmLang name STRINGS
        List<String> pgmLangStrings = seqTool.getLanguage(); //get array of strings

        for (int i = 0; i < pgmLangStrings.size(); i++) {
            String pgmLangName = pgmLangStrings.get(i); //get first pgmLang Name
            Programming_language betsPgmLang = new Programming_language(); //create new BETS pgmLang obj
            betsPgmLang.setName(pgmLangName); //set name of BETS obj
            pgmLangs.add(betsPgmLang); //add BETS pgmLang Object to pgmLang List
        }
        bets.setProgramming_language(pgmLangs);

        //Seq os to Bets Operating_system
        List<Operating_system> betsOSList = new ArrayList<>();
        List<String> osStrings = seqTool.getOs();
        for (int i = 0; i < osStrings.size(); i++) {
            String osName = osStrings.get(i);
            Operating_system os = new Operating_system();
            os.setName(osName);
            betsOSList.add(os);
        }
        bets.setOperating_system(betsOSList);

        //Set Bets References
        List<Reference> references = new ArrayList<Reference>();
        if (seqTool.getReferences() != null && seqTool.getReferences().size() > 0) {
            for (org.usd.edu.btl.betsconverter.SeqV1.Reference oldRef : seqTool.getReferences()) {
                Reference ref = new Reference();
                if (oldRef.getTitle() != null) {
                    ref.setName(oldRef.getTitle().toString().replace("[", "").replace("]", ""));

                }
                if (oldRef.getLabel() != null) {
                    ref.setId(oldRef.getLabel().replace("[", "").replace("]", ""));
                }
                references.add(ref);
            }
        }
        bets.setReferences(references);

        //seq hits, cited, references to BETS seq_specific
        Seq_specific seqSpec = new Seq_specific();
        if (seqTool.getPmPageCounter() != null) {
            seqSpec.setHits(seqTool.getPmPageCounter().toString().replace("[", "").replace("]", ""));
        }
        if (seqTool.getNumberOfCitations() != null) {
            seqSpec.setCited(seqTool.getNumberOfCitations().toString().replace("[", "").replace("]", ""));
        }

        List<Reference__> refs = new ArrayList<>();
        if (seqTool.getReferences() != null && seqTool.getReferences().size() > 0) {
            for (org.usd.edu.btl.betsconverter.SeqV1.Reference oldRef : seqTool.getReferences()) {
                Reference__ ref = new Reference__();
                if (oldRef.getTitle() != null && oldRef.getTitle().size() > 0) {
                    ref.setTitle(oldRef.getTitle().toString().replace("[", "").replace("]", ""));
                }
                try {
                    if (oldRef.getLabel() != null) {
                        ref.setLabel(oldRef.getLabel());
                    }
                    if (oldRef.getAuthor() != null && oldRef.getAuthor().toString() != null) {
                        ref.setAuthor(oldRef.getAuthor().toString().replace("[", "").replace("]", ""));
                    }
                    if (oldRef.getJournal() != null && oldRef.getJournal().toString() != null) {
                        ref.setJournal(oldRef.getJournal().toString().replace("[", "").replace("]", ""));
                    }
                    if (oldRef.getYear() != null && oldRef.getYear().toString() != null) {
                        ref.setYear(oldRef.getYear().toString().replace("[", "").replace("]", ""));
                    }
                } catch (Exception e) {
                }
                refs.add(ref);
            }

        }
        seqSpec.setReferences(refs);

        //Set Seq_Specific
        bets.setSeq_specific(seqSpec);

        //Set Links
        List<Link> links = new ArrayList<>();
        if (seqTool.getLinks() != null && seqTool.getLinks().size() > 0) {
            for (org.usd.edu.btl.betsconverter.SeqV1.Link oldLink : seqTool.getLinks()) {
                Link newLink = new Link();
                if (oldLink.getUrlDescribes() != null && oldLink.getUrlType() != null) {
                    newLink.setName(oldLink.getUrlDescribes().toString().replace("[", "").replace("]", "") + " " + oldLink.getUrlType().toString().replace("[", "").replace("]", ""));
                    newLink.setDescription(oldLink.getUrlType().toString().replace("[", "").replace("]", ""));
                    newLink.setType(oldLink.getUrlType().toString().replace("[", "").replace("]", ""));
                    links.add(newLink);
                }
            }
        }
        bets.setLinks(links);

        //Set bets technology
        if (seqTool.getBiologicalTechnology() != null && seqTool.getBiologicalTechnology().size() > 0) {
            List<Technology> tech = new ArrayList<>();
            Technology newTech;
            for (String technology : seqTool.getBiologicalTechnology()) {
                newTech = new Technology();
                newTech.setName(technology);
                tech.add(newTech);
            }
            bets.setTechnology(tech);
        }

        //Set bets Created At
        if (seqTool.getInstitute() != null && seqTool.getInstitute().size() > 0) {
            bets.setCreated_at(seqTool.getInstitute().toString().replace("[", "").replace("]", ""));
        }

        //Set bets Contact
        if (seqTool.getEmailAddress() != null && seqTool.getEmailAddress().toString() != null) {
            Contact con = new Contact();
            con.setName("Primary Contact");
            con.setEmail(seqTool.getEmailAddress().toString().replace("[", "").replace("]", ""));
            bets.setContact(con);
        }

        //Set bets Author
        if (seqTool.getCreatedBy() != null && seqTool.getCreatedBy().size() > 0) {
            String auths = seqTool.getCreatedBy().toString().replace("[", "").replace("]", "");
            bets.setAuthor(auths);
        }

        //Set bets inputs
        if (seqTool.getInputFormat() != null && seqTool.getInputFormat().size() > 0) {
            List<Input> inputs = new ArrayList<>();
            Input newInput;
            for (String old : seqTool.getInputFormat()) {
                newInput = new Input();
                newInput.setFormat(old);
                inputs.add(newInput);
            }
            bets.setInputs(inputs);
        }

        //Set bets outputs
        if (seqTool.getOutputFormat() != null && seqTool.getOutputFormat().size() > 0) {
            List<Output> outputs = new ArrayList<>();
            Output newOutput;
            for (String old : seqTool.getOutputFormat()) {
                newOutput = new Output();
                newOutput.setFormat(old);
                outputs.add(newOutput);
            }
            bets.setOutputs(outputs);
        }

        //Set bets maintained
        if (seqTool.getMaintained() != null && seqTool.getMaintained().size() > 0) {
            bets.setMaintained(seqTool.getMaintained().get(0));
        }

        //Set bets Software Library
        if (seqTool.getLibrary() != null && seqTool.getLibrary().size() > 0) {
            String lib = seqTool.getLibrary().toString().replace("[", "").replace("]", "");
            bets.setSoftware_libraries(lib);
        }

        //Set bets features
        if (seqTool.getFeatures() != null && seqTool.getFeatures().size() > 0) {
            bets.setSoftware_features(seqTool.getFeatures().get(0));
        }

        //Set bets license
        if (seqTool.getLicence() != null && seqTool.getLicence().size() > 0) {
            List<License> lic = new ArrayList<>();
            License newLicense = new License();
            newLicense.setName(seqTool.getLicence().get(0));
            lic.add(newLicense);
            bets.setLicense(lic);
        }

        //return the bets tool
        return bets;
    }

}
