/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usd.edu.btl.betsconverter.OMICToolsV1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 *
 * @author Shayla.Gustafson
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "Overall rating",
    "Description",
    "Email",
    "Categories",
    "Biological technology",
    "Article",
    "License",
    "PubMed",
    "URL",
    "Name",
    "Current release",
    "Maintainer",
    "Validation date",
    "Input",
    "Language",
    "Output",
    "Nature of tool",
    "Problem reported",
    "RRID",
    "Interface",
    "Operating system",
    "Type of tool",
    "Created at",
    "Mailing list",
    "Project long name",
    "Code maturity",
    "Any restrictions to use",
    "Parallel computation",
    "Depends",
    "Documentation",
    "Forum",
    "Funding",
    "Other information"
})
public class OMICToolV1 {

    @JsonProperty("Overall rating")
    private String OverallRating;
    @JsonProperty("Description")
    private String Description;
    @JsonProperty("Email")
    private String Email;
    @JsonProperty("Categories")
    private List<String> Categories = new ArrayList<String>();
    @JsonProperty("Biological technology")
    private String BiologicalTechnology;
    @JsonProperty("Article")
    private String Article;
    @JsonProperty("License")
    private String License;
    @JsonProperty("PubMed")
    private String PubMed;
    @JsonProperty("URL")
    private String URL;
    @JsonProperty("Name")
    private String Name;
    @JsonProperty("Current release")
    private String CurrentRelease;
    @JsonProperty("Maintainer")
    private String Maintainer;
    @JsonProperty("Validation date")
    private String ValidationDate;
    @JsonProperty("Input")
    private String Input;
    @JsonProperty("Language")
    private String Language;
    @JsonProperty("Output")
    private String Output;
    @JsonProperty("Nature of tool")
    private String NatureOfTool;
    @JsonProperty("Problem reported")
    private String ProblemReported;
    @JsonProperty("RRID")
    private String RRID;
    @JsonProperty("Interface")
    private String Interface;
    @JsonProperty("Operating system")
    private String OperatingSystem;
    @JsonProperty("Type of tool")
    private String TypeOfTool;
    @JsonProperty("Created at")
    private String CreatedAt;
    @JsonProperty("Mailing list")
    private String MailingList;
    @JsonProperty("Project long name")
    private String ProjectLongName;
    @JsonProperty("Code maturity")
    private String CodeMaturity;
    @JsonProperty("Any restrictions to use")
    private String AnyRestrictionsToUse;
    @JsonProperty("Parallel computation")
    private String ParallelComputation;
    @JsonProperty("Depends")
    private String Depends;
    @JsonProperty("Documentation")
    private String Documentation;
    @JsonProperty("Forum")
    private String Forum;
    @JsonProperty("Funding")
    private String Funding;
    @JsonProperty("Other information")
    private String OtherInformation;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return The OverallRating
     */
    @JsonProperty("Overall rating")
    public String getOverallRating() {
        return OverallRating;
    }

    /**
     *
     * @param OverallRating The Overall rating
     */
    @JsonProperty("Overall rating")
    public void setOverallRating(String OverallRating) {
        this.OverallRating = OverallRating;
    }

    /**
     *
     * @return The Description
     */
    @JsonProperty("Description")
    public String getDescription() {
        return Description;
    }

    /**
     *
     * @param Description The Description
     */
    @JsonProperty("Description")
    public void setDescription(String Description) {
        this.Description = Description;
    }

    /**
     *
     * @return The Email
     */
    @JsonProperty("Email")
    public String getEmail() {
        return Email;
    }

    /**
     *
     * @param Email The Email
     */
    @JsonProperty("Email")
    public void setEmail(String Email) {
        this.Email = Email;
    }

    /**
     *
     * @return The Categories
     */
    @JsonProperty("Categories")
    public List<String> getCategories() {
        return Categories;
    }

    /**
     *
     * @param Categories The Categories
     */
    @JsonProperty("Categories")
    public void setCategories(List<String> Categories) {
        this.Categories = Categories;
    }

    /**
     *
     * @return The BiologicalTechnology
     */
    @JsonProperty("Biological technology")
    public String getBiologicalTechnology() {
        return BiologicalTechnology;
    }

    /**
     *
     * @param BiologicalTechnology The Biological technology
     */
    @JsonProperty("Biological technology")
    public void setBiologicalTechnology(String BiologicalTechnology) {
        this.BiologicalTechnology = BiologicalTechnology;
    }

    /**
     *
     * @return The Article
     */
    @JsonProperty("Article")
    public String getArticle() {
        return Article;
    }

    /**
     *
     * @param Article The Article
     */
    @JsonProperty("Article")
    public void setArticle(String Article) {
        this.Article = Article;
    }

    /**
     *
     * @return The License
     */
    @JsonProperty("License")
    public String getLicense() {
        return License;
    }

    /**
     *
     * @param License The License
     */
    @JsonProperty("License")
    public void setLicense(String License) {
        this.License = License;
    }

    /**
     *
     * @return The PubMed
     */
    @JsonProperty("PubMed")
    public String getPubMed() {
        return PubMed;
    }

    /**
     *
     * @param PubMed The PubMed
     */
    @JsonProperty("PubMed")
    public void setPubMed(String PubMed) {
        this.PubMed = PubMed;
    }

    /**
     *
     * @return The URL
     */
    @JsonProperty("URL")
    public String getURL() {
        return URL;
    }

    /**
     *
     * @param URL The URL
     */
    @JsonProperty("URL")
    public void setURL(String URL) {
        this.URL = URL;
    }

    /**
     *
     * @return The Name
     */
    @JsonProperty("Name")
    public String getName() {
        return Name;
    }

    /**
     *
     * @param Name The Name
     */
    @JsonProperty("Name")
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     *
     * @return The CurrentRelease
     */
    @JsonProperty("Current release")
    public String getCurrentRelease() {
        return CurrentRelease;
    }

    /**
     *
     * @param CurrentRelease The Current release
     */
    @JsonProperty("Current release")
    public void setCurrentRelease(String CurrentRelease) {
        this.CurrentRelease = CurrentRelease;
    }

    /**
     *
     * @return The Maintainer
     */
    @JsonProperty("Maintainer")
    public String getMaintainer() {
        return Maintainer;
    }

    /**
     *
     * @param Maintainer The Maintainer
     */
    @JsonProperty("Maintainer")
    public void setMaintainer(String Maintainer) {
        this.Maintainer = Maintainer;
    }

    /**
     *
     * @return The ValidationDate
     */
    @JsonProperty("Validation date")
    public String getValidationDate() {
        return ValidationDate;
    }

    /**
     *
     * @param ValidationDate The Validation date
     */
    @JsonProperty("Validation date")
    public void setValidationDate(String ValidationDate) {
        this.ValidationDate = ValidationDate;
    }

    /**
     *
     * @return The Input
     */
    @JsonProperty("Input")
    public String getInput() {
        return Input;
    }

    /**
     *
     * @param Input The Input
     */
    @JsonProperty("Input")
    public void setInput(String Input) {
        this.Input = Input;
    }

    /**
     *
     * @return The Language
     */
    @JsonProperty("Language")
    public String getLanguage() {
        return Language;
    }

    /**
     *
     * @param Language The Language
     */
    @JsonProperty("Language")
    public void setLanguage(String Language) {
        this.Language = Language;
    }

    /**
     *
     * @return The Output
     */
    @JsonProperty("Output")
    public String getOutput() {
        return Output;
    }

    /**
     *
     * @param Output The Output
     */
    @JsonProperty("Output")
    public void setOutput(String Output) {
        this.Output = Output;
    }

    /**
     *
     * @return The NatureOfTool
     */
    @JsonProperty("Nature of tool")
    public String getNatureOfTool() {
        return NatureOfTool;
    }

    /**
     *
     * @param NatureOfTool The Nature of tool
     */
    @JsonProperty("Nature of tool")
    public void setNatureOfTool(String NatureOfTool) {
        this.NatureOfTool = NatureOfTool;
    }

    /**
     *
     * @return The ProblemReported
     */
    @JsonProperty("Problem reported")
    public String getProblemReported() {
        return ProblemReported;
    }

    /**
     *
     * @param ProblemReported The Problem reported
     */
    @JsonProperty("Problem reported")
    public void setProblemReported(String ProblemReported) {
        this.ProblemReported = ProblemReported;
    }

    /**
     *
     * @return The RRID
     */
    @JsonProperty("RRID")
    public String getRRID() {
        return RRID;
    }

    /**
     *
     * @param RRID The RRID
     */
    @JsonProperty("RRID")
    public void setRRID(String RRID) {
        this.RRID = RRID;
    }

    /**
     *
     * @return The Interface
     */
    @JsonProperty("Interface")
    public String getInterface() {
        return Interface;
    }

    /**
     *
     * @param Interface The Interface
     */
    @JsonProperty("Interface")
    public void setInterface(String Interface) {
        this.Interface = Interface;
    }

    /**
     *
     * @return The OperatingSystem
     */
    @JsonProperty("Operating system")
    public String getOperatingSystem() {
        return OperatingSystem;
    }

    /**
     *
     * @param OperatingSystem The Operating system
     */
    @JsonProperty("Operating system")
    public void setOperatingSystem(String OperatingSystem) {
        this.OperatingSystem = OperatingSystem;
    }

    /**
     *
     * @return The TypeOfTool
     */
    @JsonProperty("Type of tool")
    public String getTypeOfTool() {
        return TypeOfTool;
    }

    /**
     *
     * @param TypeOfTool The Type of tool
     */
    @JsonProperty("Type of tool")
    public void setTypeOfTool(String TypeOfTool) {
        this.TypeOfTool = TypeOfTool;
    }

    /**
     *
     * @return The CreatedAt
     */
    @JsonProperty("Created at")
    public String getCreatedAt() {
        return CreatedAt;
    }

    /**
     *
     * @param CreatedAt The Created at
     */
    @JsonProperty("Created at")
    public void setCreatedAt(String CreatedAt) {
        this.CreatedAt = CreatedAt;
    }

    /**
     *
     * @return The MailingList
     */
    @JsonProperty("Mailing list")
    public String getMailingList() {
        return MailingList;
    }

    /**
     *
     * @param MailingList The Mailing list
     */
    @JsonProperty("Mailing list")
    public void setMailingList(String MailingList) {
        this.MailingList = MailingList;
    }

    /**
     *
     * @return The ProjectLongName
     */
    @JsonProperty("Project long name")
    public String getProjectLongName() {
        return ProjectLongName;
    }

    /**
     *
     * @param ProjectLongName The Project long name
     */
    @JsonProperty("Project long name")
    public void setProjectLongName(String ProjectLongName) {
        this.ProjectLongName = ProjectLongName;
    }

    /**
     *
     * @return The CodeMaturity
     */
    @JsonProperty("Code maturity")
    public String getCodeMaturity() {
        return CodeMaturity;
    }

    /**
     *
     * @param CodeMaturity The Code maturity
     */
    @JsonProperty("Code maturity")
    public void setCodeMaturity(String CodeMaturity) {
        this.CodeMaturity = CodeMaturity;
    }

    /**
     *
     * @return The AnyRestrictionsToUse
     */
    @JsonProperty("Any restrictions to use")
    public String getAnyRestrictionsToUse() {
        return AnyRestrictionsToUse;
    }

    /**
     *
     * @param AnyRestrictionsToUse The Any restrictions to use
     */
    @JsonProperty("Any restrictions to use")
    public void setAnyRestrictionsToUse(String AnyRestrictionsToUse) {
        this.AnyRestrictionsToUse = AnyRestrictionsToUse;
    }

    /**
     *
     * @return The ParallelComputation
     */
    @JsonProperty("Parallel computation")
    public String getParallelComputation() {
        return ParallelComputation;
    }

    /**
     *
     * @param ParallelComputation The Parallel computation
     */
    @JsonProperty("Parallel computation")
    public void setParallelComputation(String ParallelComputation) {
        this.ParallelComputation = ParallelComputation;
    }

    /**
     *
     * @return The Depends
     */
    @JsonProperty("Depends")
    public String getDepends() {
        return Depends;
    }

    /**
     *
     * @param Depends The Depends
     */
    @JsonProperty("Depends")
    public void setDepends(String Depends) {
        this.Depends = Depends;
    }

    /**
     *
     * @return The Documentation
     */
    @JsonProperty("Documentation")
    public String getDocumentation() {
        return Documentation;
    }

    /**
     *
     * @param Documentation The Documentation
     */
    @JsonProperty("Documentation")
    public void setDocumentation(String Documentation) {
        this.Documentation = Documentation;
    }

    /**
     *
     * @return The Forum
     */
    @JsonProperty("Forum")
    public String getForum() {
        return Forum;
    }

    /**
     *
     * @param Forum The Forum
     */
    @JsonProperty("Forum")
    public void setForum(String Forum) {
        this.Forum = Forum;
    }

    /**
     *
     * @return The Funding
     */
    @JsonProperty("Funding")
    public String getFunding() {
        return Funding;
    }

    /**
     *
     * @param Funding The Funding
     */
    @JsonProperty("Funding")
    public void setFunding(String Funding) {
        this.Funding = Funding;
    }

    /**
     *
     * @return The OtherInformation
     */
    @JsonProperty("Other information")
    public String getOtherInformation() {
        return OtherInformation;
    }

    /**
     *
     * @param OtherInformation The Other information
     */
    @JsonProperty("Other information")
    public void setOtherInformation(String OtherInformation) {
        this.OtherInformation = OtherInformation;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
