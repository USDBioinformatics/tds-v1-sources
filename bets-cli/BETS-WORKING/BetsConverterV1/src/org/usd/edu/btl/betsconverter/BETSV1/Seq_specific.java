package org.usd.edu.btl.betsconverter.BETSV1;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Generated;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 *
 * @author Tyler.Jones
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "hits",
    "cited",
    "references",
    "art_size",
    "art_score"
})
public class Seq_specific {

    @JsonProperty("hits")
    private String hits;
    @JsonProperty("cited")
    private String cited;
    @JsonProperty("references")
    private List<Reference__> references;
    @JsonProperty("art_size")
    private String art_size;
    @JsonProperty("art_score")
    private String art_score;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     */
    public String getHits() {
        return hits;
    }

    /**
     *
     * @param hits
     */
    public void setHits(String hits) {
        this.hits = hits;
    }

    /**
     *
     * @return
     */
    public String getCited() {
        return cited;
    }

    /**
     *
     * @param cited
     */
    public void setCited(String cited) {
        this.cited = cited;
    }

    /**
     *
     * @return
     */
    public List<Reference__> getReferences() {
        return references;
    }

    /**
     *
     * @param references
     */
    public void setReferences(List<Reference__> references) {
        this.references = references;
    }

    /**
     *
     * @return
     */
    public String getArt_size() {
        return art_size;
    }

    /**
     *
     * @param art_size
     */
    public void setArt_size(String art_size) {
        this.art_size = art_size;
    }

    /**
     *
     * @return
     */
    public String getArt_score() {
        return art_score;
    }

    /**
     *
     * @param art_score
     */
    public void setArt_score(String art_score) {
        this.art_score = art_score;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     *
     * @return
     */
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    /**
     *
     * @param name
     * @param value
     */
    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.hits);
        hash = 47 * hash + Objects.hashCode(this.cited);
        hash = 47 * hash + Objects.hashCode(this.references);
        hash = 47 * hash + Objects.hashCode(this.art_size);
        hash = 47 * hash + Objects.hashCode(this.art_score);
        hash = 47 * hash + Objects.hashCode(this.additionalProperties);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Seq_specific other = (Seq_specific) obj;
        if (!Objects.equals(this.hits, other.hits)) {
            return false;
        }
        if (!Objects.equals(this.cited, other.cited)) {
            return false;
        }
        if (!Objects.equals(this.references, other.references)) {
            return false;
        }
        if (!Objects.equals(this.art_size, other.art_size)) {
            return false;
        }
        if (!Objects.equals(this.art_score, other.art_score)) {
            return false;
        }
        if (!Objects.equals(this.additionalProperties, other.additionalProperties)) {
            return false;
        }
        return true;
    }

}
