#!/usr/bin/env python

import json

def combine_keys(key, obj1, obj2):
    result = {}
    try:
        #try a list?
        result = obj1 + obj2
    except TypeError, e:
        pass #not a list

    try:
        for key2 in obj1:
            result[key2] = combine_keys(key2, obj1[key2], obj2[key2])

    except TypeError, e:
        pass #no idea

    return result;





example1 = open("test_merge_example1.json").read()
example2 = open("test_merge_example2.json").read()

ex1_data = json.loads(example1)
ex2_data = json.loads(example2)

result = {}

for key in ex1_data:
    #result[key] = ex1_data[key] + ex2_data[key]
    result[key] = combine_keys(key, ex1_data[key], ex2_data[key])

with open("test_merge_example_result.json", "w") as outfile:
    json.dump(result, outfile)
