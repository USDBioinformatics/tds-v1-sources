/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usd.edu.btl.betsconverter.BETSV1;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bill Conn <Bill.Conn@usd.edu>
 */
public class BETSV1MergeTest {
    
    public BETSV1MergeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testMerge() throws IOException{
        ObjectMapper mapper = new ObjectMapper();
        File one = new File("test_inputs/test_merge_example1.json");
        File two = new File("test_inputs/test_merge_example2.json");
        File result = new File("test_inputs/test_merge_example_result.json");
        
        BETSV1 bets1 = mapper.readValue(one, BETSV1.class);
        BETSV1 bets2 = mapper.readValue(two, BETSV1.class);
        BETSV1 betsResult = mapper.readValue(result, BETSV1.class);
        BETSV1 betsResult2 = bets1.combine(bets2);
        assertTrue(betsResult.equals(betsResult2));
        
    }
    
    @Test
    public void testMergeDuplicates() throws IOException{
        ObjectMapper mapper = new ObjectMapper();
        File one = new File("test_inputs/test_merge_example_result.json");
        File two = new File("test_inputs/test_merge_example_result.json");
        File result = new File("test_inputs/test_merge_example_result.json");
        
        BETSV1 bets1 = mapper.readValue(one, BETSV1.class);
        BETSV1 bets2 = mapper.readValue(two, BETSV1.class);
        BETSV1 betsResult = mapper.readValue(result, BETSV1.class);
        assertTrue(betsResult.equals(bets1.combine(bets2)));
        
    }
}
