/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usd.edu.btl.betsconverter.BETSV1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bill Conn <Bill.Conn@usd.edu>
 */
public class BETSV1Test {
    
    public BETSV1Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class BETSV1.
     */
    @Test
    public void testGetName() {
        String name = "testname";
        System.out.println("getName");
        BETSV1 instance = new BETSV1();
        instance.setName(name);
        String expResult = name;
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setName method, of class BETSV1.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "testname";
        BETSV1 instance = new BETSV1();
        instance.setName(name);
        assertEquals(name, instance.getName());
    }

    /**
     * Test of getDisplay_name method, of class BETSV1.
     */
    @Test
    public void testGetDisplay_name() {
        System.out.println("getDisplay_name");
        BETSV1 instance = new BETSV1();
        String expResult = "testname";
        instance.setName(expResult);
        String result = instance.getDisplay_name();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDisplay_name method, of class BETSV1.
     */
    @Test
    public void testSetDisplay_name() {
        System.out.println("setDisplay_name");
        String display_name = "testname";
        BETSV1 instance = new BETSV1();
        instance.setDisplay_name(display_name);
        assertEquals(display_name, instance.getDisplay_name());
    }

    /**
     * Test of getVersion method, of class BETSV1.
     */
    @Test
    public void testGetVersion() {
        System.out.println("getVersion");
        BETSV1 instance = new BETSV1();
        String expResult = "1.0";
        instance.setVersion(expResult);
        String result = instance.getVersion();
        assertEquals(expResult, result);
    }

    /**
     * Test of setVersion method, of class BETSV1.
     */
    @Test
    public void testSetVersion() {
        System.out.println("setVersion");
        String version = "1.0";
        BETSV1 instance = new BETSV1();
        instance.setVersion(version);
        assertEquals(version, instance.getVersion());
    }

    /**
     * Test of getSummary method, of class BETSV1.
     */
    @Test
    public void testGetSummary() {
        System.out.println("getSummary");
        BETSV1 instance = new BETSV1();
        String expResult = "This is a summary";
        instance.setSummary(expResult);
        String result = instance.getSummary();
        assertEquals(expResult, result);
    }

    /**
     * Test of setSummary method, of class BETSV1.
     */
    @Test
    public void testSetSummary() {
        System.out.println("setSummary");
        String summary = "This is a summary";
        BETSV1 instance = new BETSV1();
        instance.setSummary(summary);
        assertEquals(summary, instance.getSummary());
    }

    /**
     * Test of getDescription method, of class BETSV1.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        BETSV1 instance = new BETSV1();
        String expResult = "Test Description";
        instance.setDescription(expResult);
        String result = instance.getDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDescription method, of class BETSV1.
     */
    @Test
    public void testSetDescription() {
        System.out.println("setDescription");
        String description = "Test Description";
        BETSV1 instance = new BETSV1();
        instance.setDescription(description);
        assertEquals(description, instance.getDescription());
    }

    /**
     * Test of getLimitations method, of class BETSV1.
     */
    @Test
    public void testGetLimitations() {
        System.out.println("getLimitations");
        BETSV1 instance = new BETSV1();
        String expResult = "Limitations";
        instance.setLimitations(expResult);
        String result = instance.getLimitations();
        assertEquals(expResult, result);
    }

    /**
     * Test of setLimitations method, of class BETSV1.
     */
    @Test
    public void testSetLimitations() {
        System.out.println("setLimitations");
        String limitations = "";
        BETSV1 instance = new BETSV1();
        instance.setLimitations(limitations);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getExample method, of class BETSV1.
     */
    @Test
    public void testGetExample() {
        System.out.println("getExample");
        BETSV1 instance = new BETSV1();
        List<Example> expResult = null;
        List<Example> result = instance.getExample();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setExample method, of class BETSV1.
     */
    @Test
    public void testSetExample() {
        System.out.println("setExample");
        List<Example> example = null;
        BETSV1 instance = new BETSV1();
        instance.setExample(example);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLinks method, of class BETSV1.
     */
    @Test
    public void testGetLinks() {
        System.out.println("getLinks");
        BETSV1 instance = new BETSV1();
        List<Link> expResult = null;
        List<Link> result = instance.getLinks();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setLinks method, of class BETSV1.
     */
    @Test
    public void testSetLinks() {
        System.out.println("setLinks");
        List<Link> links = null;
        BETSV1 instance = new BETSV1();
        instance.setLinks(links);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getReferences method, of class BETSV1.
     */
    @Test
    public void testGetReferences() {
        System.out.println("getReferences");
        BETSV1 instance = new BETSV1();
        List<Reference> expResult = null;
        List<Reference> result = instance.getReferences();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setReferences method, of class BETSV1.
     */
    @Test
    public void testSetReferences() {
        System.out.println("setReferences");
        List<Reference> references = null;
        BETSV1 instance = new BETSV1();
        instance.setReferences(references);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAuthor method, of class BETSV1.
     */
    @Test
    public void testGetAuthor() {
        System.out.println("getAuthor");
        BETSV1 instance = new BETSV1();
        String expResult = "";
        String result = instance.getAuthor();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAuthor method, of class BETSV1.
     */
    @Test
    public void testSetAuthor() {
        System.out.println("setAuthor");
        String author = "";
        BETSV1 instance = new BETSV1();
        instance.setAuthor(author);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCreated_at method, of class BETSV1.
     */
    @Test
    public void testGetCreated_at() {
        System.out.println("getCreated_at");
        BETSV1 instance = new BETSV1();
        String expResult = "";
        String result = instance.getCreated_at();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setCreated_at method, of class BETSV1.
     */
    @Test
    public void testSetCreated_at() {
        System.out.println("setCreated_at");
        String created_at = "";
        BETSV1 instance = new BETSV1();
        instance.setCreated_at(created_at);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getOwner method, of class BETSV1.
     */
    @Test
    public void testGetOwner() {
        System.out.println("getOwner");
        BETSV1 instance = new BETSV1();
        String expResult = "";
        String result = instance.getOwner();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setOwner method, of class BETSV1.
     */
    @Test
    public void testSetOwner() {
        System.out.println("setOwner");
        String owner = "";
        BETSV1 instance = new BETSV1();
        instance.setOwner(owner);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMaintained method, of class BETSV1.
     */
    @Test
    public void testGetMaintained() {
        System.out.println("getMaintained");
        BETSV1 instance = new BETSV1();
        String expResult = "";
        String result = instance.getMaintained();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMaintained method, of class BETSV1.
     */
    @Test
    public void testSetMaintained() {
        System.out.println("setMaintained");
        String maintained = "";
        BETSV1 instance = new BETSV1();
        instance.setMaintained(maintained);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAvailability method, of class BETSV1.
     */
    @Test
    public void testGetAvailability() {
        System.out.println("getAvailability");
        BETSV1 instance = new BETSV1();
        List<Availability> expResult = null;
        List<Availability> result = instance.getAvailability();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAvailability method, of class BETSV1.
     */
    @Test
    public void testSetAvailability() {
        System.out.println("setAvailability");
        List<Availability> availability = null;
        BETSV1 instance = new BETSV1();
        instance.setAvailability(availability);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getContact method, of class BETSV1.
     */
    @Test
    public void testGetContact() {
        System.out.println("getContact");
        BETSV1 instance = new BETSV1();
        Contact expResult = null;
        Contact result = instance.getContact();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setContact method, of class BETSV1.
     */
    @Test
    public void testSetContact() {
        System.out.println("setContact");
        Contact contact = null;
        BETSV1 instance = new BETSV1();
        instance.setContact(contact);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCategory method, of class BETSV1.
     */
    @Test
    public void testGetCategory() {
        System.out.println("getCategory");
        BETSV1 instance = new BETSV1();
        Category expResult = null;
        Category result = instance.getCategory();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setCategory method, of class BETSV1.
     */
    @Test
    public void testSetCategory() {
        System.out.println("setCategory");
        Category category = null;
        BETSV1 instance = new BETSV1();
        instance.setCategory(category);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getTechnology method, of class BETSV1.
     */
    @Test
    public void testGetTechnology() {
        System.out.println("getTechnology");
        BETSV1 instance = new BETSV1();
        List<Technology> expResult = null;
        List<Technology> result = instance.getTechnology();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setTechnology method, of class BETSV1.
     */
    @Test
    public void testSetTechnology() {
        System.out.println("setTechnology");
        List<Technology> technology = null;
        BETSV1 instance = new BETSV1();
        instance.setTechnology(technology);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getProgramming_language method, of class BETSV1.
     */
    @Test
    public void testGetProgramming_language() {
        System.out.println("getProgramming_language");
        BETSV1 instance = new BETSV1();
        List<Programming_language> expResult = new ArrayList();
        List<Programming_language> result = instance.getProgramming_language();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setProgramming_language method, of class BETSV1.
     */
    @Test
    public void testSetProgramming_language() {
        System.out.println("setProgramming_language");
        List<Programming_language> programming_language = null;
        BETSV1 instance = new BETSV1();
        instance.setProgramming_language(programming_language);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLicense method, of class BETSV1.
     */
    @Test
    public void testGetLicense() {
        System.out.println("getLicense");
        BETSV1 instance = new BETSV1();
        List<License> expResult = null;
        List<License> result = instance.getLicense();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setLicense method, of class BETSV1.
     */
    @Test
    public void testSetLicense() {
        System.out.println("setLicense");
        List<License> license = null;
        BETSV1 instance = new BETSV1();
        instance.setLicense(license);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getOperating_system method, of class BETSV1.
     */
    @Test
    public void testGetOperating_system() {
        System.out.println("getOperating_system");
        BETSV1 instance = new BETSV1();
        List<Operating_system> expResult = null;
        List<Operating_system> result = instance.getOperating_system();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setOperating_system method, of class BETSV1.
     */
    @Test
    public void testSetOperating_system() {
        System.out.println("setOperating_system");
        List<Operating_system> operating_system = null;
        BETSV1 instance = new BETSV1();
        instance.setOperating_system(operating_system);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSoftware_features method, of class BETSV1.
     */
    @Test
    public void testGetSoftware_features() {
        System.out.println("getSoftware_features");
        BETSV1 instance = new BETSV1();
        String expResult = "";
        String result = instance.getSoftware_features();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSoftware_features method, of class BETSV1.
     */
    @Test
    public void testSetSoftware_features() {
        System.out.println("setSoftware_features");
        String software_features = "";
        BETSV1 instance = new BETSV1();
        instance.setSoftware_features(software_features);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSoftware_libraries method, of class BETSV1.
     */
    @Test
    public void testGetSoftware_libraries() {
        System.out.println("getSoftware_libraries");
        BETSV1 instance = new BETSV1();
        String expResult = "";
        String result = instance.getSoftware_libraries();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSoftware_libraries method, of class BETSV1.
     */
    @Test
    public void testSetSoftware_libraries() {
        System.out.println("setSoftware_libraries");
        String software_libraries = "";
        BETSV1 instance = new BETSV1();
        instance.setSoftware_libraries(software_libraries);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getInputs method, of class BETSV1.
     */
    @Test
    public void testGetInputs() {
        System.out.println("getInputs");
        BETSV1 instance = new BETSV1();
        List<Input> expResult = null;
        List<Input> result = instance.getInputs();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setInputs method, of class BETSV1.
     */
    @Test
    public void testSetInputs() {
        System.out.println("setInputs");
        List<Input> inputs = null;
        BETSV1 instance = new BETSV1();
        instance.setInputs(inputs);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getParameters method, of class BETSV1.
     */
    @Test
    public void testGetParameters() {
        System.out.println("getParameters");
        BETSV1 instance = new BETSV1();
        List<Parameter> expResult = null;
        List<Parameter> result = instance.getParameters();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setParameters method, of class BETSV1.
     */
    @Test
    public void testSetParameters() {
        System.out.println("setParameters");
        List<Parameter> parameters = null;
        BETSV1 instance = new BETSV1();
        instance.setParameters(parameters);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getOutputs method, of class BETSV1.
     */
    @Test
    public void testGetOutputs() {
        System.out.println("getOutputs");
        BETSV1 instance = new BETSV1();
        List<Output> expResult = null;
        List<Output> result = instance.getOutputs();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setOutputs method, of class BETSV1.
     */
    @Test
    public void testSetOutputs() {
        System.out.println("setOutputs");
        List<Output> outputs = null;
        BETSV1 instance = new BETSV1();
        instance.setOutputs(outputs);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRequirements method, of class BETSV1.
     */
    @Test
    public void testGetRequirements() {
        System.out.println("getRequirements");
        BETSV1 instance = new BETSV1();
        String expResult = "";
        String result = instance.getRequirements();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setRequirements method, of class BETSV1.
     */
    @Test
    public void testSetRequirements() {
        System.out.println("setRequirements");
        String requirements = "";
        BETSV1 instance = new BETSV1();
        instance.setRequirements(requirements);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getQuality method, of class BETSV1.
     */
    @Test
    public void testGetQuality() {
        System.out.println("getQuality");
        BETSV1 instance = new BETSV1();
        List<Quality_> expResult = null;
        List<Quality_> result = instance.getQuality();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setQuality method, of class BETSV1.
     */
    @Test
    public void testSetQuality() {
        System.out.println("setQuality");
        List<Quality_> quality = null;
        BETSV1 instance = new BETSV1();
        instance.setQuality(quality);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLearn_flow method, of class BETSV1.
     */
    @Test
    public void testGetLearn_flow() {
        System.out.println("getLearn_flow");
        BETSV1 instance = new BETSV1();
        List<Learn_flow> expResult = null;
        List<Learn_flow> result = instance.getLearn_flow();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setLearn_flow method, of class BETSV1.
     */
    @Test
    public void testSetLearn_flow() {
        System.out.println("setLearn_flow");
        List<Learn_flow> learn_flow = null;
        BETSV1 instance = new BETSV1();
        instance.setLearn_flow(learn_flow);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRelease_date method, of class BETSV1.
     */
    @Test
    public void testGetRelease_date() {
        System.out.println("getRelease_date");
        BETSV1 instance = new BETSV1();
        String expResult = "";
        String result = instance.getRelease_date();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setRelease_date method, of class BETSV1.
     */
    @Test
    public void testSetRelease_date() {
        System.out.println("setRelease_date");
        String release_date = "";
        BETSV1 instance = new BETSV1();
        instance.setRelease_date(release_date);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAlgorithm method, of class BETSV1.
     */
    @Test
    public void testGetAlgorithm() {
        System.out.println("getAlgorithm");
        BETSV1 instance = new BETSV1();
        List<Algorithm> expResult = null;
        List<Algorithm> result = instance.getAlgorithm();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAlgorithm method, of class BETSV1.
     */
    @Test
    public void testSetAlgorithm() {
        System.out.println("setAlgorithm");
        List<Algorithm> algorithm = null;
        BETSV1 instance = new BETSV1();
        instance.setAlgorithm(algorithm);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getValidation_date method, of class BETSV1.
     */
    @Test
    public void testGetValidation_date() {
        System.out.println("getValidation_date");
        BETSV1 instance = new BETSV1();
        String expResult = "";
        String result = instance.getValidation_date();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setValidation_date method, of class BETSV1.
     */
    @Test
    public void testSetValidation_date() {
        System.out.println("setValidation_date");
        String validation_date = "";
        BETSV1 instance = new BETSV1();
        instance.setValidation_date(validation_date);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNature_of_tool method, of class BETSV1.
     */
    @Test
    public void testGetNature_of_tool() {
        System.out.println("getNature_of_tool");
        BETSV1 instance = new BETSV1();
        List<String> expResult = null;
        List<String> result = instance.getNature_of_tool();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setNature_of_tool method, of class BETSV1.
     */
    @Test
    public void testSetNature_of_tool() {
        System.out.println("setNature_of_tool");
        List<String> nature_of_tool = null;
        BETSV1 instance = new BETSV1();
        instance.setNature_of_tool(nature_of_tool);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getTool_type method, of class BETSV1.
     */
    @Test
    public void testGetTool_type() {
        System.out.println("getTool_type");
        BETSV1 instance = new BETSV1();
        List<String> expResult = null;
        List<String> result = instance.getTool_type();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setTool_type method, of class BETSV1.
     */
    @Test
    public void testSetTool_type() {
        System.out.println("setTool_type");
        List<String> tool_type = null;
        BETSV1 instance = new BETSV1();
        instance.setTool_type(tool_type);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getInterfaces method, of class BETSV1.
     */
    @Test
    public void testGetInterfaces() {
        System.out.println("getInterfaces");
        BETSV1 instance = new BETSV1();
        List<String> expResult = null;
        List<String> result = instance.getInterfaces();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setInterfaces method, of class BETSV1.
     */
    @Test
    public void testSetInterfaces() {
        System.out.println("setInterfaces");
        List<String> interfaces = null;
        BETSV1 instance = new BETSV1();
        instance.setInterfaces(interfaces);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getIplant_specific method, of class BETSV1.
     */
    @Test
    public void testGetIplant_specific() {
        System.out.println("getIplant_specific");
        BETSV1 instance = new BETSV1();
        Iplant_specific expResult = null;
        Iplant_specific result = instance.getIplant_specific();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setIplant_specific method, of class BETSV1.
     */
    @Test
    public void testSetIplant_specific() {
        System.out.println("setIplant_specific");
        Iplant_specific iplant_specific = null;
        BETSV1 instance = new BETSV1();
        instance.setIplant_specific(iplant_specific);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSeq_specific method, of class BETSV1.
     */
    @Test
    public void testGetSeq_specific() {
        System.out.println("getSeq_specific");
        BETSV1 instance = new BETSV1();
        Seq_specific expResult = null;
        Seq_specific result = instance.getSeq_specific();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSeq_specific method, of class BETSV1.
     */
    @Test
    public void testSetSeq_specific() {
        System.out.println("setSeq_specific");
        Seq_specific seq_specific = null;
        BETSV1 instance = new BETSV1();
        instance.setSeq_specific(seq_specific);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getOmic_specific method, of class BETSV1.
     */
    @Test
    public void testGetOmic_specific() {
        System.out.println("getOmic_specific");
        BETSV1 instance = new BETSV1();
        OMIC_specific expResult = null;
        OMIC_specific result = instance.getOmic_specific();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setOmic_specific method, of class BETSV1.
     */
    @Test
    public void testSetOmic_specific() {
        System.out.println("setOmic_specific");
        OMIC_specific omic_specific = null;
        BETSV1 instance = new BETSV1();
        instance.setOmic_specific(omic_specific);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getGalaxy_specific method, of class BETSV1.
     */
    @Test
    public void testGetGalaxy_specific() {
        System.out.println("getGalaxy_specific");
        BETSV1 instance = new BETSV1();
        Galaxy_specific expResult = null;
        Galaxy_specific result = instance.getGalaxy_specific();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setGalaxy_specific method, of class BETSV1.
     */
    @Test
    public void testSetGalaxy_specific() {
        System.out.println("setGalaxy_specific");
        Galaxy_specific galaxy_specific = null;
        BETSV1 instance = new BETSV1();
        instance.setGalaxy_specific(galaxy_specific);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getBld_specific method, of class BETSV1.
     */
    @Test
    public void testGetBld_specific() {
        System.out.println("getBld_specific");
        BETSV1 instance = new BETSV1();
        Bld_specific expResult = null;
        Bld_specific result = instance.getBld_specific();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setBld_specific method, of class BETSV1.
     */
    @Test
    public void testSetBld_specific() {
        System.out.println("setBld_specific");
        Bld_specific bld_specific = null;
        BETSV1 instance = new BETSV1();
        instance.setBld_specific(bld_specific);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getParallelism method, of class BETSV1.
     */
    @Test
    public void testGetParallelism() {
        System.out.println("getParallelism");
        BETSV1 instance = new BETSV1();
        String expResult = "";
        String result = instance.getParallelism();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setParallelism method, of class BETSV1.
     */
    @Test
    public void testSetParallelism() {
        System.out.println("setParallelism");
        String parallelism = "";
        BETSV1 instance = new BETSV1();
        instance.setParallelism(parallelism);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class BETSV1.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        BETSV1 instance = new BETSV1();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of combine method, of class BETSV1.
     */
    @Test
    public void testCombine() {
        System.out.println("combine");
        BETSV1 other = null;
        BETSV1 instance = new BETSV1();
        BETSV1 expResult = null;
        BETSV1 result = instance.combine(other);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAdditionalProperties method, of class BETSV1.
     */
    @Test
    public void testGetAdditionalProperties() {
        System.out.println("getAdditionalProperties");
        BETSV1 instance = new BETSV1();
        Map<String, Object> expResult = null;
        Map<String, Object> result = instance.getAdditionalProperties();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAdditionalProperty method, of class BETSV1.
     */
    @Test
    public void testSetAdditionalProperty() {
        System.out.println("setAdditionalProperty");
        String name = "";
        Object value = null;
        BETSV1 instance = new BETSV1();
        instance.setAdditionalProperty(name, value);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
