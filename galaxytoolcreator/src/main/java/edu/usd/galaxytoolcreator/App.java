package edu.usd.galaxytoolcreator;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.tmatesoft.hg.core.HgBadArgumentException;
import org.tmatesoft.hg.core.HgCheckoutCommand;
import org.tmatesoft.hg.core.HgCloneCommand;
import org.tmatesoft.hg.core.HgException;
import org.tmatesoft.hg.core.HgPullCommand;
import org.tmatesoft.hg.repo.HgLookup;
import org.tmatesoft.hg.repo.HgRemoteRepository;
import org.tmatesoft.hg.repo.HgRepository;
import org.tmatesoft.hg.util.CancelledException;
import org.xml.sax.SAXException;

/**
 * Hello world!
 *
 */
public class App {
    public static final int timeoutTime = 15 * 1000; //Prevents call to server from timing out 
    public static final String outputFolder = "output"; //Creates folder in project to hold all XML Files when complete
    public static final String cloneFolder = "clone"; //Creates folder inside this project that will temporarily hold XML files before new attributes are added  
    public static List<File> allGalaxyXMLFiles; //Makes a list of all galaxy files that are valid tools - resets after each repository
    public static int numberOfFiles = 0; //number of files in the output folder - used in file name to ensure unique names when saving 

    // URL for the general Galaxy Tool Shed site
    public static String baseURL = "https://toolshed.g2.bx.psu.edu";

    public static void main(String[] args) throws Exception {
        InitializeFolders(); //Create 'output' and 'clone' folders if they don't already exist and make sure they are empty

        List<String> repoList = new ArrayList();
        List<ToolObj> toolList = new ArrayList();

        //Start off with the main page, browse_categories
        repoList.addAll(GetListOfRepos(baseURL));

        //check each repo in the list for a list of tools
        for (int i = 0; i < repoList.size(); i++) {
            Random rand = new Random();
            Thread.sleep(rand.nextInt(3000));
            String repo = repoList.get(i);
            try {
                toolList.addAll(GetListOfToolsFromRepo(baseURL + repo));
            } catch (Exception e) {
                System.out.println("Failed on Repo: " + repo);
                System.out.println("e: " + e.getMessage());
            }
        }
//         Once you have the list of URL's for the tool, pass each URL into the Append Attribute method
//         Gets the XML file for that tool and adds all attributes that it can find from HTML page
//         This will take up to 20 minutes 
        for (int i = 0; i < toolList.size(); i++) {
            Random rand = new Random();
            Thread.sleep(rand.nextInt(3000));
            ToolObj tool = toolList.get(i);
            AppendAttributes(tool);
        }
//        // For now, just focus on one tool at a time
//        String url = "/repository/browse_repositories_in_category?sort=name&operation=view_or_manage_repository&id=786fe460f01cefb8";
//        String verified = "no";
//        ToolObj obj = new ToolObj();
//        obj.setUrl(url);
//        obj.setVerified(verified);
//        AppendAttributes(obj);
//        AppendAttributes(obj);
    }

    /**
     * GetListOfRepos returns a list of repos partial url's that all have lists
     * of tools in them.
     *
     * @param baseURL
     * @return
     * @throws Exception
     */
    private static List<String> GetListOfRepos(String baseURL) throws Exception {
        List<String> repoList = new ArrayList();
        Document doc = Jsoup.connect(baseURL + "/repository/browse_categories").timeout(timeoutTime).get();

        Elements scriptTags = doc.select("script");
        for (Element tag : scriptTags) {
            for (DataNode node : tag.dataNodes()) {
                String data = node.getWholeData();
                Scanner scanner = new Scanner(data);

                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();

                    if (line.startsWith("{") && line.endsWith(" );")) {
                        line = line.substring(0, line.length() - 2);
                        JSONObject repos = new JSONObject(line);
                        JSONArray items = repos.getJSONArray("items");
                        for (int i = 0; i < items.length(); i++) {
                            JSONObject item = (JSONObject) items.get(i);
                            repoList.add(item.getJSONObject("column_config").getJSONObject("Name").getString("link"));
                        }
                    }
                }
            }
        }

        return repoList;
    }

    /**
     * GetListOfTools takes a repo link, then it goes through each that repo and
     * gets a list of tools from it to return.
     *
     * @param baseURL
     * @param repoList
     * @return
     * @throws Exception
     */
    private static List<ToolObj> GetListOfToolsFromRepo(String repo)
            throws Exception {
        List<ToolObj> toolList = new ArrayList();

        Document doc = Jsoup.connect(repo).timeout(timeoutTime).get();
        Elements scriptTags = doc.select("script");
        for (Element tag : scriptTags) {
            for (DataNode node : tag.dataNodes()) {
                String data = node.getWholeData();
                Scanner scanner = new Scanner(data);

                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();

                    if (line.startsWith("{") && line.endsWith(" );")) {
                        line = line.substring(0, line.length() - 2);
                        JSONObject tools = new JSONObject(line);
                        JSONArray items = tools.getJSONArray("items");
                        for (int i = 0; i < items.length(); i++) {
                            JSONObject item = (JSONObject) items.get(i);
                            ToolObj obj = new ToolObj();
                            obj.setUrl(item.getJSONObject("column_config").getJSONObject("Name").getString("link"));
                            obj.setVerified(item.getJSONObject("column_config").getJSONObject("Tools or<br/>Package<br/>Verified").getString("value"));
                            toolList.add(obj);
                        }
                    }
                }
            }
        }

        return toolList;
    }

    /**
     *
     *
     * @param url
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     */
    public static void AppendAttributes(ToolObj obj) throws SAXException, IOException, ParserConfigurationException, TransformerConfigurationException, TransformerException {
        String url = baseURL + obj.getUrl();
        Document doc = Jsoup.connect(url).timeout(timeoutTime).get();

        //Elements with class="form-row" are typically used to display the info that we want
        Elements formRowTags = doc.getElementsByClass("form-row");

        //Declare all Elements that we will add to the XML file
        String cloneCommand = null, owner = null, repoLink = null, revision = null, availability = null, timesCloned = null, verified = null;
        for (Element e : formRowTags) {
            try {
                if (e.toString().contains("<b>Clone this repository:</b>")) {
                    cloneCommand = e.child(1).text();
                } else if (e.toString().contains("<b>Owner:</b>")) {
                    owner = e.child(1).text();
                } else if (e.toString().contains("<b>Link to this repository:</b>")) {
                    repoLink = e.child(1).text();
                } else if (e.toString().contains("<b>Revision:</b>")) {
                    revision = e.child(1).text();
                } else if (e.toString().contains("<b>This revision can be installed:</b>")) {
                    availability = e.text().replace("This revision can be installed:", "").trim();
                } else if (e.toString().contains("<b>Times cloned / installed:</b>")) {
                    timesCloned = e.text();
                    break;
                }
            } catch (Exception ex) {
                // do nothing - there was a label but no value
            }
        }

        if (obj.getVerified() != null) {
            verified = obj.getVerified();
        }

        if (cloneCommand != null) {
            //Get the xml file for this tool and create a Document object for it
            allGalaxyXMLFiles = new ArrayList<File>();
            MercurialClone(cloneCommand);

            if (allGalaxyXMLFiles.size() > 0) {
                for (File f : allGalaxyXMLFiles) {

                    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                    org.w3c.dom.Document document = documentBuilder.parse(f); //this will probably be cloneFolder + fileName
                    org.w3c.dom.Element root = document.getDocumentElement();

                    //Owner tag
                    if (owner != null) {
                        org.w3c.dom.Element ownerElem = document.createElement("owner");
                        ownerElem.appendChild(document.createTextNode(owner));
                        root.appendChild(ownerElem);
                    }

                    //Link to repo
                    if (repoLink != null) {
                        org.w3c.dom.Element repoLinkElem = document.createElement("repoLink");
                        repoLinkElem.appendChild(document.createTextNode(repoLink));
                        root.appendChild(repoLinkElem);
                    }

                    //Revision 
                    if (revision != null) {
                        org.w3c.dom.Element revisionElem = document.createElement("revision");
                        revisionElem.appendChild(document.createTextNode(revision));
                        root.appendChild(revisionElem);
                    }

                    //This revision can be installed
                    if (availability != null) {
                        org.w3c.dom.Element availElem = document.createElement("available");
                        availElem.appendChild(document.createTextNode(availability));
                        root.appendChild(availElem);
                    }

                    //Times cloned
                    if (timesCloned != null) {
                        org.w3c.dom.Element qualityElem = document.createElement("quality");
                        qualityElem.appendChild(document.createTextNode(timesCloned));
                        root.appendChild(qualityElem);
                    }

                    //Verified
                    if (verified != null) {
                        org.w3c.dom.Element verifiedElem = document.createElement("verified");
                        verifiedElem.appendChild(document.createTextNode("Verified: " + verified));
                        root.appendChild(verifiedElem);
                    }

                    //Categories tag
                    Elements categories = doc.getElementsByClass("toolForm");
                    for (Element e : categories) {
                        if (e.toString().contains("/repository/browse_repositories_in_category?id")) {
                            ArrayList<CategoryObj> categoryList = new ArrayList<CategoryObj>();
                            Elements categoryElems = e.getElementsByClass("form-row");
                            for (Element categoryElem : categoryElems) {
                                Elements links = categoryElem.getAllElements();
                                for (Element link : links) {
                                    if (!"a".equals(link.tagName()) && link.getElementsByTag("a").size() > 0) {
                                        CategoryObj cat = new CategoryObj();
                                        //the link contains the name of the actual category
                                        cat.setValue(link.getElementsByTag("a").text());
                                        //the text contains the description of the category
                                        cat.setDescription(link.text());
                                        categoryList.add(cat);
                                    }
                                }
                                org.w3c.dom.Element categoryElement = document.createElement("categories");
                                for (CategoryObj categoryList1 : categoryList) {
                                    org.w3c.dom.Element catElem = document.createElement("category");
                                    catElem.setAttribute("value", categoryList1.getValue());
                                    catElem.setAttribute("description", categoryList1.getDescription());
                                    categoryElement.appendChild(catElem);
                                }
                                root.appendChild(categoryElement);
                                break;
                            }
                        }
                    }

                    DOMSource source = new DOMSource(document);

                    // Write the XML file back out to replace the original file 
                    //  Maybe change output folder? 
                    TransformerFactory transformerFactory = TransformerFactory.newInstance();
                    Transformer transformer = null;
                    transformer = transformerFactory.newTransformer();
                    StreamResult result = new StreamResult(outputFolder + "\\" + numberOfFiles + "_" + f.getName());
                    transformer.transform(source, result);
                    numberOfFiles++;
                }
            }
        }
    }

    /**
     * Initialize the output folder and clone folder. Make sure that they are
     * empty. The Output Folder is used to store the final XML files, while the
     * Clone Folder is a temporary location to save the XML files before the
     * final attributes are added.
     */
    private static void InitializeFolders() {
        // Create an empty output folder
        File file = new File(outputFolder);
        file.mkdir();
        // Make sure it is empty
        if (file.list().length > 0) {
            try {
                FileUtils.cleanDirectory(file); //delete everything in it
            } catch (IOException ex) {
                System.out.println("Failed to delete files from the existing Output Folder!");
            }
        }

        // Create an empty clone folder
        File file2 = new File(cloneFolder);
        file2.mkdir();
        // Make sure it is empty 
        if (file2.list().length > 0) {
            try {
                FileUtils.cleanDirectory(file2); //delete everything in it
            } catch (IOException ex) {
                System.out.println("Failed to delete files from the existing Clone Folder!");
            }
        }
    }

    //----------------------------------MERCURIAL----------------------------------------------------------------//
    /**
     * Handle all operations with Mercurial. Clone the repository at the given
     * url and save the tool's xml file in the CloneFolder
     *
     * @param url
     */
    private static void MercurialClone(String hgCommand) {
        int beginning = hgCommand.indexOf("http");
        hgCommand = hgCommand.substring(beginning);
        URL url;
        try {
            url = new URL(hgCommand);
            HgRemoteRepository hgRemote = new HgLookup().detect(url);

            int lastSlash = hgCommand.lastIndexOf("/");
            int secondToLastSlash = hgCommand.lastIndexOf("/", lastSlash - 1);
            String author = hgCommand.substring(secondToLastSlash, lastSlash);
            String name = hgCommand.substring(lastSlash);
            String clonePath = "clone\\" + author + "\\" + name;

            File cloneDirectory = new File(clonePath);

            HgRepository hgRepo;
            if (cloneDirectory.listFiles() != null && cloneDirectory.listFiles().length > 0) {
                System.out.println("pull");
                HgRepository repo = new HgLookup().detect(cloneDirectory);
                HgPullCommand cmd = new HgPullCommand(repo);
                cmd.source(hgRemote);
                cmd.execute();
                hgRepo = new HgLookup().detect(new File(clonePath));
            } else {
                HgCloneCommand cmd = new HgCloneCommand();
                cmd.source(hgRemote);
                cmd.destination(new File(clonePath));
                cmd.execute();

                hgRepo = new HgLookup().detect(new File(clonePath));
            }
            HgCheckoutCommand check = new HgCheckoutCommand(hgRepo);
            check.clean(true); //undo any changes we've made to repo (required, even though we haven't actually been working on repo) 
            check.changeset(HgRepository.TIP);
            check.execute();

            getFilesFromFolder(new File(clonePath)); //hopefully there's only one of these, but may be more if multiple tools in one repo
        } catch (MalformedURLException ex) {
            System.out.println("MalformedURLException for: " + hgCommand + ": " + ex);
        } catch (HgBadArgumentException ex) {
            System.out.println("HgBadArgumentException for: " + hgCommand + ": " + ex);
        } catch (HgException ex) {
            System.out.println("HgException for: " + hgCommand + ": " + ex);
        } catch (CancelledException ex) {
            System.out.println("CancelledException for: " + hgCommand + ": " + ex);
        } catch (Exception ex) {
            System.out.println("General Exception for: " + hgCommand + ": " + ex);
        }
    }

    /**
     * Recursive method to get only the valid XML Files (no Directories or other
     * entities), adding them to ArrayList called allGalaxyXMLFiles.
     *
     * @param file
     * @return List of Files
     */
    public static void getFilesFromFolder(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File x : files) {
                getFilesFromFolder(x);
            }
        } else {
            if (checkValidGalaxyFormat(file)) {
                allGalaxyXMLFiles.add(file);
            }
        }
    }

    /**
     * Checks that inputFile is a Galaxy 'tool' with a 'name' and 'id'.
     *
     * @param inputFile
     * @return boolean
     */
    public static boolean checkValidGalaxyFormat(File inputFile) {
        int ext = inputFile.getAbsolutePath().lastIndexOf(".");
        if (!inputFile.getAbsolutePath().substring(ext + 1).toLowerCase().equals("xml")) {
            return false;
        }
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
        }
        org.w3c.dom.Document doc;
        try {
            doc = dBuilder.parse(inputFile);
            if (!doc.getDocumentElement().getNodeName().equals("tool")) {
                return false;
            } else {

                if (doc.getElementsByTagName("name") == null) {
                    return false;
                }
                if (doc.getElementsByTagName("id") == null) {
                    return false;
                }
            }
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    //---------------------------------HELPER OBJECTS--------------------------------------------------------------//
    static class ToolObj {

        private String url;
        private String verified;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getVerified() {
            return verified;
        }

        public void setVerified(String verified) {
            this.verified = verified;
        }
    }

    static class CategoryObj {

        private String value;
        private String description;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
