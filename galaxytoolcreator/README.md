# GalaxyToolCreator

Purpose
------------------------
Get all of the tools from the Galaxy Tool Shed website and output a folder of XML files with the repository data and the data from the HTML page for each tool


How it works
---------------------------
GalaxyToolCreator uses the HTML page from the actual site on the Galaxy Tool Shed website to get all information about each tool. It begins at the root URL for Galaxy Tool Shed and proceeds build a list of all URLs for each of the tool. The application then calls out to each URL, gets the HTML documents, and parses it to get the tool data. It also clones/pulls the Mercurial repository for each tool. Once it has the repository and data, it opens the tool xml file from the repository, adds the extra data and saves it to the output folder. 

Warning 
----------------------------------
This application takes 3 hours to run. Beneficial to run application overnight and view results in the morning. 
