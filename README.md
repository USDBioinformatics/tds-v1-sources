# tds-v1-source

This is the sourcecode for all the projects that are required to build
the data and teh site biotds.org. This work has been published as
*Bio-TDS: bioscience query tool discovery system* in Nucleic Acids
Research 2016.  It has the doi: 10.1093/nar/gkw940 and is available
online.

## Projects


### bets-cli
This contains the converters for all of the tool formats the biotds site
ingests and converts to our BETS format.  This is a cli converter
project in this as well.  This may be handy for testing out conversions
or seeing what a given sites tool looks like in BETS format.

### bioannotatortool
This project annotates the bets tools with ontology terms gathered from
NCBO.

### biolinksdir
This project converts input files saved from bioinformatics links
directory (https://bioinformatics.ca/links_directory/) to single files
that our converter can consume.

### btl-rest
This is the project that supplies RESTful endpoints for all the
converted tools.  Tools are stored in a MySQL database and can be
retrieved, paged through, and return specific information about what
repositories they come from.

### galaxytoolcreator
This project creates a set of tool xmls from the galaxy tool shed
(https://toolshed.g2.bx.psu.edu/).  These are gotten from the mercurial
repositories that represent each tool in the galaxy tool shed.

### iplantappssplitter
This simple project splits the tool list returned from the (defunct)
iplant (now cyverse) foundation api.  Individual tool files can then be
converted to BETS format.

### seqdatafromjson
This tool splits up the output from a custom wiki query we use on the
seqanswers wiki page (http://seqanswers.com/wiki/SEQanswers) to gather
info on all the tools they have listed.  Each individual output can then
be converted to BETS.

### toolschecker
This project does some rudimentary checking on format conversions from
a tools original format to the BETS format that we have created.  It
will cull some tools if they don't pass the checks.  Output from this
project will be imported into the btl-rest site and stored in the db.

### tqwebpage
This project has a main class that builds the searchable index of tools.
When building this project it will automatically build the index in the
default location if it doesn't already exist. This site allows searching
of these tools and provides some basic divisions on which you can browse
the tools.

## License
This software is licensed via the GPLv3.  Please see the LICENSE file
for more information.

## Credits
© 2016 University of South Dakota Biomedical Engineering

The authors would also like to acknowledge support from South Dakota BRIN,
NSF, SDEPSCoR, and the BioSNTR.
