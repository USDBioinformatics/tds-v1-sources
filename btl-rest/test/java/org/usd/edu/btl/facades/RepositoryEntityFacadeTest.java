///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package org.usd.edu.btl.facades;
//
//import java.util.List;
//import javax.ejb.embeddable.EJBContainer;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//import org.usd.edu.btl.entities.RepositoryEntity;
//
///**
// *
// * @author Shayla.Gustafson
// */
//public class RepositoryEntityFacadeTest {
//    
//    public RepositoryEntityFacadeTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of remove method, of class RepositoryEntityFacade.
//     */
//    @Test
//    public void testRemove() throws Exception {
//        System.out.println("remove");
//        RepositoryEntity entity = null;
//        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
//        RepositoryEntityFacade instance = (RepositoryEntityFacade)container.getContext().lookup("java:global/classes/RepositoryEntityFacade");
//        instance.remove(entity);
//        container.close();
//        // TODO review the generated test code and remove the default call to fail.
//        assertTrue(true);
//    }
//
//    /**
//     * Test of find method, of class RepositoryEntityFacade.
//     */
//    @Test
//    public void testFind() throws Exception {
//        System.out.println("find");
//        Object id = null;
//        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
//        RepositoryEntityFacade instance = (RepositoryEntityFacade)container.getContext().lookup("java:global/classes/RepositoryEntityFacade");
//        RepositoryEntity expResult = null;
//        RepositoryEntity result = instance.find(id);
//        
//        container.close();
//        // TODO review the generated test code and remove the default call to fail.
//        assertTrue(true);
//    }
//
//    /**
//     * Test of findAll method, of class RepositoryEntityFacade.
//     */
//    @Test
//    public void testFindAll() throws Exception {
//        System.out.println("findAll");
//        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
//        RepositoryEntityFacade instance = (RepositoryEntityFacade)container.getContext().lookup("java:global/classes/RepositoryEntityFacade");
//        List expResult = null;
//        List result = instance.findAll();
//        
//        container.close();
//        // TODO review the generated test code and remove the default call to fail.
//        assertTrue(true);
//    }
//
//    /**
//     * Test of findRange method, of class RepositoryEntityFacade.
//     */
//    @Test
//    public void testFindRange() throws Exception {
//        System.out.println("findRange");
//        int[] range = null;
//        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
//        RepositoryEntityFacade instance = (RepositoryEntityFacade)container.getContext().lookup("java:global/classes/RepositoryEntityFacade");
//        List expResult = null;
//        List result = instance.findRange(range);
//        
//        container.close();
//        // TODO review the generated test code and remove the default call to fail.
//        assertTrue(true);
//    }
//
//    /**
//     * Test of count method, of class RepositoryEntityFacade.
//     */
//    @Test
//    public void testCount() throws Exception {
//        System.out.println("count");
//        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
//        RepositoryEntityFacade instance = (RepositoryEntityFacade)container.getContext().lookup("java:global/classes/RepositoryEntityFacade");
//        int expResult = 0;
//        int result = instance.count();
//        
//        container.close();
//        // TODO review the generated test code and remove the default call to fail.
//        assertTrue(true);
//    }
//
//    /**
//     * Test of create method, of class RepositoryEntityFacade.
//     */
//    @Test
//    public void testCreate() throws Exception {
//        System.out.println("create");
//        RepositoryEntity repos = null;
//        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
//        RepositoryEntityFacade instance = (RepositoryEntityFacade)container.getContext().lookup("java:global/classes/RepositoryEntityFacade");
//        instance.create(repos);
//        container.close();
//        // TODO review the generated test code and remove the default call to fail.
//        assertTrue(true);
//    }
//
//    /**
//     * Test of edit method, of class RepositoryEntityFacade.
//     */
//    @Test
//    public void testEdit() throws Exception {
//        System.out.println("edit");
//        RepositoryEntity repos = null;
//        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
//        RepositoryEntityFacade instance = (RepositoryEntityFacade)container.getContext().lookup("java:global/classes/RepositoryEntityFacade");
//        instance.edit(repos);
//        container.close();
//        // TODO review the generated test code and remove the default call to fail.
//        assertTrue(true);
//    }
//
//    /**
//     * Test of findByName method, of class RepositoryEntityFacade.
//     */
//    @Test
//    public void testFindByName() throws Exception {
//        System.out.println("findByName");
//        String name = "";
//        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
//        RepositoryEntityFacade instance = (RepositoryEntityFacade)container.getContext().lookup("java:global/classes/RepositoryEntityFacade");
//        RepositoryEntity expResult = null;
//        RepositoryEntity result = instance.findByName(name);
//        
//        container.close();
//        // TODO review the generated test code and remove the default call to fail.
//        assertTrue(true);
//    }
//}