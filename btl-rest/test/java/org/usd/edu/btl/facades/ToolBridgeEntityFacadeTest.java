///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package org.usd.edu.btl.facades;
//
//import java.util.List;
//import javax.ejb.embeddable.EJBContainer;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//import org.usd.edu.btl.entities.ToolBridgeEntity;
//
///**
// *
// * @author Shayla.Gustafson
// */
//public class ToolBridgeEntityFacadeTest {
//    
//    public ToolBridgeEntityFacadeTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of create method, of class ToolBridgeEntityFacade.
//     */
//    @Test
//    public void testCreate() throws Exception {
//        System.out.println("create");
//        ToolBridgeEntity entity = null;
//        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
//        ToolBridgeEntityFacade instance = (ToolBridgeEntityFacade)container.getContext().lookup("java:global/classes/ToolBridgeEntityFacade");
//        instance.create(entity);
//        container.close();
//        // TODO review the generated test code and remove the default call to fail.
//        assertTrue(true);
//    }
//
//    /**
//     * Test of edit method, of class ToolBridgeEntityFacade.
//     */
//    @Test
//    public void testEdit() throws Exception {
//        System.out.println("edit");
//        ToolBridgeEntity entity = null;
//        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
//        ToolBridgeEntityFacade instance = (ToolBridgeEntityFacade)container.getContext().lookup("java:global/classes/ToolBridgeEntityFacade");
//        instance.edit(entity);
//        container.close();
//        // TODO review the generated test code and remove the default call to fail.
//        assertTrue(true);
//    }
//
//    /**
//     * Test of remove method, of class ToolBridgeEntityFacade.
//     */
//    @Test
//    public void testRemove() throws Exception {
//        System.out.println("remove");
//        ToolBridgeEntity entity = null;
//        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
//        ToolBridgeEntityFacade instance = (ToolBridgeEntityFacade)container.getContext().lookup("java:global/classes/ToolBridgeEntityFacade");
//        instance.remove(entity);
//        container.close();
//        // TODO review the generated test code and remove the default call to fail.
//        assertTrue(true);
//    }
//
//    /**
//     * Test of find method, of class ToolBridgeEntityFacade.
//     */
//    @Test
//    public void testFind() throws Exception {
//        System.out.println("find");
//        Object id = null;
//        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
//        ToolBridgeEntityFacade instance = (ToolBridgeEntityFacade)container.getContext().lookup("java:global/classes/ToolBridgeEntityFacade");
//        ToolBridgeEntity expResult = null;
//        ToolBridgeEntity result = instance.find(id);
//        
//        container.close();
//        // TODO review the generated test code and remove the default call to fail.
//        assertTrue(true);
//    }
//
//    /**
//     * Test of findAll method, of class ToolBridgeEntityFacade.
//     */
//    @Test
//    public void testFindAll() throws Exception {
//        System.out.println("findAll");
//        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
//        ToolBridgeEntityFacade instance = (ToolBridgeEntityFacade)container.getContext().lookup("java:global/classes/ToolBridgeEntityFacade");
//        List expResult = null;
//        List result = instance.findAll();
//        
//        container.close();
//        // TODO review the generated test code and remove the default call to fail.
//        assertTrue(true);
//    }
//
//    /**
//     * Test of findRange method, of class ToolBridgeEntityFacade.
//     */
//    @Test
//    public void testFindRange() throws Exception {
//        System.out.println("findRange");
//        int[] range = null;
//        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
//        ToolBridgeEntityFacade instance = (ToolBridgeEntityFacade)container.getContext().lookup("java:global/classes/ToolBridgeEntityFacade");
//        List expResult = null;
//        List result = instance.findRange(range);
//        
//        container.close();
//        // TODO review the generated test code and remove the default call to fail.
//        assertTrue(true);
//    }
//
//    /**
//     * Test of count method, of class ToolBridgeEntityFacade.
//     */
//    @Test
//    public void testCount() throws Exception {
//        System.out.println("count");
//        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
//        ToolBridgeEntityFacade instance = (ToolBridgeEntityFacade)container.getContext().lookup("java:global/classes/ToolBridgeEntityFacade");
//        int expResult = 0;
//        int result = instance.count();
//        
//        container.close();
//        // TODO review the generated test code and remove the default call to fail.
//        assertTrue(true);
//    }
//}