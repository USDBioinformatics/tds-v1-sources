/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Tyler
 */
@Entity
@Table(name = "tool_bets")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "findAllTools", query = "SELECT t FROM ToolBetsEntity t"),
    @NamedQuery(name = "findToolById", query = "SELECT t FROM ToolBetsEntity t WHERE t.id = :id"),
    @NamedQuery(name = "findToolByName", query = "SELECT t FROM ToolBetsEntity t WHERE t.name = :name"),
    @NamedQuery(name = "findToolByVersion", query = "SELECT t FROM ToolBetsEntity t WHERE t.version = :version"),
    @NamedQuery(name = "findToolsByIdFromStartToEnd", query = "SELECT t FROM ToolBetsEntity t WHERE t.id >= :start AND t.id <= :end "),
    @NamedQuery(name = "findBetsById", query = "SELECT t.bets FROM ToolBetsEntity t WHERE t.id = :id"),
    @NamedQuery(name = "getAllBets", query = "SELECT t.bets FROM ToolBetsEntity t"),
    @NamedQuery(name = "getToolIcon", query = "SELECT t.icon FROM ToolBetsEntity t WHERE t.id = :id"),
    @NamedQuery(name = "getAllSummaries", query = "SELECT t.id, t.summary FROM ToolBetsEntity t "),
    @NamedQuery(name = "findToolByNameAndVersion", query = "SELECT t FROM ToolBetsEntity t WHERE t.name = :name AND t.version = :version")})
public class ToolBetsEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;
    @Size(max = 255)
    @Column(name = "version")
    private String version;
    @Lob
    @Size(max = 65535)
    @Column(name = "summary")
    private String summary;
    @Lob
    @Column(name = "bets")
    private byte[] bets;
    @Column(name="icon")
    private String icon;
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateAdded")
    private Date dateAdded;
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateUpdated")
    private Date dateUpdated;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "toolId")
    private List<ToolBridgeEntity> toolBridgeList;

    public ToolBetsEntity() {
    }

    public ToolBetsEntity(Integer id) {
        this.id = id;
    }

    public ToolBetsEntity(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public byte[] getBets() {
        return bets;
    }

    public void setBets(byte[] bets) {
        this.bets = bets;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
    
    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    @XmlTransient
    public List<ToolBridgeEntity> getToolBridgeList() {
        return toolBridgeList;
    }

    public void setToolBridgeList(List<ToolBridgeEntity> toolBridgeList) {
        this.toolBridgeList = toolBridgeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ToolBetsEntity)) {
            return false;
        }
        ToolBetsEntity other = (ToolBetsEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.usd.edu.btl.rest.ToolBets[ id=" + id + " ]";
    }
}
