/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.usd.bme.btl.rest.resources.JSONentities;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 *
 * @author Bill Conn <Bill.Conn@usd.edu>
 */
public class Summary {
    private Integer id;
    private String summary;

    
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("summary")
    public String getSummary() {
        return summary;
    }

    @JsonProperty("summary")
    public void setSummary(String summary) {
        this.summary = summary;
    }
}
