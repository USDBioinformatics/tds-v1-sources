/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.rest.resources.repos;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import edu.usd.bme.btl.entities.RepositoryEntity;
import edu.usd.bme.btl.entities.RepositoryToolEntity;
import edu.usd.bme.btl.facades.AbstractFacade;
import edu.usd.bme.btl.rest.resources.JSONentities.RepoTool;

/**
 * RepositoryToolREST is a class that manages the restful endpoint located at
 * resources/repo/tool. It returns a list of everything in the databases
 * repositorytool table. You can also retrieve individual entries based on their
 * id at the /resources/repo/{id} endpoints.
 *
 * @author Tyler
 * @author Bill Conn <Bill.Conn@usd.edu>
 */
@Stateless
@Path("repos/tools")
@Produces({"application/json"})
public class RepositoryToolREST extends AbstractFacade<RepositoryToolEntity> {

    // Need the entity manager
    @PersistenceContext(unitName = "org.usd.edu_BTL-REST_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Default Constructor for the class
     */
    public RepositoryToolREST() {
        super(RepositoryToolEntity.class);
    }

    /**
     * find returns the proper RepositoryEntity for a given id. It will also
     * return pretty results if requested.
     *
     * @param id
     * @param prettyPrint
     * @return
     * @throws JsonProcessingException
     */
    @GET
    @Path("{id}")
    public Response find(@PathParam("id") Integer id, @DefaultValue("false") @QueryParam("PrettyPrint") boolean prettyPrint) throws JsonProcessingException {
        RepositoryToolEntity temp = super.find(id);
        RepoTool repoTool = new RepoTool(temp);
        
        // Check if the user wants us to pretty print or not
        //  Pretty printing uses jackson's objectmapper.
        if(prettyPrint) {
            ObjectMapper mapper = new ObjectMapper();
            String prettyRepoTool = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(repoTool);
            return Response.ok().entity(prettyRepoTool).build();
        }
        else {
            return Response.ok().entity(repoTool).build();
        }
    }

    /**
     * getAllRepoTools is the base RESTful endpoint and will return all of the
     *  RepositoryTool Entities in the database.  It will also prettify the
     *  results if requested.
     * 
     * @param prettyPrint
     * @return
     * @throws JsonProcessingException 
     */
    @GET
    public Response getAllRepoTools(@DefaultValue("false") @QueryParam("PrettyPrint") boolean prettyPrint) throws JsonProcessingException {
        List<RepoTool> repoTools = convertRepoToolEntityToRepoTool(super.findAll());

        // Check if the user wants us to pretty print or not
        //  Pretty printing uses jackson's objectmapper.
        if (prettyPrint) {
            ObjectMapper mapper = new ObjectMapper();
            String prettyResults = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(repoTools);
            return Response.ok().entity(prettyResults).build();
        } else {
            return Response.ok().entity(repoTools).build();
        }
    }

    /**
     * getEntityManager is a required method override from AbstraceFacade.
     * 
     * @return 
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * convertRepoToolEntityToRepoTool is a private helper method to convert
     *  objects from the database format to something we can return without
     *  treating all subobjects as objects themselves.  This gets around having
     *  an infinite recursion problem.
     * 
     * @param tools
     * @return 
     */
    private List<RepoTool> convertRepoToolEntityToRepoTool(List<RepositoryToolEntity> tools) {
        List<RepoTool> returnTools = new ArrayList();

        for (RepositoryToolEntity tool : tools) {
            returnTools.add(new RepoTool(tool));
        }

        return returnTools;
    }

}
