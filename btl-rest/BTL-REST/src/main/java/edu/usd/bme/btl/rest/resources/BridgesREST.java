/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.rest.resources;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import edu.usd.bme.btl.entities.ToolBridgeEntity;
import edu.usd.bme.btl.rest.resources.JSONentities.Bridge;

/**
 *
 * @author Tyler
 */
@Stateless
@Path("bridges")
@Produces({"application/json"})
@Consumes({"application/json"})
public class BridgesREST extends AbstractFacade<ToolBridgeEntity> {

    @PersistenceContext(unitName = "org.usd.edu_BTL-REST_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public BridgesREST() {
        super(ToolBridgeEntity.class);
    }

    @GET
    @Path("{id}")
    public Response find(@PathParam("id") Integer id) {
        Query getBridgeById = em.createNamedQuery("ToolBridgeEntity.findById");
        getBridgeById.setParameter("id",id);
        List<ToolBridgeEntity> bridge = getBridgeById.getResultList();
        if (bridge.size() < 1) {
            return Response.noContent().build();
        } else {
            return Response.ok(makeJsonEntityFromToolBridgeEntity(bridge.get(0))).build();
        }
    }

    @GET
    public Response findAllBridges() {
		if(true) {
			return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Get bridge ranges").build();
		}
        Query getAllBridges = em.createNamedQuery("ToolBridgeEntity.findAll");
        List<ToolBridgeEntity> bridgeList = getAllBridges.getResultList(); //list<string||integer will return an array
        
        List<Bridge> jsonBridges = new ArrayList();        
        for (ToolBridgeEntity bridge : bridgeList) {
            jsonBridges.add(makeJsonEntityFromToolBridgeEntity(bridge));
        }
        return Response.ok(jsonBridges).build();
    }

    @GET
    @Path("{from}/{to}")
    public Response findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
		if(to - from < 1) {
			return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Bad Range").build();
		}
		if(to - from > 1000) {
			return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Max range is 1000").build();
		}
        Query queryBridgesByIdFromStartToEnd = em.createNamedQuery("ToolBridgeEntity.findByIdFromStartToEnd");
        queryBridgesByIdFromStartToEnd.setParameter("start", from );
        queryBridgesByIdFromStartToEnd.setParameter("end", to);
        
        List<ToolBridgeEntity> toolBridges = queryBridgesByIdFromStartToEnd.getResultList();
        List<Bridge> jsonBridges = new ArrayList();
        for (ToolBridgeEntity toolEnt : toolBridges) {
            jsonBridges.add(makeJsonEntityFromToolBridgeEntity(toolEnt));
        }
        return Response.ok(jsonBridges).build();
    }
    
    /**
     * Private helper method to make Bridge JSON
     * @param tBE
     * @return 
     */
    private Bridge makeJsonEntityFromToolBridgeEntity(ToolBridgeEntity tBE) {
        Bridge jsonBridge = new Bridge();
        
        jsonBridge.setBridgeId(tBE.getId());
        jsonBridge.setOntologyId(tBE.getOntologyId());
        jsonBridge.setToolId(tBE.getToolId().getId());
        return jsonBridge;
    }

    @GET
    @Path("count")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
