package edu.usd.bme.btl.rest.resources.repos;

///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package org.usd.edu.btl.rest.resources.repos;
//
//import javax.ejb.Stateless;
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import javax.persistence.Query;
//import javax.ws.rs.Consumes;
//import javax.ws.rs.POST;
//import javax.ws.rs.Path;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.Response;
//import org.usd.edu.btl.entities.RepositoryEntity;
//import org.usd.edu.btl.entities.RepositoryToolEntity;
//import org.usd.edu.btl.entities.ToolBetsEntity;
//import org.usd.edu.btl.facades.AbstractFacade;
//import org.usd.edu.btl.rest.resources.JSONentities.RepoToolJSON;
//
///**
// *
// * @author Bill Conn <Bill.Conn@usd.edu>
// */
//@Stateless
//@Path("repos/getbytool")
//@Produces({"application/json"})
//@Consumes({"application/json"})
//public class RepositoryGetSingleToolREST extends AbstractFacade<RepositoryToolEntity> {
//    
//    @PersistenceContext(unitName = "org.usd.edu_BTL-REST_war_1.0-SNAPSHOTPU")
//    private EntityManager em;
//    
//    public RepositoryGetSingleToolREST() {
//        super(RepositoryToolEntity.class);
//    }
//    
//    @POST
//    public Response findId(RepoToolJSON incomingTool) {
//        Query getTool;
//        ToolBetsEntity temp = new ToolBetsEntity();
//        temp.setId(incomingTool.getToolId());
//        getTool = em.createNamedQuery("RepositoryTool.findByToolId");
//        getTool.setParameter("toolId", temp);
//        
//        RepositoryToolEntity repoEnt = (RepositoryToolEntity) getTool.getSingleResult();
//        
//        RepoToolJSON tool = new RepoToolJSON(repoEnt);
//        return Response.status(200).entity(tool).build();
//    }
//    @Override
//    protected EntityManager getEntityManager() {
//        return em;
//    }
//}
