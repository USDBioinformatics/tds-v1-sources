/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.facades;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import edu.usd.bme.btl.entities.RepositoryEntity;

/**
 *
 * @author Shayla.Gustafson
 */
@Stateless
public class RepositoryEntityFacade extends AbstractFacade<RepositoryEntity> {

    @PersistenceContext(unitName = "org.usd.edu_BTL-REST_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RepositoryEntityFacade() {
        super(RepositoryEntity.class);
    }

    @Override
    public void create(RepositoryEntity repos) throws Exception {
        RepositoryEntity temp = findByName(repos.getName());
        if (temp == null) {
            super.create(repos);
        } else {
            throw new Exception("Repository already exists");
        }
    }

    @Override
    public void edit(RepositoryEntity repos) throws Exception {
        RepositoryEntity temp = findByName(repos.getName());
        if (temp != null) {
            if (temp.getId() != repos.getId()) {
                throw new Exception("Repository name must be unique");
            } else {
                super.edit(repos);
            }
        }
    }

    public RepositoryEntity findByName(String name) {
        TypedQuery<RepositoryEntity> query = em.createNamedQuery("Repository.findByName", RepositoryEntity.class);
        query.setParameter("name", name);
        List<RepositoryEntity> list = query.getResultList();
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }
}
