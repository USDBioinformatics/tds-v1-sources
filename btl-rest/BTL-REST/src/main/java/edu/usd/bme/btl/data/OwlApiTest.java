package edu.usd.bme.btl.data;

///* This file is part of the OWL API.
// * The contents of this file are subject to the LGPL License, Version 3.0.
// * Copyright 2014, The University of Manchester
// * 
// * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
// * You should have received a copy of the GNU General Public License along with this program.  If not, see http://www.gnu.org/licenses/.
// *
// * Alternatively, the contents of this file may be used under the terms of the Apache License, Version 2.0 in which case, the provisions of the Apache License Version 2.0 are applicable instead of those above.
// * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
// * http://www.apache.org/licenses/LICENSE-2.0
// * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. */
//package org.usd.edu.btl.data;
//
//import java.io.File;
//import java.net.URI;
//import java.util.Collections;
//import javax.annotation.Nonnull;
//
//import org.semanticweb.owlapi.apibinding.OWLManager;
//import org.semanticweb.owlapi.model.IRI;
//import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
//import org.semanticweb.owlapi.model.OWLOntology;
//import org.semanticweb.owlapi.model.OWLOntologyCreationException;
//import org.semanticweb.owlapi.model.OWLOntologyManager;
//import org.semanticweb.owlapi.util.OWLOntologyWalker;
//import org.semanticweb.owlapi.util.OWLOntologyWalkerVisitor;
//import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
//import org.semanticweb.owlapi.util.OWLOntologyWalkerVisitorEx;
//
///**
// * Simple Rendering Example. Reads an ontology and then renders it.
// *
// * @author Sean Bechhofer, The University Of Manchester, Information Management
// * Group
// * @since 2.0.0
// */
//public class OwlApiTest {
//
//    /**
//     * @param inputOntology input ontology IRI
//     * @param outputOntology output ontology IRI
//     * @throws OWLOntologyCreationException OWLOntologyCreationException
//     */
//    public static void render(@Nonnull URI inputOntology, @Nonnull URI outputOntology) throws OWLOntologyCreationException {
//        // A simple example of how to load and save an ontology
//        /* Get an Ontology Manager */
//        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
//        IRI inputDocumentIRI = IRI.create(inputOntology);
//        IRI outputDocumentIRI = IRI.create(outputOntology); //not used yet
//
//        /* Load an ontology from a document IRI */
//        OWLOntology ontology = manager.loadOntologyFromOntologyDocument(inputDocumentIRI);
//        /* Report information about the ontology */
//        System.out.println("Ontology Loaded...");
//        System.out.println("Document IRI: " + inputDocumentIRI);
//        System.out.println("Logical IRI : " + ontology.getOntologyID());
//        System.out.println("Format      : "
//                + manager.getOntologyFormat(ontology));
//
//        System.out.println("=== Walking the Ontology ===");
//    }
//
//    public static void walk(URI input) throws Exception {
//        IRI inputIRI = IRI.create(input);
//        // How to walk the asserted structure of an ontology
//        OWLOntologyManager m = OWLManager.createOWLOntologyManager();
//        OWLOntology o = m.loadOntologyFromOntologyDocument(inputIRI);
//
//        //Create the walker
//        OWLOntologyWalker walker = new OWLOntologyWalker(Collections.singleton(o));
//// Now ask our walker to walk over the ontology
//        
//        OWLOntologyWalkerVisitorEx<Object> visitor = new OWLOntologyWalkerVisitorEx<Object>(walker) {
//            int ct = 0;
//            @Override
//            public Object visit(OWLObjectSomeValuesFrom desc) {
//                System.out.println("Vist #: " + ct);
//                // Print out the restriction
//                System.out.println(desc);
//                // Print out the axiom where the restriction is used
//                System.out.println("         " + getCurrentAxiom());
//                System.out.println(getCurrentAnnotation());
//// We don't need to return anything here.
//                ct++;
//                return "";
//            }
//        };
//        // Now ask the walker to walk over the ontology structure using our visitor instance.
//        walker.walkStructure(visitor);
//    }
//
//    public static void printOntology() throws OWLOntologyCreationException {
//        File inputF = new File("C:\\Users\\Tyler\\Documents\\GitHub\\BTL-REST\\EDAM.owl");
//        URI inputURI = inputF.toURI();
//        IRI inputIRI = IRI.create(inputURI);
//
//        OWLOntologyManager printManager = OWLManager.createOWLOntologyManager();
//        OWLOntology ontol = printManager.loadOntologyFromOntologyDocument(inputIRI);
//
//        IRI ontologyIRI = ontol.getOntologyID().getOntologyIRI().get();
//        IRI documentIRI = printManager.getOntologyDocumentIRI(ontol);
//        System.out.println("Printing Ontology ===");
//        System.out.println(ontologyIRI == null ? "anonymous" : ontologyIRI.toQuotedString());
//        System.out.println("    from " + documentIRI.toQuotedString());
//    }
//}
