/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.rest.resources.JSONentities;

import com.fasterxml.jackson.annotation.JsonProperty;
import edu.usd.bme.btl.entities.RepositoryToolEntity;

/**
 *
 * @author Bill Conn <Bill.Conn@usd.edu>
 */
public class RepoToolJSON {

    private Integer toolId;
    private Integer repoId;
    private String repoName;

    public RepoToolJSON() {

    }

    public RepoToolJSON(RepositoryToolEntity repoEnt) {
        if (repoEnt.getToolId() != null) {
            this.toolId = repoEnt.getToolId().getId();
        }
        if (repoEnt.getRepositoryId() != null) {
            this.repoId = repoEnt.getRepositoryId().getId();
            this.repoName = repoEnt.getRepositoryId().getName();
        }
    }

    @JsonProperty("toolId")
    public Integer getToolId() {
        return toolId;
    }

    @JsonProperty("toolId")
    public void setToolId(Integer toolId) {
        this.toolId = toolId;
    }

    @JsonProperty("repoId")
    public Integer getRepoId() {
        return repoId;
    }

    @JsonProperty("repoId")
    public void setRepoId(Integer repoId) {
        this.repoId = repoId;
    }

    @JsonProperty("repoName")
    public String getRepoName() {
        return repoName;
    }

    @JsonProperty("repoName")
    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }
}
