/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.rest.resources.JSONentities;

import org.codehaus.jackson.annotate.JsonProperty;
import edu.usd.bme.btl.entities.RepositoryToolEntity;

/**
 *
 * @author Bill Conn <Bill.Conn@usd.edu>
 */
public class RepoTool {

    private Integer id;
    private Integer toolId;
    private Integer repoId;

    public RepoTool() {

    }

    public RepoTool(RepositoryToolEntity inputTool) {
        this.id = inputTool.getId();
        if (inputTool.getToolId() != null) {
            this.toolId = inputTool.getToolId().getId();
        }
        if (inputTool.getRepositoryId() != null) {
            this.repoId = inputTool.getRepositoryId().getId();
        }
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("toolId")
    public Integer getToolId() {
        return toolId;
    }

    @JsonProperty("toolId")
    public void setToolId(Integer toolId) {
        this.toolId = toolId;
    }

    @JsonProperty("repoId")
    public Integer getRepoId() {
        return repoId;
    }

    @JsonProperty("repoId")
    public void setRepoId(Integer repoId) {
        this.repoId = repoId;
    }
}
