/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.rest.resources.repos;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import edu.usd.bme.btl.entities.RepositoryToolEntity;
import edu.usd.bme.btl.entities.ToolBetsEntity;
import edu.usd.bme.btl.facades.AbstractFacade;
import edu.usd.bme.btl.rest.resources.JSONentities.RepoToolJSON;

/**
 * RepositoryGetByToolREST is a class that manages the restful endpoint of
 *  resources/repos/getbytoolids.  This endpoint returns a null object in an
 *  array if you just get the endpoint and it takes in a list of the same type
 *  of JSON objects with the POST method.  The POST method retrieves the proper
 *  data from the database about the requested tools and fills in this data,
 *  then returns it to the client.
 * 
 * @author Bill Conn <Bill.Conn@usd.edu>
 */
@Stateless
@Path("repos/getbytoolids")
@Produces({"application/json"})
@Consumes({"application/json"})
public class RepositoryGetByToolREST extends AbstractFacade<RepositoryToolEntity> {

    // Need an entity manager to pull things out of the database
    @PersistenceContext(unitName = "org.usd.edu_BTL-REST_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Default Constructor for this class
     */
    public RepositoryGetByToolREST() {
        super(RepositoryToolEntity.class);
    }

    /**
     * returnTemplate is a method that returns an array with 1 json object to
     *  show the client how the POST method expects input to be formated.
     * 
     * @param prettyPrint
     * @return
     * @throws JsonProcessingException 
     */
    @GET
    public Response returnTemplate(@DefaultValue("false") @QueryParam("PrettyPrint") boolean prettyPrint) throws JsonProcessingException{
        List<RepoToolJSON> results = new ArrayList();
        //create a new object full of 'nulls' to return
        results.add(new RepoToolJSON());
        
        // Check if the user wants us to pretty print or not
        //  Pretty printing uses jackson's objectmapper.
        if (prettyPrint) {
            ObjectMapper mapper = new ObjectMapper();
            String prettyResults = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(results);
            return Response.status(200).entity(prettyResults).build();
        } else {
            return Response.status(200).entity(results).build();
        }
    }

    /**
     * findIds is a method that fills out the repository name and id for every
     *  tool id that is submitted to it in the POST.  This is currently a slow
     *  method because if you request all the tools, it will loop through every
     *  single thing in the database 1 request at a time.
     * 
     * @param prettyPrint
     * @param incomingTools
     * @return
     * @throws JsonProcessingException 
     */
    @POST
    public Response findIds(@DefaultValue("false") @QueryParam("PrettyPrint") boolean prettyPrint, List<RepoToolJSON> incomingTools) throws JsonProcessingException {
        List<RepoToolJSON> results = new ArrayList();
        List<ToolBetsEntity> tools = new ArrayList();

        // For each tool id that was passed in, find it's repo info and add it
        //  to the list of results
        for (RepoToolJSON temp : incomingTools) {
            Query getTool;
            ToolBetsEntity tempBE = new ToolBetsEntity();
            tempBE.setId(temp.getToolId());
            getTool = em.createNamedQuery("RepositoryTool.findByToolId");
            getTool.setParameter("toolId", tempBE);

            // Because repos get added more than once if a tool gets data from
            //  more than one source, we will return only the first result
            List<RepositoryToolEntity> tempResults = getTool.getResultList();
            if (tempResults.size() > 0) {
                results.add(new RepoToolJSON(tempResults.get(0)));
            }
        }

        // Check if the user wants us to pretty print or not
        //  Pretty printing uses jackson's objectmapper.
        if (prettyPrint) {
            ObjectMapper mapper = new ObjectMapper();
            String prettyResults = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(results);
            return Response.status(200).entity(prettyResults).build();
        } else {
            return Response.status(200).entity(results).build();
        }
    }

    // Required override function to return entity manager
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
