package edu.usd.bme.btl.facades;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import org.usd.edu.btl.betsconverter.BETSV1.BETSV1;
import edu.usd.bme.btl.entities.ToolBetsEntity;

/**
 *
 * @author Tyler_000
 */
@Stateless
public class ToolBetsEntityFacade extends AbstractFacade<ToolBetsEntity> {

    Date today = Calendar.getInstance().getTime();
    Timestamp timestamp = new Timestamp(today.getTime());

    @PersistenceContext(unitName = "org.usd.edu_BTL-REST_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ToolBetsEntityFacade() {
        super(ToolBetsEntity.class);
    }

    @Override
    public void create(ToolBetsEntity tool) throws Exception {
        ToolBetsEntity temp = findByNameAndVersion(tool.getName(), tool.getVersion());
        if (temp != null) {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                BETSV1 newBets = objectMapper.readValue(tool.getBets(), BETSV1.class);
                BETSV1 oldBets = objectMapper.readValue(temp.getBets(), BETSV1.class);
                newBets = newBets.combine(oldBets);
                ObjectWriter ow = objectMapper.writer();
                temp.setBets(ow.writeValueAsString(newBets).getBytes("UTF-8"));
                temp.setDateUpdated(timestamp);
                super.edit(temp);
            } catch (Exception ex) {
                Logger.getLogger(ToolBetsEntityFacade.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            tool.setDateAdded(timestamp);
            tool.setDateUpdated(timestamp);
            super.create(tool);
        } 
    }

    @Override
    public void edit(ToolBetsEntity tool) throws Exception {
        ToolBetsEntity temp = findByNameAndVersion(tool.getName(), tool.getVersion());
        if (temp != null) {
            if (temp.getId() != tool.getId()) {
                throw new Exception("Tool name and version must be unique");
            } else {
                ObjectMapper objectMapper = new ObjectMapper();
                BETSV1 newBets = objectMapper.readValue(tool.getBets(), BETSV1.class);
                BETSV1 oldBets = objectMapper.readValue(temp.getBets(), BETSV1.class);
                newBets = newBets.combine(oldBets);
                ObjectWriter ow = objectMapper.writer();
                tool.setBets(ow.writeValueAsString(newBets).getBytes("UTF-8"));
                tool.setDateUpdated(timestamp);
                super.edit(tool);
            }
        }
    }

    public ToolBetsEntity findByNameAndVersion(String name, String version) {
        TypedQuery<ToolBetsEntity> query;
        if (version == null || version.length() == 0) {
            query = em.createNamedQuery("findToolByName", ToolBetsEntity.class);
        } else {
            query = em.createNamedQuery("findToolByNameAndVersion", ToolBetsEntity.class);
            query.setParameter("version", version);
        }
        query.setParameter("name", name);

        List<ToolBetsEntity> list = query.getResultList();
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }
}
