/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.rest.resources.repos;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import edu.usd.bme.btl.entities.RepositoryEntity;
import edu.usd.bme.btl.facades.AbstractFacade;

/**
 * RepositoryREST class is for managing the resources/repo endpoint.  It simply
 *   returns a list of all the repos our database is compiled from.
 * 
 * @author Tyler
 * @author Bill Conn <Bill.Conn@usd.edu>
 */
@Stateless
@Path("repos")
@Produces({"application/json"})
public class RepositoryREST extends AbstractFacade<RepositoryEntity> {

    // Need the entity manager
    @PersistenceContext(unitName = "org.usd.edu_BTL-REST_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Default Constructor
     */
    public RepositoryREST() {
        super(RepositoryEntity.class);
    }

    /**
     * find will return a particular Repository Entity based on the supplied id.
     *   It will also pretty print if requested.
     * 
     * @param id
     * @param prettyPrint
     * @return
     * @throws JsonProcessingException 
     */
    @GET
    @Path("{id}")
    public Response find(@PathParam("id") Integer id, @DefaultValue("false") @QueryParam("PrettyPrint") boolean prettyPrint) throws JsonProcessingException {
        RepositoryEntity repository = super.find(id);
        
        // Check if the user wants us to pretty print or not
        //  Pretty printing uses jackson's objectmapper.
        if (prettyPrint) {
            ObjectMapper mapper = new ObjectMapper();
            String prettyResults = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(repository);
            return Response.ok().entity(prettyResults).build();
        } else {
            return Response.ok().entity(repository).build();
        }
    }

    /**
     * returnRepoInformation returns a list of every Repository, it's name, and
     *  its url to the user.  It will pretty print if necessary.
     * 
     * @param prettyPrint
     * @return
     * @throws JsonProcessingException 
     */
    @GET
    public Response returnRepoInformation(@DefaultValue("false") @QueryParam("PrettyPrint") boolean prettyPrint) throws JsonProcessingException {
        //Use a named query to get all of the repos in the database
        Query getAllBets = em.createNamedQuery("Repository.findAll");
        List<RepositoryEntity> repos = getAllBets.getResultList(); //list of objects

        // Check if the user wants us to pretty print or not
        //  Pretty printing uses jackson's objectmapper.
        if (prettyPrint) {
            ObjectMapper mapper = new ObjectMapper();
            String prettyResults = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(repos);
            return Response.ok().entity(prettyResults).build();
        } else {
            return Response.ok().entity(repos).build();
        }
    }

    /**
     * countREST returns the count of the number of different repositories our
     *  tools have come from.
     * 
     * @return 
     */
    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    /**
     * getEntityManager is a required override from AbstractFacade
     * 
     * @return 
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
