package edu.usd.bme.btl.data;

//package org.usd.edu.btl.data;
//
//import java.io.File;
//import java.io.IOException;
//import java.net.URI;
//import java.util.ArrayList;
//import java.util.Collections;
//import org.semanticweb.owlapi.apibinding.OWLManager;
//import org.semanticweb.owlapi.model.IRI;
//import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
//import org.semanticweb.owlapi.model.OWLOntology;
//import org.semanticweb.owlapi.model.OWLOntologyManager;
//import org.semanticweb.owlapi.util.AutoIRIMapper;
//import org.semanticweb.owlapi.util.OWLOntologyWalker;
//import org.semanticweb.owlapi.util.OWLOntologyWalkerVisitor;
//import org.semanticweb.owlapi.util.SimpleIRIMapper;
//
///**
// *
// * @author Tyler
// */
//public class AddBridges {
//
//    public static void main(String[] args) throws Exception {
//        //create URI from local file
//        File f = new File("C:\\Users\\Tyler\\Documents\\GitHub\\BTL-REST\\EDAM.owl");
//        URI u = f.toURI();
//
//        //create ontology manager
//        OWLOntologyManager m = OWLManager.createOWLOntologyManager();
//        IRI edamIRI = IRI.create(u); //map local owl file to IRI
//
//        OWLOntology edamOntology = m.loadOntologyFromOntologyDocument(edamIRI);
//        System.out.println("Loaded Ontology: " + edamOntology);
//
//        // How to walk the asserted structure of an ontology
//// Create the walker
//        OWLOntologyWalker walker
//                = new OWLOntologyWalker(Collections.singleton(edamOntology));
//// Now ask our walker to walk over the ontology
//        OWLOntologyWalkerVisitor<Object> visitor
//                = new OWLOntologyWalkerVisitor<Object>(walker) {
//                    @Override
//                    public Object visit(OWLObjectSomeValuesFrom desc) {
//                        System.out.println(desc);
//                        System.out.println(" " + getCurrentAxiom());
//                        return null;
//                    }
//                };
//// Have the walker walk...
//        walker.walkStructure(visitor);
//    }
//}
