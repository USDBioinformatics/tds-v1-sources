/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import org.usd.edu.btl.betsconverter.BETSV1.BETSV1;
import org.usd.edu.btl.betsconverter.BLDV1.BLDV1;
import org.usd.edu.btl.betsconverter.GalaxyV1.Tool;
import org.usd.edu.btl.betsconverter.OMICToolsV1.OMICToolV1;
import org.usd.edu.btl.betsconverter.SeqV1.SeqV1;
import org.usd.edu.btl.betsconverter.iPlantV1.IplantV1;
import org.usd.edu.btl.converters.BLDConverter;
import org.usd.edu.btl.converters.GalaxyConverter;
import org.usd.edu.btl.converters.IplantConverter;
import org.usd.edu.btl.converters.OMICToolConverter;
import org.usd.edu.btl.converters.SeqConverter;
import edu.usd.bme.btl.entities.RepositoryEntity;
import edu.usd.bme.btl.entities.RepositoryToolEntity;
import edu.usd.bme.btl.entities.ToolBetsEntity;
import edu.usd.bme.btl.entities.ToolBridgeEntity;
import edu.usd.bme.btl.facades.RepositoryEntityFacade;
import edu.usd.bme.btl.facades.RepositoryToolEntityFacade;
import edu.usd.bme.btl.facades.ToolBetsEntityFacade;

/**
 *
 * @author Shayla.Gustafson
 *
 * Class for adding
 * @{Link ToolBetsEntity} to the database. Specify the root path of all tools to
 * be added and then call writeToBd(). (Not sure why it extends HttpServlet)
 */
@ManagedBean
public class TestServlet extends HttpServlet {
    //Folders containing the input files from SEQ, Galaxy, BLD, and IPlant
    //These should be the output folders from ToolChecker project 

    private static String SEQ_INPUT_FILES = "C:\\Users\\Bill.Conn\\Documents\\NetBeansProjects\\BTL-ToolData\\2-AfterToolsChecker\\Seq_Success/";
    private static String GALAXY_INPUT_FILES = "C:\\Users\\Bill.Conn\\Documents\\NetBeansProjects\\BTL-ToolData\\2-AfterToolsChecker\\Galaxy_Success/";
    private static String BIO_LINK_DIR_INPUT_FILES = "C:\\Users\\Bill.Conn\\Documents\\NetBeansProjects\\BTL-ToolData\\2-AfterToolsChecker\\BLD_Success/";
    private static String IPLANT_INPUT_FILES = "C:\\Users\\Bill.Conn\\Documents\\NetBeansProjects\\BTL-ToolData\\2-AfterToolsChecker\\iPlant_Success/";
    private static String OMIC_INPUT_FILES = "C:\\Users\\Bill.Conn\\Documents\\NetBeansProjects\\BTL-ToolData\\2-AfterToolsChecker\\Omic_Success6/";
    //Names of the Repositories - IF CHANGED, ADDITIONAL REPOSITORIES WILL BE CREATED IN DATABASE
    private final String SEQ_REPOS = "SEQanswers";
    private final String GALAXY_REPOS = "Galaxy";
    private final String BIO_LINK_DIR_REPOS = "Bioinformatics Link Directory";
    private final String IPLANT_REPOS = "iPlant";
    private final String OMIC_REPOS = "OMIC Tools";
    @EJB
    private ToolBetsEntityFacade toolFacade;
    @EJB
    private RepositoryEntityFacade reposFacade;
    @EJB
    private RepositoryToolEntityFacade reposToolFacade;
    private ArrayList<File> inputFiles = new ArrayList();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        /* TODO output your page here. You may use following sample code. */
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlet TestServlet</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Servlet TestServlet at " + request.getContextPath() + " " + request.getMethod() + "</h1>");
        out.println("</body>");
        out.println("</html>");
        try {
            writeToDb();
        } catch (Exception e) {
        }
    }

    /**
     * writeToDb class simply writes to the db whatever files are read in.
     * currently it reads 4 individual files but galaxy and bld converter is
     * broken so the galaxy skips reading and the bld one throws a spectatular
     * error so I skip entering it.
     *
     * @throws java.lang.Exception
     */
    public void writeToDb() throws Exception {
        ObjectMapper mapper = new ObjectMapper(); //create new Jackson Mapper
        RepositoryEntity repos;
        System.out.println("Writing to DB....");
        //List<File> files = new ArrayList<File>();

        //files.add(new File("/data_files/test_galaxy.xml"));
        //files.add(new File("C:\\Users\\Bill.Conn\\Documents\\NetBeansProjects\\BTL-REST\\BTL-REST\\data_files\\seq.json"));
        //files.add(new File("C:\\Users\\Bill.Conn\\Documents\\NetBeansProjects\\BTL-REST\\BTL-REST\\data_files\\test_galaxy.xml"));
        //files.add(new File("C:\\Users\\Bill.Conn\\Documents\\NetBeansProjects\\BTL-REST\\BTL-REST\\data_files\\seq.json"));
        //files.add(new File("/Users/Tyler/Documents/GitHub/BTL-REST/BTL-REST/data_files/A5.json"));
        //files.add(new File("/Users/Tyler/Documents/GitHub/BTL-REST/BTL-REST/data_files/test_iplant.json"));
        // files.add(new File("/Users/Tyler/Documents/GitHub/BTL-REST/BTL-REST/data_files/test_BLD.json"));
////==============================================================================
////      SEQANSWERS
////==============================================================================
//        // <editor-fold defaultstate="collapsed" desc="code to store seqanswers tools in db">
//        repos = new RepositoryEntity();
//        repos.setName(SEQ_REPOS);
//        try {
//            reposFacade.create(repos);
//        } catch (Exception e) {
//            //Repository already exists
//        }
//        //Get the RepositoryTool back from the database... now has an Id
//        repos = reposFacade.findByName(SEQ_REPOS);
//
//        File[] seqFiles = getSeqFiles();
//        for (File seqFile : seqFiles) {
//            inputFiles.add(seqFile); //add all seq files to global File array
//        }
//        System.out.println("INPUT FILE SIZE = " + inputFiles.size());
//        //TestFileRead ontoRead = new TestFileRead();
//        for (File file : inputFiles) {
//            File seqFile = file;
//            try {
//                BETSV1 tmpBETS = seqToBETS(seqFile);
//                ToolBetsEntity dbBETS = new ToolBetsEntity();
//
//                dbBETS.setName(tmpBETS.getName());
//                dbBETS.setSummary(tmpBETS.getSummary());
//                dbBETS.setVersion(tmpBETS.getVersion());
//
//                ObjectWriter ow = mapper.writer();
//                dbBETS.setBets(ow.writeValueAsString(tmpBETS).getBytes("UTF-8"));
//                toolFacade.create(dbBETS);
//
//                //Get that ToolBetsEntity back and create a RepositoryToolEntity 
//                dbBETS = toolFacade.findByNameAndVersion(tmpBETS.getName(), tmpBETS.getVersion());
//                RepositoryToolEntity reposTool = new RepositoryToolEntity();
//                reposTool.setRepositoryId(repos);
//                reposTool.setToolId(dbBETS);
//                reposToolFacade.create(reposTool);
//                //If this already exists, will throw an exception... Do we want to catch all
//                //exceptions separately and handle differently for each?
//            } catch (Exception e) {
//                System.out.println("Exception: " + e.getMessage());
//                System.out.println("For: " + file.toString());
//            }
//
//        }
//        System.out.println("Seq Finished");
//        inputFiles.clear(); //clear files from inputFiles
//        // </editor-fold>
//
////==============================================================================
////      GALAXY
////==============================================================================
//        // <editor-fold defaultstate="collapsed" desc="code to insert galaxy tools">
//        repos = new RepositoryEntity();
//        repos.setName(GALAXY_REPOS);
//        try {
//            reposFacade.create(repos);
//        } catch (Exception e) {
//            //Repository already exists
//        }
//        //Get the RepositoryTool back from the database... now has an Id
//        repos = reposFacade.findByName(GALAXY_REPOS);
//
//        File[] galaxyFiles = getGalaxyFiles();
//        for (File file : galaxyFiles) {
//            inputFiles.add(file);
//        }
//        System.out.println("INPUT FILE SIZE = " + inputFiles.size());
//
//        for (File file : inputFiles) {
//            File galaxyFile = file;
//            BETSV1 tmpBETS = null;
//            try {
//                tmpBETS = galaxyToBETS(galaxyFile);
//                ToolBetsEntity dbBETS = new ToolBetsEntity();
//
//                dbBETS.setName(tmpBETS.getName());
//                dbBETS.setSummary(tmpBETS.getSummary());
//                dbBETS.setVersion(tmpBETS.getVersion());
//                ObjectWriter ow = mapper.writer();
//                dbBETS.setBets(ow.writeValueAsString(tmpBETS).getBytes("UTF-8"));
//
//                toolFacade.create(dbBETS);
//
//                //Get that ToolBetsEntity back and create a RepositoryToolEntity 
//                dbBETS = toolFacade.findByNameAndVersion(tmpBETS.getName(), tmpBETS.getVersion());
//                RepositoryToolEntity reposTool = new RepositoryToolEntity();
//                reposTool.setRepositoryId(repos);
//                reposTool.setToolId(dbBETS);
//                reposToolFacade.create(reposTool);
//                //If this already exists, will throw an exception... Do we want to catch all
//                //exceptions separately and handle differently for each?
//            } catch (Exception e) {
//                System.out.println("Exception: " + e.getMessage());
//                System.out.println("For:" + file.toString());
//            }
//        }
//        System.out.println("Galaxy Finished");
//        inputFiles.clear();
//        // </editor-fold>
//
////==============================================================================
////      BIOINFORMATICS LINK DIRECTORY
////==============================================================================
//        // <editor-fold defaultstate="collapsed" desc="code to store bld tools in db">
//        repos = new RepositoryEntity();
//        repos.setName(BIO_LINK_DIR_REPOS);
//        try {
//            reposFacade.create(repos);
//        } catch (Exception e) {
//            //Repository already exists
//        }
//        //Get the RepositoryTool back from the database... now has an Id
//        repos = reposFacade.findByName(BIO_LINK_DIR_REPOS);
//
//        File[] bldFiles = getBldFiles();
//        for (File file : bldFiles) {
//            inputFiles.add(file);
//        }
//
//        System.out.println("INPUT FILE SIZE = " + inputFiles.size());
//
//        for (File file : inputFiles) {
//            File bldFile = file;
//            BETSV1 tmpBETS = null;
//            try {
//                tmpBETS = bldToBETS(bldFile);
//                ToolBetsEntity dbBETS = new ToolBetsEntity();
//
//                dbBETS.setName(tmpBETS.getName());
//                dbBETS.setSummary(tmpBETS.getSummary());
//                dbBETS.setVersion(tmpBETS.getVersion());
//                ObjectWriter ow = mapper.writer();
//                dbBETS.setBets(ow.writeValueAsString(tmpBETS).getBytes("UTF-8"));
//
//                if (dbBETS.getName() != null) {
//                    toolFacade.create(dbBETS);
//                } else {
//                    System.out.println("NULL NAME");
//                    break;
//                }
//
//                //Get that ToolBetsEntity back and create a RepositoryToolEntity 
//                dbBETS = toolFacade.findByNameAndVersion(tmpBETS.getName(), tmpBETS.getVersion());
//                RepositoryToolEntity reposTool = new RepositoryToolEntity();
//                reposTool.setRepositoryId(repos);
//                reposTool.setToolId(dbBETS);
//                reposToolFacade.create(reposTool);
//                //If this already exists, will throw an exception... Do we want to catch all
//                //exceptions separately and handle differently for each?
//            } catch (Exception e) {
//                System.out.println("Exception: " + e.getMessage());
//                System.out.println("For:" + file.toString());
//            }
//        }
//        System.out.println("BLD finished");
//        inputFiles.clear();
//        // </editor-fold>
//
////==============================================================================
////      IPLANT
////==============================================================================
//        // <editor-fold defaultstate="collapsed" desc="code to store iplant v1 toosl in db">
//        repos = new RepositoryEntity();
//        repos.setName(IPLANT_REPOS);
//        try {
//            reposFacade.create(repos);
//        } catch (Exception e) {
//            //Repository already exists
//        }
//        //Get the RepositoryTool back from the database... now has an Id
//        repos = reposFacade.findByName(IPLANT_REPOS);
//        File[] iplantFiles = getIplantFiles();
//        for (File file : iplantFiles) {
//            inputFiles.add(file);
//        }
//
//        System.out.println("INPUT FILE SIZE = " + inputFiles.size());
//
//        for (File file : inputFiles) {
//            File iplantFile = file;
//            BETSV1 tmpBETS = null;
//            try {
//                tmpBETS = iplantToBETS(iplantFile);
//                ToolBetsEntity dbBETS = new ToolBetsEntity();
//
//                dbBETS.setName(tmpBETS.getName());
//                dbBETS.setSummary(tmpBETS.getSummary());
//                dbBETS.setVersion(tmpBETS.getVersion());
//                ObjectWriter ow = mapper.writer();
//                dbBETS.setBets(ow.writeValueAsString(tmpBETS).getBytes("UTF-8"));
//
//                if (dbBETS.getName() != null) {
//                    toolFacade.create(dbBETS);
//                } else {
//                    System.out.println("NULL NAME");
//                    break;
//                }
//
//                //Get that ToolBetsEntity back and create a RepositoryToolEntity 
//                dbBETS = toolFacade.findByNameAndVersion(tmpBETS.getName(), tmpBETS.getVersion());
//                RepositoryToolEntity reposTool = new RepositoryToolEntity();
//                reposTool.setRepositoryId(repos);
//                reposTool.setToolId(dbBETS);
//
//                reposToolFacade.create(reposTool);
//                //If this already exists, will throw an exception... Do we want to catch all
//                //exceptions separately and handle differently for each?
//            } catch (Exception e) {
//                System.out.println("Exception: " + e.getMessage());
//                System.out.println("For:" + file.toString());
//            }
//        }
//        System.out.println("iPlant finished");
//        inputFiles.clear();
//        // </editor-fold>
//
//==============================================================================
//      OMIC
//==============================================================================
        // <editor-fold defaultstate="collapsed" desc="code to store omic tools in the db">
        repos = new RepositoryEntity();
        repos.setName(OMIC_REPOS);
        try {
            reposFacade.create(repos);
        } catch (Exception e) {
            //Repository already exists
        }
        //Get the RepositoryTool back from the database... now has an Id
        repos = reposFacade.findByName(OMIC_REPOS);
        File[] omicFiles = getOmicFiles();
        for (File file : omicFiles) {
            inputFiles.add(file);
        }        
        
        System.out.println("INPUT FILE SIZE = " + inputFiles.size());

        for (File file : omicFiles) {
            File omicFile = file;
            BETSV1 tmpBETS = null;
            try {
                tmpBETS = omicToBETS(omicFile);
                ToolBetsEntity dbBETS = new ToolBetsEntity();

                dbBETS.setName(tmpBETS.getName());
                dbBETS.setSummary(tmpBETS.getSummary());
                dbBETS.setVersion(tmpBETS.getVersion());
                ObjectWriter ow = mapper.writer();
                dbBETS.setBets(ow.writeValueAsString(tmpBETS).getBytes("UTF-8"));

                if (dbBETS.getName() != null) {
                    toolFacade.create(dbBETS);
                } else {
                    System.out.println("NULL NAME");
                    break;
                }

                //Get that ToolBetsEntity back and create a RepositoryToolEntity 
                dbBETS = toolFacade.findByNameAndVersion(tmpBETS.getName(), tmpBETS.getVersion());
                RepositoryToolEntity reposTool = new RepositoryToolEntity();
                reposTool.setRepositoryId(repos);
                reposTool.setToolId(dbBETS);

                reposToolFacade.create(reposTool);
                //If this already exists, will throw an exception... Do we want to catch all
                //exceptions separately and handle differently for each?
            } catch (Exception e) {
                System.out.println("Exception: " + e.getMessage());
                System.out.println("For:" + file.toString());
            }
        }
        System.out.println("Omic Finished");
        inputFiles.clear();
        // </editor-fold>
    }

    /* 
     HELPER METHODS
     */
    // <editor-fold defaultstate="collapsed" desc="helper methods to get files">
    public static File[] getSeqFiles() {
        File f = new File(SEQ_INPUT_FILES);
        File[] files = f.listFiles();
        System.out.println("---Seq files have been created!---");
        return files;
    }

    public static File[] getGalaxyFiles() {
        File f = new File(GALAXY_INPUT_FILES);
        File[] files = f.listFiles();
        System.out.println("---galaxy files have been created!---");
        return files;
    }

    public static File[] getBldFiles() {
        File f = new File(BIO_LINK_DIR_INPUT_FILES);
        File[] files = f.listFiles();
        System.out.println("---bld files have been created!---");
        return files;
    }

    public static File[] getIplantFiles() {
        File f = new File(IPLANT_INPUT_FILES);
        File[] files = f.listFiles();
        System.out.println("---iPlant files have been created!---");
        return files;
    }

    public static File[] getOmicFiles() {
        File f = new File(OMIC_INPUT_FILES);
        File[] files = f.listFiles();
        System.out.println("---OMIC files have been created!---");
        return files;
    }
    // </editor-fold>


    // <editor-fold defaultstate="collapsed" desc="String converter methods.">
    /**
     * iplantToBets takes an iplant JSON file and returns a BETS object
     *
     * @param iplantJSON File
     * @return bets
     */
    public BETSV1 iplantToBETS(File iplantJSON) {
        String betsJSON;
        BETSV1 bets = null;
        ObjectMapper mapper = new ObjectMapper(); //create new Jackson Mapper
        IplantV1 ipTool;

        try {
            //map input json files to iplant class
            ipTool = mapper.readValue(iplantJSON, IplantV1.class);
            bets = IplantConverter.toBETS(ipTool); //pass the iplant tool spec, convert to bets
            ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
            betsJSON = ow.writeValueAsString(bets); //write Json as String
//            System.out.println("=== IPLANT TO BETS JSON === \n" + betsJSON);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return bets;
    }

    /**
     * seqToBETS takes a seq JSON File and returns a BETS object
     *
     * @param seqJSON File
     * @return BETS
     */
    public BETSV1 seqToBETS(File seqJSON) {
        String betsJSON;
        BETSV1 bets = null;
        ObjectMapper mapper = new ObjectMapper(); //create new Jackson Mapper
        SeqV1 seqTool;

        try {
            //map input json files to iplant class
            seqTool = mapper.readValue(seqJSON, SeqV1.class);
            bets = SeqConverter.toBETS(seqTool); //convert the seq tool
            ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
            betsJSON = ow.writeValueAsString(bets); //write Json as String
//            System.out.println("=== SEQ TO BETS JSON === \n" + betsJSON);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return bets;
    }

    /**
     * bldToBETS takes a BLD JSON File and returns a BETS object
     *
     * @param bldJSON File
     * @return BETS
     */
    public BETSV1 bldToBETS(File bldJSON) {
        String betsJSON;
        BETSV1 bets = null;
        ObjectMapper mapper = new ObjectMapper(); //create new Jackson Mapper
        BLDV1 bldTool;

        try {
            //map input json files to bld class
            bldTool = mapper.readValue(bldJSON, BLDV1.class);
            bets = BLDConverter.toBETS(bldTool); //convert he bld tool
            //need to do some ugly hacks here, should fix converter

            bets.setName(bets.getDisplay_name());
            bets.setSummary(bets.getDescription());
            ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
            betsJSON = ow.writeValueAsString(bets); //write Json as String
//            System.out.println("=== BLD TO BETS JSON === \n" + betsJSON);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return bets;
    }

    /**
     * galaxyToBETS takes a galaxy XML file and returns a BETS object
     *
     * @param galaxyXML File
     * @return BETS
     */
    public BETSV1 galaxyToBETS(File galaxyXML) throws Exception {
        String betsJSON;
        BETSV1 bets = null;
        ObjectMapper mapper = new ObjectMapper(); //create new Jackson Mapper
        Tool galaxyTool; //Galaxy main class is stupidly named 'Tool'

        InputStream infile = null;

        try {
            //map input json files to galaxy class
            JAXBContext jaxbContext = JAXBContext.newInstance(Tool.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller(); //Unmarshalling – Conversion XML content into a Java Object.
            infile = new FileInputStream(galaxyXML);
            galaxyTool = (Tool) unmarshaller.unmarshal(infile);
            bets = GalaxyConverter.toBETS(galaxyTool);
            ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
            betsJSON = ow.writeValueAsString(bets); //write Json as String
//            System.out.println("=== GALAXY TO BETS JSON === \n" + betsJSON);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new Exception(e.fillInStackTrace());
        } finally {
            try {
                infile.close();
            } catch (Exception e) {
                System.out.println("Terrible things afoot");
            }
        }
        return bets;
    }

    /**
     * omicToBETS takes a OMIC JSON File and returns a BETS object
     *
     * @param omicJSON File
     * @return BETS
     */
    public BETSV1 omicToBETS(File omicJSON) {
        String betsJSON;
        BETSV1 bets = null;
        ObjectMapper mapper = new ObjectMapper(); //create new Jackson Mapper
        OMICToolV1 omicTool;

        try {
            //map input json files to bld class
            omicTool = mapper.readValue(omicJSON, OMICToolV1.class);
            bets = OMICToolConverter.toBETS(omicTool); //convert he bld tool

            ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
            betsJSON = ow.writeValueAsString(bets); //write Json as String
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return bets;
    }

//</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
