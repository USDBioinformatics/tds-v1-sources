/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.rest.resources.JSONentities;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 *
 * @author Tyler
 */
public class ToolCount {
    
    private Integer size;
    
    @JsonProperty("size")
    public Integer getSize() {
        return size;
    }
    
    @JsonProperty("size")
    public void setSize(Integer size) {
        this.size = size;
    }
    
    
    
}
