/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Shayla.Gustafson
 */
@Entity
@Table(name = "repositorytool")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RepositoryTool.findAll", query = "SELECT r FROM RepositoryToolEntity r"),
    @NamedQuery(name = "RepositoryTool.findById", query = "SELECT r FROM RepositoryToolEntity r WHERE r.id = :id"),
    @NamedQuery(name = "RepositoryTool.findByToolId", query = "SELECT r FROM RepositoryToolEntity r WHERE r.toolId = :toolId"),
    @NamedQuery(name = "RepositoryTool.findByRepositoryId", query = "SELECT r FROM RepositoryToolEntity r WHERE r.repositoryId = :repositoryId"),
    @NamedQuery(name = "RepositoryTool.findByReposIdAndToolId", query = "SELECT r FROM RepositoryToolEntity r WHERE r.repositoryId = :repositoryId AND r.toolId = :toolId"),
    @NamedQuery(name = "RepositoryTool.findByToolIdList", query = "SELECT r FROM RepositoryToolEntity r WHERE r.toolId in :toolList") 
})
public class RepositoryToolEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
//    @NotNull
    @JoinColumn(name="tool_id", referencedColumnName="id")
    @ManyToOne(optional = false)
    private ToolBetsEntity toolId;
 //   @NotNull
    @JoinColumn(name = "repository_id", referencedColumnName="id")
    @ManyToOne(optional = false)
    private RepositoryEntity repositoryId;

    public RepositoryToolEntity() {
    }

    public RepositoryToolEntity(Integer id) {
        this.id = id;
    }

    public RepositoryToolEntity(Integer id, ToolBetsEntity toolId, RepositoryEntity repositoryId) {
        this.id = id;
        this.toolId = toolId;
        this.repositoryId = repositoryId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ToolBetsEntity getToolId() {
        return toolId;
    }

    public void setToolId(ToolBetsEntity toolId) {
        this.toolId = toolId;
    }

    public RepositoryEntity getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(RepositoryEntity repositoryId) {
        this.repositoryId = repositoryId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RepositoryToolEntity)) {
            return false;
        }
        RepositoryToolEntity other = (RepositoryToolEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.usd.edu.btl.entities.RepositoryTool[ id=" + id + ", tool= " + toolId + ", repository= "+ repositoryId + " ]";
    }
    
}
