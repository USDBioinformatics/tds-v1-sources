/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.usd.bme.btl.rest.resources;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Tyler
 */
@javax.ws.rs.ApplicationPath("resources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<Class<?>>();
        // following code can be used to customize Jersey 1.x JSON provider:
        try {
            Class jacksonProvider = Class.forName("org.codehaus.jackson.jaxrs.JacksonJsonProvider");
            resources.add(jacksonProvider);
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(edu.usd.bme.btl.rest.resources.BetsDataREST.class);
        resources.add(edu.usd.bme.btl.rest.resources.BridgesREST.class);
        resources.add(edu.usd.bme.btl.rest.resources.ToolsREST.class);
        resources.add(edu.usd.bme.btl.rest.resources.repos.RepositoryGetByToolREST.class);
        resources.add(edu.usd.bme.btl.rest.resources.repos.RepositoryREST.class);
        resources.add(edu.usd.bme.btl.rest.resources.repos.RepositoryToolREST.class);
        resources.add(edu.usd.bme.btl.rest.resources.store.PostBridges.class);
        resources.add(edu.usd.bme.btl.rest.resources.store.PostTools.class);
		resources.add(org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider.class);
		resources.add(org.codehaus.jackson.jaxrs.JacksonJsonProvider.class);
		resources.add(org.codehaus.jackson.jaxrs.JsonMappingExceptionMapper.class);
		resources.add(org.codehaus.jackson.jaxrs.JsonParseExceptionMapper.class);
    }
    
}
