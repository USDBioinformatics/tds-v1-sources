/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.facades;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import edu.usd.bme.btl.facades.AbstractFacade;
import edu.usd.bme.btl.entities.ToolBridgeEntity;

/**
 *
 * @author Tyler_000
 */
@Stateless
public class ToolBridgeEntityFacade extends AbstractFacade<ToolBridgeEntity> {
    @PersistenceContext(unitName = "org.usd.edu_BTL-REST_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ToolBridgeEntityFacade() {
        super(ToolBridgeEntity.class);
    }
    
}
