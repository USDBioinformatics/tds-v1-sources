/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.rest.resources.JSONentities;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 *
 * @author Tyler
 */
public class Bridge {
    
    private Integer bridgeId;
    private String ontologyId;
    private Integer toolId;
    
    @JsonProperty("bridgeId")
    public Integer getBridgeId() {
        return bridgeId;
    }
    @JsonProperty("bridgeId")
    public void setBridgeId(Integer bridgeId) {
        this.bridgeId = bridgeId;
    }
    @JsonProperty("ontologyId")
    public String getOntologyId() {
        return ontologyId;
    }
    @JsonProperty("ontologyId")
    public void setOntologyId(String ontologyId) {
        this.ontologyId = ontologyId;
    }
    @JsonProperty("toolId")
    public Integer getToolId() {
        return toolId;
    }
    @JsonProperty("toolId")
    public void setToolId(Integer toolId) {
        this.toolId = toolId;
    }
    
    
}
