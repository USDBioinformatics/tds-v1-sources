/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.rest.resources;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import org.json.JSONObject;
import org.json.JSONArray;
//import org.json.simple.*;
import org.json.simple.parser.*;
import org.usd.edu.btl.betsconverter.BETSV1.BETSV1;
import edu.usd.bme.btl.entities.ToolBetsEntity;
import javax.persistence.NoResultException;

/**
 *
 * @author Tyler
 */
@Stateless
@Path("bets")
@Produces({"application/json"})
//@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes({"application/json"})
public class BetsDataREST {

    @PersistenceContext(unitName = "org.usd.edu_BTL-REST_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    /*
     * REST Methods to get any Key/Value pair from BETS JSON
     */
    @GET
    public Response getAllBets(
			@DefaultValue("0") @QueryParam("index") Integer index,
			@DefaultValue("25") @QueryParam("count") Integer count,
			@DefaultValue("false") @QueryParam("pretty") Boolean pretty) {
		ObjectMapper mapper = new ObjectMapper();
		
		if(index < 0) {
			return Response.status(Response.Status.NOT_ACCEPTABLE)
					.entity("Invalid index location").build();
		}
		if(count < 1 || count > 1000) {
			return Response.status(Response.Status.NOT_ACCEPTABLE)
					.entity("Count range is 1 to 1000").build();
		}
		
		Query query = em.createNamedQuery("getAllBets")
				.setFirstResult(index)
				.setMaxResults(count);
        List<byte[]> betsList = query.getResultList();

		List<BETSV1> jsonBets = new ArrayList();
		for (byte[] bets : betsList) {
			try {
				jsonBets.add(mapper.readValue(new String((byte[]) bets, "UTF-8"),
						BETSV1.class));
			} catch (IOException e) {
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
						.build();
			}
		}
		
		if(pretty) {
			ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
			try {
				return Response.status(Response.Status.OK)
						.entity(writer.writeValueAsString(jsonBets)).build();
			} catch (JsonProcessingException e) {
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
						.entity(e.getMessage()).build();
			}
		} else {
			return Response.status(Response.Status.OK)
					.entity(jsonBets).build();
		}
    }
    
    
    @GET
    @Path("{id}")
    public Response getBETSById(
			@PathParam("id") Integer id,
			@DefaultValue("false") @QueryParam("pretty") Boolean pretty) {
		ObjectMapper mapper = new ObjectMapper();
		
        Query query = em.createNamedQuery("findBetsById");
        query.setParameter("id", id);
		
		byte[] bets;
		try {
			bets = (byte[]) query.getSingleResult();
		} catch (NoResultException e) {
			return Response.noContent().build();
		}
		
		BETSV1 jsonBets;
		try {
			jsonBets = mapper.readValue(
							new String((byte[]) bets, "UTF-8"),
							BETSV1.class);
		} catch (IOException e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.build();
		}
		
		if(pretty) {
			ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
			try {
				return Response.status(Response.Status.OK)
						.entity(writer.writeValueAsString(jsonBets)).build();
			} catch (JsonProcessingException e) {
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
						.entity(e.getMessage()).build();
			}
		} else {
			return Response.status(Response.Status.OK)
					.entity(jsonBets).build();
		}
    }

    /*
     * Get inputs for a bets, by id.
     *
     * @returns Response
     */
    @GET
    @Path("inputs/{id}")
    public Response getInputsById(@PathParam("id") Integer id) {
        //create query
        Query getBETSById = em.createNamedQuery("findToolById");
        getBETSById.setParameter("id", id);

        if(!getBETSById.getResultList().isEmpty()) {
            //get result as a ToolBetsEntity object
            ToolBetsEntity tool = (ToolBetsEntity) getBETSById.getSingleResult();
            byte[] bets = tool.getBets(); //this returns the BETS byte[]

            try {
            //convert byte[] to JSON string
            String betsJson = new String(bets, "UTF-8");
            //create parser to parse the bets string
            JSONParser parser = new JSONParser();
            JSONObject toolJson = (JSONObject) parser.parse(betsJson);
            //pull out the inputs JSONArray to return
            JSONArray inputList = (JSONArray) toolJson.getJSONArray("inputs");
            return Response.ok(inputList).build(); //displays bets byte[]
            }
            catch (UnsupportedEncodingException e) {
                return Response.notModified("Bad BETS encoding").build();
            }
            catch (ParseException e) {
                return Response.notModified("BETS parse error").build();
            }
        }
        return Response.noContent().build();
    }

}
