/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.rest.resources.store;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import edu.usd.bme.btl.entities.ToolBetsEntity;
import edu.usd.bme.btl.entities.ToolBridgeEntity;
import edu.usd.bme.btl.facades.AbstractFacade;
import edu.usd.bme.btl.rest.resources.JSONentities.Bridge;

/**
 *
 * @author Bill Conn <Bill.Conn@usd.edu>
 */
@Stateless
@Path("store-bridges")
@Produces({"application/json"})
@Consumes({"application/json"})
public class PostBridges extends AbstractFacade<ToolBridgeEntity> {

    @PersistenceContext(unitName = "org.usd.edu_BTL-REST_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public PostBridges() {
        super(ToolBridgeEntity.class);
    }

    @GET
    public Response getEmptyToolJson(@DefaultValue("false") @QueryParam("PrettyPrint") boolean prettyPrint) {
        Bridge blank = new Bridge();
//        ArrayList<ToolBetsJsonEntity> result = new ArrayList();
//        result.add(blank);

        return buildResponse(prettyPrint, blank);
    }

    @POST
    public Response saveNewBridge(Bridge bridge) {
        //first, make sure they aren't trying to update a specific brige entry.
        //Also, let's make sure the tools exist
        if (bridge == null || bridge.getToolId() == null || bridge.getOntologyId() == null) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
        ToolBetsEntity tool = getEntityManager().find(ToolBetsEntity.class, bridge.getToolId());
        if (tool == null || bridge.getBridgeId() != null) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
        //Build new Bridge entities and persist them
        URI newURI = null;
        ToolBridgeEntity temp = null;
        try {
            temp = new ToolBridgeEntity();
            temp.setOntologyId(bridge.getOntologyId());
            temp.setToolId(getEntityManager().find(ToolBetsEntity.class, bridge.getToolId()));
            getEntityManager().persist(temp);
            getEntityManager().flush();
            newURI = new URI("/BTL-REST/resources/bridges/"+temp.getId());            
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        return Response.status(Response.Status.CREATED).contentLocation(newURI).build();
    }

    @Path("list")
    @POST
    public Response saveNewBridges(List<Bridge> bridgeList) {
        //first, make sure they aren't trying to update a specific brige entry.
        //Also, let's make sure the tools exist
        for (Bridge bridge : bridgeList) {
            if (bridge != null) {
                ToolBetsEntity tool = getEntityManager().find(ToolBetsEntity.class, bridge.getToolId());
                if (tool == null || bridge.getBridgeId() != null) {
                    return Response.status(Response.Status.NOT_ACCEPTABLE).build();
                }
            }
        }
        for (Bridge bridge : bridgeList) {
            //Build new Bridge entities and persist them
            if (bridge != null) {
                ToolBridgeEntity temp = new ToolBridgeEntity();
                temp.setOntologyId(bridge.getOntologyId());
                temp.setToolId(getEntityManager().find(ToolBetsEntity.class, bridge.getToolId()));
                getEntityManager().persist(temp);
            }
        }
        return Response.status(Response.Status.CREATED).build();
    }

    @Path("{id}")
    @POST
    public Response updateABridge(@PathParam("id") Integer id, Bridge toUpdate) {
        //First Find the Bridge
        ToolBridgeEntity tbe = (ToolBridgeEntity) getEntityManager().find(ToolBridgeEntity.class, id);
        if (tbe == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        //Then make sure we are updating the one we say we are
        if (!id.equals(toUpdate.getBridgeId())) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
        //If they submitted new ontology, set that
        if (toUpdate.getOntologyId() != null) {
            tbe.setOntologyId(toUpdate.getOntologyId());
        }
        //If they submitted new tool id, set that
        if (toUpdate.getToolId() != null) {
            ToolBetsEntity tbe2 = getEntityManager().find(ToolBetsEntity.class, toUpdate.getToolId());
            tbe.setToolId(tbe2);
        }

        //Save the updated version
        getEntityManager().merge(tbe);

        return Response.status(Response.Status.ACCEPTED).build();
    }

    private Response buildResponse(boolean prettyPrint, Object toReturn) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            if (prettyPrint) {
                String prettyResults = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(toReturn);
                return Response.ok().entity(prettyResults).build();
            } else {
                return Response.ok(toReturn).build();
            }
        } catch (IOException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
