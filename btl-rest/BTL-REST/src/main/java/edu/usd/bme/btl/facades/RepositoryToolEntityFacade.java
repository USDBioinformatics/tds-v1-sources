/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.facades;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import edu.usd.bme.btl.entities.RepositoryEntity;
import edu.usd.bme.btl.entities.RepositoryToolEntity;
import edu.usd.bme.btl.entities.ToolBetsEntity;

/**
 *
 * @author Shayla.Gustafson
 */
@Stateless
public class RepositoryToolEntityFacade extends AbstractFacade<RepositoryToolEntity> {

    @PersistenceContext(unitName = "org.usd.edu_BTL-REST_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RepositoryToolEntityFacade() {
        super(RepositoryToolEntity.class);
    }

    @Override
    public void create(RepositoryToolEntity reposTool) throws Exception {
        RepositoryToolEntity temp = findByReposIdAndToolId(reposTool.getRepositoryId(), reposTool.getToolId());
        if (temp == null) {
            super.create(reposTool);
        } else {
            //throw new Exception("RepositoryTool already exists");
            //we can ignor this
        }
    }

    @Override
    public void edit(RepositoryToolEntity reposTool) throws Exception {
        RepositoryToolEntity temp = findByReposIdAndToolId(reposTool.getRepositoryId(), reposTool.getToolId());
        if (temp != null) {
            if (temp.getId() != reposTool.getId()) {
                throw new Exception("RepositoryTool already exists");
            } else {
                super.edit(reposTool);
            }
        }
    }

    private RepositoryToolEntity findByReposIdAndToolId(RepositoryEntity repositoryId, ToolBetsEntity toolId) {
        TypedQuery<RepositoryToolEntity> query = em.createNamedQuery("RepositoryTool.findByReposIdAndToolId", RepositoryToolEntity.class);
        query.setParameter("repositoryId", repositoryId);
        query.setParameter("toolId", toolId);
        List<RepositoryToolEntity> list = query.getResultList();
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
        //test comment, make sure you can sync to github.
        //added you as a collaborator to the repo, so you can sync.
    }
}
