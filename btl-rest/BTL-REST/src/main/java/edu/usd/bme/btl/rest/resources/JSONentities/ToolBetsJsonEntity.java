/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.rest.resources.JSONentities;

import java.util.Date;
import java.util.List;
import org.codehaus.jackson.annotate.JsonProperty;
import org.json.simple.JSONObject;
import edu.usd.bme.btl.entities.ToolBridgeEntity;

/**
 *
 * @author Tyler
 */
public class ToolBetsJsonEntity {

    private Integer id;
    private String name;
    private String version;
    private String summary;
    private JSONObject bets;
    private String icon;
    private Date dateAdded;
    private Date dateUpdated;
    private List<Bridge> bridgeList;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return this.name;
    }

    @JsonProperty("name")
    public String setName(String name) {
        return this.name = name;
    }

    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    @JsonProperty("summary")
    public String getSummary() {
        return summary;
    }

    @JsonProperty("summary")
    public void setSummary(String summary) {
        this.summary = summary;
    }

    @JsonProperty("bets")
    public JSONObject getBets() {
        return bets;
    }

    @JsonProperty("bets")
    public void setBets(JSONObject bets) {
        this.bets = bets;
    }

    @JsonProperty("icon")
    public String getIcon() {
        return icon;
    }

    @JsonProperty("icon")
    public void setIcon(String icon) {
        this.icon = icon;
    }

    @JsonProperty("dateAdded")
    public Date getDateAdded() {
        return dateAdded;
    }

    @JsonProperty("dateAdded")
    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    @JsonProperty("dateUpdated")
    public Date getDateUpdated() {
        return dateUpdated;
    }

    @JsonProperty("dateUpdated")
    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    @JsonProperty("bridgeList")
    public List<Bridge> getBridgeList() {
        return bridgeList;
    }

    @JsonProperty("bridgeList")
    public void setBridgeList(List<Bridge> bridgeList) {
        this.bridgeList = bridgeList;
    }

}
