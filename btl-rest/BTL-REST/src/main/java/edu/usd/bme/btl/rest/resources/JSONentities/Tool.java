/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.rest.resources.JSONentities;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 *
 * @author Tyler
 */
public class Tool {

    private Integer id;
    private String name;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return this.name;
    }

    @JsonProperty("name")
    public String setName(String name) {
        return this.name = name;
    }
}
