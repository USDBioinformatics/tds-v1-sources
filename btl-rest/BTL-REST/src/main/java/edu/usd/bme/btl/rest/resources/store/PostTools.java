/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.usd.bme.btl.rest.resources.store;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.io.IOException;
import java.net.URI;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.DeserializationConfig;
import org.usd.edu.btl.betsconverter.BETSV1.BETSV1;
import edu.usd.bme.btl.entities.RepositoryToolEntity;
import edu.usd.bme.btl.entities.ToolBetsEntity;
import edu.usd.bme.btl.entities.ToolBridgeEntity;
import edu.usd.bme.btl.facades.AbstractFacade;
import edu.usd.bme.btl.rest.resources.JSONentities.Bridge;
import edu.usd.bme.btl.rest.resources.JSONentities.RepoToolJSON;
import edu.usd.bme.btl.rest.resources.JSONentities.ToolBetsJsonEntity;

/**
 *
 * @author Bill Conn <Bill.Conn@usd.edu>
 */
@Stateless
@Path("store-tools")
@Produces({"application/json"})
@Consumes({"application/json"})
public class PostTools extends AbstractFacade<ToolBetsEntity> {
    
    @PersistenceContext(unitName = "org.usd.edu_BTL-REST_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    public PostTools() {
        super(ToolBetsEntity.class);
    }
    
    @GET
    public Response getEmptyToolJson(@DefaultValue("false") @QueryParam("PrettyPrint") boolean prettyPrint) {
        ToolBetsJsonEntity blank = new ToolBetsJsonEntity();
        
        return buildResponse(prettyPrint, blank);
    }

    @POST
    public Response storeToolInDatabase (ToolBetsJsonEntity newTool) {
        //Need to make sure no one is trying to insert tools that already exist
        if (newTool == null || newTool.getId() != null) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
        
        //need to have someway to set the repo too...
        // So I'm only stubbing this out until later
        //return Response.status(Response.Status.CREATED).build();        
        return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();
    }
    
    @Path("list")
    @POST
    public Response storeToolListInDatabase (List<ToolBetsJsonEntity> newTools) {
        //Need to make sure no one is trying to insert tools that already exist
        for (ToolBetsJsonEntity tool : newTools) {
            if (tool == null || tool.getId() != null) {
                return Response.status(Response.Status.NOT_ACCEPTABLE).build();
            }
        }
        
        //need to have someway to set the repo too...
        // So I'm only stubbing this out until later
        //return Response.status(Response.Status.CREATED).build();        
        return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();
    }
    
    @Path("{id}")
    @POST
    public Response updateToolById(@PathParam("id") Integer id, ToolBetsJsonEntity updateTool) {
        //First find the bets to update
        ToolBetsEntity tbe = getEntityManager().find(ToolBetsEntity.class, id);
        if (tbe == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        //Then make sure we are updating the one we think we are
        if (!id.equals(updateTool.getId())) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }

		URI updatedURI = null;
        //Then update the fields, and persist it.
        try {
            tbe = buildToolForDatabase(tbe, updateTool);
            getEntityManager().merge(tbe);
			getEntityManager().flush();
			updatedURI = new URI("/BTL-REST/resources/tools/"+tbe.getId());
        } catch (Exception e) {
			System.err.println(e.getMessage());
            return Response.notModified(e.getMessage()).status(Response.Status.BAD_REQUEST).build();
        }
        
		//No clear answer on what to return on an updated URI.  Returning 201
		// makes it easier on the client expecting a 201 for a creation or an
		// update.
        return Response.status(Response.Status.CREATED).contentLocation(updatedURI).build();
    }
    
    private Response buildResponse(boolean prettyPrint, Object toReturn) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            if (prettyPrint) {
                String prettyResults = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(toReturn);
                return Response.ok().entity(prettyResults).build();
            }
            else {
                return Response.ok(toReturn).build();
            }
        } catch (IOException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    private boolean checkToolBetsStringEquality(String bets, String tool) {
        if (bets == null || tool == null) {
            if (bets == null && tool == null) {
                return true;
            }
        }
        else if (bets.equals(tool)) {
            return true;
        }
        
        return false;
    }
    
    private ToolBetsEntity buildToolForDatabase(ToolBetsEntity dbTool, ToolBetsJsonEntity updateTool) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer();
        BETSV1 incomingBets;
        byte[] betsBytes;
        Date today = Calendar.getInstance().getTime();
        
        if (updateTool.getBets() == null) {
            throw new IOException("Bad BETS supplied");
        }
        incomingBets = mapper.readValue(updateTool.getBets().toString(), BETSV1.class);
        betsBytes = ow.writeValueAsString(incomingBets).getBytes("UTF-8");
        
        //Build Bridge list
        List<ToolBridgeEntity> tBridgeList = new ArrayList();
        try {
            if ( updateTool.getBridgeList() != null) {
                for (Bridge bridge : updateTool.getBridgeList()) {
                    ToolBridgeEntity temp = new ToolBridgeEntity();
                    temp.setId(bridge.getBridgeId());
                    temp.setOntologyId(bridge.getOntologyId());
                    if (!bridge.getToolId().equals(updateTool.getId())) {
                        throw new Exception ("Tool id Mismatch in bridges");
                    }
                    temp.setToolId(dbTool);
                    tBridgeList.add(temp);
                }
            }
        } catch (Exception e) {
            throw new IOException("Bad Bridges supplied");
        }
        
        //Double check that the user didn't send us inconsistent data.
        if (updateTool.getName() == null || !checkToolBetsStringEquality(incomingBets.getName(), updateTool.getName())) {
            throw new IOException("Inconsistent names supplied");
        }
        if (!checkToolBetsStringEquality(incomingBets.getVersion(), updateTool.getVersion())) {
            throw new IOException("Inconsistent vesions supplied");
        }
        if (!checkToolBetsStringEquality(incomingBets.getSummary(), updateTool.getSummary())) {
            throw new IOException("Inconsistent summaries supplied");
        }
		if (!updateTool.getDateAdded().equals(dbTool.getDateAdded())) {
			throw new IOException("Inconsistent date added supplied");
		}
        
        //Set all the updated info for the Tool Entity
        dbTool.setName(incomingBets.getName());
        dbTool.setVersion(incomingBets.getVersion());
        dbTool.setSummary(incomingBets.getSummary());
        dbTool.setBets(betsBytes);
        dbTool.setIcon(updateTool.getIcon());
        dbTool.setDateUpdated(new Timestamp(today.getTime()));
        dbTool.setToolBridgeList(tBridgeList);
        return dbTool;
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
