package edu.usd.bme.btl.jsf;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import edu.usd.bme.btl.entities.ToolBetsEntity;
import edu.usd.bme.btl.facades.ToolBetsEntityFacade;
import edu.usd.bme.btl.jsf.util.JsfUtil;
import edu.usd.bme.btl.jsf.util.JsfUtil.PersistAction;

//Should this be session scoped or view scoped? 
@ManagedBean(name = "toolBetsEntityController")
@SessionScoped
public class ToolBetsEntityController implements Serializable {

    @EJB
    private ToolBetsEntityFacade ejbFacade;
    private List<ToolBetsEntity> items = null;
    private ToolBetsEntity selected;

    public ToolBetsEntityController() {
    }

    public ToolBetsEntity getSelected() {
        return selected;
    }

    public void setSelected(ToolBetsEntity selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }
    
    private ToolBetsEntityFacade getFacade() {
        return ejbFacade;
    }

    public ToolBetsEntity prepareCreate() {
        selected = new ToolBetsEntity();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("ToolBetsEntityCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("ToolBetsEntityUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("ToolBetsEntityDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<ToolBetsEntity> getItems() {
//        items.add(getFacade().findByNameAndVersion("TESTTOOLLLL", "1.0.0.0.0.0.0"));
//        items.add(getFacade().findByNameAndVersion("XYPlot", "0.0.1"));
        
        //TODO: If the contents of the database change, will 'items' ever change?
        if (items == null) {
            items = new ArrayList<ToolBetsEntity>();
            //System.out.println("No records were found... try getting");        
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction == PersistAction.DELETE) {
                    getFacade().remove(selected);
                } else if (persistAction == PersistAction.CREATE) {
                    getFacade().create(selected);
                } else {
                    getFacade().edit(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public List<ToolBetsEntity> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<ToolBetsEntity> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = ToolBetsEntity.class)
    public static class ToolBetsEntityControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ToolBetsEntityController controller = (ToolBetsEntityController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "toolBetsEntityController");
            return controller.getFacade().find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof ToolBetsEntity) {
                ToolBetsEntity o = (ToolBetsEntity) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), ToolBetsEntity.class.getName()});
                return null;
            }
        }

    }

}
