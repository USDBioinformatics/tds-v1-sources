package edu.usd.bme.btl.rest.resources;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import edu.usd.bme.btl.entities.ToolBetsEntity;
import edu.usd.bme.btl.entities.ToolBridgeEntity;
import edu.usd.bme.btl.rest.resources.JSONentities.Bridge;
import edu.usd.bme.btl.rest.resources.JSONentities.Tool;
import edu.usd.bme.btl.rest.resources.JSONentities.ToolBetsJsonEntity;
import edu.usd.bme.btl.rest.resources.JSONentities.ToolCount;
import edu.usd.bme.btl.rest.resources.JSONentities.Summary;
import javax.persistence.NoResultException;

/**
 *
 * @author Tyler
 */
@Stateless
@Path("tools")
@Produces({"application/json"})
//@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes({"application/json"})
public class ToolsREST extends AbstractFacade<ToolBetsEntity> {

    @PersistenceContext(unitName = "org.usd.edu_BTL-REST_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public ToolsREST() {
        super(ToolBetsEntity.class);
    }
    /*
     * Default @GET Method
     */

    @GET
	public Response getAllTools(
			@DefaultValue("0") @QueryParam("index") Integer index,
			@DefaultValue("25") @QueryParam("count") Integer count,
			@DefaultValue("false") @QueryParam("pretty") Boolean pretty) {
		
		if(index < 0) {
			return Response.status(Response.Status.NOT_ACCEPTABLE)
					.entity("Invalid index location").build();
		}
		if(count < 1 || count > 1000) {
			return Response.status(Response.Status.NOT_ACCEPTABLE)
					.entity("Count range is 1 to 1000").build();
		}
		
		Query query = em.createNamedQuery("findAllTools")
				.setFirstResult(index)
				.setMaxResults(count);
        List<ToolBetsEntity> toolList = query.getResultList();

		List<ToolBetsJsonEntity> jsonTools = new ArrayList();
		for (ToolBetsEntity tool : toolList) {
            jsonTools.add(makeJsonEntityFromToolBetsEntity(tool));
		}
		
		if(pretty) {
			ObjectMapper mapper = new ObjectMapper();
			ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
			try {
				return Response.status(Response.Status.OK)
						.entity(writer.writeValueAsString(jsonTools)).build();
			} catch (JsonProcessingException e) {
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
						.entity(e.getMessage()).build();
			}
		} else {
			return Response.status(Response.Status.OK)
					.entity(jsonTools).build();
		}
    }

    @GET
    @Path("{id}")
    public Response find(
			@PathParam("id") Integer id,
			@DefaultValue("false") @QueryParam("pretty") Boolean pretty) {
		
        Query query = em.createNamedQuery("findToolById");
        query.setParameter("id", id);
		
		ToolBetsEntity tool;
		try {
			tool = (ToolBetsEntity) query.getSingleResult();
		} catch (NoResultException e) {
			return Response.noContent().build();
		}
		
		ToolBetsJsonEntity jsonTool = makeJsonEntityFromToolBetsEntity(tool);
		
		if(pretty) {
			ObjectMapper mapper = new ObjectMapper();
			ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
			try {
				return Response.status(Response.Status.OK)
						.entity(writer.writeValueAsString(jsonTool)).build();
			} catch (JsonProcessingException e) {
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
						.entity(e.getMessage()).build();
			}
		} else {
			return Response.status(Response.Status.OK)
					.entity(jsonTool).build();
		}
    }

    @GET
    @Path("{from}/{to}")
    public Response findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
		if(to - from < 1) {
			return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Bad Range").build();
		}
		if(to - from > 1000) {
			return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Max range is 1000").build();
		}
        Query queryToolsByIdFromStartToEnd = em.createNamedQuery("findToolsByIdFromStartToEnd");
        queryToolsByIdFromStartToEnd.setParameter("start", from);
        queryToolsByIdFromStartToEnd.setParameter("end", to);
        //List<ToolBetsEntity> toolEnts = super.findRange(new int[]{from, to});
        List<ToolBetsEntity> toolEnts = queryToolsByIdFromStartToEnd.getResultList();
        List<ToolBetsJsonEntity> jsonTools = new ArrayList();
        for (ToolBetsEntity toolEnt : toolEnts) {
            jsonTools.add(makeJsonEntityFromToolBetsEntity(toolEnt));
        }
        return Response.ok(jsonTools).build();
    }

    @GET
    @Path("count")
    public List getToolCount() throws IOException {
//        System.out.println("Super= " + super.count());
//        ToolCount count = new ToolCount();
//        count.setSize(super.count());
//
//        ObjectMapper mapper = new ObjectMapper();
//        String jsonCount = mapper.writeValueAsString(count);
        return super.count();
    }

    /*
     * Returns a JSON string created with Jackson.  Necessary to create valid json objects for display in Angular.
     * {id:"", name:""}
     */
    @GET
    @Path("names")
    public Response getToolNames() {
        try {
            javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
            cq.select(cq.from(ToolBetsEntity.class));
            //get list of all tools
            List<ToolBetsEntity> foundToolList = getEntityManager().createQuery(cq).getResultList();
            //System.out.println("# of Tools: " + foundToolList.size() + "!!!");

            //get name of each tool
            List<Tool> nameList = new ArrayList();
            for (ToolBetsEntity tool : foundToolList) {
                Tool toolInst = new Tool();
                toolInst.setId(tool.getId());
                toolInst.setName(tool.getName());
                nameList.add(toolInst);
            }
//            //sort the list alphabetically
//            Collections.sort(nameList);

            ObjectMapper mapper = new ObjectMapper();
            String jsonNames = mapper.writeValueAsString(nameList);
            return Response.ok(jsonNames).build();
        } catch (Exception e) {
            e.getMessage();
        }
        return Response.noContent().build();
    }

    @GET
    @Path("summaries")
    public Response getAllSummaries() {
        List<Summary> returnableSummaries = new ArrayList<Summary>();
        Query getAllSummaries = em.createNamedQuery("getAllSummaries");
        List<Object[]> summaryList = getAllSummaries.getResultList();

        try {
            for (Object[] obj : summaryList) {
                Summary temp = new Summary();
                temp.setId(Integer.valueOf(obj[0].toString()));
                temp.setSummary(String.valueOf(obj[1]));
                returnableSummaries.add(temp);
            }
        } catch (NumberFormatException e) {
            //System.out.println(e.getStackTrace());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(returnableSummaries).build();
    }

    @GET
    @Path("/icon/{id}")
    public Object getToolIcon(@PathParam("id") Integer id) throws IOException {

        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = builder.createQuery();
        Root<ToolBetsEntity> tool = cq.from(ToolBetsEntity.class);

        cq.select(tool.get("icon"));
        cq.where(builder.equal(tool.get("id"), id));

        Object locTool = getEntityManager().createQuery(cq).getSingleResult();
        return locTool;
//        TypedQuery<ToolBetsEntity> query = em.createNamedQuery("getToolIcon", ToolBetsEntity.class);
//        query.setParameter("id", id);
//        System.out.println("ID: " + id);
//        ToolBetsEntity foundTool = query.getSingleResult();
//        return foundTool.getIcon();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    private ToolBetsJsonEntity makeJsonEntityFromToolBetsEntity(ToolBetsEntity toolEnt) {
        ToolBetsJsonEntity jsonTool = new ToolBetsJsonEntity();
        jsonTool.setId(toolEnt.getId());
        jsonTool.setName(toolEnt.getName());
        jsonTool.setVersion(toolEnt.getVersion());
        jsonTool.setSummary(toolEnt.getSummary());
        //this converts the byte array to a string, parses it, and passes a
        // JSONObject back to the JSONObject's we are returning so the output
        // doesn't look like a mangled mess.  Bill
        try {
            JSONParser parser = new JSONParser();
            jsonTool.setBets((JSONObject) parser.parse(new String(toolEnt.getBets(), "UTF-8")));
        } catch (Exception e) {
            jsonTool.setBets(null);
        }
        jsonTool.setIcon(toolEnt.getIcon());
        jsonTool.setDateAdded(toolEnt.getDateAdded());
        jsonTool.setDateUpdated(toolEnt.getDateUpdated());
        //create a new bridge list for each tool
        List<Bridge> jsonBridges = new ArrayList();
        //set bridge list
        for (ToolBridgeEntity toolBridge : toolEnt.getToolBridgeList()) {
            Bridge bridgeJson = new Bridge();
            bridgeJson.setBridgeId(toolBridge.getId());
            bridgeJson.setOntologyId(toolBridge.getOntologyId());
            bridgeJson.setToolId(toolBridge.getToolId().getId());
            jsonBridges.add(bridgeJson);
        }
        //set the bridge list for the tool
        jsonTool.setBridgeList(jsonBridges);
        return jsonTool;
    }

}
