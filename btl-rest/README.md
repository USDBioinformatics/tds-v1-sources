# BTL-REST
## REST Endpoints

### Base URL http://jacksons.usd.edu/BTL-REST

#### Tools
| URL           |   HTTP VERB   | Returns |
| ------------- | ------------- | ------- |
| /resources/tools/       | GET | List of All Tools |
| /resources/tools/{id}      | GET | Tool by ID |
| /resources/tools/{from}/{to}| GET | List of tools within range |
| /resources/tools/count  | GET | Count of All Tools |
| /resources/tools/names  | GET | All Tool Names |
| /resources/tools/summaries  | GET | All Tool Summaries |

#### BETS
| URL           |   HTTP VERB   | Returns |
| ------------- | ------------- | ------- |
| /resources/bets     | GET | All BETS data for each tool |
| /resources/bets/{id}      | GET | BETS for tool by ID |
| /resources/bets/inputs/{id}| GET | Inputs for Tool by ID |

#### Bridges
| URL           |   HTTP VERB   | Returns |
| ------------- | ------------- | ------- |
| /resources/bridges     | GET | All Bridges |
| /resources/bridges/{id}      | GET | All Bridges by ID |
| /resources/bets/{from}/{to} | GET | Bridges within a range |
| /resources/bets/count | GET | Count of the Bridges |

#### Repositories
| URL           |   HTTP VERB | Returns |
| ------------- | ----------- | ------- |
| /resources/repos/ | GET | List of the Repositories our tools came from |
| /resources/repos/tools | GET | List of Every Tool ID and its Repo ID |
| /resources/repos/tools/{id} | GET | ResourceTool by ID |
| /resources/repos/getbytoolids | GET | Get example JSON for posting |
| /resources/repos/getbytoolids | POST | Post an array of tool ids to get their repository names |


#### Storing Bridges or Tools (these are normally disabled)
| URL           |   HTTP VERB | Returns |
| ------------- | ----------- | ------- |
| /resources/store-bridges | GET | An example Bridge JSON to POST |
| /resources/store-bridges | POST | Returns the URI of the created bridge |
| /resources/store-bridges/list | POST | Disabled |
| /resources/store-bridges/{id} | POST | Accepted if it updates the bridge id specified |
| /resources/store-tools | GET | Returns an example Tool JSON to Post |
| /resources/store-tools | POST | Disabled |
| /resources/store-tools/list | POST | Disabled |
| /resources/store-tools/{id} | POST | Accepted if it updates the tool id specified |


*Subject to change

*Query parameter "PrettyPrint=true" will return prettified JSON
