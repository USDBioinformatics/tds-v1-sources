/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usd.csci.btl.annotator.input;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 * HttpResource contains generic static helper methods for http use
 *
 * @author Menno.VanDiermen
 */
public class HttpResource {
    
    /**
     * HTTP GET method using Apache HttpClient
     * @param u
     * @return
     * @throws Exception 
     */
    public static String GET(URI u) throws Exception {
        //initialize GET client
        CloseableHttpClient client = HttpClients.createDefault();
        HttpGet method = new HttpGet(u);
        //execute GET method
        CloseableHttpResponse response = client.execute(method);
        if(response.getStatusLine().getStatusCode() >= 300) {
            throw new HttpResponseException(
                    response.getStatusLine().getStatusCode(),
                    response.getStatusLine().getReasonPhrase());
        }
        //return response InputStream
        String result = readInput(response.getEntity().getContent());
        response.close();
        client.close();
        return result;
    }
    
    /**
     * HTTP POST method using Apache HttpClient Currently returns error
     * @param u
     * @param bridgeJSON
     * @return 
     * @throws HttpResponseException
     * @throws IOException 
     */
    /* public static String POST(URI u, String bridgeJSON) throws HttpResponseException, IOException {
        //initialize POST client
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost method = new HttpPost(u);
        method.setEntity(new StringEntity(bridgeJSON, ContentType.create("application/json")));
        //execute POST method
        CloseableHttpResponse response = client.execute(method);
        if(response.getStatusLine().getStatusCode() >= 300) {
            throw new HttpResponseException(
                    response.getStatusLine().getStatusCode(),
                    response.getStatusLine().getReasonPhrase());
        }
        //return response InputStream
        String result = readInput(response.getEntity().getContent());
        response.close();
        client.close();
        return result;
    } */
    
    /**
     * HTTP POST method using HttpUrlConnection
     * @param urlToPost
     * @param JSONData
     * @return
     * @throws Exception 
     */
    public static String basicPost(String urlToPost, String JSONData) throws Exception {
        String result = "";
        try {
            URL u = new URL(urlToPost);
            HttpURLConnection conn = (HttpURLConnection) u.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json");

            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(JSONData);
            wr.flush();
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result += line;
            }
            rd.close();
        } catch (Exception e) {
            throw e;
        }
        return result;
    }
    
    /**
     * returns String representing contents of InputStream is
     * 
     * @param is
     * @return String
     * @throws IOException 
     */
    private static String readInput(InputStream is) throws IOException {
        String result = "";
        
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(is));
        String line;
        while ((line = rd.readLine()) != null) {
            result += line;
        }
        rd.close();
        
        return result;
    }
}
