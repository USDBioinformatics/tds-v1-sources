/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usd.csci.btl.annotator.util;

/**
 *
 * @author Menno.VanDiermen
 */
public class AnnotatorException extends Exception {
    
    public AnnotatorException() {
        
    }
    
    public AnnotatorException(String message) {
        super(message);
    }
    
}
