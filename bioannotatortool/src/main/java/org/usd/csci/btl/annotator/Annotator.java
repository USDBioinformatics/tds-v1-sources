/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usd.csci.btl.annotator;

import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;
import org.usd.csci.btl.annotator.input.BioPortalResource;
import org.usd.csci.btl.annotator.util.BioAnnotation;
import org.usd.csci.btl.annotator.util.BioAnnotationTyped;
import org.usd.csci.btl.annotator.util.Mention;

/**
 *
 * @author Menno.VanDiermen
 */
public class Annotator {
    
    public Annotator() {
    }

    /**
     * Annotates using machine learning algorithm. Not currently implemented.
     * Returns null.
     *
     * @param document
     * @return
     */
    @Deprecated
    public AnnotationSet annotateWithRanking(String document) {
        return null;
    }

    /**
     * Retrieves AnnotationSet of annotations provided by the NCBO Annotator
     *
     * @param document
     * @return
     * @throws Exception
     */
    public AnnotationSet annotateNCBOOnly(String document) throws Exception {
        AnnotationSet master = new AnnotationSet();

        try {
            NCBOAnnotate(document, master);
        } catch (Exception e) {
            String[] lines = split(document);
            for (String line : lines) {
                if (line == null || line.isEmpty()) {
                    continue;
                }
                try {
                    NCBOAnnotate(line, master);
                } catch (Exception e2) {
                    line = "\"" + line + "\"";
                    try {
                        NCBOAnnotate(line, master);
                    } catch(Exception e3) {
                        String[] sublines = postprocess(line);
                        for(String subline: sublines) {
                            try {
                                NCBOAnnotate(subline, master);
                            } catch(Exception e4) {
                                subline = "\"" + subline + "\"";
                                try {
                                    NCBOAnnotate(subline, master);
                                } catch(Exception e5) {
                                    throw new Exception(e5.getMessage() + " -- " + subline);
                                }
                            }
                        }
                    }
                }
            }
        }

        return master;
    }

    /**
     * Preprocess BETS to remove all JSON keys and leave only values. Returns
     * String[] containing all separate field values.
     *
     * @param document
     * @return
     */
    public String preprocessJSON(String document) {
        document = document.replaceAll("\"\\w+\":", "   ");
        document = document.replaceAll("[\\[\\]\\(\\)\\{\\}]", "   ");
        document = document.replaceAll("\\s{3}[,\\s]+", "");
        return document;
    }

    /**
     *
     * @param document
     * @return
     */
    private String[] split(String document) {
        return document.split("(?<=[.,:;?!-\\/()]\\s)");
    }

    /**
     * Process line, splitting on two or more whitespaces or hyphens.
     * Intended for especially lengthy sections, e.g. some descriptions.
     *
     * @param line
     * @return
     */
    private String[] postprocess(String line) {
        return line.split("[\\s-][\\s-]+|\\t");
    }

    /**
     * Populates given AnnotationSet with NCBO annotations for supplied
     * document.
     *
     * @param document
     * @param set
     * @throws Exception
     */
    private void NCBOAnnotate(String document, AnnotationSet set) throws Exception {

        JSONArray ncboAnnotations = new JSONArray(BioPortalResource
                .getAnnotatorResponse(document));
        //each entry is an annotation
        for (int i = 0; i < ncboAnnotations.length(); i++) {
            //get JSONObject of NCBO results
            JSONObject jo = ncboAnnotations.getJSONObject(i);
            JSONObject annotation = jo.getJSONObject("annotatedClass");
            JSONObject links = annotation.getJSONObject("links");
            //prep annotation data
            String URI = annotation.getString("@id");
            String dataLink = links.getString("self");
            String ontLink = links.getString("ontology");
            String ontology = ontLink.substring((ontLink.lastIndexOf('/') + 1));
            //retrieve missing data for annotation
            JSONObject concept = new JSONObject(
                    BioPortalResource.getBioDataResponse(dataLink));
            String label = concept.getString("prefLabel");
            String[] nameTokens = label.split("\\s");
            BioAnnotation ba = new BioAnnotation(label, URI, ontology, dataLink);
            // add synonyms
            JSONArray syns = concept.getJSONArray("synonym");
            ba.setSynonyms(new String[syns.length()]);
            for (int j = 0; j < syns.length(); j++) {
                ba.getSynonyms()[j] = syns.getString(j);
            }
            //retrieve mentions
            JSONArray matches = jo.getJSONArray("annotations");
            for (int g = 0; g < matches.length(); g++) {
                JSONObject match = matches.getJSONObject(g);
                //add mentions to set of mentions
                Mention m = new Mention(match.getString("text"));
                m = set.getMentionInstance(m);

                m.addAnnotation(new BioAnnotationTyped(ba, match.getString("matchType")));

                //String[] tokenArray = mention.getText().split("\\s");
                //for(String t: tokenArray) tokens.add(t);
            }
        }
    }
}
