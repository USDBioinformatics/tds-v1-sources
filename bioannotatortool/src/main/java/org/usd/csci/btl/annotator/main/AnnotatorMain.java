/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usd.csci.btl.annotator.main;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.usd.csci.btl.annotator.input.BTLResource;
import org.usd.csci.btl.annotator.concurrent.AnnotatorThreadManager;

/**
 *
 * @author Menno.VanDiermen
 */
public class AnnotatorMain {
    
    // use tester instead. multithreading results in too-many-requests errors at NCBO endpoint.
    
    /*public static void main(String[] args) throws Exception {        
        List<Integer> ids = BTLResource.getToolIDs();
        
        Map<Integer, List<String>> bridges = new HashMap<Integer, List<String>>();
        AnnotatorThreadManager manager = new AnnotatorThreadManager();
        manager.prepareLoaders(ids); 
        Thread t = new Thread(manager); t.start();
        t.join();
    }*/
}