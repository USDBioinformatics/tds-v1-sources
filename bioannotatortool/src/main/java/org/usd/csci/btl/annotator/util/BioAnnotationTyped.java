/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usd.csci.btl.annotator.util;

/**
 *
 * @author Menno.VanDiermen
 */
public class BioAnnotationTyped extends BioAnnotation {
    
    private final String type;
    
    public BioAnnotationTyped(BioAnnotation ba, String type) {
        super(ba.getLabel(), ba.getURI(), ba.getOntology(), ba.getDataLink());
        this.type = type;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }
    
}
