/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usd.csci.btl.annotator.output;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.json.JSONObject;
import org.usd.csci.btl.annotator.AnnotationSet;
import org.usd.csci.btl.annotator.input.BTLResource;
import org.usd.csci.btl.annotator.util.BioAnnotationTyped;
import org.usd.csci.btl.annotator.util.Mention;

/**
 * AnnotatorOutput contains a set of I/O helper methods to write annotations 
 * and BETS Strings to file
 *
 * @author Menno.VanDiermen
 */
public class AnnotatorOutput {
    
    
    public static void writeStringToFile(File f, String content) throws IOException {
        BufferedWriter write = new BufferedWriter(new FileWriter(f));
        write.append(content);
        write.close();
    }
    
    /**
     * write bridges to txt file
     * @param id
     * @param set
     * @throws IOException 
     */
    public static void writeNCBOBridge(int id, AnnotationSet set) throws IOException {
        List<String> jsons = getBridges(id, set); mkdir();
        File file = new File("../Bridges/" + id + ".txt");
        BufferedWriter write = new BufferedWriter(new FileWriter(file));
        for(String s: jsons) {
            write.append(s); write.newLine();
        }
        write.close();
    }
    
    /**
     * POST txt file bridges to BTL-REST post endpoint
     * @throws Exception 
     */
    public static void postBridges() throws Exception {
        File dir = new File("../Bridges");
        File[] files = dir.listFiles();
        for(File f: files) {
            int id = Integer.parseInt(f.getName().substring(0, f.getName().indexOf('.')));
            Scanner sc = new Scanner(f);
            List<String> bridges = new ArrayList<String>();
            while(sc.hasNext()) {
                String line = sc.nextLine();
                JSONObject bridge = new JSONObject(line);
                JSONObject sub = bridge.getJSONObject("ontologyId");
                bridge.remove("ontologyId");
                bridge.put("ontologyId", sub.toString());
                bridges.add(bridge.toString());
            }
            try {
            BTLResource.postAnnotations(id, bridges);
            }catch(Exception e) {
                System.out.println("Failed: " + id + " -- " + e.getMessage());
            }
        }
    }
    
    /**
     * make bridges folder if it does not exist
     * @return 
     */
    public static boolean mkdir() {
        File dir = new File("../Bridges");
        if(!dir.exists()) return dir.mkdir();
        return dir.exists();
    }
    
    /**
     * convert AnnotationSet for tool id to list of JSON strings for POSTing
     * @param id
     * @param set
     * @return 
     */
    public static List<String> getBridges(int id, AnnotationSet set) {
        List<String> annotations = new ArrayList<String>();
        for(Mention m: set.getMentions().values()) {
            for(BioAnnotationTyped a: m.getAnnotations()) {
                JSONObject annotation = new JSONObject();
                JSONObject mention = new JSONObject();
                mention.put("name", a.getLabel());
                mention.put("URI", a.getURI());
                mention.put("type", a.getType());
                annotation.put(m.getText(), mention);
                JSONObject entry = new JSONObject();
                entry.put("toolId", id); entry.put("ontologyId", annotation);
                annotations.add(entry.toString());
            }
        }
        return annotations;
    }
    
    public static boolean bridgeExists(int id) {
        File f = new File("../Bridges/" + id + ".txt");
        return f.exists();
    }
    
    public static void convertToTabDelimited(int id) throws Exception {
        Scanner sc = new Scanner(new File("../Bridges/"+id+".txt"));
        File out = new File("../BridgesTabbed/"+id+".txt");
        BufferedWriter write = new BufferedWriter(new FileWriter(out));
        while(sc.hasNext()) {
            JSONObject bridge = new JSONObject(sc.nextLine());
            JSONObject annotation = new JSONObject(bridge.get("ontologyId").toString());
            String mention = annotation.keys().next().toString();
            JSONObject data = new JSONObject(annotation.get(mention).toString());
            String result = mention + "\t" + data.getString("name") + "\t" + data.getString("URI");
            write.append(result); write.newLine();
        }
        write.close();
    }
    
    public static void writeTabDelimited(String filename, AnnotationSet set) throws Exception {
        File out = new File(filename);
        BufferedWriter write = new BufferedWriter(new FileWriter(out));
        for(Mention m: set.getMentions().values()) {
            String mention = m.getText();
            for(BioAnnotationTyped ba: m.getAnnotations()) {
                String name = ba.getLabel();
                String uri = ba.getURI();
                String ont = ba.getOntology();
                write.append(mention + "\t" + name + "\t" + uri + "\t" + ont);
                write.newLine();
            }
        }
        write.close();
    }
    
    public static void writeAnnotatedBets(int id) throws Exception {
        String bets = BTLResource.getAnnotatedBETS(id);
        File f = new File("../AnnotatedBETS/"+id+".json");
        BufferedWriter write = new BufferedWriter(new FileWriter(f));
        write.append(bets);
        write.close();
    }
}