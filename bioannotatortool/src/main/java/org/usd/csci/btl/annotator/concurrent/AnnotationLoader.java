/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usd.csci.btl.annotator.concurrent;

import java.io.IOException;
import org.usd.csci.btl.annotator.AnnotationSet;
import org.usd.csci.btl.annotator.Annotator;
import org.usd.csci.btl.annotator.input.BTLResource;
import org.usd.csci.btl.annotator.output.AnnotatorOutput;

/**
 *
 * @author Menno.VanDiermen
 */
public class AnnotationLoader implements Runnable {
            
    private String document;
    private final int id;
    private AnnotationSet set;
        
    public AnnotationLoader(int id) {
        this.id = id;
    }

    /**
     * Gets annotations for its tool id.
     */
    @Override
    public void run() {
        Annotator a = new Annotator(); boolean init = false;
        try {
            document = BTLResource.getBETS(getId());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            set = a.annotateNCBOOnly(document);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            AnnotatorOutput.writeNCBOBridge(id, set);
        } catch (NullPointerException ex) { 
            try {
                set = a.annotateNCBOOnly(document);
                AnnotatorOutput.writeNCBOBridge(id, set);
            } catch (Exception ex1) {
                ex1.printStackTrace();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the set
     */
    public AnnotationSet getSet() {
        return set;
    }
}
