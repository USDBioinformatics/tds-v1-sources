/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usd.csci.btl.annotator.concurrent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.usd.csci.btl.annotator.output.AnnotatorOutput;

/**
 *
 * @author Menno.VanDiermen
 */
public class AnnotatorThreadManager implements Runnable {
    
    private final List<AnnotationLoader> loaders;
    private final ExecutorService executor;
    
    public AnnotatorThreadManager() {
        loaders = new ArrayList<AnnotationLoader>();
        executor = Executors.newFixedThreadPool(2);
    }

    @Override
    public void run() {
        try {
            runLoaders();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * prepare annotationLoaders for given list of IDs
     * @param ids
     * @return 
     */
    public boolean prepareLoaders(List<Integer> ids) {
        for(Integer id: ids) {
            if(id <= 12000) continue;
            if(id > 15000) break;
            if(AnnotatorOutput.bridgeExists(id)) continue;
            AnnotationLoader loader = new AnnotationLoader(id);
            loaders.add(loader);
        }
        System.out.println("Loaders: " + loaders.size());
        return true;
    }
    
    /**
     * run prepared annotationLoaders
     * @throws InterruptedException 
     */
    private void runLoaders() throws InterruptedException {
        for(AnnotationLoader loader: loaders) {
            executor.execute(loader);
        }
        executor.shutdown();
        executor.awaitTermination(2, TimeUnit.DAYS);
    }
}
