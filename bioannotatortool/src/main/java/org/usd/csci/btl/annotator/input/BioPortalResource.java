/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usd.csci.btl.annotator.input;

import java.net.URI;
import org.apache.http.client.utils.URIBuilder;

/**
 * BioPortalResource contains static helper methods specific to the BioPortal
 * REST endpoint.
 *
 * @author Menno.VanDiermen
 */
public class BioPortalResource {
    
    private static final String REST_URL = "data.bioontology.org";
    // API Key associated with BioPortal account "USDBioMed", password: "bioportal". Required for authorization of query.
    private static final String API_KEY = "89556c26-77e1-48e1-806c-90d5bb746f68";
    private static final String ANNOTATOR = "/annotator";
    private static String ontologies = "EDAM,EFO,BIM,IDQA,SAO ";
    
    /**
     * returns NCBO Annotator EDAM, SWO & NGSONTO annotations for provided 
     * document as JSON String
     * 
     * @param document
     * @return String 
     * @throws java.net.URISyntaxException
     * @throws org.apache.http.client.HttpResponseException 
     * @throws java.io.IOException 
     */
    public static String getAnnotatorResponse(String document) throws Exception {
        //build URI
        URI u = new URIBuilder()
                .setScheme("http")
                .setHost(REST_URL)
                .setPath(ANNOTATOR)
                .addParameter("apikey", API_KEY)
                .addParameter("text", document)
                .addParameter("ontologies", ontologies)
                //.addParameter("format", "json") //currently unnecessary as json is default
                .build();
        return HttpResource.GET(u);
    }
    
    /**
     * Returns JSON String representing BioPortal REST endpoint entry matching
     * provided valid data.bioontology.org URI
     * 
     * @param bioDataURI
     * @return
     * @throws Exception 
     */
    public static String getBioDataResponse(String bioDataURI) throws Exception {
        URI u = new URIBuilder(bioDataURI)
                .setParameter("apikey", API_KEY)
                //.addParameter("format", "json") //currently unnecessary as json is default
                .build();
        return HttpResource.GET(u);
    }
    
}
