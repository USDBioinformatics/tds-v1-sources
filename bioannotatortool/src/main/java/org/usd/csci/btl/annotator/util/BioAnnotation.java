/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usd.csci.btl.annotator.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * BioAnnotation represents an ontology concept that has been annotated to
 * a meaningful phrase.
 *
 * @author Menno.VanDiermen
 */
public class BioAnnotation {
    
    private static final String PARENT_PATH = "/parents";
    private static final String CHILDREN_PATH = "/children";
    
    private final String label;
    private String[] synonyms;
    private final String URI;
    private final String ontology;
    private final String dataLink;
    
    private final List<MentionTyped> matches;
    
        
    public BioAnnotation(String label, String URI, String ontology, String link) {
        this.label = label; this.URI = URI; this.ontology = ontology;
        this.dataLink = link;
        matches = new ArrayList<MentionTyped>();
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof BioAnnotation)) return false;
        BioAnnotation other = (BioAnnotation) obj;
        return (this.getURI().equals(other.getURI()) && this.getOntology().equals(other.getOntology()));
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.URI);
        hash = 67 * hash + Objects.hashCode(this.ontology);
        return hash;
    }
    
    public void addTypedMention(MentionTyped tm) {
        getMatches().add(tm);
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @return the synonyms
     */
    public String[] getSynonyms() {
        return synonyms;
    }

    /**
     * @return the URI
     */
    public String getURI() {
        return URI;
    }
    
    /**
     * @return the ontology
     */
    public String getOntology() {
        return ontology;
    }

    /**
     * @return the dataLink
     */
    public String getDataLink() {
        return dataLink;
    }
    
    /**
     * @return the parentsLink
     */
    public String getParentsLink() {
        return dataLink + PARENT_PATH;
    }
    
    /**
     * @return the childrenLink
     */
    public String getChildrenLink() {
        return dataLink + CHILDREN_PATH;
    }

    /**
     * @return the matches
     */
    public List<MentionTyped> getMatches() {
        return matches;
    }

    /**
     * @param synonyms the synonyms to set
     */
    public void setSynonyms(String[] synonyms) {
        this.synonyms = synonyms;
    }
}
