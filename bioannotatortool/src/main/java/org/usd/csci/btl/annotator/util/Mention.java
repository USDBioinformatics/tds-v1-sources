    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usd.csci.btl.annotator.util;

import java.util.HashSet;
import java.util.Set;

/**
 * Mention represents a meaningful phrase that has been annotated with 
 an ontology concept.
 *
 * @author Menno.VanDiermen
 */
public class Mention {
    
    protected final String text;
    private final Set<BioAnnotationTyped> annotations;
    
    public Mention(String text) {
        this.text = text;
        annotations = new HashSet<BioAnnotationTyped>();
    }
    
    public void addAnnotation(BioAnnotationTyped annotation) {
        annotations.add(annotation);
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof Mention)) return false;
        Mention other = (Mention) obj;
        return (this.getText().equals(other.getText()));
    }

    @Override
    public int hashCode() {
        return text.hashCode();
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @return the suggestions
     */
    public Set<BioAnnotationTyped> getAnnotations() {
        return annotations;
    }
}
