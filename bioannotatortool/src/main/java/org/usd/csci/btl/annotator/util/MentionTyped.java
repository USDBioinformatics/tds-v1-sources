/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usd.csci.btl.annotator.util;

/**
 *
 * @author Menno.VanDiermen
 */
public class MentionTyped extends Mention {
    
    private final String matchType;
    
    public MentionTyped(Mention m, String type) {
        super(m.getText());
        matchType = type;
    }

    /**
     * @return the matchType
     */
    public String getMatchType() {
        return matchType;
    }
}
