/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usd.csci.btl.annotator.input;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * BTLResource contains static helper methods specific to the BTL REST endpoint
 *
 * @author Menno.VanDiermen
 */
public class BTLResource {
    
    private static final String 
            TOOLS_URL = "http://localhost:8080/BTL-REST/resources/tools", 
            BETS_URL = "http://jacksons.usd.edu/BTL-REST/resources/bets",
            POST_URL = "http://localhost:8080/BTL-REST/resources/store-bridges/",
            COUNT_URL = "https://jacksons.usd.edu/BTL-REST/resources/tools/count";
    
    /**
     * get all BETS strings for given tool ids
     * @param ids
     * @return
     * @throws Exception 
     */
    public static List<String> getBETSStrings(List<Integer> ids) throws Exception {
        List<String> strings = new ArrayList<String>();
        for(Integer i: ids) {
            strings.add(getBETS(i));
        }
        return strings;
    }
    
    /**
     * gets list of all tool ids
     * @return
     * @throws Exception 
     */
    public static List<Integer> getToolIDs() throws Exception {
        URI u = new URI(COUNT_URL);
        List<Integer> ids = new ArrayList<Integer>();
        JSONArray ct = new JSONArray(HttpResource.GET(u));
        int count = ct.getInt(0);
        for(int i = 1; i <= count; i++) ids.add(i);
        return ids;
    }
    
    /**
     * gets BETS string for given tool id
     * @param id
     * @return
     * @throws Exception 
     */
    public static String getBETS(int id) throws Exception {
        URI u = new URI(BETS_URL + "/" + id);
        JSONObject tool = new JSONObject(HttpResource.GET(u));
        tool.append("id", id);
        return tool.toString();
    }
    
    /**
     * gets annotated BETS string for given tool id
     * @param id
     * @return
     * @throws Exception 
     */
    public static String getAnnotatedBETS(int id) throws Exception {
        URI u = new URI(TOOLS_URL + "/" + id);
        JSONObject tool = new JSONObject(HttpResource.GET(u));
        return tool.toString();
    }
    
    /**
     * post supplied bridge entry with given tool id to bridge endpoint
     * @param tool_id
     * @param bridges
     * @throws Exception 
     */
    public static void postAnnotations(int tool_id, List<String> bridges) throws Exception {
        for(String b: bridges) {
            HttpResource.basicPost(POST_URL, b);
        }
    }
}