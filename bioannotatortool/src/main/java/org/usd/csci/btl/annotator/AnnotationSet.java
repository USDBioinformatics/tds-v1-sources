
package org.usd.csci.btl.annotator;

import java.util.HashMap;
import java.util.Map;
import org.usd.csci.btl.annotator.util.Mention;

/**
 * This class stores a map of mentions, each of which may have one or more annotations.
 * It uses a map so that annotations can be organized by mention, and if a mention
 * is listed on multiple annotations, later annotations can simply be appended to the mention
 * 
 * @author Menno.VanDiermen
 */
public class AnnotationSet {
    
    private final Map<Mention, Mention> mentions;
    
    /**
     * Constructor (initializes mentions map)
     */
    public AnnotationSet() {
        mentions = new HashMap<Mention, Mention>();
    }
    
    /**
     * returns the mention if existing, adds and returns if not
     * @param m
     * @return 
     */
    public Mention getMentionInstance(Mention m) {
        if(!mentions.containsKey(m)) {
            mentions.put(m, m);
        }
        return mentions.get(m);
    }

    /**
     * @return the mentions
     */
    public Map<Mention, Mention> getMentions() {
        return mentions;
    }
}
