/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.usd.csci.btl.annotator.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import org.json.JSONObject;
import org.usd.csci.btl.annotator.Annotator;
import org.usd.csci.btl.annotator.AnnotationSet;
import org.usd.csci.btl.annotator.input.BTLResource;
import org.usd.csci.btl.annotator.input.HttpResource;
import org.usd.csci.btl.annotator.output.AnnotatorOutput;

/**
 *
 * @author Menno.VanDiermen
 */
public class Tester {
    
    static Annotator a;
    
    public static void main(String[] args) throws Exception {
        a = new Annotator();
        File dir = new File("BBBC");
        File[] files = dir.listFiles();
        for(File f: files) {
            String name = f.getName();
            name = name.substring(0, name.indexOf('.'));
            Scanner sc = new Scanner(f, "UTF-8");
            String string = "";
            while(sc.hasNext()) string += sc.nextLine();
            AnnotationSet set = a.annotateNCBOOnly(string);
            String filename = "BBBCAnnotated/annotated" + name + ".tsv";
            AnnotatorOutput.writeTabDelimited(filename, set);
        }
        System.out.println("Execution Complete");
    }
    
    public static void testAllNCBO() throws Exception {
        List<Integer> ids = BTLResource.getToolIDs();
        for(Integer i: ids) {
            try {
                if(AnnotatorOutput.bridgeExists(i)) continue;
                String bets = BTLResource.getBETS(i);
                AnnotationSet set = a.annotateNCBOOnly(bets);
                AnnotatorOutput.writeNCBOBridge(i, set);
            } catch(Exception e) {
                System.out.println("Failed: " + i + " -- " + e.getMessage());
                e.printStackTrace();
            }
        }
    }
    
    public static void testSingleNCBO(int id) throws Exception {
        try {
            String testBets = BTLResource.getBETS(id);
            AnnotationSet set = a.annotateNCBOOnly(testBets);
            AnnotatorOutput.writeNCBOBridge(id, set);
        } catch(Exception e) {
            System.out.println("Error in " + id + ": " + e.getMessage());
        }
    }
    
    public static void testFromFileNCBO() throws Exception {
        File nums = new File("RandNums.txt");
        Scanner sc = new Scanner(nums);
        while(sc.hasNext()) {
            int id = Integer.parseInt(sc.nextLine());
            testSingleNCBO(id);
        }
        sc.close();
    }
    
    public static void testSingleRanked(int id) throws Exception {
        String testBets = BTLResource.getBETS(id);
        AnnotationSet set = a.annotateWithRanking(testBets);
        AnnotatorOutput.writeNCBOBridge(id, set);
    }
    
    public static void testFromFileRanked() throws Exception {
        File nums = new File("RandNums.txt");
        Scanner sc = new Scanner(nums);
        while(sc.hasNext()) {
            int id = Integer.parseInt(sc.nextLine());
            testSingleRanked(id);
        }
        sc.close();
    }
}